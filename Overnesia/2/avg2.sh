#!/bin/bash

function getArray {
    i=1
    while IFS=  ; do
        #echo "$line"
        for val in $( echo "$line" | cut -f 2 -d '!' ) ; do
            acum[$i]=$(echo "${acum[$i]} + ${val}" | bc -l)
        done
#echo "${acum1[$i]} : ${acum2[$i]}"
#echo "${lines[$itn,$ln,1]} : ${lines[$itn,$ln,2]}"
        i=$((i + 1))
    done < "${iter}processedResults.txt"
}

rm -r -- "avg"
mkdir -- "avg"
numLines=10
declare -a acum1
declare -a acum2
for (( i=0; i<$numLines; i++ )); do
    acum1[$i]=0
    acum2[$i]=0
done
actV=0
for var in */ ; do
    if [ "$var" != "avg/" ];
    then
        echo "Var: $var"
        itn=0
        for iter in ${var}*/ ; do
            i=0
            while IFS= read -r line; do
                if [ $i = 0 ]
                then
                    acum1[${actV}]=$(echo "${acum1[${actV}]} + ${line}" | bc -l)
                elif [ $i = 1 ]
                then
                    acum2[${actV}]=$(echo "${acum2[${actV}]} + ${line}" | bc -l)
                fi
                i=$((i+1))
            done < "${iter}processedResults.txt"
            itn=$((itn + 1))
        done
        acum1[$actV]=$(echo "${acum1[$actV]} / $itn" | bc -l)
        acum2[$actV]=$(echo "${acum2[$actV]} / $itn" | bc -l)
        actV=$((actV + 1))
    fi
done
for (( i=0; i<$actV; i++ )); do
    ln=$((i + 1))
    echo "${ln}_${acum1[$i]}_${acum2[$i]}" >> "avg/processedResults.txt"
done

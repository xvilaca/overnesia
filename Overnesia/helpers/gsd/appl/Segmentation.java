package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Segmentation {

	public static void main(String[] args) throws FileNotFoundException {
		
		ArrayList<String> elements = new ArrayList<String>();
		
		Scanner sc = new Scanner(new File(args[0]));
		int begin = Integer.parseInt(args[1]);
		int end = Integer.parseInt(args[2]);
		
		while(sc.hasNextLine())
			elements.add(sc.nextLine());
		
		for(begin--; begin < end; begin++) {
			System.out.println(elements.get(begin));
		}
	}
	
}

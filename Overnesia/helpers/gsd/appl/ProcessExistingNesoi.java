package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public class ProcessExistingNesoi {

	private Scanner sc;
	private int targetEpoch;
	private HashMap<UUID, Integer> nesoi;
	private int nodes;
	
	public ProcessExistingNesoi(String inputFile, int targetEpoch) throws FileNotFoundException {
		this.sc = new Scanner(new File(inputFile));
		this.targetEpoch = targetEpoch;
		this.nesoi = new HashMap<UUID, Integer>();
		this.nodes = 0;
	}
	
	public void run() {
		StringTokenizer tokens = null;
		while(sc.hasNextLine()) {
			tokens = new StringTokenizer(sc.nextLine(), " ");
			if(tokens.nextToken().equals("OVFIL")){ //This is the identifier of Overlay Filiation
				tokens.nextToken();tokens.nextToken();tokens.nextToken(); //Ignore the time stamp elements
				if(Integer.parseInt(tokens.nextToken()) == targetEpoch) { //Verify if this entry is for the correct epoch
					this.nodes++; //One more node found
					tokens = new StringTokenizer(sc.nextLine(), " "); //read next line;
					tokens.nextToken(); //Ignore node identifier
					UUID id = UUID.fromString(tokens.nextToken()); //Read the nesos Identifier
					int nesos_size = Integer.parseInt(new StringTokenizer(sc.nextLine(), " ").nextToken()); //Read, get first, convert (nesos size)
					if(!nesoi.containsKey(id)) {
						nesoi.put(id, nesos_size);
					} else {
						if(nesoi.get(id) != nesos_size) 
							System.err.println("Size Inconsistency: " + id);
					}
				}
			}
		}
		
		System.err.println("I have found " + nodes + " nodes");
		for(UUID id: nesoi.keySet()) {
			System.out.println(id + " " + nesoi.get(id));
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException {
		ProcessExistingNesoi p = new ProcessExistingNesoi(args[0], Integer.parseInt(args[1]));
		p.run();
	}
	
}

package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class CleanBroadcastLogs {

	private String log;
	private TreeSet<String> set;
	
	public CleanBroadcastLogs(String log) {
		this.log = log;
		this.set = new TreeSet<String>();
	}
	
	private void execute() throws FileNotFoundException {
		Scanner scan = new Scanner(new File(this.log));
		PrintStream out = new PrintStream(new File(this.log+".clean"));
		int ignored = 0;
		
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			StringTokenizer st = new StringTokenizer(line, " ");
			if(st.nextToken().equalsIgnoreCase("R")) {
				st.nextToken(); //Remove experience ID...
				String message_id = st.nextToken() + " " + st.nextToken();
				
				if(set.contains(message_id)) {
					ignored++;
				} else {
					set.add(message_id);
					out.println(line);
				}
			} else {
				out.println(line);
			}
		}
	}
	
	public static void main(String[] argv) {
		if(argv.length != 1) {
			System.err.println("Invalid argument number.\nUsage: " + CleanBroadcastLogs.class.getCanonicalName() + " <logfile>");
		}
		
		CleanBroadcastLogs cbl = new CleanBroadcastLogs(argv[0]);
		try {
			cbl.execute();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}

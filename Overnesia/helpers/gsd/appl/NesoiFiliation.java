package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public class NesoiFiliation {

	private Scanner sc;
	private int targetEpoch;
	private UUID targetNesos;
	private ArrayList<String> filiation;
	
	public NesoiFiliation(String inputFile, UUID target, int targetEpoch) throws FileNotFoundException {
		this.sc = new Scanner(new File(inputFile));
		this.targetEpoch = targetEpoch;
		this.targetNesos = target;
		this.filiation = new ArrayList<String>();
	}
	
	public void run() {
		StringTokenizer tokens = null;
		while(sc.hasNextLine()) {
			tokens = new StringTokenizer(sc.nextLine(), " ");
			if(tokens.nextToken().equals("OVFIL")){ //This is the identifier of Overlay Filiation
				tokens.nextToken();tokens.nextToken();tokens.nextToken(); //Ignore the time stamp elements
				if(Integer.parseInt(tokens.nextToken()) == targetEpoch) { //Verify if this entry is for the correct epoch
					String line = sc.nextLine(); //read next line;
					tokens = new StringTokenizer(line, " "); 
					tokens.nextToken(); //Ignore node identifier
					UUID id = UUID.fromString(tokens.nextToken()); //Read the nesos Identifier
					line = line + " " + sc.nextLine();
					if(id.equals(targetNesos))
						this.filiation.add(line);
				}
			}
		}
		
		for(String line: filiation) {
			System.out.println(line);
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException {
		NesoiFiliation p = new NesoiFiliation(args[0], UUID.fromString(args[1]), Integer.parseInt(args[2]));
		p.run();
	}
	
}

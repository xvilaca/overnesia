package gsd.appl;

import gsd.appl.util.Statistics;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class ProcessBroadcastExperienceLogs {
		
	private String experience_id;
	private File main_log;
	
	public ProcessBroadcastExperienceLogs( String log) {
		this.main_log = new File(log);
	}

	public static void main(String[] argv) {
		
		if(argv.length != 1) {
			System.err.println("Invalid number of arguments.\nUsage: java " + ProcessBroadcastExperienceLogs.class.getCanonicalName() + " <LOG_FILE>");
			System.exit(1);
		}
		
		ProcessBroadcastExperienceLogs processor = new ProcessBroadcastExperienceLogs(argv[0]);
		try {
			System.exit(processor.execute());
		} catch (FileNotFoundException e) {
			System.err.println("Log file was not found: " + argv[0]);
			System.exit(1);
		}
		
	}

	private void openNodeLogs(ArrayList<Scanner> node_logs) throws FileNotFoundException {
		for(Scanner s: node_logs)
			s.close();
		
		node_logs.clear();
		
		Scanner main = new Scanner(this.main_log);
		int missing_files = 0;
		
		String header = main.nextLine();
		this.experience_id = header.split(" ")[5];
		
		main.nextLine();//Time info
		main.nextLine();//empty line
		main.nextLine();//Starting nodes
		main.nextLine();//Finishing nodes
		
		header = main.nextLine(); //failed node header
		header = header.split(" ")[2].substring(1);
		header = header.substring(0, header.length()-1);
		int node_count = Integer.parseInt(header);
		for(int i = 0; i < node_count; i++)
			main.nextLine();//failed node
		
		header = main.nextLine(); //missing node header
		header = header.split(" ")[2].substring(1);
		header = header.substring(0, header.length()-1);
		node_count = Integer.parseInt(header);
		for(int i = 0; i < node_count; i++)
			main.nextLine();//missing node
		
		header = main.nextLine(); //correct nodes
		header = header.split(" ")[3].substring(1);
		header = header.substring(0, header.length()-1);
		node_count = Integer.parseInt(header);
		for(int i = 0; i < node_count; i++) {
			header = main.nextLine().split("/")[0];//correct node
			File node_log = new File(this.experience_id + "/" + this.experience_id + "-" + header + ".log.clean");
			try{
				node_logs.add(new Scanner(node_log));
			} catch (FileNotFoundException e) {
				System.err.println("File <" + node_log.getName() + "> not found. Try to recover this file then run this command again.");
				missing_files++;
			}
		}
		
		if(missing_files > 0) {
			System.err.println("There are " + missing_files +  " node logs missing. Terminating...");
			System.exit(2);
		}
	}
	
	private int execute() throws FileNotFoundException {
		ArrayList<Scanner> node_logs = new ArrayList<Scanner>();
		this.openNodeLogs(node_logs);
		
		HashMap<String, MessageStatistics> messages = this.scanForMessages(node_logs);
		
		int ignored_message_receptions = this.parseReceptionLogs(node_logs, messages);
		Statistics reliability = this.calculateReliability(node_logs, messages);
		Statistics maxLatency = this.calculateMaxLatency(node_logs, messages);
		
		System.out.println("Experience ID: " + this.experience_id);
		System.out.println("Number of nodes: " + node_logs.size());
		System.out.println("Number of messages exchanged: " + messages.keySet().size());
		System.out.println("(Ignored " + ignored_message_receptions + " message reception log entries (issuer was not considered in experience).");
		reliability.dump(System.out, "Reliability:");
		maxLatency.dump(System.out, "Maximum Latency:");
		
		return 0;
	}
	
	private Statistics calculateMaxLatency(ArrayList<Scanner> node_logs,
			HashMap<String, MessageStatistics> messages) {
		Statistics result = new Statistics(0.95);
		
		for(String message_id: messages.keySet()) {
			result.add(messages.get(message_id).max_latency);
		}
		
		return result;
	}

	private Statistics calculateReliability(ArrayList<Scanner> node_logs,
			HashMap<String, MessageStatistics> messages) {
		Statistics result = new Statistics(0.95);
		
		HashMap<Long,MessageStatistics> timeSet = new HashMap<Long, MessageStatistics>();
		
		for(String message_id: messages.keySet()) {
			while(timeSet.containsKey(messages.get(message_id).creationTime))
				messages.get(message_id).creationTime++;
			timeSet.put(messages.get(message_id).creationTime, messages.get(message_id));
			result.add(((double)messages.get(message_id).delivered) / node_logs.size());
		}
		
		TreeSet<Long> orderedKeys = new TreeSet<Long>(timeSet.keySet());
		
		Iterator<Long> iterator = orderedKeys.iterator();
		
		while(iterator.hasNext()) {
			MessageStatistics info = timeSet.get(iterator.next());
			System.out.println(info.creationTime + " " + info.delivered +"/"+node_logs.size()+" "+((double)info.delivered) / node_logs.size());
		}
		
		return result;
	}

	private int parseReceptionLogs(ArrayList<Scanner> node_logs,
			HashMap<String, MessageStatistics> messages) {
	
		int ignored_message_receptions = 0;
		int i = 0;
		
		try {
			for(i = 0; i < node_logs.size(); i++) {
				while(node_logs.get(i).hasNextLine()) {
					StringTokenizer st = new StringTokenizer(node_logs.get(i).nextLine(), " ");
					if(st.nextToken().equalsIgnoreCase("R")) {
						st.nextToken(); //Remove experience ID...
						String message_id = st.nextToken() + " " + st.nextToken();
						String time_creation = st.nextToken() + " " + st.nextToken() + " " + st.nextToken();
						
						//System.err.println("Processing reception log for message '" + message_id + "'");
						
						if(messages.containsKey(message_id)) {
							try {
								messages.get(message_id).registerReception(new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss.SSS").parse(time_creation).getTime());
							} catch (ParseException e) {
								System.err.println("Error while parsing date on log with index " + i);
								e.printStackTrace();
								System.exit(3);
							}
						} else {
							ignored_message_receptions++;
						}
					
					}		
				}
			}
		} catch (NoSuchElementException e) {
			System.err.println("Unexpected end of file e node log with index " + i);
			System.exit(6);
		}
		try { this.openNodeLogs(node_logs); } catch (FileNotFoundException e) {}
		return ignored_message_receptions;
	}

	private HashMap<String, MessageStatistics> scanForMessages(ArrayList<Scanner> node_logs) {
		HashMap<String, MessageStatistics> messages = new HashMap<String, MessageStatistics>();
		
		for(int i = 0; i < node_logs.size(); i++) {
			while(node_logs.get(i).hasNextLine()) {
				StringTokenizer st = new StringTokenizer(node_logs.get(i).nextLine(), " ");
				if(st.nextToken().equalsIgnoreCase("I")) {
					st.nextToken(); //Remove experience ID...
					String message_id = st.nextToken();
					message_id = message_id.substring(message_id.indexOf("/"));
					message_id = message_id + " " + st.nextToken();
					//System.err.println("Creating register for message '" + message_id + "'");
					String time_creation = st.nextToken() + " " + st.nextToken() + " " + st.nextToken();
					try {
						messages.put(message_id, new MessageStatistics(message_id, new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss.SSS").parse(time_creation).getTime()));
					} catch (ParseException e) {
						System.err.println("Error while parsing date.");
						e.printStackTrace();
						System.exit(3);
					}
				}		
			}
		}
		
		try { this.openNodeLogs(node_logs); } catch (FileNotFoundException e) {}
		return messages;
	}

	class MessageStatistics {
		
		
		public MessageStatistics(String message_id, long creation_time) {
			this.message_id = message_id;
			this.creationTime = creation_time;
			//this.latency = new IncrementalFreq();
			this.max_latency = -1;
		}
		
		public void registerReception(long time) {
			this.delivered++;
			long delta = time - this.creationTime;
			if(delta < 0) delta = 0;
			//this.latency.add((int) delta);
			if(delta > this.max_latency)
				max_latency = delta;
		}

		String message_id;
		long creationTime;
		//IncrementalFreq latency;
		double max_latency;
		int delivered = 0;
		
	}
	
}

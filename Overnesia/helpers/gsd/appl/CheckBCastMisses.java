package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class CheckBCastMisses {

	private Scanner[] scans;
	private Object[] hosts_failed;
	
	public CheckBCastMisses(String[] filenames) throws FileNotFoundException {
		this.scans = new Scanner[filenames.length];
		this.hosts_failed = new Object[filenames.length];
	
		for(int i = 0; i < filenames.length; i++) {
			this.scans[i] = new Scanner(new File(filenames[i]));
			this.hosts_failed[i] = new ArrayList<String>();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void run() {
		for(int i = 0; i < scans.length; i++) {
			System.err.println("Reading file: " + (i+1));
			
			Scanner s = this.scans[i];
			ArrayList<String> r = new ArrayList<String>();
			ArrayList<String> t = new ArrayList<String>();
			ArrayList<String> f = (ArrayList<String>) this.hosts_failed[i];
			
	 		s.nextLine(); s.nextLine(); s.nextLine(); s.nextLine(); s.nextLine(); s.nextLine(); //skip header
			String line = s.nextLine().trim();
		
			while(!line.equalsIgnoreCase("")) {
				r.add(new StringTokenizer(line, "/").nextToken().toLowerCase());
				line = s.nextLine().trim();
			}
			
			s.nextLine(); s.nextLine(); //Skip latency header
				line = s.nextLine().trim();
	
				while(!line.equalsIgnoreCase("")) {
				t.add(new StringTokenizer(line, " ").nextToken().toLowerCase());
				line = s.nextLine().trim();
			}
			
			for(int j = 0; j < t.size(); j++) {
				if(!r.contains(t.get(j)))
					f.add(t.get(j));
			}
		
		}
		
		for(int i = 0; i < scans.length; i++) {
			ArrayList<String> f = (ArrayList<String>) this.hosts_failed[i];
			while(f.size() > 0) {
				String host = f.remove(0);
				int counter = 1;
				for(int j = i; j < scans.length; j++) {
					ArrayList<String> aux = (ArrayList<String>) this.hosts_failed[j];
					if(aux.contains(host)) {
						counter++;
						aux.remove(host);
					}
				}
				System.out.println(host + " " + counter);
			}
		}
		
	}
	
	public static void main(String[] args) {
		if(args.length == 0) {
			System.err.println("Usage: java " + CheckBCastMisses.class.getName() + " filename [filename2 .. filenameN]");
			System.exit(1);
		}
		
		try { 
			CheckBCastMisses cbcm = new CheckBCastMisses(args);
			cbcm.run();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}


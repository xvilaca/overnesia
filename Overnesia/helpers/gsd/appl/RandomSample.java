package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class RandomSample {

	public static void main(String[] args) throws FileNotFoundException {
		
		ArrayList<String> elements = new ArrayList<String>();
		Random rand = new Random(System.currentTimeMillis());
		Scanner sc = new Scanner(new File(args[0]));
		int remaining = Integer.parseInt(args[1]);
		
		while(sc.hasNextLine())
			elements.add(sc.nextLine());
		
		while(remaining > 0 && elements.size() > 0) {
			System.out.println(elements.remove(rand.nextInt(elements.size())));
			remaining --;
		}
	}
	
}

package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class AggregateMessageCost {

	private Scanner sc[];

	public AggregateMessageCost(String inputFile) throws FileNotFoundException {
		this.sc = new Scanner[5];
		for(int i = 0; i < 5; i++)
			this.sc[i] = new Scanner(new File(inputFile + (i+1) + ".txt"));
	}
	
	public void run() throws FileNotFoundException {
		
		StringTokenizer[] sts = new StringTokenizer[5];
		
		while(sc[0].hasNextLine()) {
			for(int i = 0; i < 5; i++)
				sts[i] = new StringTokenizer(sc[i].nextLine(), " ");
	
			System.out.print(sts[0].nextToken() + " ");
			for(int i = 1 ; i < 5; i++)
				sts[i].nextToken();
			
			for(int t=0; t < 3; t++) {
				double value = 0;
				for(int i = 0; i < 5; i++)
					value += Double.parseDouble(sts[i].nextToken());
				System.out.print((value/5) + " ");
			}
			System.out.println();
	
		}		
	}
	
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException {
		AggregateMessageCost p = new AggregateMessageCost(args[0]);
		p.run();
	}
	
}

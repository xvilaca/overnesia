package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class ProcessMessageCost {

	private Scanner sc;
	private HashMap<Integer, Double> messages_S;
	private HashMap<Integer, Double> messages_R;
	private HashMap<Integer, Integer> nodes;
	
	public ProcessMessageCost(String inputFile) throws FileNotFoundException {
		this.sc = new Scanner(new File(inputFile));
		this.messages_S = new HashMap<Integer, Double>();
		this.messages_R = new HashMap<Integer, Double>();
		this.nodes = new HashMap<Integer, Integer>();
	}
	
	public void run() throws FileNotFoundException {
		
		StringTokenizer tokens = null;
		while(sc.hasNextLine()) {
			Scanner input = new Scanner(new File(sc.nextLine()));

			while(input.hasNextLine()) {
				tokens = new StringTokenizer(input.nextLine(), " ");
				tokens.nextToken();			
				tokens.nextToken();			
				tokens.nextToken();			
				tokens.nextToken();			
				int time = Integer.parseInt(tokens.nextToken());
				int send = Integer.parseInt(tokens.nextToken());
				int recv = Integer.parseInt(tokens.nextToken());
				if(this.nodes.containsKey(time)) {
					this.nodes.put(time, this.nodes.remove(time) + 1);
					this.messages_S.put(time, this.messages_S.remove(time) + send);
					this.messages_R.put(time, this.messages_R.remove(time) + recv);
				} else {
					this.nodes.put(time, 1);
					this.messages_S.put(time, (double) send);
					this.messages_R.put(time, (double) recv);
				}
			}
		}
		
		Iterator<Integer> iterator = new TreeSet<Integer>(this.nodes.keySet()).iterator();
		while(iterator.hasNext()) {
			int time = iterator.next();
			int nodes = this.nodes.get(time);
			System.out.println(((time * 30) - 30) + " " + (this.messages_S.get(time) / nodes) + " " + (this.messages_R.get(time) / nodes) + " " + nodes);
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException {
		ProcessMessageCost p = new ProcessMessageCost(args[0]);
		p.run();
	}
	
}

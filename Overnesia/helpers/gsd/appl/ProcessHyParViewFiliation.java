package gsd.appl;

import gsd.appl.util.IncrementalFreq;
import gsd.common.NodeID;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public class ProcessHyParViewFiliation {

	private Scanner sc;
	private int targetEpoch;
	private HashMap<UUID, ArrayList<UUID>> neighbors;
	private HashMap<UUID, NodeID> identifiers;
	
	public ProcessHyParViewFiliation(String inputFile, int targetEpoch) throws FileNotFoundException {
		this.sc = new Scanner(new File(inputFile));
		this.targetEpoch = targetEpoch;
		this.neighbors = new HashMap<UUID, ArrayList<UUID>>();
		this.identifiers = new HashMap<UUID, NodeID>();
	}
	
	public void run() {
		StringTokenizer tokens = null;
		while(sc.hasNextLine()) {
			tokens = new StringTokenizer(sc.nextLine(), " ");
			if(tokens.nextToken().equals("OVFIL")){ //This is the identifier of Overlay Filiation
				tokens.nextToken();tokens.nextToken();tokens.nextToken(); //Ignore the time stamp elements
				if(Integer.parseInt(tokens.nextToken()) == targetEpoch) { //Verify if this entry is for the correct epoch
					tokens = new StringTokenizer(sc.nextLine(), "/"); //read next line;
					String hostname = tokens.nextToken();
					StringTokenizer inet_st = new StringTokenizer(tokens.nextToken(), ":");
					inet_st.nextToken();
					InetSocketAddress addr = new InetSocketAddress(hostname, Integer.parseInt(inet_st.nextToken()));
					UUID id = UUID.fromString(inet_st.nextToken()); //Read the node identifier
			
					this.identifiers.put(id, new NodeID(addr, id));
					
					ArrayList<UUID> node_neighbors = new ArrayList<UUID>();
					this.neighbors.put(id, node_neighbors);
					
					tokens = new StringTokenizer(sc.nextLine(), " "); //Read
					
					int active_size = Integer.parseInt(tokens.nextToken()); //get first, convert
					int passive_size = Integer.parseInt(tokens.nextToken());
					
					for(int i = 0; i < active_size; i++) {
						tokens = new StringTokenizer(sc.nextLine(), ":");
						tokens.nextToken();
						tokens.nextToken();
						node_neighbors.add(UUID.fromString(tokens.nextToken()));
					}
					
					for(int i = 0; i < passive_size; i++) {
						sc.nextLine(); //for now ignore the passive view.
					}
					
					sc.nextLine(); //Ignore the TCP connections statistics (for now)...
				}
			}
		}
		
		System.err.println("I have found " + this.identifiers.size() + " nodes");
		
	}
	
	public static void main(String[] args) throws NumberFormatException, FileNotFoundException {
		ProcessHyParViewFiliation p = new ProcessHyParViewFiliation(args[0], Integer.parseInt(args[1]));
		p.run();
		System.err.println("Active View Consistency Test: " + (p.activeViewConsistency() ? "Passed" : "Failed" ));
		System.err.println("Active View Symmetry Test: " + (p.activeViewSymmetry() ? "Passed" : "Failed" ));
		System.err.println("Active View Distribution Test: "); 
		p.activeViewSizeDistribution();
		System.err.println("Test Complete"); 
	}

	private boolean activeViewSizeDistribution() {
		boolean okay = true;
		
		IncrementalFreq stats = new IncrementalFreq();
		
		for(UUID id: this.neighbors.keySet()) {
			stats.add(this.neighbors.get(id).size());
		}
		
		System.out.print(stats.toString());
		
		return okay;
	}

	private boolean activeViewSymmetry() {
		boolean okay = true;
		
		for(UUID id: this.neighbors.keySet()) {
			ArrayList<UUID> n = this.neighbors.get(id);
			for(UUID neighbor: n) {
				ArrayList<UUID> n2 = this.neighbors.get(neighbor);
				if(n2 != null && !n2.contains(id)) {
					okay = false;
					System.err.println("Inconsistency: Symmetry violated " + this.identifiers.get(id) + " ---> " + this.identifiers.get(neighbor) + " :: " + this.identifiers.get(id) + " <-/- " + this.identifiers.get(neighbor));
				}
			}
		}
		
		return okay;
	}

	private boolean activeViewConsistency() {
		boolean okay = true;
		
		for(UUID id: this.neighbors.keySet()) {
			ArrayList<UUID> n = this.neighbors.get(id);
			for(int i = 0; i < n.size(); i++) {
				if(n.get(i).equals(id)) {
					okay = false;
					System.err.println("Inconsistency: " + this.identifiers.get(id) + " is neighbor of itself");
				}
				
				if(this.identifiers.get(n.get(i)) == null)
					System.err.println("Warning: " + this.identifiers.get(id) + " has a neighbor that I don't know about: " + n.get(i));
				
				for(int j = i+1; j < n.size(); j++)
					if(n.get(i).equals(n.get(j))) {
						okay = false;
						System.err.println("Inconsistency: " + this.identifiers.get(id) + " has repated neighbor: " + this.identifiers.get(n.get(i)));
					}
			}
		}
		
		return okay;
	}
	
}

package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MassPercentageDistribution {

	private ArrayList<Double> values;
	
	private MassPercentageDistribution(File input) throws FileNotFoundException {
		Scanner sc = new Scanner(input);
		this.values = new ArrayList<Double>();
		while(sc.hasNextLine()) {
			String s = sc.nextLine();
			values.add(Double.parseDouble(s.substring(0, s.length()-2)));
		}
	}
	
	public void execute() {
		double mass[] = new double[(int) ((100*(1/0.5)) + 1)];
		for(int i = 0; i < mass.length; i++) {
			mass[i] = 0;
			for(double v: this.values)
				if(v >= (0.5*i))
					mass[i]++;
			mass[i] = (mass[i] / this.values.size()) * 100;			
		}
		
		for(int i = 0; i < mass.length; i++) 
			System.out.println((i*0.5) + " " + mass[i]);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("Usage: java " + MassPercentageDistribution.class.getCanonicalName() + " inputfile");
			System.exit(1);
		}
		
		try {
			new MassPercentageDistribution(new File(args[0])).execute();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}

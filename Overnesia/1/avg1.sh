#!/bin/bash

function getArray {
    i=1
    while IFS=  ; do
        #echo "$line"
        for val in $( echo "$line" | cut -f 2 -d '!' ) ; do
            acum[$i]=$(echo "${acum[$i]} + ${val}" | bc -l)
        done
#echo "${acum1[$i]} : ${acum2[$i]}"
#echo "${lines[$itn,$ln,1]} : ${lines[$itn,$ln,2]}"
        i=$((i + 1))
    done < "${iter}processedResults.txt"
}

rm -r -- "avg"
mkdir -- "avg"
numLines=10
declare -a acum
for (( i=0; i<$numLines; i++ )); do
    acum[$i]=0
done
actV=0
for var in */ ; do
    if [ "$var" != "avg/" ];
    then
        echo "Var: $var"
        itn=0
        for iter in ${var}*/ ; do
            read -r line < "${iter}processedResults.txt"
            for val in $( echo "$line" | cut -f 1 -d ';' ) ; do
                acum[${actV}]=$(echo "${acum[${actV}]} + ${val}" | bc -l)
            done
            itn=$((itn + 1))
        done
        acum[$actV]=$(echo "${acum[$actV]} / $itn" | bc -l)
        actV=$((actV + 1))
    fi
done
for (( i=0; i<$actV; i++ )); do
    ln=$((i + 1))
    echo "${ln}_${acum[$i]}" >> "avg/processedResults.txt"
done

package gsd.lab.peersim.protocols;

import gsd.lab.peersim.protocols.interfaces.LinkablePlus;

import java.util.ArrayList;

import peersim.cdsim.CDProtocol;
import peersim.core.Node;

public class DummyProtocol implements CDProtocol, LinkablePlus {

	private int id;
	private ArrayList<Node> neighbors;
	
	public DummyProtocol(String prefix) {
		this. id = -1;
		this.neighbors = new ArrayList<Node>();
	}
	
	public Object clone() {
		DummyProtocol ret = null;
		
		try {
			ret = (DummyProtocol) super.clone();
			ret.neighbors = new ArrayList<Node>();
		} catch (CloneNotSupportedException e) {
			//Never happens
		}
		
		return ret;
	}

	public void nextCycle(Node arg0, int arg1) {
		; //This is a dummy protocol... it does squat...
	}

	public boolean addNeighbor(Node node) {
		return this.neighbors.add(node);
	}

	public boolean contains(Node node) {
		return this.neighbors.contains(node);
	}

	public int degree() {
		return this.neighbors.size();
	}

	public Node getNeighbor(int position) {
		return this.neighbors.get(position);
	}

	public void pack() {
		; //nothing to do
	}

	public void onKill() {
		this.neighbors = null;
		this.id = -1;
	}

	public void setNodeID(int id) {
		this.id = id;
	}
	
	public int getNodeID() {
		return this.id;
	}
	
}

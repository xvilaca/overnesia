package gsd.lab.peersim.protocols.interfaces;

import peersim.core.Linkable;

public interface LinkablePlus extends Linkable {

	public void setNodeID(int id);
	
	public int getNodeID();
	
}

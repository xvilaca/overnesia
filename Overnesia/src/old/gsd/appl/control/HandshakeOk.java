package old.gsd.appl.control;

import java.io.Serializable;

public class HandshakeOk implements Serializable {

	private boolean reply;
	/**
	 * 
	 */
	private static final long serialVersionUID = 9191416788465342660L;

	public HandshakeOk() {
		this.reply = true;
	}
	
	public boolean getReply() {
		return reply;
	}
}

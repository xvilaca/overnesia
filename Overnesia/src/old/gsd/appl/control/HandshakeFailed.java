package old.gsd.appl.control;

import java.io.Serializable;

public class HandshakeFailed implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5541334094796386122L;
	private boolean reply;

	public HandshakeFailed() {
		this.reply = false;
	}
	
	public boolean getReply() {
		return reply;
	}
}

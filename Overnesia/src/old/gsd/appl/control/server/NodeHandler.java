package old.gsd.appl.control.server;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import old.gsd.appl.control.NoLongerOnOperationSet;

public class NodeHandler implements Runnable {

	private final String nodeID;
	private Socket link;
	private NodeListener listener;
	
	private Thread nodeControlThread;
	private boolean run;
	
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	public NodeHandler(String id, Socket link, NodeListener listener) {
		this.nodeID = id;
		this.link = link;
		try {
			this.in = new ObjectInputStream(this.link.getInputStream());
			this.out = new ObjectOutputStream(this.link.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.listener = listener;
		
		this.nodeControlThread = new Thread(this);
		this.run = true;
		this.nodeControlThread.start();
	}
	
	public synchronized void stop() {
		this.run = false;
		try {
			this.out = null;
			this.in = null;
			this.link.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void send(Object msg) {
		try {
			this.out.writeObject(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {		
		while(this.run) {
			try {
				Object o = this.in.readObject();
				this.listener.deliver(this.nodeID, o);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
	}

	public String getNodeID() {
		return this.nodeID;
	}

	public synchronized void terminate() {
		try {
			this.out.writeObject(new NoLongerOnOperationSet());
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.stop();
	}	
}

package old.gsd.appl.control.server;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

import old.gsd.appl.control.HandshakeFailed;
import old.gsd.appl.control.HandshakeMessage;
import old.gsd.appl.control.HandshakeOk;

public class PlanetLabControl implements Runnable, NodeListener{

	private String operation_list_file;
	private HashMap<String,NodeHandler> nodes; 
	private ServerSocket listen;
	
	public PlanetLabControl() throws IOException {
		this.operation_list_file = "./operation.list";
		this.nodes = new HashMap<String, NodeHandler>();
		this.loadOperationList();
		this.listen = new ServerSocket(28349);
	}
	
	private synchronized void loadOperationList() {
		HashMap<String,NodeHandler> operation = new HashMap<String, NodeHandler>();
		Scanner nodes = null;
		try {
			nodes = new Scanner(new File(this.operation_list_file));
		} catch (FileNotFoundException e) {
			System.err.println("File not found: '" + this.operation_list_file + "': node list not changed.");
			return;
		}
		
		while(nodes.hasNextLine()) {
			String node = nodes.nextLine().trim();
			if(this.nodes.containsKey(node)) {
				NodeHandler handler = this.nodes.remove(node);
				if(handler != null)
					operation.put(handler.getNodeID(), handler);
				else
					operation.put(node, null);
			} else {
				operation.put(node, null);
			}
		}
		
		for(String discontinued_node: this.nodes.keySet()) {
			NodeHandler handler = this.nodes.remove(discontinued_node);
			if(handler != null) 
				handler.terminate();
		}
		
		this.nodes = operation;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PlanetLabControl plc = null;
		try {
			plc = new PlanetLabControl();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		Thread server = new Thread(plc);
		server.start();
		
		Scanner scan = new Scanner(System.in);
		while(true) {
			String cmd = scan.nextLine();
			System.out.println("Command received: '" + cmd + "'");
			if(cmd.equalsIgnoreCase("ls")) {
				plc.listNodes();
			}
		}
	}

	private synchronized void listNodes() {
		int ok = 0;
		int nok = 0;
		for(String n: this.nodes.keySet()) {
			NodeHandler h = this.nodes.get(n);
			if(h != null) {
				ok++;
				System.out.println(n);
			} else {
				nok++;
			}
		}
		
		System.out.println("Total: " + (ok + nok) + " nodes. " + ok + " are connected. " + nok + " not connected.");
	}

	public synchronized void deliver(String nodeID, Object o) {
		
	}

	public void run() {
		while(true) {
			try {
				Socket s = this.listen.accept();
				try {
					Object o = new ObjectInputStream(s.getInputStream()).readObject();
					if(o instanceof HandshakeMessage) {
						String host = ((HandshakeMessage) o).getHostname();
						if(this.nodes.containsKey(host)) {
							this.nodes.put(host, new NodeHandler(host, s, this));
							new ObjectOutputStream(s.getOutputStream()).writeObject(new HandshakeOk());
						} else {
							new ObjectOutputStream(s.getOutputStream()).writeObject(new HandshakeFailed());
						}
							
					} else {
						System.err.println("Not valid protocol message from: "  + s.getRemoteSocketAddress());
						s.close();
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

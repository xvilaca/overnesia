package old.gsd.appl.control.server;

public interface NodeListener {

	void deliver(String nodeID, Object o);

}

package old.gsd.appl.control.client;

import gsd.appl.utils.DateTimeRepresentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import old.gsd.appl.control.HandshakeFailed;
import old.gsd.appl.control.HandshakeMessage;
import old.gsd.appl.control.HandshakeOk;

public class PlanetLabNode implements Runnable {

	private InetSocketAddress myID;
	private InetSocketAddress serverAddress;
	private PrintStream localLog;
	private Socket link;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	private Thread controlThread;
	private Thread readerThread;
	
	public PlanetLabNode(InetSocketAddress myID, InetSocketAddress serverAddress) {
		this.myID = myID;
		this.serverAddress = serverAddress;
		try {
			this.localLog = new PrintStream(new FileOutputStream("./" + this.myID.getHostName() + "_overlay.log"));
		} catch (FileNotFoundException e) {
			this.localLog = System.out;
			e.printStackTrace();
		}
		this.link = new Socket();
		
		this.controlThread = new Thread(this);
		this.controlThread.start();
	}
	
	private synchronized void connectToServer() {
		if(!this.link.isConnected())
			try {
				this.link.connect(this.serverAddress);
				this.in = new ObjectInputStream(this.link.getInputStream());
				this.out = new ObjectOutputStream(this.link.getOutputStream());
				this.out.writeObject(new HandshakeMessage(this.myID.getHostName()));
				this.readerThread = new Thread(new Runnable(){ public void run(){
					try {
						Object o = in.readObject();
						if(o instanceof HandshakeOk) {
							System.err.println("Connected to server.");
						} else if(o instanceof HandshakeFailed) {
							System.err.println("Not in operation list.");
						}
					} catch (IOException e) {
						e.printStackTrace();
						return;
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
						return;
					}
				} });
				this.readerThread.start();
			} catch (IOException e) {
				try { this.link.close(); } catch (IOException e1) { e1.printStackTrace(); }
				this.in = null;
				this.out = null;
				this.localLog.println("ERROR " + DateTimeRepresentation.timeStamp() + " NOSERVER " + this.serverAddress + " cannot connect to server: " + e.getClass().getName() + " " + e.getMessage());
				return;
			}
			
		
	}
	
	public void run() {
		this.connectToServer();
		while(true) {
			try {
				Object o = in.readObject();
				System.out.println(o);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	public synchronized void log(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		this.localLog.println(line);
		try {
			if(this.out != null)
				this.out.writeObject(line);
			else
				throw new IOException("No connection to server.");
		} catch (IOException e) {
			e.printStackTrace();
			this.localLog.println("ERROR " + DateTimeRepresentation.timeStamp() + " " + "NOSEND " + line + " " + e.getClass().getName() + " " + e.getMessage());
		}
	}

}

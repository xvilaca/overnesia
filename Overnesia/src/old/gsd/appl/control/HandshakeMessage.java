package old.gsd.appl.control;

import java.io.Serializable;

public class HandshakeMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6598910773042852838L;
	private String hostname;
	
	public HandshakeMessage(String hostName) {
		this.hostname = hostName;
	}
	
	public String getHostname() {
		return this.hostname;
	}

}

package gsd.appl;

import gsd.api.P2PChannel;
import gsd.common.OvernesiaNodeID;
import gsd.impl.Config;
import gsd.impl.Periodic;
import gsd.impl.overnesia.Overnesia;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Set;

public class NonInteractiveProtoApplication extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private P2PChannel channel;
	
	private Periodic DebugOperation;
	
	public NonInteractiveProtoApplication(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException {
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new P2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
					
	}
	
	public NonInteractiveProtoApplication(InetSocketAddress myId) throws UnknownHostException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new P2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
	
	}
	
	public boolean debugOperation() {
		Overnesia overlay = channel.getOverlayInstance();
		
		System.err.println("ID: " + overlay.getId());
		System.err.println("Nesos Size: " + overlay.getNesosSize());
		System.err.println("External Neighbors Size: " + overlay.getExternalNeighborsSize());
		System.err.println("Passive View Size: " + overlay.getPassiveViewSize());
		System.err.println("Nesos Filiation:");
		Set<OvernesiaNodeID> nv = overlay.getNesosFiliation();
		for(OvernesiaNodeID id: nv)
			System.err.println("  " + id);
		System.err.println("External Neighbors:");
		Set<OvernesiaNodeID> ext = overlay.getExternalIDs();
		for(OvernesiaNodeID id: ext)
			System.err.println("  " + id);
		System.err.println("Number of TCP Connections: " + overlay.connections().length + " [Network layer: " + overlay.network().connections().length + "]");
		for(OvernesiaNodeID id: overlay.getPassiveView()) {
			if(nv.contains(id))
				System.err.println("ERROR: " + id + " is both in passive view and nesos view");
			if(ext.contains(id))
				System.err.println("ERROr: " + id + " is both in passive view and external view");
		}
		return true;
	}
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		if(this.contactId != null)
			System.out.println("Contact id: " + this.contactId.toString());
		else
			System.out.println("Contact id: [NONE]");
		
		//Initialize the overlay network level
		channel.connect(this.contactId);
		
		try {
			while(true) {
				byte[] buf = new byte[1000];
				ByteBuffer bb = ByteBuffer.wrap(buf);
				channel.read(bb);
				System.out.println((new String(buf)).trim());
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), 30000);
		InetSocketAddress contact = new InetSocketAddress("192.16.125.11",30000);
		
		if(local.getAddress().equals(contact.getAddress())) {
			System.out.println("Primary node is on-line.");
			new NonInteractiveProtoApplication(local).execute();
		} else {
			System.out.println("Node is on-line.");
			new NonInteractiveProtoApplication(local, contact).execute();
		}
		
	}

}

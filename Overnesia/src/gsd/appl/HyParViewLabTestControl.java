package gsd.appl;

import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.experiences.data.DataBroadcastRequest;
import gsd.appl.control.experiences.data.FileBroadcastRequest;
import gsd.appl.utils.DateTimeRepresentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.log4j.Logger;

public class HyParViewLabTestControl {

	public static int controlPort = 30500;
	
	private static Logger log = Logger.getLogger(HyParViewLabTestControl.class);
	public ArrayList<Slave> slaves;
	public HashMap<UUID, ExperienceInstance> experiences;
	public CommandLine cmdl;
	private Random r;
	public VersionManager versions = VersionManager.instance; 
		
	private final boolean debugON = true;
	public void debug(Object sommuner, String s) {
		if(debugON) {
			log.debug(DateTimeRepresentation.timeStamp() + " " + sommuner.getClass().getName() + ": " + s);
		}
	}
	
	public HyParViewLabTestControl() {
		this.slaves = new ArrayList<Slave>();
		this.experiences = new HashMap<UUID, ExperienceInstance>();
		this.r = new Random(System.nanoTime());
	}
	
	private void initialize(String filename) throws FileNotFoundException {
		Scanner hosts = new Scanner(new File(filename));
		
		while(hosts.hasNextLine()) {
			this.slaves.add(new Slave(this, hosts.nextLine(), this.r));
		}
		System.err.println("Loaded " + this.slaves.size() + " hosts.");
	}
	
	private void startCommandLine() {
		this.cmdl = new CommandLine(this);
		this.cmdl.start();
	}
	
	private void startConnectionsManager() {
		
		while(true) {
			synchronized (this.slaves) {
				boolean alldone = true;
				for(int i = 0; i < this.slaves.size(); i++) {
					//this.cmdl.output("Attemping connection to: " + this.slaves.get(i).host);
					if(this.slaves.get(i).state.isOFF())
						alldone = this.slaves.get(i).connect() && alldone;
				}
				
				if(alldone) {
					try { this.slaves.wait(); } catch (InterruptedException e) {e.printStackTrace();}
				} 
			}
			try { Thread.sleep(10*1000); } catch (InterruptedException e) { e.printStackTrace(); }
		}
		
	}
	
	public synchronized void addNodeToList(String host) {
		synchronized (this.slaves)  {
			this.slaves.add(new Slave(this, host, this.r));
			this.slaves.notify();
		}	
	}
	
	public synchronized void removeNodeFromList(ArrayList<Integer> list) {
		if(list.size() == 0) {
			this.cmdl.output("Unknown host. Cannot remove.");
		}
		
		synchronized (this.slaves) {
			for(int position: list) {
				Slave out = this.slaves.remove(position);
				out.stop();
				this.cmdl.output("Removed node: " + position + " " + out.toString());			 
			}
		}
		
	}
	
	public synchronized void countActiveNodes() {
		int count = 0;
		for(Slave slave: this.slaves) {
			if(slave.isOn()) 
				count++;
		}
		this.cmdl.output("Number of active nodes: " + count);
	}
	
	public synchronized void listNodes() {
		String answer = "All nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			answer = answer + i + "\t " + slaves.get(i).toString();
		}
		this.cmdl.output(answer);
	}
	
	public synchronized void listExperiences() {
		String answer = "Experiences Running;\n";
		int i = 0;
		for(UUID id: new TreeSet<UUID>(this.experiences.keySet())) {
			answer = answer + i + "\t " + this.experiences.get(id).toString() + "\n";
			i++;
		}
		this.cmdl.output(answer);
	}
	
	public synchronized void printNodeInformation(ArrayList<Integer> list) {
		if(list.size() == 0)
			this.cmdl.output("Unknown host.");
		else 
			for(int position: list)
				this.cmdl.output(position + "\t " + slaves.get(position).toString());			
	}
	
	public synchronized void cleanNodeStatus(ArrayList<Integer> list) {
		if(list.size() == 0)
			this.cmdl.output("Unknown host.");
		else 
			for(int position: list) {
				Slave node = slaves.get(position);
				node.clean();
				this.cmdl.output(position + " " + slaves.get(position).toString() + ": reseting internal state.");
				
			}
				
				
	}
	
	public synchronized void exportlistNodes() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(new File("list")));
			for(int i = 0; i < slaves.size(); i++) {
				out.println(slaves.get(i).host);
			}
			out.close();
			this.cmdl.output("Export complete (file: list)");
		} catch (FileNotFoundException e) {
			this.cmdl.output("Cannot export list: " + e.getMessage());
		}
	}
	
	public synchronized void literalExportNodes(String f) {
		PrintStream out = System.out;
		if(f != null)
			try {
				out = new PrintStream(new File(f));
			} catch (FileNotFoundException e) {
				this.cmdl.output("Cannot open file: '" + f +"'");
				return;
			}
			
		for(int i = 0; i < slaves.size(); i++) {
			out.println(slaves.get(i).toString());
		}
		
		out = null;
	}
	
	
	public synchronized void listActiveNodes() {
		String answer = "Active nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			if(slaves.get(i).isOn())
				answer = answer + i + "\t " + slaves.get(i).toString();
		}
		this.cmdl.output(answer);
	}
	
	public synchronized void exportActiveNodes() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(new File("active")));
			for(int i = 0; i < slaves.size(); i++) {
				if(slaves.get(i).isOn())
					out.println(slaves.get(i).host);
			}
			out.close();
			this.cmdl.output("Export complete (file: active)");
		} catch (FileNotFoundException e) {
			this.cmdl.output("Cannot export list: " + e.getMessage());
		}
	}
	
	public synchronized void literalExportActiveNodes(String f) {
		PrintStream out = System.out;
		if(f != null)
			try {
				out = new PrintStream(new File(f));
			} catch (FileNotFoundException e) {
				this.cmdl.output("Cannot open file: '" + f +"'");
				return;
			}
			
		for(int i = 0; i < this.slaves.size(); i++) {
			if(this.slaves.get(i).isOn())
				out.println(this.slaves.get(i).toString());
		}
		
		out = null;
	}
	
	public synchronized void listInactiveNodes() {
		String answer = "Inactive nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			if(!slaves.get(i).isOn())
			answer = answer + i + "\t " + slaves.get(i).toString();
		}
		this.cmdl.output(answer);
	}

	public synchronized void exportInactiveNodes() {
		try {
			PrintStream out = new PrintStream(new FileOutputStream(new File("inactive")));
			for(int i = 0; i < slaves.size(); i++) {
				if(!slaves.get(i).isOn())
					out.println(slaves.get(i).host);
			}
			out.close();
			this.cmdl.output("Export complete (file: inactive)");
		} catch (FileNotFoundException e) {
			this.cmdl.output("Cannot export list: " + e.getMessage());
		}
	}


	public synchronized void literalExportInactiveNodes(String f) {
		PrintStream out = System.out;
		if(f != null)
			try {
				out = new PrintStream(new File(f));
			} catch (FileNotFoundException e) {
				this.cmdl.output("Cannot open file: '" + f +"'");
				return;
			}
			
		for(int i = 0; i < slaves.size(); i++) {
			if(!slaves.get(i).isOn())
				out.println(slaves.get(i).toString());
		}
			
		out = null;
	}
	
	public synchronized void kill(ArrayList<Integer> target) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for command kill");
			return;
		}
		
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command kill not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue('k');
		}	
	}
	
	public synchronized void reset(ArrayList<Integer> target) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for command reset");
			return;
		}
		
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command reset not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue('r');
		}		
	}
	
	public void crashRecover(ArrayList<Integer> target) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for command crash");
			return;
		}
		
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command crash not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue('c');
		}
	}
	
	public synchronized void updateStatus(ArrayList<Integer> target) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for command filiation");
			return;
		}
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command get updater status not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue('g');	
		}
	}
	
	public synchronized void filiation(ArrayList<Integer> target) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for command filiation.");
			return;
		}
		
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command filiation not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue('f');
		}  
	}
	
	public synchronized void broadcastMessage(int index, String msg) {
		if(index < 0 || index >= this.slaves.size()) {
			this.cmdl.output("Error: invalid index for command broadcast");
			return;
		}
		
		if(!this.slaves.get(index).isOn()) {
			this.cmdl.output("Error: targeted host is inactive. Command broadcast not issued.");
			return;
		}
		
		this.slaves.get(index).addToQueue(new DataBroadcastRequest(index, UUID.randomUUID(), ByteBuffer.wrap(msg.getBytes())));
	}
	
	public void checkFile(ArrayList<Integer> target, String argument) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for check file command.");
			return;
		}
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command check file not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue("checkfile " + argument);
		} 
	}
	
	public void deleteFile(ArrayList<Integer> target, String argument) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for check file command.");
			return;
		}
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command check file not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue("delete " + argument);
		} 
	}
	
	public void fbcastStatus(ArrayList<Integer> target, String argument) {
		if(target.size() == 0) {
			this.cmdl.output("Error: invalid argument for checking the status of a file broadcast operation.");
			return;
		}
		
		for(int node: target) {
			if(!this.slaves.get(node).isOn()) {
				this.cmdl.output("Error: targeted host is inactive. Command checke status of a file broadcast not issued.");
				continue;
			}
			this.slaves.get(node).addToQueue("checkfbc " + argument);
		} 
	}
	
	public synchronized void requestFileBroadcast(int index, String source, String dest) {
		if(index < 0 || index >= this.slaves.size()) {
			this.cmdl.output("Error: invalid index for command filiation");
			return;
		}
		
		if(!this.slaves.get(index).isOn()) {
			this.cmdl.output("Error: targeted host is inactive. Command filiation not issued.");
			return;
		}
		
		this.slaves.get(index).addToQueue(new FileBroadcastRequest(UUID.randomUUID(), source, dest));
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		HyParViewLabTestControl ctl = new HyParViewLabTestControl();
		
		try { ctl.initialize(args[0]); } catch (FileNotFoundException e) { System.err.println("Host list not found."); System.exit(1); }
		ctl.startCommandLine();
		try {
			ctl.startConnectionsManager();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}
	}

	public void loadAllActive(ArrayList<Integer> list) {
		synchronized (this.slaves) {
			for(int i = 0; i < this.slaves.size(); i++) {
				if(this.slaves.get(i).isOn()) 
					list.add(i);
			}
		}
	}

	public int lookupNodeIndex(String argument) {
		int ret = -1;
		synchronized (this.slaves) {
			for(int i = 0; ret == -1 && i < this.slaves.size(); i++) {
				if(this.slaves.get(i).host.equalsIgnoreCase(argument)) {
					ret = i;
				}	
			}
		}
		return ret;
	}	
}

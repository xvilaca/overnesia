package gsd.appl;

import gsd.api.HyParViewPLabP2PChannel;
import gsd.appl.control.components.EnvironmentControl;
import gsd.appl.control.components.messages.PingAlive;
import gsd.appl.control.experiences.data.Filiation;
import gsd.appl.control.experiences.data.FiliationDataRecord;
import gsd.appl.control.experiences.data.FiliationRequest;
import gsd.appl.control.experiences.data.Request;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.impl.Config;
import gsd.impl.Connection;
import gsd.impl.Periodic;
import gsd.impl.hyparview.ImmortalHyParView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.NoSuchElementException;
import java.util.Set;

public class HyParViewLabTest_Backup0 extends Config{
	
	public static final int hyparviewPort = 30599;
	public static final String version = "V0.15 - IO                                                                                                                                                ";
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private HyParViewPLabP2PChannel channel;
	
	private Periodic aliveOperation;
	
	public long epoch;
	
	private PrintStream out; 
	private PrintStream overlay;
	
	private Thread controller;
	
	public short filiationRequestID = 1;
	
	public HyParViewLabTest_Backup0(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException, FileNotFoundException {
		System.setErr(System.out);
		
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new HyParViewPLabP2PChannel(this.myId);
		} catch(BindException e1) {
			System.err.println("Unable to bind socket: " + this.myId.toString());
			e1.printStackTrace();
			System.exit(1);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		//Use this for one hour period: 60 * 60 * 1000
		//Use this for 10 minute period: 60 * 10 * 1000
		//Use this for 5 minute period: 60 * 5 * 1000
		//Use this for 1 minute period: 60 * 1000
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		

		this.initializeFiles();
	}
	
	public HyParViewLabTest_Backup0(InetSocketAddress myId) throws UnknownHostException, FileNotFoundException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new HyParViewPLabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		
	
		this.initializeFiles();
	}
	
	private void initializeFiles() throws FileNotFoundException {
		this.out = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_alive.log"));
		this.overlay = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_overlay.log"));
		this.addToOutLog("ACTIV", myId.getHostName() + " " + myId.getAddress() + ":" + myId.getPort());
	}

	public synchronized void addToOutLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.out.println(line);
	}
	
	
	public synchronized  void addToOverlayLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		this.overlay.println(line);
	}
	
	public boolean debugOperation() {
		this.addToOutLog("ALIVE", channel.getOverlayInstance().getNodeIdentifier() + " " + epoch);
		this.out.flush();
		this.overlay.flush();
		return true;
	}
	
	private void bcastFiliationRequest() {
		addToOutLog("BCSND", "Filiation Request");
		ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 8);
		msg.putShort(filiationRequestID);
		NodeID.writeNodeIdToBuffer(msg, channel.getOverlayInstance().getNodeIdentifier());
		msg.putLong(epoch);
		msg.flip();
		channel.broadcast(new ByteBuffer[]{msg});
	}	
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		channel.connect(this.contactId);
		
		//Start the command receiver thread...0
		this.controller = new Controller(this);
		this.controller.start();
		
		try {
			while(true) {
				ByteBuffer bb = ByteBuffer.allocate(1024);
				channel.read(bb);
				bb.rewind();
				short op_code = bb.getShort();
				if(op_code == this.filiationRequestID) {
					NodeID sender = NodeID.readNodeIDFromBuffer(bb);
					long recv_epoch = bb.getLong();
					recv_epoch++;
					if(this.epoch < recv_epoch)
						this.epoch = recv_epoch;
					this.addToOutLog("BCRCV", "Filiation Request " + sender );
					this.printFiliation(sender);
				} else {
					this.addToOutLog("ERROR", "Unrecognized operation code received: " + op_code);
				}
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	private void printFiliation(NodeID sender) {
		this.addToOverlayLog("OVFIL", this.getCurrentFiliation(sender));
	}
	
	private String getCurrentFiliation(NodeID sender) {
		ImmortalHyParView ovlay = channel.getOverlayInstance();
		Set<NodeID> activeView = ovlay.getNeighbors().keySet(); 
		NodeID[] passiveView = ovlay.getPassivePeersPresentInfo();
		
		int passiveSize = 0;
		for(NodeID id: passiveView)
			if(id != null)
				passiveSize++;
		
		String line = this.epoch + "\n" + ovlay.getNodeIdentifier() + "\n" 
			+ activeView.size() + " " + passiveSize + "\n";
		for(NodeID id: activeView)
			line = line + id + "\n";
		for(NodeID id: passiveView)
			if(id != null) line = line + id + "\n";
		line = line + ovlay.connections().length + " " + ovlay.network().connections().length + " " + ovlay.pendingConns();
		if(ovlay.connections().length != (ovlay.network().connections().length - 1))
			for(Connection c: ovlay.network().connections())
				line = line + "\n    -- " + c.id + " " + c.getPeer();
		return line;
	}
	
	private Filiation retrieveLocalFiliation() {
		Filiation f = new Filiation();
		f.fillActiveView(channel.getOverlayInstance().getNeighbors().keySet());
		f.fillPassiveView(channel.getOverlayInstance().getPassivePeersPresentInfo().clone());
		return f;
	}

	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), HyParViewLabTest_Backup0.hyparviewPort);
		InetSocketAddress contact = new InetSocketAddress("192.16.125.11", HyParViewLabTest_Backup0.hyparviewPort);
	
		try {
			if(local.getAddress().equals(contact.getAddress())) {
				System.out.println("Primary node is on-line.");
				new HyParViewLabTest_Backup0(local).execute();
			} else {
				System.out.println("Node is on-line.");
				new HyParViewLabTest_Backup0(local, contact).execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	
	/**
	 * Helper Class Controller (extends a Thread) which goal e to receive instructions from a centralized server.
	 */
	protected class Controller extends Thread {
		
		private HyParViewLabTest_Backup0 instance;
		
		public Controller(HyParViewLabTest_Backup0 instance) {
			this.instance = instance;	
		}
		
		@Override
		public void run() {
			ServerSocket ss = null;
			try {
				ss = new ServerSocket();
				ss.bind(new InetSocketAddress(InetAddress.getLocalHost(), HyParViewLabTestControl.controlPort));
			} catch (IOException e) {
				e.printStackTrace();
				instance.addToOutLog("COMM", "Failed to create socket: " + e.getMessage());
				return;
			}
			
			Socket commander = null;
			ObjectInputStream commands = null;
			ObjectOutputStream replies = null;
			
			while(true) {
				try{
					commander = ss.accept();
					replies = new ObjectOutputStream(commander.getOutputStream());
					commands = new ObjectInputStream(commander.getInputStream());
					
					instance.addToOutLog("COMM", "Incomming command connection from: " + commander.getRemoteSocketAddress().toString());
					String s = (String) commands.readObject();
					if(!s.equals("06:9c:13:69:a1:fa:36:fa:45:1c:2e:9f:e6:99:a1:fa")) {
						instance.addToOutLog("COMM", "Received incorrect verification string: " + s + " Closing connection.");
						commands.close();
						commander.close();
						continue;
					} else {
						s = "OK " + HyParViewLabTest_Backup0.version;
						instance.addToOutLog("COMM", "Received verification string. Accepting commands");
						replies.writeObject(s);
						s = null;
						
					}
				} catch (NoSuchElementException e) {
					e.printStackTrace();
					instance.addToOutLog("COMM", "Error accepting incoming connection: " + e.getMessage());
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					continue;
				} catch (IOException e) {
					e.printStackTrace();
					instance.addToOutLog("COMM", "Error accepting incoming connection: " + e.getMessage());
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					continue;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					instance.addToOutLog("COMM", "Error accepting incoming connection: " + e.getMessage());
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					continue;
				}
				
				try {
					while(true) {
						this.instance.addToOutLog("COMM", "Waiting commands.");
						Object message = commands.readObject();
						if(message instanceof PingAlive) {
							replies.writeObject(message);
						} else if(message instanceof Request) {
							this.handleExperienceRequest((Request) message, replies);
						} else if(message instanceof Character) {
							this.handleBasicControlRequest(((Character) message).charValue(), replies);
						} else 
							this.instance.addToOutLog("COMM", "Unknown request received: " + message);		
					}
				} catch (Exception e) {
					this.instance.addToOutLog("COMM", "Command connection lost.");//do nothing and retry
					this.instance.addToOutLog("COMM", "Exception " + e.getClass().getName() + ": " + e.getMessage() );
				}
					
			}
		}
		
		private void handleExperienceRequest(Request message, ObjectOutputStream replies) throws IOException {
			this.instance.addToOutLog("COMM", "Received an Experience related request");
			switch (message.getOperationCode()) {
			case FiliationRequest.operCode:
					FiliationDataRecord fdr = new FiliationDataRecord(message.getExperinceID(), instance.channel.getGossipNodeID(), message.getNodeIndex());
					fdr.data = retrieveLocalFiliation();
					replies.writeObject(fdr);
					this.instance.addToOutLog("COMM", "Received a filliation request for experience: " + message.getExperinceID());
				break;
				default:
					this.instance.addToOutLog("COMM", "Unknown code request received: " + message.getOperationCode());	
			}
		}

		

		public void handleBasicControlRequest(char cmd, ObjectOutputStream replies) throws IOException {
			this.instance.addToOutLog("COMM", "Received command code: '" + cmd + "'");
			switch(cmd) {
				case 'k':
					this.instance.addToOutLog("COMM", "Received kill command.");
					EnvironmentControl.stopOperation();
					break;
				case 'r':
					this.instance.addToOutLog("COMM", "Received reset command.");
					EnvironmentControl.resetOperation(this.instance.channel.getOverlayInstance(), this.instance.channel.getTransport());
					break;
				case 'f':
					this.instance.addToOutLog("COMM", "Received filiation request command.");
					replies.writeObject(getCurrentFiliation(null));
					break;
				case 'l':
					this.instance.addToOutLog("COMM", "Received hyparview.cache list command.");
					
					break;
				case 'b':
					this.instance.addToOutLog("COMM", "Received request to issue a bcast on the filiation.");
					this.instance.bcastFiliationRequest();
					break;
				default:
					this.instance.addToOutLog("COMM", "Unrecognized command.");
					break;
			}
		}
	}
	
}

package gsd.appl;

import gsd.appl.utils.DateTimeRepresentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

public class HyParViewLabTestControl_Backup0 {

	public static int controlPort = 30500;
	
	private static Logger log = Logger.getLogger(HyParViewLabTestControl_Backup0.class);
	private ArrayList<Slave> slaves;
	private CommandLine cmdl;
	
	private final boolean debugON = true;
	public void debug(Object sommuner, String s) {
		if(debugON) {
			log.debug(DateTimeRepresentation.timeStamp() + " " + sommuner.getClass().getName() + ": " + s);
		}
	}
	
	public HyParViewLabTestControl_Backup0() {
		this.slaves = new ArrayList<Slave>();
	}
	
	private void initialize(String filename) throws FileNotFoundException {
		Scanner hosts = new Scanner(new File(filename));
		while(hosts.hasNextLine()) {
			this.slaves.add(new Slave(this, hosts.nextLine()));
		}
		System.err.println("Loaded " + this.slaves.size() + " hosts.");
	}
	
	private void startCommandLine() {
		this.cmdl = new CommandLine(this);
		this.cmdl.start();
	}
	
	private void startConnectionsManager() {
		
		while(true) {
			synchronized (this.slaves) {
				boolean alldone = true;
				for(int i = 0; i < this.slaves.size(); i++) {
					//this.cmdl.output("Attemping connection to: " + this.slaves.get(i).host);
					if(!this.slaves.get(i).isOn())
						alldone = this.slaves.get(i).connect() && alldone;
				}
				
				if(alldone) {
					try { this.slaves.wait(); } catch (InterruptedException e) {e.printStackTrace();}
				} 
			}
			try { Thread.sleep(10*1000); } catch (InterruptedException e) { e.printStackTrace(); }
		}
		
	}
	
	public synchronized void addNodeToList(String host) {
		synchronized (this.slaves)  {
			this.slaves.add(new Slave(this, host));
			this.slaves.notify();
		}	
	}
	
	private synchronized void countActiveNodes() {
		int count = 0;
		for(Slave slave: this.slaves) {
			if(slave.isOn) 
				count++;
		}
		this.cmdl.output("Number of active nodes: " + count);
	}
	
	private synchronized void listNodes() {
		String answer = "All nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			answer = answer + i + " " + slaves.get(i).host + " " + (slaves.get(i).isOn() ? "active":"inactive") + "\n";
		}
		this.cmdl.output(answer);
	}
	
	private synchronized void listActiveNodes() {
		String answer = "Active nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			if(slaves.get(i).isOn())
				answer = answer + i + " " + slaves.get(i).host + "\n";
		}
		this.cmdl.output(answer);
	}
	
	private synchronized void listInactiveNodes() {
		String answer = "Inactive nodes:\n";
		for(int i = 0; i < slaves.size(); i++) {
			if(!slaves.get(i).isOn())
			answer = answer + i + " " + slaves.get(i).host + "\n";
		}
		this.cmdl.output(answer);
	}

	private synchronized void kill(int index) {
		if(index < 0 || index >= this.slaves.size()) {
			this.cmdl.output("Error: invalid index for command kill");
			return;
		}
		
		if(!this.slaves.get(index).isOn()) {
			this.cmdl.output("Error: targeted host is inactive. Command kill not issued.");
			return;
		}
		
		
		synchronized (this.slaves.get(index).writer.queue) {
			this.slaves.get(index).writer.queue.add('k');
			this.slaves.get(index).writer.queue.notify();
		}	
	}
	
	private synchronized void filiationBcast(int index) {
		this.cmdl.output("Trying to issue bcast command to node " + index);
		
		if(index < 0 || index >= this.slaves.size()) {
			this.cmdl.output("Error: invalid index for command bcast");
			return;
		}
		
		if(!this.slaves.get(index).isOn()) {
			this.cmdl.output("Error: targeted host is inactive. Command bcast not issued.");
			return;
		}
		
		this.cmdl.output("Adding to queue...");
		synchronized (this.slaves.get(index).writer.queue) {
			this.slaves.get(index).writer.queue.add('b');
			this.cmdl.output("Added");
			this.slaves.get(index).writer.queue.notify();
			this.cmdl.output("Notified...");
		}		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		HyParViewLabTestControl_Backup0 ctl = new HyParViewLabTestControl_Backup0();
		try { ctl.initialize(args[0]); } catch (FileNotFoundException e) { System.err.println("Host list not found."); System.exit(1); }
		ctl.startCommandLine();
		ctl.startConnectionsManager();
	}
	
	protected class CommandLine extends Thread{
		
		private HyParViewLabTestControl_Backup0 instance;
		private String prompt;
		
		public CommandLine(HyParViewLabTestControl_Backup0 instance) {
			this.instance = instance;
			prompt = "Cmd:> ";
		}
		
		@Override
		public void run() {
			Scanner sc = new Scanner(System.in);
			String cmd = null;
			String t = null;
			while(true) {
				cmd = sc.nextLine();
				if(cmd.trim().equalsIgnoreCase("")) continue;
				StringTokenizer st = new StringTokenizer(cmd," ");
				t = st.nextToken();
				if(t.equalsIgnoreCase("list")) {
					if(st.hasMoreTokens()) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("active")) {
							this.instance.listActiveNodes();
						} else if(t.equalsIgnoreCase("inactive")) {
							this.instance.listInactiveNodes();
						} else if(t.equalsIgnoreCase("all")) {
							this.instance.listNodes();
						} else {
							this.output("Unrecognized option. Usage: 'list all|active|inactive'");
							continue;
						}
					} else {
						this.instance.listNodes();
					}
				} else if(t.equalsIgnoreCase("add")) {
					if(st.countTokens() == 1 || st.countTokens() == 2) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("-f")) {
							Scanner scan;
							try {
								scan = new Scanner(new File(st.nextToken()));
							} catch (FileNotFoundException e) {
								this.instance.cmdl.output("File not found.");
								continue;
							}
							while(scan.hasNextLine()) {
								this.instance.addNodeToList(scan.nextLine());
							}
						} else {
							this.instance.addNodeToList(t);
						}
					} else {
						this.output("Expecting Token. Usage: 'add node|-f filename'");
						continue;
					}
				} else if(t.equalsIgnoreCase("count")) { 
					this.instance.countActiveNodes();
				} else if(t.equalsIgnoreCase("kill")) {
					if(st.hasMoreTokens()) {
						this.instance.kill(Integer.parseInt(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'kill node_index'");
						continue;
					}
				} else if(t.equalsIgnoreCase("bcast")) {
					if(st.hasMoreTokens()) {
						this.instance.filiationBcast(Integer.parseInt(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'bcast node_index'");
						continue;
					}
				} else {
					this.output("Command not recognized.");
					continue;
				}
				printprompt();
			}
			
		}
		
		public synchronized void output(String s) {
			System.out.println();
			System.out.println(s);
			System.out.print(prompt);
		}
		
		public synchronized void printprompt() {
			System.out.print(prompt);
		}
		
	}
	
	protected class Slave {
	
		private HyParViewLabTestControl_Backup0 instance;
		private Boolean isOn;
		private String host;
		private Socket sock;
		private PrintStream out;
		public Writer writer;
		public Thread reader;
		
		public Slave(HyParViewLabTestControl_Backup0 instance, String host) {
			this.instance = instance;
			this.host = host;
			this.isOn = false;	
			
		}
		
		public boolean isOn() { 
			synchronized (isOn) {
				return isOn;
			} 
		}

		public boolean connect() {
			try {
				debug(this, "Attempting connection to: " + host);
				this.sock = new Socket();
				this.sock.connect(new InetSocketAddress(this.host, HyParViewLabTestControl_Backup0.controlPort));
				debug(this, host + ": connection created.");
				this.out = new PrintStream(this.sock.getOutputStream());
				this.out.println("06:9c:13:69:a1:fa:36:fa:45:1c:2e:9f:e6:99:a1:fa");
				debug(this, host + ": Sent verification key.");
				String r = new Scanner(this.sock.getInputStream()).nextLine();
				if(! r.equals(new String("OK"))) {
					instance.cmdl.output("host " + this.host + " provided invalid reply: " + r);
					throw new IOException("Invalida reply...");
				}
				debug(this, host + ": OK verified...");
				r = null;
				synchronized (isOn) {
					isOn = true;
				}
				this.writer = new Writer(instance, isOn, host, out);
				this.writer.start();
				this.reader = new Thread() {
					@Override
					public void run() {
						while(true) {
							try{
								if(sock.getInputStream().read() == -1) {
									synchronized (isOn) {
										isOn = false;
									}
									writer.interrupt();
									synchronized (instance.slaves) {	
										instance.slaves.notify(); 
									}	
									try {sock.close();} catch (IOException e1) {;}
									return;
								}
							} catch (IOException e) {
								synchronized (isOn) {
									isOn = false;
								}
								writer.interrupt();
								synchronized (instance.slaves) {	
									instance.slaves.notify(); 
								}	
								try {sock.close();} catch (IOException e1) {;}
								return;
							}
						}
					}
				};
				this.reader.start();
				instance.cmdl.output("New connection to " + host +" established.");
				return true;
			} catch (IOException e) {
				try { this.sock.close();} catch (IOException e1) {	} //nothing to do in case of exception
				//instance.cmdl.output("Failed connection to: " + host +": " + e.getMessage());
				return false;
			}
		}
	}

	public class Writer extends Thread {
		
		public ArrayList<Character> queue;
		public Boolean isOn;
		public HyParViewLabTestControl_Backup0 instance;
		public String host;
		public PrintStream out;
		
		public Writer(HyParViewLabTestControl_Backup0 instance, Boolean isOn, String host, PrintStream out) {
			this.queue = new ArrayList<Character>();
			this.isOn = isOn;
			this.instance = instance;
			this.host = host;
			this.out = out;
		}
		
		@Override
		public void run() {
			Character c = null;
			
			boolean run = true;
			synchronized (isOn) {
				run = isOn.booleanValue();
			}		
			
			while(run) {
				synchronized (this.queue) {
					if(this.queue.size() > 0)
						c = this.queue.remove(0);
					else
						c = null;
				
				
					if(c == null) {
						debug(this, host + " is waiting...");
						try { this.queue.wait(); } catch (InterruptedException e) {	}
						debug(this, host + " is awake...");
					} else {
						debug(this, "Issuing command: '" + c.toString() + "'");
						out.println(c.toString());
					}
				}
				
				synchronized (isOn) {
					run = isOn.booleanValue();
				}
			}
		}
	}
	
}

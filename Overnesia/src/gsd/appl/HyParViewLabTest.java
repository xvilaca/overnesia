package gsd.appl;

import gsd.api.ExternalControlInterface;
import gsd.api.HyParViewChannel;
import gsd.api.ConstValues;
import gsd.api.PropertiesFactory;
import gsd.api.TestResults;
import gsd.appl.control.components.EnvironmentControl;
import gsd.appl.control.components.messages.PingAlive;
import gsd.appl.control.experiences.data.DataBroadcastRequest;
import gsd.appl.control.experiences.data.DataHelper;
import gsd.appl.control.experiences.data.FileBroadcastRequest;
import gsd.appl.control.experiences.data.Filiation;
import gsd.appl.control.experiences.data.FiliationDataRecord;
import gsd.appl.control.experiences.data.FiliationRequest;
import gsd.appl.control.experiences.data.Request;
import gsd.appl.control.experiences.data.StartBroadcastExperienceReply;
import gsd.appl.control.experiences.data.StartBroadcastExperienceRequest;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.impl.Config;
import gsd.impl.Connection;
import gsd.impl.Dissemination;
import gsd.impl.PackDissemination;
import gsd.impl.Periodic;
import gsd.impl.TestCollect;
import gsd.impl.components.GossipBasedUpdater;
import gsd.impl.components.FileDissemination.FileDisseminationProtocol;
import gsd.impl.hyparview.ImmortalHyParView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

public class HyParViewLabTest extends Config{
	
	boolean connected = true;
	private TestResults test_results;
	
	private PropertiesFactory prop;
	public static Logger log = Logger.getLogger(HyParViewLabTest.class);
	public static final int hyparviewPort = 30599;
	public static final double version_numb = 0.955;
	public static final String version = "LEX";
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private HyParViewChannel channel;
	
	private Periodic aliveOperation;
	
	private Thread controller;
	//public ConcurrentHashMap<Integer,Integer> redundancyCounter = new ConcurrentHashMap<Integer, Integer>();
	
	public HyParViewLabTest(InetSocketAddress contact, PropertiesFactory p) throws UnknownHostException, FileNotFoundException {
		System.setErr(System.out);
		
		this.prop = p;
		int port = 2000 + (int)(Math.random() * 100);
		System.out.println("Random port: " + port);
		
		InetSocketAddress myId = new InetSocketAddress(InetAddress.getLocalHost(), port);
		
		this.contactId = contact;
		this.myId = myId;
		test_results = new TestResults(new NodeID(this.myId)); 
		
		System.out.println("Contact: " + contact.toString());
		System.out.println("My Id: " + myId.toString());
	
		try {
			this.channel = new HyParViewChannel(this.myId, test_results,prop); //DQ: add ConcurrentHashMap
			//this.channel.getOverlayInstance().setProperties(this.prop);
		} catch(BindException e1) {
			System.err.println("Unable to bind socket: " + this.myId.toString());
			e1.printStackTrace();
			System.exit(1);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		//Use this for one hour period: 60 * 60 * 1000
		//Use this for 10 minute period: 60 * 10 * 1000
		//Use this for 5 minute period: 60 * 5 * 1000
		//Use this for 1 minute period: 60 * 1000
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(),  60 * 60 * 1000, 0) {
			public void run() {
				HyParViewLabTest.addToOutLog("ALIVE", channel.getOverlayInstance().getNodeIdentifier().toString());
			}
		};
		this.aliveOperation.start();

		HyParViewLabTest.addToOutLog("ACTIV", myId.getHostName() + " " + myId.getAddress() + ":" + myId.getPort());
	}


	public static void addToOutLog(String oper, String msg) {
		HyParViewLabTest.log.info(oper + " " + DateTimeRepresentation.timeStamp() + " " + msg);
		
	}
	
	public void execute() {
		channel.connect(this.contactId);
		
		//Start the command receiver thread...0
		this.controller = new Controller(this);
		try {
			this.controller.start();
		} catch (Exception e) {
			HyParViewLabTest.addToOutLog("COMM", "Exited of the main operation loop for processing console commands: " + e.getClass().getName());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace();
			System.exit(1);
		}
		channel.setExternalControlInterface((ExternalControlInterface) this.controller);
		
		try {
			while(true) {
				ByteBuffer bb = ByteBuffer.allocate(1024);
				channel.read(bb);
				bb.rewind();
				HyParViewLabTest.addToOutLog("OUTPUT", "DATA:" + new String(bb.array()));
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	private String getCurrentGossipUpdaterStatus() {
		GossipBasedUpdater updater = channel.getUpdater();
		return channel.getGossipNodeID() + "\n" 
			+ updater.getInternalStatus();
	}
	
	private String getFileBroadCastSessionStatus(UUID sessionID) {
		FileDisseminationProtocol protocol = channel.getFileDisseminationProtocol();
		return protocol.getSessionStatus(sessionID);
	}
	
	private String getCurrentFiliation() {
		try {
			ImmortalHyParView ovlay = channel.getOverlayInstance();
			HashMap<NodeID, Connection> activeSet = ovlay.getNeighbors();
			Set<NodeID> activeView = activeSet.keySet(); 
			NodeID[] passiveView = ovlay.getPassivePeersPresentInfo();
			
			int passiveSize = 0;
			for(NodeID id: passiveView)
				if(id != null)
					passiveSize++;
			
			String line = ovlay.getNodeIdentifier() + "\n" 
				+ activeView.size() + " " + passiveSize + "\n";
			for(NodeID id: activeView)
				line = line + id + " queued msg to send: " + activeSet.get(id).queueSize() + "\n--\n";
			for(NodeID id: passiveView)
				if(id != null) line = line + id + "\n";
			line = line + ovlay.connections().length + " " + ovlay.network().connections().length + " " + ovlay.pendingConns();
			if(ovlay.connections().length != (ovlay.network().connections().length - 1))
				for(Connection c: ovlay.network().connections())
					line = line + "\n    -- " + c.id + " " + c.getPeer();
			return line;
		} catch (Exception e) {
			return "Unable to provide an answer due to a " + e.getClass().getName() + ": " + e.getMessage();
		}
	}
	
	private Filiation retrieveLocalFiliation() {
		Filiation f = new Filiation();
		f.fillActiveView(channel.getOverlayInstance().getNeighbors().keySet());
		f.fillPassiveView(channel.getOverlayInstance().getPassivePeersPresentInfo().clone());
		return f;
	}
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) {
	
		int expN = 0;
		try {
			expN = ConstValues.setExpProperties();
		} catch (FileNotFoundException e1) {
			System.out.println("File exp not found, please contact Daniel for more info");//TODO haha change this
			e1.printStackTrace();
			return;
		}
		PropertiesFactory prop = new PropertiesFactory(expN);
		try {
		File dir = new File(".");
		for (File f: dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				char [] namae = name.toCharArray();
				return namae.length >= 3 && namae[namae.length-1] == 'p' && namae[namae.length-2] == 'm' && namae[namae.length-3] == 't';
			}
		}) ) {
			f.delete();
		} } catch (Exception e) {
			System.err.println("Failed cleanup process (1)");
			e.printStackTrace(System.err);
		}
		
		try {
			File f = new File("hyparview.sh");
			if(f.exists()) f.delete();
			f = new File("Helper.jar");
			if(f.exists()) f.delete();
			f = new File("run.sh");
			if(f.exists()) f.delete();
			f = new File("overnesia.sh");
			if(f.exists()) f.delete();
			f = new File("V1");
			if(f.exists()) f.delete();
		}  catch (Exception e) {
			System.err.println("Failed cleanup process (2)");
			e.printStackTrace(System.err);
		}
		
		
		String contactServer = prop.getProperty("serverIP");
		InetSocketAddress contact = new InetSocketAddress(contactServer, prop.getIntProperty("primPort"));
		
		try {
				System.out.println("Node is on-line.");
				new HyParViewLabTest(contact, prop).execute();
		
		} catch (Exception e) {
			System.err.println("Unhandled exception: " + e.getClass().getName());
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	
	/**
	 * Helper Class Controller (extends a Thread) which goal e to receive instructions from a centralized server.
	 */
	protected class Controller extends Thread implements ExternalControlInterface {
		
		private HyParViewLabTest instance;
		private Socket commander = null;
		private ObjectInputStream commands = null;
		private ObjectOutputStream replies = null;
		
		public Controller(HyParViewLabTest instance) {
			this.instance = instance;	
			this.commander = null;
			this.commands = null;
			this.replies = null;
		}
		
		public ServerSocket setListenSocketUp() {
			try {
				ServerSocket ss = new ServerSocket();
				int slave1Port = myId.getPort()+1000;//listen port
				//ss.bind(new InetSocketAddress("127.0.0.1",slave1Port));
				ss.bind(new InetSocketAddress(InetAddress.getLocalHost(),slave1Port));
				//ss.bind(new InetSocketAddress(InetAddress.getLocalHost(), HyParViewLabTestControl.controlPort));
				return ss;
			} catch (IOException e) {
				System.err.println("Cannot bind to command and control listenning socket: " + e.getMessage());
				e.printStackTrace(System.err);
				HyParViewLabTest.addToOutLog("COMM", "Failed to create socket: " + e.getMessage());
				System.exit(1);
				return null;
			}
		}
		
		@Override
		public void run() {
			ServerSocket ss = setListenSocketUp();
			
			while(true) {
				try{
					if(ss == null || ss.isClosed())
						ss = setListenSocketUp();
					System.out.println("Listening in port:"+ss.getLocalPort());
					
					this.commander = ss.accept();
					this.replies = new ObjectOutputStream(commander.getOutputStream());
					this.commands = new ObjectInputStream(commander.getInputStream());
					 
					HyParViewLabTest.addToOutLog("COMM", "Incomming command connection from: " + commander.getRemoteSocketAddress().toString());
					String s = (String) commands.readObject();
					if(!s.equals("06:9c:13:69:a1:fa:36:fa:45:1c:2e:9f:e6:99:a1:fa")) {
						HyParViewLabTest.addToOutLog("COMM", "Received incorrect verification string: " + s + " Closing connection.");
						commands.close();
						commander.close();
						continue;
					} else {
						s = "OK V" + Double.toString(HyParViewLabTest.version_numb) + " - " + HyParViewLabTest.version;
						HyParViewLabTest.addToOutLog("COMM", "Received verification string. Accepting commands");
						replies.writeObject(s);
						s = null;						
					}
				} catch (NoSuchElementException e) {
					System.err.println("Error on command and control connection: " + e.getMessage());
					HyParViewLabTest.addToOutLog("COMM", "Error on incoming connection: " + e.getMessage());
					try { ss.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); ss = null;}
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					continue;
				} catch (IOException e) {
					System.err.println("Error on command and control connection: " + e.getMessage());
					HyParViewLabTest.addToOutLog("COMM", "Error accepting incoming connection: " + e.getMessage());
					try { ss.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); ss = null;}
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					e.printStackTrace();
					continue;
				} catch (ClassNotFoundException e) {
					System.err.println("Error on command and control connection: " + e.getMessage());
					HyParViewLabTest.addToOutLog("COMM", "Error accepting incoming connection: " + e.getMessage());
					try { ss.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); ss = null;}
					try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
					continue;
				}
				connected = true;
				try {
					while(connected) {
						System.out.println("waiting for a message in port: ");
						Object message = commands.readObject();
						System.out.println("receive this message: "+ message.toString());
						if(message instanceof PingAlive) {
							this.sendMessage(message);
						} else if(message instanceof Request) {
							this.handleExperienceRequest((Request) message);
						} else if(message instanceof Character) {
							this.handleBasicControlRequest(((Character) message).charValue());
						} else if(message instanceof String) {
							this.handleControlRequest((String) message);
						} else
							HyParViewLabTest.addToOutLog("COMM", "Unknown request received: " + message);		
					}
				} catch (Exception e) {
					e.printStackTrace();
					HyParViewLabTest.addToOutLog("COMM", "Command connection lost.");//do nothing and retry
					HyParViewLabTest.addToOutLog("COMM", "Exception " + e.getClass().getName() + ": " + e.getMessage() );
					System.err.println(DateTimeRepresentation.timeStamp() + " Connection with command and control lost.");
					//e.printStackTrace(System.err);
					try {
						this.commander.close();
					} catch (IOException e1) {
						e.printStackTrace();
						HyParViewLabTest.addToOutLog("COMM", "Could not close control socket.");//do nothing and retry
						HyParViewLabTest.addToOutLog("COMM", "Exception " + e1.getClass().getName() + ": " + e1.getMessage() );
						System.err.println(DateTimeRepresentation.timeStamp() + " Could not close the command and control socket (nullfying)");
						this.commander = null;
						//e1.printStackTrace(System.err);
					}
					finally{
						//System.exit(1);
					}
				}
			}
		}
		
		
		private void handleFreeRiderRequest() throws IOException {
			channel.getOverlayInstance().setFreeRider(true);
			//this.commands.close();
			//this.replies.close();
			//this.commander.close();
		}
		
		private void handleDisseminationRequest() throws IOException,ClassNotFoundException{
			
			//System.out.println("handleDisseminationRequestServer");
			//TODO organize/divide this Dissemination Class into protocol and pack
			PackDissemination packDiss = new PackDissemination(this.commander.getLocalPort()); 
			packDiss.PackReceiveProtocol(this.commands, this.replies);
			this.instance.channel.getOverlayInstance().handleServerDissem(packDiss);
			//this.commands.close();
			//this.replies.close();
			//this.commander.close();
		}
		
		
		private void handleRedundancyRequest() throws IOException {
			new TestCollect(this.commands, this.replies, test_results).collectData();
			//this.commands.close();
			//this.replies.close();
			//this.commander.close();
		}
			
		
		private void handleControlRequest(String message) throws IOException {
			
			//System.out.println("handleControlRequestServer");
			HyParViewLabTest.addToOutLog("COMM", "Received a control request");
			StringTokenizer st = new StringTokenizer(message, " ");
			String cmd = st.nextToken();
			if(cmd.equalsIgnoreCase("checkfile") && st.hasMoreTokens()) {
				File f = new File(st.nextToken());
				this.sendMessage(this.instance.channel.getGossipNodeID() + " " + f.exists() + (f.exists() ? " " + f.getName() + " " + f.length(): ""));
			} else if(cmd.equals("checkfbc") && st.hasMoreTokens()) {
				UUID bcast_session_id = UUID.fromString(st.nextToken());
				this.sendMessage(this.instance.channel.getGossipNodeID() + " " + getFileBroadCastSessionStatus(bcast_session_id));
			} else if(cmd.equalsIgnoreCase("delete") && st.hasMoreTokens()) {
				File f = new File(st.nextToken());
				if(f.exists()) {
					f.delete();
					this.sendMessage(this.instance.channel.getGossipNodeID() + " deleted: " + f.getName() + " " + f.length());
				} else {
					this.sendMessage(this.instance.channel.getGossipNodeID() + " no such file.");
				}
			} else {
				this.sendMessage(this.instance.channel.getGossipNodeID() + " " + "Unknown request received: '" + message + "'" );
			}
		}
		
		

		private void handleExperienceRequest(Request message) throws IOException {
			
			//System.out.println("handleControlRequestServer");
			HyParViewLabTest.addToOutLog("COMM", "Received an Experience related request");
			switch (message.getOperationCode()) {
			case FileBroadcastRequest.operCode:
				System.err.println("Initiate broadcast of file: " + ((FileBroadcastRequest) message).toString());
				this.instance.channel.filebroadcast(((FileBroadcastRequest) message).toString());
				break;
			case DataBroadcastRequest.operCode:
				System.err.println("DATA PIECE IS: " + new String(((DataBroadcastRequest) message).getData().array()) );
				this.instance.channel.broadcast(DataHelper.encode((DataBroadcastRequest) message));
				break;
			case FiliationRequest.operCode:
				FiliationDataRecord fdr = new FiliationDataRecord(message.getExperinceID(), instance.channel.getGossipNodeID(), message.getNodeIndex());
				fdr.data = retrieveLocalFiliation();
				this.sendMessage(fdr);
				HyParViewLabTest.addToOutLog("COMM", "Received a filliation request for experience: " + message.getExperinceID());
				break;	
			case StartBroadcastExperienceRequest.operCode:
				StartBroadcastExperienceRequest sber = (StartBroadcastExperienceRequest) message;
				this.instance.channel.initiateBroadcastExperience(sber);
				this.sendMessage(new StartBroadcastExperienceReply(sber.getExperinceID(), this.instance.channel.getGossipNodeID(), sber.getNodeIndex()));
				break;
				default:
					HyParViewLabTest.addToOutLog("COMM", "Unknown code request received: " + message.getOperationCode());	
			}
		}

		

		public void handleBasicControlRequest(char cmd) throws IOException, ClassNotFoundException {
			HyParViewLabTest.addToOutLog("COMM", "Received command code: '" + cmd + "'");
			switch(cmd) {
				case 'd':
					HyParViewLabTest.addToOutLog("COMM", "Received gossip command.");
					handleDisseminationRequest();
					//connected = false;
					break;
				case 'x':
					handleFreeRiderRequest();
					//connected = false;
					break;
				case 'k':
					HyParViewLabTest.addToOutLog("COMM", "Received kill command.");
					EnvironmentControl.stopOperation();
					break;
				case 'r':
					HyParViewLabTest.addToOutLog("COMM", "Received redundancy command.");
					handleRedundancyRequest();
					//connected = false;
					break;
				case 'c':
					HyParViewLabTest.addToOutLog("COMM", "Received crash-recover command.");
					EnvironmentControl.crashRecover(this.instance.channel.getOverlayInstance(), this.instance.channel.getTransport());
					break;
				case 'f':
					HyParViewLabTest.addToOutLog("COMM", "Received filiation request command.");
					this.sendMessage(getCurrentFiliation());
					break;
				case 'g':
					HyParViewLabTest.addToOutLog("COMM", "Received gossip based updater status request.");
					this.sendMessage(getCurrentGossipUpdaterStatus());
					break;
				case 'l':
					HyParViewLabTest.addToOutLog("COMM", "Received hyparview.cache list command.");
					String s = "";
					Scanner sc = new Scanner(new File(ImmortalHyParView.cache_filename));
					while(sc.hasNextLine()) {
						s = s + sc.nextLine() + "\n";
					}
					this.sendMessage(s);
					sc.close();
					s = null;
					break;
				case 'q':
					System.out.println("<<<<Closing command line with server>>>>");
					connected = false;
					try{
						this.commander.close();
					}
					catch(IOException e){
						e.printStackTrace();
					}
					System.exit(1);
					break;
				default:
					HyParViewLabTest.addToOutLog("COMM", "Unrecognized command.");
					break;
			}
		}

		
		public synchronized void sendMessage(Object m) throws IOException {
			if(!(m instanceof PingAlive))
				HyParViewLabTest.addToOutLog("COMM", "Sending message (" + m.getClass().getName() + ") to control.");
			
			try{ 
				this.replies.writeObject(m); 
			} catch (SocketException e) {
				this.commander.close();
				this.commander = null;
				this.commands.close();
				this.commands = null;
				HyParViewLabTest.addToOutLog("COMM", "Error while sending message to control (" + m.getClass().getName() + "). " + e.getClass().getName() + ": " + e.getMessage());
				System.err.println("Could not send message to command and control (Msg type: " + m.getClass().getName() +").");
			} catch (Exception e) {
				this.commander.close();
				this.commander = null;
				this.commands.close();
				this.commands = null;
				HyParViewLabTest.addToOutLog("COMM", "Error while sending message to control (" + m.getClass().getName() + "). " + e.getClass().getName() + ": " + e.getMessage());
				System.err.println("Could not send message to command and control (Msg type: " + m.getClass().getName() +").");
			}
			if(!(m instanceof PingAlive))
				HyParViewLabTest.addToOutLog("COMM", "Message sent.");
		}
	}
}

package gsd.appl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import gsd.impl.CryptoPuzzle;

public class TestCrypto {
	private static final int NUM_ITER = 10;
	private static final int MAX_WINDOW_SIZE = 30;
	private static final int MAX_CHAL_SIZE = 1024;
	
	private static String generateTableAsString(){
		StringBuilder sb = new StringBuilder();
	    for(int i = 0; i < MAX_WINDOW_SIZE; i++){			
			double acum = 0; 
			for(int j = 0; j < NUM_ITER; j++){
				CryptoPuzzle.setAvgHashTime(1024);
				System.out.println("Iter: " + i + " - " + j);
				CryptoPuzzle puzzle = CryptoPuzzle.generatePuzzle(MAX_CHAL_SIZE, i+1);
				System.out.println("Generated puzzle");
				acum += puzzle.emulate();
				System.out.println("Solved it");
			}
			sb.append(String.format("%d,%d%n",i+1, 
					(long)Math.ceil(acum/NUM_ITER)));
	    }		
		
		return sb.toString();
	}
	
	public static void generateTable(){
		String table = generateTableAsString();
		
		PrintWriter writer = null;
		File file = null;
		try{
			file = new File("/home/istple_fastrank/temp/");
			if(!file.exists()){
				file.mkdirs();
			}
		    writer = new PrintWriter("/home/istple_fastrank/temp/crypto.txt");
		    writer.write(table);
			writer.flush();
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		finally{
			if(writer != null){
				writer.close();
			}
		}
	}
	
	public static void main(String [] args){
		generateTable();
	}
	
	public static int getClosestNumBits(long time) throws FileNotFoundException{
		time = time*1000000L;
		Scanner in = new Scanner(new FileReader("crypto.txt"));
		in.useDelimiter(",|\n");
		
		long [] table = new long[MAX_WINDOW_SIZE];
		
		while(in.hasNext()){
			int i = in.nextInt() - 1;
			long val = in.nextLong();
			table[i] = val;
		}
		
		in.close();
		
		for(int i = 0; i < MAX_WINDOW_SIZE; i++){
			long dif = table[i] - time;
			if(dif > 0)
				return i;
		}
		
		return -1;
	}
}

package gsd.appl;

import gsd.api.ConstValues;
import gsd.api.PropertiesFactory;
import gsd.api.TestResults;
import gsd.appl.ServerHyParView.DissemOper;
import gsd.appl.control.components.messages.PingAlive;
import gsd.common.NodeID;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;

public class ClientHandler extends Thread{

	private Socket client;
	private ObjectOutputStream reply = null;
	private ObjectInputStream listen = null;
	private ServerHyParView server;
	public PropertiesFactory prop ;
	
	
	public ClientHandler(Socket VIPclient, ServerHyParView server, PropertiesFactory p) {
		this.server = server;
		this.client = VIPclient;
		prop = p;
		
		System.out.println("New connection accepted with" + client.getInetAddress());
		try {
			this.reply = new ObjectOutputStream(client.getOutputStream());
			this.listen = new ObjectInputStream(client.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void run(){
		NodeID slaveNode = null;
		try{
			System.out.println("Nanny ready:");
			String replyAck = null;
			String code = (String) listen.readObject();
			System.out.println("Code: "+ code);
			if(!code.equals(ConstValues.accessCode)) {
				System.out.println("COMM" + "Received incorrect verification string: " + code + " Closing connection.");
				listen.close();
				reply.close();
				client.close();
			} else {
				String port = (String) listen.readObject();
				//System.out.println("REmote: "+client.getRemoteSocketAddress());
				//System.out.println("Inetadd: "+client.getInetAddress());
				
				slaveNode = new NodeID(new InetSocketAddress(client.getInetAddress(),Integer.valueOf(port)));
				server.addSlave2SlaveList(slaveNode);
				System.out.println("New Slave added to the list: " + slaveNode.toString());
				replyAck = "OK"; // + Double.toString(HyParViewLabTest.version_numb) + " - " + HyParViewLabTest.version;
				System.out.println("COMM" +"Received verification string. Accepting commands");
				
				reply.writeObject(replyAck);
				replyAck = null;
			}
		} catch (NoSuchElementException e) {
			System.err.println("Error on command and control connection: " + e.getMessage());
			try { client.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); client = null;}
			try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
		} catch (IOException e) {
			System.err.println("Error on command and control connection: " + e.getMessage());
			try { client.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); client = null;}
			try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
		} catch (ClassNotFoundException e) {
			System.err.println("Error on command and control connection: " + e.getMessage());
			try { client.close(); } catch (IOException e2) { System.err.println("Could not close the server side socket."); client = null;}
			try { Thread.sleep(10*1000); } catch (InterruptedException e1) {	} //10 seconds back off...
		}
		//VersionA sequencial messages change, see versionB below
		Object message;
		try {
			message = listen.readObject();
			if(message instanceof String){
				this.handleControlRequest((String) message);
				String ack =(String) listen.readObject();
				if(ack.equals("ack")){
					System.out.println("Disconnect:" + client.getInetAddress().toString());
					listen.close();
					reply.close();
					client.close();
				}
			}else
				System.out.println("Comm protocol error: Wrong message in: join network");
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			listen.close();
			reply.close();
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	private void handleControlRequest(String message) throws IOException {
		System.out.println("COMM"+ "Received a control request");
		StringTokenizer st = new StringTokenizer(message, " ");
		String cmd = st.nextToken();
		int lSize = server.numberNodes();
		
		int n = prop.getIntProperty("nNewNeighbors");
		System.out.println("n:"+ n);
		System.out.println("list size: "+ lSize);
		System.out.println("Total nodes: "+ server.numberAllNodes());
		if(cmd.equalsIgnoreCase("joinnet")){
			if(lSize==1)
				this.reply.writeObject('f');
			if(lSize < n)
				n = lSize;
			sendList(n); //send a list of strings. Ex "193.231.13.13:32"
			this.reply.writeObject('f');
		} else {
			System.out.println("Fail to join");
		}
	}
	
	
	/**
	 * Generate a list size n with random listSlaves's index. Other option is to shuffle listSlaves :)
	 * // Implementing Fisher–Yates shuffle
       static void shuffleArray(int[] ar){
       // If running on Java 6 or older, use `new Random()` on RHS here
       	  Random rnd = ThreadLocalRandom.current();
       		for (int i = ar.length - 1; i > 0; i--){
  				int index = rnd.nextInt(i + 1);
  				// Simple swap
  				int a = ar[index];
  				ar[index] = ar[i];
  				ar[i] = a;
		  }
		   }
	*/
	private void sendList(int n) {		
		int size = server.numberNodes();
		n = (n > size)? size : n;
		List<NodeID> nodes = new ArrayList<NodeID>();
		synchronized(server.getListControlSlaves()){
			nodes.addAll(server.getListSlaves());
		}
		ServerHyParView.shuffle(nodes);
		
		for(int i = 0; i < n; i++){
			try {
				NodeID node = nodes.get(i);
				NodeID send = new NodeID(node);
				send.clean();
				System.out.println(send);
				this.reply.writeObject(send);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	

	public void handleBasicControlRequest(char cmd) throws IOException {
		//HyParViewLabTest.addToOutLog("COMM", "Received command code: '" + cmd + "'");
		switch(cmd) {
			case 'k':
				System.out.println("Received kill command.\n I am death or DAD hahah");
				this.reply.close();
				this.listen.close();
				break;
			default:
				HyParViewLabTest.addToOutLog("COMM", "Unrecognized command.");
				break;
		}
	}

	public void sendMessage(Object m) throws IOException {
		if(m instanceof PingAlive) 
			System.out.println("PING!!");
	}
	
	
}
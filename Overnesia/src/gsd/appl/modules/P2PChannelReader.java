package gsd.appl.modules;

import gsd.api.P2PChannel;
import gsd.appl.utils.BroadcastMessage;
import gsd.appl.utils.LogManager;

import java.nio.ByteBuffer;



public class P2PChannelReader extends Thread {

	private P2PChannel neem;
		
	public P2PChannelReader(P2PChannel neem) {
		this.neem = neem;
	}
	
	@Override
	public void run() {
		BroadcastMessage bm = null;
		
		while(true) {
			byte[] buf = new byte[1000];
			ByteBuffer bb = ByteBuffer.wrap(buf);
			try {
				neem.read(bb);
			} catch (Exception e) {
				LogManager.logException(e);
				break;
			}
			bm = BroadcastMessage.readBroadcastMessageFromBuffer(new ByteBuffer[]{bb});
		
			LogManager.logMessageReception(bm);
		}
		
		LogManager.logWarn("Exited from the reception thead main procedure.");
	}
}

package gsd.appl.modules;

import gsd.appl.utils.Action;
import gsd.appl.utils.ActionManager;
import gsd.appl.utils.ConfigManager;
import gsd.appl.utils.LogManager;

public abstract class ExperimentalNode {

	private ConfigManager cm;
	private ActionManager acm;
	//private LogManager lm;
	private boolean initialized;
	//private Logger periodic;
	private long periodicInterval;
	
	protected long myIdNumber;
	
	public ExperimentalNode() throws Exception{
		this.cm = new ConfigManager();
		this.myIdNumber = cm.getMyIdNumber();
		this.acm = new ActionManager(this.myIdNumber,cm.getStartTime(),this);
		this.periodicInterval = cm.getPeriodicInterval();
		//this.lm = LogManager.getInstance(myIdNumber);
		//this.periodic = lm.createNewLogger("Periodic");
		this.initialized = false;
	}
	
	public ExperimentalNode(long myIdNumber) throws Exception {
		this.myIdNumber = myIdNumber;
		this.cm = new ConfigManager();
		this.acm = new ActionManager(this.myIdNumber,cm.getStartTime(),this);
		this.periodicInterval = cm.getPeriodicInterval();
		//this.lm = LogManager.getInstance(myIdNumber);
		//this.periodic = lm.createNewLogger("Periodic");
		this.initialized = false;
	}
	
	protected final ConfigManager getConfigurationManager() {
		return this.cm;
	}
	
	public final void initExperiment() {
		this.acm.start();
		this.initialized = true;
	}
	
	public boolean isInicialized() {
		return this.initialized;
	}
	
	private final void keepAlive() {
		LogManager.logDebug("Alive");
	}
	
	protected final void experimentBody() {
		while(true) {		
			this.keepAlive();
			if(!this.periodicOperation())
				return;
			try {
				Thread.sleep(this.periodicInterval);
			} catch (InterruptedException e) {
				LogManager.logException(e);
			}
		}
	}
	
	/**
	 * This method should do clean up and other specific behaviors.
	 */
	public final void endExperiment() {
		this.localCleanUp();
	}
	
	public abstract void executeAction(Action event);

	public abstract boolean periodicOperation();

	/**
	 * This method should be implemented by any class that extends ExperimentalNode
	 * It is implicitly invoked when executing the final method endExperiment, and it should
	 * handle any specific data or cleanUp required by the class that implements it.
	 */
	public abstract void localCleanUp();
}

package gsd.appl.modules;

import gsd.api.P2PChannel;
import gsd.appl.utils.Action;
import gsd.appl.utils.BroadcastMessage;
import gsd.appl.utils.ConfigManager;
import gsd.appl.utils.LogManager;
import gsd.common.OvernesiaNodeID;
import gsd.impl.overnesia.Overnesia;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;




public class DisseminationStrategy extends ExperimentalNode{

	/**
	 * ACTIONS LISTS: (these are actions that are supported by the this experimental node)
	 */
	public static final String ACTION_BROADCAST = "broadcast";
	public static final String ACTION_FAIL = "fail";
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private P2PChannel channel;
	private Thread receiver;
	
	private long mySequenceNumber;
	
	public DisseminationStrategy() throws Exception {
		super();
		ConfigManager cfm = this.getConfigurationManager();
		this.contactId = cfm.getContactId();
		this.myId = cfm.getMyId();
		this.mySequenceNumber = 0;
		
		try {
			this.channel = new P2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		receiver = new Thread() { 
			@Override
			public void run() {
				try {
					while(true) {
						byte[] buf = new byte[1000];
						ByteBuffer bb = ByteBuffer.wrap(buf);
						channel.read(bb);
						System.out.println(new String(buf));
					}
				} catch(Exception e) { e.printStackTrace(); }
			}
		};
	}
	
	public void execute() {
		LogManager.logDebug("My id: " + this.myId.toString());
		LogManager.logDebug("Contact id: " + this.contactId.toString());
		
		//Start Neem join procedure
		channel.connect(this.contactId);
		LogManager.logDebug("Neem has executed the connect method.");
		
		//Start receiving thread
		receiver.start();
		LogManager.logDebug("Started receiving thread.");
		
		this.experimentBody();
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DisseminationStrategy process = null;
		try {
			process = new DisseminationStrategy();
			process.execute();
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e);
			System.exit(2);
		}
		
	}

	/**
	 * This is the method which is invoked by the ActionManager to force this node to execute some operation
	 */
	@Override
	public void executeAction(Action event) {
		if(DisseminationStrategy.ACTION_BROADCAST.compareToIgnoreCase(event.getAction()) == 0) {
			try {
				BroadcastMessage bm = new BroadcastMessage(channel.getGossipNodeID(), this.myIdNumber, this.mySequenceNumber);
				this.channel.write(BroadcastMessage.writeBroadcastMessageToBuffer(bm));
				LogManager.logMessageBroadcast(bm);
				this.mySequenceNumber++;
			} catch (ClosedChannelException e) {
				LogManager.logError("Failed to send message with id: " + this.mySequenceNumber);
				LogManager.logException(e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void localCleanUp() {
		channel.close();		
	}

	@Override
	public boolean periodicOperation() {
		Overnesia overlay = channel.getOverlayInstance();
		
		LogManager.logDebug("ID: " + overlay.getId());
		LogManager.logDebug("Nesos Size: " + overlay.getNesosSize());
		LogManager.logDebug("External Neighbors Size: " + overlay.getExternalNeighborsSize());
		LogManager.logDebug("Nesos Filiation:");
		for(OvernesiaNodeID id: overlay.getNesosFiliation())
			LogManager.logDebug("  " + id);
		LogManager.logDebug("External Neighbors:");
		for(OvernesiaNodeID id: overlay.getExternalIDs())
			LogManager.logDebug("  " + id);
		LogManager.logDebug("Number of TCP Connections: " + overlay.connections().length);
		
		return true;
	}
	
	
}

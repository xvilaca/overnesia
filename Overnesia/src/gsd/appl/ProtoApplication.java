package gsd.appl;

import gsd.api.P2PChannel;
import gsd.common.OvernesiaNodeID;
import gsd.impl.Config;
import gsd.impl.Periodic;
import gsd.impl.overnesia.Overnesia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

//import org.apache.log4j.Logger;

public class ProtoApplication extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private P2PChannel channel;
	private Thread receiver;
	
	private Periodic DebugOperation;
	
	//private static Logger log = Logger.getLogger("Proto Application");
	
	public ProtoApplication(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException {
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new P2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
					
		receiver = new Thread() {
			@Override
			public void run() {
				try {
					while(true) {
						byte[] buf = new byte[1000];
						ByteBuffer bb = ByteBuffer.wrap(buf);
						channel.read(bb);
						System.out.println((new String(buf)).trim());
					}
				} catch(Exception e) { e.printStackTrace(); }
			}
		};
	}
	
	public ProtoApplication(InetSocketAddress myId) throws UnknownHostException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new P2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
	
		receiver = new Thread() { 
			@Override
			public void run() {
				try {
					while(true) {
						byte[] buf = new byte[1000];
						ByteBuffer bb = ByteBuffer.wrap(buf);
						channel.read(bb);
						System.out.println(new String(buf));
					}
				} catch(Exception e) { e.printStackTrace(); }
			}
		};
	}
	
	public boolean debugOperation() {
		Overnesia overlay = channel.getOverlayInstance();
		
		System.err.println("ID: " + overlay.getId());
		System.err.println("Nesos Size: " + overlay.getNesosSize());
		System.err.println("External Neighbors Size: " + overlay.getExternalNeighborsSize());
		System.err.println("Nesos Filiation:");
		for(OvernesiaNodeID id: overlay.getNesosFiliation())
			System.err.println("  " + id);
		System.err.println("External Neighbors:");
		for(OvernesiaNodeID id: overlay.getExternalIDs())
			System.err.println("  " + id);
		System.err.println("Number of TCP Connections: " + overlay.connections().length);
		
		return true;
	}
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		if(this.contactId != null)
			System.out.println("Contact id: " + this.contactId.toString());
		else
			System.out.println("Contact id: [NONE]");
		
		//Initialize the overlay network level
		channel.connect(this.contactId);
		
		//Create my reader object
		BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
		String line;
		
		//Start receiving thread
		receiver.start();
		
		System.out.println("Application ready");
		System.out.println();
		
		try {
			while((line = this.myId.toString() + " says: " +  r.readLine()) != null) {
				channel.write(ByteBuffer.wrap(line.getBytes()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			channel.close();
			System.exit(2);
		} 
		
		channel.close();
		
	}
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
		if(args.length != 2 && args.length != 4) {
			System.err.println("Invalid arguments!");
			System.err.println("Usage: java gsd.appl.ProtoApplication <MY_IP> <MY_PORT> [IP] [PORT]");
			System.exit(1);
		}
		
		ProtoApplication process = null;
		
		if(args.length == 2)
			process = new ProtoApplication(new InetSocketAddress(args[0],Integer.parseInt(args[1])));
		else if(args.length == 4) {
			byte[] contactAddr = InetAddress.getByName(args[2]).getAddress();
//			MODELNET, change remote address's second byte
			if ( Config.modelnet )
				contactAddr[1]|=0x80;
// 			MODELNET	
			process = new ProtoApplication(new InetSocketAddress(args[0],Integer.parseInt(args[1])),new InetSocketAddress(InetAddress.getByAddress(contactAddr),Integer.parseInt(args[3])));
		}
		process.execute();
	}

}

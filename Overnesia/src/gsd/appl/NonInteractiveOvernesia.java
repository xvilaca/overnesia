package gsd.appl;

import gsd.api.OvernesiaPlabP2PChannel;

import gsd.appl.tests.ExperimentalTest;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.OvernesiaNodeID;
import gsd.impl.Config;
import gsd.impl.Periodic;
import gsd.impl.UUIDs;
import gsd.impl.overnesia.Overnesia;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.UUID;

public class NonInteractiveOvernesia extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private OvernesiaPlabP2PChannel channel;
	
	private Periodic aliveOperation;
	
	public long epoch;
	
	private PrintStream out; 
	private PrintStream query;
	private PrintStream overlay;
	
	private Thread cmdLine;
	
	public short filiationRequestID = 1;
	public short queryRequestID = 2;
	public short queryReplyID = 3;
	public short sendQueryRequestID = 4;
	public short flushQueryDisseminationRequestID = 5;
	public short reconfigureParameterID = 6;
	public short flushOvernesiaRequestID = 7;
	
	private Timer operations;
	
	public NonInteractiveOvernesia(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException, FileNotFoundException {
		System.setErr(System.out);
		
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new OvernesiaPlabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		//Use this for one hour period: 60 * 60 * 1000
		//Use this for 10 minute period: 60 * 10 * 1000
		//Use this for 5 minute period: 60 * 5 * 1000
		//Use this for 1 minute period: 60 * 1000
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		

		this.initializeFiles();
	}
	
	public NonInteractiveOvernesia(InetSocketAddress myId) throws UnknownHostException, FileNotFoundException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new OvernesiaPlabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		
	
		this.initializeFiles();
	}
	
	private void initializeFiles() throws FileNotFoundException {
		this.out = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_alive.log"));
		this.overlay = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_overlay.log"));
		this.query = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_query.log"));
		this.addToOutLog("ACTIV", myId.getHostName() + " " + myId.getAddress() + ":" + myId.getPort());
	}

	public void addToOutLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.out.println(line);
	}
	
	public void addToQueryLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.query.println(line);
	}
	
	public void addToOverlayLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.overlay.println(line);
	}
	
	public boolean debugOperation() {
		this.addToOutLog("ALIVE", channel.getOverlayInstance().getId() + " " + epoch);
		this.out.flush();
		this.overlay.flush();
		this.query.flush();
		return true;
	}
	

	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		channel.connect(this.contactId);
		if(this.contactId == null) {
			this.operations = new Timer();
			final NonInteractiveOvernesia appl = this;
			cmdLine = new Thread() {
				@Override
				public void run() {
					BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
					String cmd;
					try {
						while(true) {
							cmd = r.readLine();
							if(cmd != null) {
								if(cmd.equalsIgnoreCase("bcast")) {
									addToOutLog("BCSND", "Filiation Request");
									ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 8);
									msg.putShort(filiationRequestID);
									OvernesiaNodeID.writeNodeIdToBuffer(msg, channel.getGossipNodeID());
									msg.putLong(epoch);
									msg.flip();
									channel.broadcast(new ByteBuffer[]{msg});
								} else if(cmd.equalsIgnoreCase("query")) {
									UUID uuid = UUID.randomUUID();
									addToOutLog("QRSND", "Query Request");
									addToQueryLog("QRSND", uuid.toString());
									ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 16);
									msg.putShort(queryRequestID);
									OvernesiaNodeID.writeNodeIdToBuffer(msg, channel.getGossipNodeID());
									UUIDs.writeUUIDToBuffer(msg,uuid);
									msg.flip();
									channel.disseminateQuery(new ByteBuffer[]{msg});
								} else if(cmd.equalsIgnoreCase("random")) {
									UUID uuid = UUID.randomUUID();
									addToOutLog("QRGEN", "Random Query Request " + uuid);
									ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 16);
									msg.putShort(sendQueryRequestID);
									OvernesiaNodeID.writeNodeIdToBuffer(msg, channel.getGossipNodeID());
									UUIDs.writeUUIDToBuffer(msg,uuid);
									msg.flip();
									channel.randomWalk(new ByteBuffer[]{msg});
								} else if(cmd.equalsIgnoreCase("flush")) {
									addToOutLog("FLUSH", "Send Flush Request");
									ByteBuffer msg = ByteBuffer.allocate(2);
									msg.putShort(flushQueryDisseminationRequestID);
									msg.flip();
									channel.broadcast(new ByteBuffer[]{msg});
								} else {
									StringTokenizer st = new StringTokenizer(cmd, " ");
									String token = st.nextToken();
									if(token.equalsIgnoreCase("test")) {
										ExperimentalTest experience = ExperimentalTest.getTestInstance(st.nextToken(), appl, channel);
										if(experience == null) {
											System.out.println("Unrecognized command: " + cmd);
										} else {
											operations.schedule(experience, 0, experience.getInterval());
										}
									} else if(token.equalsIgnoreCase("set")) {
										if(st.nextToken().equalsIgnoreCase("parameter")) {
											short parameterCode = Parameter.encode(st.nextToken());
											short value = Short.parseShort(st.nextToken());
											if(parameterCode != -1) {
												addToOutLog("BCSND", "Set Parameter " + parameterCode + " " + value);
												ByteBuffer msg = ByteBuffer.allocate(2 + 2 + 2);
												msg.putShort(reconfigureParameterID);
												msg.putShort(parameterCode);
												msg.putShort(value);
												msg.flip();
												channel.broadcast(new ByteBuffer[]{msg});
											}
										} else {
											System.out.println("Unrecognized command: " + cmd);
										}
									} else {
										System.out.println("Unrecognized command: " + cmd);
									}
								} 
							} 
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("Input cmd line is off...");
					}
				}
			};
			cmdLine.start();
		}
		
		
		
		try {
			while(true) {
				ByteBuffer bb = ByteBuffer.allocate(1024);
				channel.read(bb);
				bb.rewind();
				short op_code = bb.getShort();
				if(op_code == this.filiationRequestID) {
					OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(bb);
					long recv_epoch = bb.getLong();
					recv_epoch++;
					if(this.epoch < recv_epoch)
						this.epoch = recv_epoch;
					this.addToOutLog("BCRCV", "Filiation Request " + sender );
					this.printFiliation(sender);
				} else if(op_code == this.queryRequestID) {
					OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(bb);
					UUID target = UUIDs.readUUIDFromBuffer(bb);
					this.addToOutLog("QRRCV", "Query Request " + sender);
					this.addToQueryLog("QRRCV", target + " " + sender);
					this.processQuery(sender, target);
				} else if(op_code == this.queryReplyID) {
					OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(bb);
					UUID target = UUIDs.readUUIDFromBuffer(bb);
					this.addToOutLog("QRPLY", "Query Reply " + sender);
					this.addToQueryLog("QRPLY", sender + " " + target);
				} else if(op_code == this.sendQueryRequestID) {
					OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(bb);
					UUID target = UUIDs.readUUIDFromBuffer(bb);
					this.addToOutLog("QRGRC", "Random Query Request Received for " + target + " issued by " + sender);
					addToOutLog("QRSND", "Query Request");
					addToQueryLog("QRSND", target.toString());					
					ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 16);
					msg.putShort(queryRequestID);
					OvernesiaNodeID.writeNodeIdToBuffer(msg, channel.getGossipNodeID());
					UUIDs.writeUUIDToBuffer(msg, target);
					msg.flip();
					channel.disseminateQuery(new ByteBuffer[]{msg});
				} else if(op_code == this.flushQueryDisseminationRequestID) {
					addToOutLog("FLUSH", "Received Flush Request");
					this.channel.flushStatistics(this.query);
				} else if(op_code == this.flushOvernesiaRequestID) {
					long recv_epoch = bb.getLong();
					recv_epoch++;
					if(this.epoch < recv_epoch)
						this.epoch = recv_epoch;
					addToOutLog("MSGFR", "Received Overnesia Flush Request");
					this.channel.flushOvernesia(this.overlay, this.epoch);
				} else if(op_code == this.reconfigureParameterID) {
					Parameter param = new Parameter(bb.getShort(), bb.getShort());
					addToOutLog("BCRCV", "Received Set Parameter Request " + param);
					this.channel.applyReconfiguration(param);					
				} else {
					this.addToOutLog("ERROR", "Unrecognized operation code received: " + op_code);
				}
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	private void printFiliation(OvernesiaNodeID sender) {
		Overnesia ovlay = channel.getOverlayInstance();
		
		String line = this.epoch + "\n" + ovlay.getId() + "\n" 
			+ ovlay.getNesosSize() + " " + ovlay.getExternalNeighborsSize() + " " + ovlay.getPassiveViewSize() + "\n";
		for(OvernesiaNodeID id: ovlay.getNesosFiliation())
			line = line + id + "\n";
		for(OvernesiaNodeID id: ovlay.getExternalIDs())
			line = line + id + "\n";
		for(OvernesiaNodeID id: ovlay.getPassiveView())
			line = line + id + "\n";
		line = line + ovlay.connections().length + " " + ovlay.network().connections().length + " " + ovlay.pendingConns();
		
		this.addToOverlayLog("OVFIL", line);
	}

	private synchronized void processQuery(OvernesiaNodeID sender, UUID target) {			
		try {
			ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 16);
			msg.putShort(this.queryReplyID);
			OvernesiaNodeID.writeNodeIdToBuffer(msg,channel.getGossipNodeID());
			UUIDs.writeUUIDToBuffer(msg, target);
			msg.flip();
			this.channel.replyToQuery(new ByteBuffer[]{msg}, sender.getSocketAddress());
			this.addToOutLog("QRRYS", "Query Reply");
			this.addToQueryLog("QRRYS", "Query Reply" + sender + " " + target);
		} catch (ClosedChannelException e) {
			e.printStackTrace();
			this.addToOutLog("QRRYF", "Failed Query Reply");
			this.addToQueryLog("QRRYF", "Failed Query Reply" + sender + " " + target);
		}
	}

	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), 28349);
		InetSocketAddress contact = new InetSocketAddress("192.16.125.11",28349);
	
		try {
			if(local.getAddress().equals(contact.getAddress())) {
				System.out.println("Primary node is on-line.");
				new NonInteractiveOvernesia(local).execute();
			} else {
				System.out.println("Node is on-line.");
				new NonInteractiveOvernesia(local, contact).execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

}

package gsd.appl;

import gsd.impl.queryDissemination.OvernesiaQueryDissemination;

public class Parameter {

	short parameter;
	short value;
	
	public Parameter(short p, short v) {
		this.parameter = p;
		this.value = v;
	}
	
	public void execute(OvernesiaQueryDissemination module) {
		switch (parameter) {
		case 1:
			module.setFanout(value);
			break;

		case 2:
			module.setTTL(value);
			break;
			
		case 3:
			module.setIntTTL(value);
			break;
			
		case 4:
			module.setMaxIds(value);
			break;
		
		case 5:
			module.setInitialFloodinf(value);
			break;
		
		default:
			break;
		}
	}
	
	public static short encode(String token) {
		if(token.equalsIgnoreCase("fanout"))
			return 1;
		else if(token.equalsIgnoreCase("ttl"))
			return 2;
		else if(token.equalsIgnoreCase("ittl"))
			return 3;
		else if(token.equalsIgnoreCase("maxids"))
			return 4;
		else if(token.equalsIgnoreCase("flood"))
			return 5;
		else
			return -1;
	}

	@Override
	public String toString () {
		return this.parameter + " " + this.value;
	}
}

package gsd.appl.tests;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

import gsd.api.OvernesiaPlabP2PChannel;
import gsd.appl.NonInteractiveOvernesia;

public class OvernesiaMessageCountTest extends ExperimentalTest {

	private OvernesiaPlabP2PChannel channel;
	private NonInteractiveOvernesia appl;
	
	public OvernesiaMessageCountTest(NonInteractiveOvernesia appl, OvernesiaPlabP2PChannel channel) {
		this.channel = channel;
		this.appl = appl;
	}
 	
	@Override
	public void run() {
		appl.addToOutLog("MSGFS", "Send Overnesia Flush Request");
		ByteBuffer msg = ByteBuffer.allocate(2 + 8);
		msg.putShort(appl.flushOvernesiaRequestID);
		msg.putLong(appl.epoch);
		msg.flip();
		try {
			channel.broadcast(new ByteBuffer[]{msg});
			System.out.println("Sent an overnesia message flush request");
		} catch (ClosedChannelException e) {
			e.printStackTrace();
		}
	}

	@Override
	public long getInterval() {
		return 30*1000; //30 seconds
	}

	@Override
	public String toString() {
		return "Overnesia Message Count Test";
	}
}

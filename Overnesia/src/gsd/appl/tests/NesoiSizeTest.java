package gsd.appl.tests;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

import gsd.api.OvernesiaPlabP2PChannel;
import gsd.appl.NonInteractiveOvernesia;
import gsd.common.OvernesiaNodeID;

public class NesoiSizeTest extends ExperimentalTest {
	
	public int repeats;
	public int done;
	private OvernesiaPlabP2PChannel channel;
	private NonInteractiveOvernesia appl;
	
	public NesoiSizeTest(NonInteractiveOvernesia appl, OvernesiaPlabP2PChannel channel) {
		this.repeats = 10;
		this.done = 0;
		this.channel = channel;
		this.appl = appl;
	}
 	
	@Override
	public void run() {
		appl.addToOutLog("BCSND", "Filiation Request");
		ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 8);
		msg.putShort(appl.filiationRequestID);
		OvernesiaNodeID.writeNodeIdToBuffer(msg, channel.getGossipNodeID());
		msg.putLong(appl.epoch);
		msg.flip();
		try {
			channel.broadcast(new ByteBuffer[]{msg});
			System.out.println("Sent a filiation flush request");
		} catch (ClosedChannelException e) {
			e.printStackTrace();
		}
		
		done ++;
		
		if(done == repeats)
			this.endTest();
	}

	@Override
	public long getInterval() {
		return 30*1000; //30 seconds
	}

	@Override
	public String toString() {
		return "Nesoi Size Test";
	}
}

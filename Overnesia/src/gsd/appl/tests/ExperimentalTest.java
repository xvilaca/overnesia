package gsd.appl.tests;

import gsd.api.OvernesiaPlabP2PChannel;
import gsd.appl.NonInteractiveOvernesia;

import java.util.TimerTask;

public abstract class ExperimentalTest extends TimerTask {
	
	public static ExperimentalTest getTestInstance(String description, NonInteractiveOvernesia appl, OvernesiaPlabP2PChannel channel) {
		if(description.equalsIgnoreCase("size"))
			return new NesoiSizeTest(appl, channel);
		else if(description.equalsIgnoreCase("msgs"))
			return new OvernesiaMessageCountTest(appl, channel);
		else if(description.equalsIgnoreCase("msgs2"))
			return new OvernesiaStableMessageCountTest(appl, channel);
		
		return null;
	}
	
	public abstract long getInterval();
	
	public void endTest() {
		this.cancel();
		System.out.println("Test " + this.toString() + " completed.");
	}

}

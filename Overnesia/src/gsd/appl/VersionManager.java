package gsd.appl;

import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;

import java.util.ArrayList;
import java.util.HashMap;

public class VersionManager {

	public static final VersionManager instance = new VersionManager();
	
	private HashMap<String,ArrayList<Slave>> list;
	
	private VersionManager() {
		this.list = new HashMap<String,ArrayList<Slave>>();
	}
	
	public static void unregister(String version, Slave node) {
		VersionManager.instance.remove(version, node);
	}
	
	public static String register(String version, Slave node) {
		return VersionManager.instance.add(version, node);
	}
	
	private void remove(String version, Slave node) {
		synchronized (this.list) {
			if(this.list.containsKey(version)) {
				ArrayList<Slave> item = this.list.get(version);
				if(item.contains(node)) {
					item.remove(node);
					if(item.size() == 0)
						this.list.remove(version);
				}
			}
		}
	}
	
	private String add(String version, Slave node) {
		synchronized (this.list) {
			if(this.list.containsKey(version)) {
				ArrayList<Slave> item  = this.list.get(version);
				if(!item.contains(node))
					item.add(node);
				for(String s: this.list.keySet())
					if(s.equalsIgnoreCase(version))
						return s;
				return version;
			} else {
				ArrayList<Slave> item = new ArrayList<Slave>();
				item.add(node);
				this.list.put(version, item);
				return version;
			}
		}
	}
	
	public void listVersions(CommandLine cmdl) {
		synchronized (this.list) {
			cmdl.output("Known versions:");
			for(String s: this.list.keySet()) {
				cmdl.output(s + "\t\t" + this.list.get(s).size() + " active nodes");
			}
		}
	}
	public void listNodes(String version, CommandLine cmdl) {
		synchronized (this.list) {
			if(this.list.containsKey(version)) {
				ArrayList<Slave> item = this.list.get(version);
				cmdl.output("Nodes running version: " + version);
				String output = "";
				for(Slave s: item) {
					output = output + s.toString();
				}
				cmdl.output(output);
				cmdl.output("Total of: " + item.size() + " nodes");
			} else {
				cmdl.output("No node is running version: " + version);
			}
			
		}
	}
	
}

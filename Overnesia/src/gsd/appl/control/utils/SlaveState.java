package gsd.appl.control.utils;

public class SlaveState {

	public static final int STATE_OFF = 0;
	public static final int STATE_CONNECTING = 1;
	public static final int STATE_ON = 2;
	
	private int state;
	
	public SlaveState(){
		this.state = STATE_OFF;
	}
	
	public synchronized boolean isON() {
		return this.state == STATE_ON;
	}
	
	public synchronized boolean isConnecting(){
		return this.state == STATE_CONNECTING;
	}
	
	public synchronized boolean isOFF() {
		return this.state == STATE_OFF;
	}
	
	public synchronized void setStateON() {
		this.state = STATE_ON;
	}
	
	public synchronized void setStateCONNECTING() {
		this.state = STATE_CONNECTING;
	}
	
	public synchronized void setStateOFF() {
		this.state = STATE_OFF;
	}

	@Override
	public synchronized String toString() {
		switch(this.state) {
		case STATE_OFF:
			return "inactive  ";
		case STATE_CONNECTING:
			return "connecting";
		case STATE_ON:
			return "active    ";
		default:
			return "undefined ";
		}
	}
	
}

package gsd.appl.control.components;

import java.util.Random;

import gsd.appl.control.utils.SlaveState;

public class PingControler extends Thread {

	private Slave parent;
	private SlaveState state;
	private Random r;
	
	public PingControler(Slave parent, SlaveState state, Random r) {
		this.parent = parent;
		this.state = state;
		this.r = r;
	}
	
	@Override
	public void run() {
		long salt = 0;
		
		while(true) {
			
			if(this.state.isON()) {
				synchronized (this.r) {
					parent.performPingOperation();
					salt = r.nextInt(60) * 1000;
				}
			}
			
			try { Thread.sleep(60*1000 + salt); } catch (InterruptedException e) { } 
		}
		
	}
	
}

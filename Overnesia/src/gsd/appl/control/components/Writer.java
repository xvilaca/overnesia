package gsd.appl.control.components;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.VersionManager;
import gsd.appl.control.components.messages.PingAlive;
import gsd.appl.control.utils.SlaveState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Writer extends Thread {

	private ArrayList<Object> queue;
	public HyParViewLabTestControl instance;
	public String host;
	public ObjectOutputStream out;
	public ObjectInputStream in;
	public Socket sock;
	private Slave parent;
	public Reader reader;
	public final SlaveState state;
	public String version_running;
	
	public Writer(HyParViewLabTestControl instance, Slave parent, SlaveState state, String host, Socket sock, Reader reader, String version) {
		this.queue = new ArrayList<Object>();
		this.instance = instance;
		this.host = host;
		this.sock = sock;
		this.parent = parent;
		this.reader = reader;
		this.state = state;
		this.version_running = version;
	}
	
	//public final static long comm_timeout = 5 * 60 * 1000; //5 min
	
	private boolean extablishConnection() {
		try {
			sock.connect(new InetSocketAddress(this.host, HyParViewLabTestControl.controlPort));
			
			this.out = new ObjectOutputStream(sock.getOutputStream());
			this.in = new ObjectInputStream(sock.getInputStream());
			this.out.writeObject("06:9c:13:69:a1:fa:36:fa:45:1c:2e:9f:e6:99:a1:fa");
			
			String r = (String) this.in.readObject();
			StringTokenizer st = new StringTokenizer(r, " ");
			if(!st.hasMoreTokens() || !st.nextToken().equals(new String("OK"))) {
				instance.debug(this, host + ": provided invalid reply '" + r + "'");
				throw new IOException("Invalida reply...");
			}
			instance.debug(this, host + ": connection extablished.");
			
			
			if(this.version_running != null) VersionManager.unregister(this.version_running, this.parent);
			this.version_running = "";
			
			while(st.hasMoreTokens()) {
				this.version_running = this.version_running + st.nextToken() + " ";
			}
			
			this.version_running = VersionManager.register(this.version_running.trim(), this.parent);
			
			
			st = null;
			r = null;
			this.state.setStateON();
			this.reader.registerWriter(this);
			this.reader.registerInputStream(this.in);
			this.reader.start();	
			this.parent.activeCallback();
			return true;
		} catch (Exception e) {
			if(e.getMessage() == null || (e.getMessage().equalsIgnoreCase("Connection refused") && e.getMessage().equalsIgnoreCase("Network is unreachable")))
				instance.debug(this, "Error ocurred while connecting to " + host + ". Exception " + e.getClass().getName() + ": " + e.getMessage());
			this.state.setStateOFF();
			try { this.sock.close();} catch (IOException e1) {	} //nothing to do in case of exception
			//instance.debug(this, "Failed connection to: " + host +": " + e.getMessage());
			synchronized (instance.slaves) {	
				instance.slaves.notify(); 
			}
			return false;
		} 
	}
	
	@Override
	public void run() {
		
		if(!this.extablishConnection())
			return;
		
		Object c = null;
		
		boolean run = !state.isOFF();
		
		while(run) {
			synchronized (this.queue) {
				if(this.queue.size() > 0)
					c = this.queue.remove(0);
				else
					c = null;
			
			
				if(c == null) {
					try { this.queue.wait(); } catch (InterruptedException e) {	}
				} else {
					if(c instanceof PingAlive) ((PingAlive) c).timestampSend();
					try {
						if(!(c instanceof PingAlive))
							instance.debug(this, "Writting a " + c.getClass().getName() + " to host: " + host);
						out.writeObject(c);
					} catch (IOException e) {
						instance.debug(this, "Error while sending message to " + host + ". Message type: " + c.getClass().getName() + " [" + c.toString() + "]");
						break;
					}
				}
			}
			
			run = !state.isOFF();
		}
		
		state.setStateOFF();
		reader.interrupt();
		parent.inactiveCallback();
		try { this.sock.close(); } catch (IOException e) { } //hopefully will never generate an error..
	}
	
	
	public ArrayList<Object> getQueue() {
		return this.queue;
	}
}

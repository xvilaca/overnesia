package gsd.appl.control.components;

import gsd.appl.HyParViewLabTest;
import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.experiences.ExperienceInstance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public class CommandLine extends Thread {

	private HyParViewLabTestControl instance;
	private String prompt;
	
	public CommandLine(HyParViewLabTestControl instance) {
		this.instance = instance;
		prompt = "Cmd:> ";
	}
	
	@Override
	public void run() {
		try {
			Scanner sc = new Scanner(System.in);
			String cmd = null;
			String t = null;
			System.out.println("PlanetLab-Control Console.");
			System.out.println("Version: " + HyParViewLabTest.version_numb + " " + HyParViewLabTest.version);
			System.out.println("");
			this.printprompt();
			while(true) {
				cmd = sc.nextLine();
				if(cmd.trim().equalsIgnoreCase("")) continue;
				StringTokenizer st = new StringTokenizer(cmd," ");
				t = st.nextToken();
				if(t.equalsIgnoreCase("list")) {
					if(st.hasMoreTokens()) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("active")) {
							this.instance.listActiveNodes();
						} else if(t.equalsIgnoreCase("inactive")) {
							this.instance.listInactiveNodes();
						} else if(t.equalsIgnoreCase("all")) {
							this.instance.listNodes();
						} else if(t.equalsIgnoreCase("experience")) {
							this.instance.listExperiences();
						} else if(t.equalsIgnoreCase("version")) {
							this.instance.versions.listVersions(this);
						} else {
							this.output("Unrecognized option. Usage: 'list all|active|inactive|experience|version'");
						}
					} else {
						this.instance.listNodes();
					}
				} else if(t.equalsIgnoreCase("version")) {
					if(st.countTokens() < 1) {
						this.output("Usage: 'version <name>'");
					} else {
						String name = st.nextToken();
						while(st.hasMoreTokens())
							name = name + " " + st.nextToken();
						this.instance.versions.listNodes(name.trim(), this);	
					}
				} else if(t.equalsIgnoreCase("create")) {
					if(st.countTokens() <= 2 || !st.nextToken().equalsIgnoreCase("experience")) {
						this.output("Usage: 'create experience <type> <name>'");
					} else {
						String type = st.nextToken();
						String name = st.nextToken();
						while(st.hasMoreTokens())
							name = name + " " + st.nextToken();
						ExperienceInstance ei = ExperienceInstance.createNewExperience(instance, type, name);
						if(ei == null) {
							this.output("Invalid experience type. Valid types are: " + ExperienceInstance.validTypes());
						} else {
							instance.experiences.put(ei.getID(), ei);
							this.output("New experience created with unique id: " + ei.getID());
							this.output("Entering interactive mode.");
							ei.interactiveMode(this, sc);
						}
					}
				} else if(t.equalsIgnoreCase("enter")) {
					if(st.countTokens() <= 1 || !st.nextToken().equalsIgnoreCase("experience")) {
						this.output("Usage: 'enter experience <name>'");
					} else {
						String name = st.nextToken();
						while(st.hasMoreTokens())
							name = name + " " + st.nextToken();
						
						ExperienceInstance ei = null;
						for(UUID id: instance.experiences.keySet()) {
							if(instance.experiences.get(id).isIt(name)) {
								ei = instance.experiences.get(id);
								break;
							}
						}
						
						if(ei != null) {
							this.output("Entering interactive mode.");
							ei.interactiveMode(this, sc);
						} else {
							this.output("Experience not found: " + name);
						}
					}
				} else if(t.equalsIgnoreCase("release")) {
					if(st.countTokens() <= 1 || !st.nextToken().equalsIgnoreCase("experience")) {
						this.output("Usage: 'release experience <name>'");
					} else {
						String name = st.nextToken();
						while(st.hasMoreTokens())
							name = name + " " + st.nextToken();
						
						ExperienceInstance ei = null;
						for(UUID id: instance.experiences.keySet()) {
							if(instance.experiences.get(id).isIt(name)) {
								ei = instance.experiences.remove(id);
								break;
							}
						}
						
						if(ei != null) {
							ei.stopExecution();
							ei.releaseAll();
							this.output("Experience released");
						} else {
							this.output("Experience not found: " + name);
						}
					}
				} else if(t.equalsIgnoreCase("export")) {
					if(st.hasMoreTokens()) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("active")) {
							this.instance.exportActiveNodes();
						} else if(t.equalsIgnoreCase("inactive")) {
							this.instance.exportInactiveNodes();
						} else if(t.equalsIgnoreCase("all")) {
							this.instance.exportlistNodes();
						} else {
							this.output("Unrecognized option. Usage: 'export all|active|inactive'");
						}
					} else {
						this.instance.listNodes();
					}
				} else if(t.equalsIgnoreCase("snapshot")) {
					if(st.hasMoreTokens()) {
						t = st.nextToken();
						String f = null;
						if(st.hasMoreTokens()) {
							f = "";
						} else 
							f = st.nextToken();
						if(t.equalsIgnoreCase("active")) {
							this.instance.literalExportActiveNodes(f);
						} else if(t.equalsIgnoreCase("inactive")) {
							this.instance.literalExportInactiveNodes(f);
						} else if(t.equalsIgnoreCase("all")) {
							this.instance.literalExportNodes(f);
						} else {
							this.output("Unrecognized option. Usage: 'snapshot all|active|inactive [filename]'");
						}
					} else {
						this.output("Unrecognized option. Usage: 'snapshot all|active|inactive [filename]'");
					}
				} else if(t.equalsIgnoreCase("add")) {
					if(st.countTokens() == 1 || st.countTokens() == 2) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("-f")) {
							Scanner scan;
							try {
								scan = new Scanner(new File(st.nextToken()));
							} catch (FileNotFoundException e) {
								this.instance.cmdl.output("File not found.");
								this.instance.cmdl.printprompt();
								continue;
							}
							while(scan.hasNextLine()) {
								this.instance.addNodeToList(scan.nextLine());
							}
						} else {
							this.instance.addNodeToList(t);
						}
					} else {
						this.output("Expecting Token. Usage: 'add node|-f filename'");
					}
				} else if(t.equalsIgnoreCase("del")) {
					if(st.hasMoreTokens()) {
						t = st.nextToken();
						if(t.equalsIgnoreCase("-f")) {
							Scanner scan;
							try{
								scan = new Scanner(new File(st.nextToken()));	
							} catch (FileNotFoundException e) {
								this.instance.cmdl.output("File not found.");
								this.instance.cmdl.printprompt();
								continue;
							}
							while(scan.hasNextLine()) {
								this.instance.removeNodeFromList(processArgument(scan.nextLine()));
							}
						} else {
							this.instance.removeNodeFromList(processArgument(t));
						}
					} else {
						this.output("Expecting Token. Usage: 'del name|index|-f filename'");
					}
				} else if(t.equalsIgnoreCase("info")) {
					if(st.hasMoreTokens()) {
						this.instance.printNodeInformation(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'info hostname|index'");
					}
				} else if(t.equalsIgnoreCase("clean")) {
					if(st.hasMoreTokens()) {
						this.instance.cleanNodeStatus(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'clean hostname|index'");
					}
				} else if(t.equalsIgnoreCase("count")) { 
					this.instance.countActiveNodes();
				} else if(t.equalsIgnoreCase("kill")) {
					if(st.hasMoreTokens()) {
						this.instance.kill(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'kill node_index'");
					}
				} else if(t.equalsIgnoreCase("reset")) {
					if(st.hasMoreTokens()) {
						this.instance.reset(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'reset node_index'");
					}
				} else if(t.equalsIgnoreCase("filiation")) {
					if(st.hasMoreTokens()) {
						this.instance.filiation(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'filiation node_index'");
					}
				} else if(t.equalsIgnoreCase("bcast")) {
					if(st.countTokens() >= 2) {
						try {
							int index = Integer.parseInt(st.nextToken());
							String msg = st.nextToken();
							while(st.hasMoreTokens()) {
								msg = msg + " " + st.nextToken();
							}
							this.instance.broadcastMessage(index, msg);
						} catch (NumberFormatException e) {
							this.output("Invalid argument. Usage: 'bcast node_index message");
						}
					} else {
						this.output("Expecting Token. Usage: 'bcast node_index message");
					}
				} else if(t.equalsIgnoreCase("fbcast")) {
					if(st.countTokens() >= 3) {
						try {
							ArrayList<Integer> node = processArgument(st.nextToken());
							String source = st.nextToken();
							String dest = st.nextToken();
							
							if(node.size() == 1)
								this.instance.requestFileBroadcast(node.get(0), source, dest);
							else
								this.output("Invalid node.");
						} catch (NumberFormatException e) {
							this.output("Invalid argument. Usage: 'bcast node_index message");
						}
					} else {
						this.output("Expecting Token. Usage: 'bcast node_index message");
					}
				} else if(t.equalsIgnoreCase("checkfile")) {
					if(st.countTokens() == 2) {
						this.instance.checkFile(processArgument(st.nextToken()), st.nextToken());
					} else {
						this.output("Expecting Tokens. Usage: 'checkfile node filename'");
					}
				} else if(t.equalsIgnoreCase("deletefile")) {
					if(st.countTokens() == 2) {
						this.instance.deleteFile(processArgument(st.nextToken()), st.nextToken());
					} else {
						this.output("Expecting Tokens. Usage: 'deletefile node filename'");
					}
				} else if(t.equalsIgnoreCase("fbstatus")) {
					if(st.countTokens() == 2) {
						this.instance.fbcastStatus(processArgument(st.nextToken()), st.nextToken());
					} else {
						this.output("Expecting Tokens. Usage: 'fbstatus node session_id'");
					}
				} else if(t.equalsIgnoreCase("crash")) {
					if(st.hasMoreTokens()) {
						this.instance.crashRecover(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'crash node_index'");
					}
				} else if(t.equalsIgnoreCase("gupdate")) {
					if(st.hasMoreTokens()) {
						this.instance.updateStatus(processArgument(st.nextToken()));
					} else {
						this.output("Expecting Token. Usage: 'gupdate node_index'");
					}
				} else if(t.equalsIgnoreCase("set")) {
					String m = st.nextToken();
					if(m.equalsIgnoreCase("output")) {
						m = st.nextToken();
						if(m.equalsIgnoreCase("off")) {
							if(this.output != null) {
								this.output("Disabling secondary output.");
								this.output.close();
								this.output = null;
							}
						} else {
							this.output("Setting secondary output to file: '" + m + "'");
							try {
								this.output = new PrintStream(new File(m));
							} catch (FileNotFoundException e) {
								this.output("Cannot open file '" + m +"': " + e.getMessage());
								this.output = null;
							}
						}
					} else {
						this.output("Invalid usage. Usage: set output off|<file name>");
					}
				} else {
					this.output("Command not recognized.");
				}
				printprompt();
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			this.start();
		} catch (Throwable t) {
			t.printStackTrace(System.err);
			this.start();
		}
		
	}
	
	public ArrayList<Integer> processArgument(String argument) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(argument.equalsIgnoreCase("all")) {
			this.instance.loadAllActive(list);
		} 
		
		int x = -1;
		
		try {
			x = Integer.parseInt(argument);
		} catch (NumberFormatException e) {
			x = this.instance.lookupNodeIndex(argument);
		}
		
		if(x != -1)
			list.add(x);
		
		return list;
	}
	
	private PrintStream output;
	
	public synchronized void output(String s) {
		System.out.println();
		System.out.println(s);
		if(this.output != null)
			output.println(s);
	}
	
	public synchronized void printprompt() {
		System.out.print(prompt);
	}
	
}

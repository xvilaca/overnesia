package gsd.appl.control.components;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.messages.PingAlive;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.utils.SlaveState;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Slave {

	public final static short MAX_PENDING_PINGS = 10;
	
	private HyParViewLabTestControl instance;
	public SlaveState state;
	public String host;
	private Socket sock;
	public Writer writer;
	public Reader reader;
	private ArrayList<ExperienceInstance> onHold;
	private PingControler control;
	private long latency;
	private int pendings;
	
	public Slave(HyParViewLabTestControl instance, String host, Random r) {
		this.instance = instance;
		this.host = host;
		this.state = new SlaveState();	
		this.onHold = new ArrayList<ExperienceInstance>();
		this.control = new PingControler(this, this.state, r);
		this.latency = new Long(0);
		this.pendings = 0;
		
	}
	
	public boolean isOn() { 
		return state.isON();
	}

	public boolean connect() {
		//instance.debug(this, "Attempting connection to: " + host);
		this.pendings = 0;
		this.sock = new Socket();
		this.state.setStateCONNECTING();
		
		this.reader = new Reader(instance, this.sock, this, state, host);
		this.writer = new Writer(instance, this, state, host, this.sock, reader, writer != null ? writer.version_running : null);
		this.writer.start();
		return true;
	}
	
	public void activeCallback() {
		if(!this.control.isAlive())
			this.control.start();
	}
	
	public void inactiveCallback() {
		synchronized (this.onHold) {
			for(ExperienceInstance ei: this.onHold) {
				ei.notifyNodeFailure(this);
			}
			this.onHold.clear();
		}
	}
	
	public void stop() {
		this.state.setStateOFF();
		synchronized(this.writer.getQueue()) {
			this.writer.getQueue().notify();
		}
	}
	
	@Override
	public String toString() {
		String r = this.host;
		int spaces = 60 - r.length();
		for(int i = 0; i < spaces; i++)
			r = r + " ";
		
		r = r + state + "       " + "[ " + (writer != null ? writer.version_running : "" ) + " ]\t\t[ Latency: " + (this.latency == 0 ? "unknown" : this.latency + "ms") +  " ]\n";
		return r;
	}

	public void registerExperience(ExperienceInstance filiationExperience) {
		synchronized (this.onHold) {
			if(!this.onHold.contains(filiationExperience))
				this.onHold.add(filiationExperience);
		}	
	}

	public void unregisterExperience(ExperienceInstance filiationExperience) {
		synchronized (this.onHold) {
			this.onHold.remove(filiationExperience);
		}	
	}
	
	public void addToQueue(Object c) {
		synchronized (this.writer.getQueue()) {
			this.writer.getQueue().add(c);
			this.writer.getQueue().notify();
		}
	}

	public void performPingOperation() {
		if(this.state.isON()) {
			if(this.pendings >= Slave.MAX_PENDING_PINGS) {
				this.instance.debug(this, "Terminating connection with " + host + " (missed: " + this.pendings + ")");
				this.state.setStateOFF();
				this.writer.interrupt();
				this.reader.interrupt();
				this.pendings = 0; //You must reset this counter or this will never re-connect for long :P
				synchronized (instance.slaves) {
					instance.slaves.notify();
				}
				this.inactiveCallback();
				try { sock.close(); } catch (IOException e) { e.printStackTrace(); }
			} else {
				this.pendings++;
				this.addToQueue(new PingAlive());
			}
		}
	}
	
	public void receivedPingReply(PingAlive message) {	
		this.pendings--;
		this.latency = message.latency();
	}

	public long latency() {
		return this.latency;

	}
	
	public void clean() {
		this.latency = new Long(0);
		this.state.setStateOFF();
		this.writer.interrupt();
		this.reader.interrupt();
		this.pendings = 0; //You must reset this counter or this will never re-connect for long :P
		synchronized (instance.slaves) {
			instance.slaves.notify();
		}
		this.inactiveCallback();
		try { sock.close(); } catch (IOException e) { e.printStackTrace(); }
		this.writer = null;
		this.reader = null;
		this.connect();
	}

}

package gsd.appl.control.components;

import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;
import gsd.impl.hyparview.ImmortalHyParView;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

public class EnvironmentControl {
	
	private static Logger log = Logger.getLogger(EnvironmentControl.class);

	public static void stopOperation() {
		File f = new File("stop");
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				log.error("Cannot create 'stop' file.");
				log.error(e.getClass().getName());
			}
		}
		
		log.info("Deactivating node.");
		System.exit(0);
	}
	
	public static void resetOperation(OverlayNetwork overlay, Transport network) {
		log.info("Reseting node.");
		
		overlay.leave();
		
		File f = new File("restart");
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				log.error("Cannot create 'restart' file.");
				log.error(e.getClass().getName());
				System.exit(1);
			}
		}
		
		network.schedule(new Runnable() {
			public void run() {
				System.exit(0);
			}		
		}, 2000);
	}
	
	public static void crashRecover(ImmortalHyParView overlayInstance,
			Transport transport) {
		
		log.info("Crashing node after cleaning up...");
		File f = new File(ImmortalHyParView.recovery_filaname);
		if(f.exists()) f.delete();
		f = new File(ImmortalHyParView.cache_filename);
		if(f.exists()) f.delete();
		
		f = new File("restart");
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				log.error("Cannot create 'restart' file.");
				log.error(e.getClass().getName());
				System.exit(1);
			}
		}
		
		System.exit(0);
	}
	
}

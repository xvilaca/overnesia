package gsd.appl.control.components.messages;

import java.io.Serializable;

public class PingAlive implements Serializable {

	private static final long serialVersionUID = 6818089565430157401L;
	private long sent;
	private transient long recv;
	
	public PingAlive() {
		this.sent = 0;
	}
	
	public void timestampSend() {
		this.sent = System.currentTimeMillis();
	}
	
	public void timestampReceive() {
		this.recv = System.currentTimeMillis();
	}
	
	public long latency() {
		return (this.recv - this.sent) / 2;
	}
	
}

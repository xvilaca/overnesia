package gsd.appl.control.components;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.messages.PingAlive;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.experiences.data.DataRecord;
import gsd.appl.control.utils.SlaveState;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Reader extends Thread {

	public HyParViewLabTestControl instance;
	public Writer writer;
	public Socket sock;
	private Slave parent;
	public SlaveState state;
	public String host;
	public ObjectInputStream input;
	
	public Reader(HyParViewLabTestControl instance, Socket sock, Slave parent, SlaveState state, String host) {
		this.instance = instance;
		this.sock = sock;
		this.parent = parent;
		this.state = state;
		this.host = host;
	}
	
	public void registerWriter(Writer own) {
		this.writer = own;
	}
	
	public void registerInputStream(ObjectInputStream input) {
		this.input = input;
	}
	
	@Override
	public void run() {
		while(true) {			
			try{
				Object o = input.readObject();
				if(o instanceof PingAlive) {
				  ((PingAlive) o).timestampReceive();
				  parent.receivedPingReply((PingAlive) o);
				} else if(o instanceof DataRecord) {
					DataRecord dr = (DataRecord) o;
					dr.timestamp();
					ExperienceInstance ei = instance.experiences.get(dr.getExperienceID());
					if(ei != null) {
						synchronized (ei) {
							ei.registerDataRecord(dr);
						}
					} else {
						instance.cmdl.output(dr.toString());
						instance.cmdl.printprompt();
					}
					
				} else if(o instanceof String) {
					instance.cmdl.output("From: " + this.host + ":\n" + o); 
					instance.cmdl.printprompt();
				} else {
					instance.cmdl.output("From: " + this.host + ":\nUnknown message type: " + o.getClass().getCanonicalName() + "\n" + o);
					instance.cmdl.printprompt();
				}
			} catch (Exception e) {
				instance.cmdl.output("Lost connection to " + host + " (reason: " + e.getClass().getName() + ": " + e.getMessage() + ")");
				instance.debug(this, "Lost connection to " + host + " (reason: " + e.getClass().getName() + ": " + e.getMessage() + ")");
				state.setStateOFF();
				writer.interrupt();
				parent.inactiveCallback();
					
				try {input.close(); sock.close();} catch (IOException e1) {;}
				
				synchronized (instance.slaves) {	
					instance.slaves.notify(); 
				}
				return;
			} catch (Throwable t) { //This is also debug code that could (most probably) be removed.
				instance.cmdl.output("Lost connection to " + host + " (reason: " + t.getClass().getName() + ": " + t.getMessage() + ")");
				instance.debug(this, "Lost connection to " + host + " (reason: " + t.getClass().getName() + ": " + t.getMessage() + ")");
				state.setStateOFF();
				writer.interrupt();
				parent.inactiveCallback();
				
				try {input.close(); sock.close();} catch (IOException e1) {;}
				
				synchronized (instance.slaves) {	
					instance.slaves.notify(); 
				}	
				return;
			}
		}
	}
	
}

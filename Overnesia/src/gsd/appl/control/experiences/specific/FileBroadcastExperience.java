package gsd.appl.control.experiences.specific;

import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.experiences.data.DataBroadcastRequest;
import gsd.appl.control.experiences.data.DataRecord;
import gsd.appl.control.experiences.data.FileBroadcastNotification;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;

public class FileBroadcastExperience extends ExperienceInstance { 

	private int nodes;
	private long sendTime;
	private long lastReceivedReport;
	private int nodes_failed;
	private Slave source;
	private ArrayList<Slave> nodes_in_experience;
	
	private HashMap<NodeID, Long> results; 
	
	public FileBroadcastExperience(HyParViewLabTestControl instance, String name) {
		super(instance, name);
		this.nodes = 0;
		this.results = new HashMap<NodeID, Long>();
		this.nodes_failed = 0;
		this.nodes_in_experience = new ArrayList<Slave>();
	}
	
	@Override
	protected void releaseState() {
		this.nodes = 0;
		this.results.clear();
		this.results = null;
	}
	
	
	@Override
	public void dumpData(PrintStream out) {
		out.println("File upload request issued at: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.sendTime)) + "[" + this.sendTime + "]");
		out.println("Last report received at: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.lastReceivedReport)) + "[" + this.lastReceivedReport + "]");
		out.println("Source node: " + this.source);
		out.println("Total upload time: " + (this.lastReceivedReport - this.sendTime) + " ms");
		for(NodeID id: this.results.keySet()) {		
			out.println(id + " " + (this.results.get(id) - this.sendTime));
		}
		
		out.println("");
		out.println(" --- console-to-node latency report ---");
		out.println("");
		
		long min = this.source.latency();
		long max = this.source.latency();
		long sum = 0;
		
		for(Slave s: this.nodes_in_experience) {
			long l = s.latency();
			if(min > l) min = l;
			if(max < l) max = l;
			sum += l;
			out.println(s.host + " " + l + " ms");
		}
		
		out.println();
		out.println("Min: " + min + " Max: " + max + " Average: " + (((double)sum) / this.nodes_in_experience.size()));
	}

	@Override
	public synchronized void registerDataRecord(DataRecord rec) {
		if( (!(rec instanceof FileBroadcastNotification)) || this.interrupted ) return;
	
		this.results.put(rec.getSource(), rec.getReceptionTimestamp());	
		if(rec.getReceptionTimestamp() > this.lastReceivedReport)
			this.lastReceivedReport = rec.getReceptionTimestamp();
		this.nodes--;
		
		if(this.nodes == 0)
			this.end();
	}

	@Override
	protected void endOperation() {
		//Nothing to do I guess...
	}

	@Override
	protected void startOperation() {
		for(int i = 0; i < instance.slaves.size(); i++) {
			Slave node = instance.slaves.get(i);
			if(node.isOn()) {
				node.registerExperience(this);
				nodes_in_experience.add(node);
				this.nodes++;
			}
		}
		while(this.source == null || !this.source.isOn())
			this.source = nodes_in_experience.get(new Random(System.currentTimeMillis()).nextInt(nodes_in_experience.size()));
		
		if(!this.source.isOn()) {
			this.instance.cmdl.output("Source node is not online. Aborting...");
			return;
		}
		this.source.addToQueue(new DataBroadcastRequest(this.source.hashCode(), this.getID(), ByteBuffer.wrap(this.getID().toString().getBytes())));
		this.sendTime = System.currentTimeMillis();	
	}

	@Override
	public synchronized void notifyNodeFailure(Slave n) {
		this.nodes--;
		this.nodes_failed++;
		if(this.nodes == 0)
			this.end();
	}

	private boolean interrupted = false;
	
	@Override
	protected void interruptOperation() {
		for(Slave s: this.nodes_in_experience)
			s.unregisterExperience(this);
		this.interrupted = true;
	}

	@Override
	protected String getStatus() {
		return "Received " + this.results.keySet().size() + " answers. Waiting for " + this.nodes + " replies. (" + this.nodes_failed + " failed)";
	}

	@Override
	protected void setProperty(CommandLine commandLine, String propertyName,
			String newValue) {
		if(propertyName.equalsIgnoreCase("source")) {
			if(newValue.equalsIgnoreCase("random")) {
				this.source = null;
				return;
			}	
			ArrayList<Integer> arg = commandLine.processArgument(newValue);
			if(arg.size() != 1 || !instance.slaves.get(arg.get(0)).isOn() ) {
				commandLine.output("Invalid argument or node is down. [arg = " + newValue + "]");
			} else {
				this.source = instance.slaves.get(arg.get(0));
			}
		} else
			commandLine.output("Unknown property: " + propertyName);
	}
}

package gsd.appl.control.experiences.specific;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.experiences.data.DataRecord;
import gsd.appl.control.experiences.data.Filiation;
import gsd.appl.control.experiences.data.FiliationDataRecord;
import gsd.appl.control.experiences.data.FiliationRequest;
import gsd.common.NodeID;

public class FiliationExperience extends ExperienceInstance { 

	private int nodes;
	private HashMap<NodeID, Filiation> results; 
	private HashMap<NodeID, Integer> translation;
	
	public FiliationExperience(HyParViewLabTestControl instance, String name) {
		super(instance, name);
		this.nodes = 0;
		this.results = new HashMap<NodeID, Filiation>();
	}
	
	@Override
	protected void releaseState() {
		this.nodes = 0;
		this.results.clear();
		this.results = null;
		this.translation.clear();
		this.translation = null;
	}
	
	
	@Override
	public void dumpData(PrintStream out) {
		for(NodeID id: this.results.keySet()) {
			Filiation f = this.results.get(id);
			String line = this.translation.get(id) + " " + f.getActiveViewSize();
			Iterator<NodeID> i = f.getActiveViewIterator();
			while(i.hasNext()) {
				line = line + " " + this.translation.get(i.next());
			}
			line = line + " " + f.getPassiveViewSize();
			i = f.getPassiveViewIterator();
			while(i.hasNext()) {
				line = line + " " + this.translation.get(i.next());
			}
			out.println(line);
		}
		
		out.println("");
		out.println(" --- raw data ---");
		out.println("");
		
		for(NodeID id: this.results.keySet()) {
			Filiation f = this.results.get(id);
			out.println(id);
			out.println(f.getActiveViewSize());
			Iterator<NodeID> i = f.getActiveViewIterator();
			while(i.hasNext()) {
				out.println(i.next());
			}
			out.println(f.getPassiveViewSize());
			i = f.getPassiveViewIterator();
			while(i.hasNext()) {
				out.println(i.next());
			}
		}
		
		out.println("");
		out.println(" --- translation table ---");
		out.println("");
		
		for(NodeID id: this.translation.keySet()) {
			out.println(id + "            " + this.translation.get(id));
		}
	}

	@Override
	public synchronized void registerDataRecord(DataRecord rec) {
		if( this.interrupted ) return;
		
		instance.slaves.get(rec.getNodeIndex()).unregisterExperience(this);
		FiliationDataRecord data = (FiliationDataRecord) rec;
		this.results.put(data.getSource(), data.data);	
		this.nodes--;
		
		if(this.nodes == 0)
			this.end();
	}

	@Override
	protected void endOperation() {
		this.translation = new HashMap<NodeID, Integer>();
		int next = 0;
		for(NodeID id: this.results.keySet()) {
			if(!this.translation.containsKey(id)) {
				this.translation.put(id, next);
				next++;
			}
		}
		
		for(NodeID id: this.results.keySet()) {
			Iterator<NodeID> i = this.results.get(id).getActiveViewIterator();
			while(i.hasNext()) {
				NodeID x = i.next();
				if(!this.translation.containsKey(x)) {
					this.translation.put(x, next);
					next++;
				}
					
			}
			
			i = this.results.get(id).getPassiveViewIterator();
			while(i.hasNext()) {
				NodeID x = i.next();
				if(!this.translation.containsKey(x)) {
					this.translation.put(x, next);
					next++;
				}
					
			}
		}
	}

	@Override
	protected void startOperation() {
		for(int i = 0; i < instance.slaves.size(); i++) {
			Slave node = instance.slaves.get(i);
			if(node.isOn()) {
				this.nodes++;
				node.registerExperience(this);
				node.addToQueue(new FiliationRequest(i, this.getID()));
			}
		}
	}

	@Override
	public synchronized void notifyNodeFailure(Slave n) {
		this.nodes--;
		if(this.nodes == 0)
			this.end();
	}

	private boolean interrupted = false;
	
	@Override
	protected void interruptOperation() {
		for(Slave s: instance.slaves)
			s.unregisterExperience(this);
		this.interrupted = true;
	}

	@Override
	protected String getStatus() {
		return "Received " + this.results.keySet().size() + " answers. Waiting for " + this.nodes + " replies.";
	}
	
	@Override
	protected void setProperty(CommandLine commandLine, String propertyName,
			String newValue) {
		commandLine.output("Unknown property: " + propertyName);
	}
}

package gsd.appl.control.experiences.specific;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;
import gsd.appl.control.experiences.ExperienceInstance;
import gsd.appl.control.experiences.data.BroadcastStatistics;
import gsd.appl.control.experiences.data.DataRecord;
import gsd.appl.control.experiences.data.StartBroadcastExperienceReply;
import gsd.appl.control.experiences.data.StartBroadcastExperienceRequest;
import gsd.common.NodeID;

import java.io.PrintStream;
import java.util.ArrayList;

public class BroadcastExperience extends ExperienceInstance { 

	private double p; //probability to broadcast in one round
	private long d; //duration of each round in milliseconds (default is 100)
	private long r; //number of rounds (default is 1000)
	private short port; //Communication port to be used
	private ArrayList<NodeID> elements;
	private ArrayList<NodeID> received_report;
	private ArrayList<NodeID> failed;
	
	public BroadcastExperience(HyParViewLabTestControl instance, String name) {
		super(instance, name);
		this.p = 0.01;
		this.d = 100;
		this.r = 1000;
		this.port = 234;
		this.elements = new ArrayList<NodeID>();
		this.received_report = new ArrayList<NodeID>();
		this.failed = new ArrayList<NodeID>();
	}
	
	@Override
	protected void releaseState() {
		this.elements.clear();
		this.elements = null;
		this.received_report.clear();
		this.received_report = null;
		this.failed.clear();
		this.failed = null;
	}
	
	
	@Override
	public void dumpData(PrintStream out) {
		out.println("Experience started with: " + (this.elements.size() + this.received_report.size() + this.failed.size()) + " nodes");
		out.println("Experience ended with: " + (this.received_report.size()) + " nodes");
		out.println("Failed nodes: [" + this.failed.size() + "]");
		for(NodeID n: this.failed) {
			out.println(n.toString());
		}
		out.println("Missing nodes: [" + this.elements.size() + "]");
		for(NodeID n: this.elements) {
			out.println(n.toString());
		}
		out.println("Received reports from: [" + this.received_report.size() + "]");
		for(NodeID n: this.received_report) {
			out.println(n.toString());
		}
	}

	@Override
	public synchronized void registerDataRecord(DataRecord record) {
		if( this.interrupted ) return;
		
		if(record instanceof StartBroadcastExperienceReply) {
			this.elements.add(record.getSource());
		} else if(record instanceof BroadcastStatistics) {
			BroadcastStatistics r = (BroadcastStatistics) record;
			this.elements.remove(r.getSource());
			this.received_report.add(r.getSource());
		} else {
			this.elements.remove(record.getSource());
			this.received_report.remove(record.getSource());
			this.failed.add(record.getSource());
		}
		
	
		if(this.elements.size() == 0)
			this.end();
	}

	@Override
	protected void endOperation() {
		instance.cmdl.output("Unable to perform such operation in this Experience");
	}

	@Override
	protected void startOperation() {
		for(int i = 0; i < instance.slaves.size(); i++) {
			Slave node = instance.slaves.get(i);
			if(node.isOn()) {
				node.addToQueue(new StartBroadcastExperienceRequest(i, this.getID(), this.p, this.d, this.r, this.port));
			}
		}
	}

	@Override
	public synchronized void notifyNodeFailure(Slave n) {
		NodeID target = null;
		for(int i = 0; target == null && i < this.elements.size(); i++) {
			if(this.elements.get(i).getSocketAddress().getAddress().getHostName().equals(n.host))
					target = this.elements.remove(i);
		}
		if(target != null) {
			this.failed.add(target);
		}
	}

	private boolean interrupted = false;
	
	@Override
	protected void interruptOperation() {
		for(Slave s: instance.slaves)
			s.unregisterExperience(this);
		this.interrupted = true;
	}

	@Override
	protected String getStatus() {
		return "Received " + this.received_report.size() + " answers. Waiting for " + this.elements.size() + " replies.";
	}
	
	@Override
	protected void setProperty(CommandLine commandLine, String propertyName,
			String newValue) {
		if(propertyName.equalsIgnoreCase("p"))
			try {
				this.p = Double.parseDouble(newValue);
			} catch (NumberFormatException e) {
				commandLine.output("parameter 'p' must receive a double [invalid number format].");
			}
		else if(propertyName.equalsIgnoreCase("d"))
			try {
				this.d = Long.parseLong(newValue);
			} catch (NumberFormatException e) {
				commandLine.output("parameter 'd' must receive a long [invalid number format].");
			}
		else if(propertyName.equalsIgnoreCase("r"))
			try {
				this.r = Long.parseLong(newValue);
			} catch (NumberFormatException e) {
				commandLine.output("parameter 'r' must receive a long [invalid number format].");
			}
		else if(propertyName.equalsIgnoreCase("port"))
			try {
				this.port = Short.parseShort(newValue);
			} catch (NumberFormatException e) {
				commandLine.output("parameter 'port' must receive a short [invalid number format].");
			}
		else
			commandLine.output("Unknown property: " + propertyName + ". Valid properties are: p, d, r, port");
	}
}

package gsd.appl.control.experiences;

import gsd.appl.HyParViewLabTestControl;
import gsd.appl.control.components.CommandLine;
import gsd.appl.control.components.Slave;
import gsd.appl.control.experiences.data.DataRecord;
import gsd.appl.control.experiences.specific.BroadcastExperience;
import gsd.appl.control.experiences.specific.FileBroadcastExperience;
import gsd.appl.control.experiences.specific.FiliationExperience;
import gsd.appl.control.experiences.specific.SimpleBroadcastExperience;
import gsd.appl.utils.DateTimeRepresentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public abstract class ExperienceInstance {

	private String experienceName;
	private UUID experienceID;
	private PrintStream output;
	private long start;
	private long end;
	protected HyParViewLabTestControl instance;
	private CommandLine cmdl;
	
	public ExperienceInstance(HyParViewLabTestControl instance, String name) {
		this.experienceID = UUID.randomUUID();
		this.experienceName = name;
		this.output = System.out;
		this.instance = instance;
		this.start = 0;
		this.end = 0;
	}
	
	public void setOutput(PrintStream output) {
		this.output = output;
	}
	
	public boolean isIt(String name) {
		return this.experienceName.equalsIgnoreCase(name.trim());
	}
	
	public void execute() {
		this.start = DateTimeRepresentation.currentTime();
		this.startOperation();
	}
	
	public void stopExecution() {
		this.end = DateTimeRepresentation.currentTime();
		this.interruptOperation();
	}
	
	public void releaseAll() {
		this.experienceName = null;
		this.experienceID = null;
		this.output = null;
		this.instance = null;
		this.releaseState();
	}
	
	public void end() {
		this.end = DateTimeRepresentation.currentTime();
		this.cmdl.output("Experience is now terminated.");
		this.endOperation();
		this.cmdl.output(this.status());
		this.output();
		this.cmdl.output("Output is written.");
		this.cmdl.printprompt();
	}
		
	public final void output() {
		if(this.end == 0) {
			this.cmdl.output("Experience is not terminated. Output was not written.");
			this.cmdl.printprompt();
			return;
		}
		this.output.println("Experience: " + this.experienceName + " [ unique id: " + this.experienceID + " ]");
		this.output.println("Start: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.start)) + " End: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.end)) + " Total duration: " + DateTimeRepresentation.timeElapsed(this.start, this.end) + " miliseconds");
		this.output.println("");
		this.dumpData(this.output);
	} 
	
	public final String status() {
		long now = this.end == 0 ? DateTimeRepresentation.currentTime() : this.end;
		return "Experience: " + this.experienceName + " [ unique id: " + this.experienceID + " ]\n" + 
			"Start: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.start)) + 
			(this.end == 0 ? "" : "End: " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.end))) +
			" Total duration: " + DateTimeRepresentation.timeElapsed(this.start, now) + " miliseconds\n" +
			this.getStatus();
	}
	
	abstract public void registerDataRecord(DataRecord rec);
	
	abstract public void dumpData(PrintStream out);
	
	abstract protected void startOperation();
	
	abstract protected void endOperation();
	
	abstract protected void interruptOperation();
	
	abstract protected String getStatus();
	
	abstract protected void releaseState();
	
	
	
	public static ExperienceInstance createNewExperience(HyParViewLabTestControl instance, String type, String name) {
		if(type.equalsIgnoreCase("bcast")) {
			return new SimpleBroadcastExperience(instance, name);
		} else if(type.equalsIgnoreCase("fbcast")) {
			return new FileBroadcastExperience(instance, name);
		} else if(type.equalsIgnoreCase("filiation")) {
			return new FiliationExperience(instance, name);
		} else if(type.equalsIgnoreCase("broadcast")) {
			return new BroadcastExperience(instance, name);
		} else 
			return null;
	}
	
	public abstract void notifyNodeFailure(Slave n);

	public static String validTypes() {
		return "filiation";
	}

	public UUID getID() {
		return this.experienceID;
	}

	public void interactiveMode(CommandLine commandLine, Scanner sc) {
		if(this.cmdl == null) this.cmdl = commandLine;
	
		String cmd = "";
		while(true) {
			commandLine.printprompt();
			cmd = sc.nextLine();
			cmd = cmd.trim();
			if(cmd.equals(""))
				return;
			StringTokenizer st = new StringTokenizer(cmd, " ");
			String x = st.nextToken();
			if(x.equalsIgnoreCase("exit")) {
				break;
			} else if(x.equalsIgnoreCase("set")) {
				x = st.nextToken();
				if(x.equalsIgnoreCase("output")) {
					x = st.nextToken();
					if(x.equalsIgnoreCase("std")) {
						commandLine.output("Setting output to screen.");
						this.setOutput(System.out);
					} else {
						commandLine.output("Setting output to file: '" + x + "'");
						try {
							this.setOutput(new PrintStream(new File(x)));
						} catch (FileNotFoundException e) {
							commandLine.output("Cannot open file '" + x +"': " + e.getMessage());
							continue;
						}
					}
				} else if (x.equalsIgnoreCase("property") && st.countTokens() == 2) {
					this.setProperty(commandLine, st.nextToken(), st.nextToken());
				} else {
					commandLine.output("Invalid usage. Usage: 'set output std|<file name>' or 'set property name value'");
				}
			} else if (x.equalsIgnoreCase("start")) {
				commandLine.output("starting experience");
				this.execute();
			} else if (x.equalsIgnoreCase("stop")) {
				commandLine.output("interrupting experience");
				this.stopExecution();
			} else if (x.equalsIgnoreCase("output")) {
				this.output();
			} else if (x.equalsIgnoreCase("status")) {
				commandLine.output(this.status());
			} else {
				commandLine.output("Unrecognized comaand: " + cmd);
			}
		}
		commandLine.output("Exiting experience interactive mode.");
		commandLine.printprompt();			
	}
	
	protected abstract void setProperty(CommandLine commandLine, String propertyName, String newValue);
	
	@Override
	public String toString() {
		return this.getClass().getName() + "\t\t" + this.experienceName + "\t[ " + this.experienceID + " ]";
	}
	
}

package gsd.appl.control.experiences.data;

import java.io.Serializable;
import java.util.UUID;

public abstract class Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8439835337786099327L;
	private int nodeIndex;
	private UUID experienceID;
	protected short operCode;
	
	public Request(int nodeIndex, UUID experienceID, short operCode) {
		this.nodeIndex = nodeIndex;
		this.experienceID = experienceID;
		this.operCode = operCode;
	}
	
	public int getNodeIndex() {
		return this.nodeIndex;
	}
	
	public UUID getExperinceID() {
		return this.experienceID;
	}
	
	public short getOperationCode() {
		return this.operCode;
	}
	
	@Override
	public String toString() {
		return "Request operation for experience " + this.experienceID + " type: " + this.getClass().getName();
	}
	
}

package gsd.appl.control.experiences.data;

import gsd.common.NodeID;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Filiation implements Serializable {

	private static final long serialVersionUID = 8502529062522726798L;
	
	private ArrayList<NodeID> activeView;
	private ArrayList<NodeID> passiveView;
	
	public Filiation() {
		this.activeView = new ArrayList<NodeID>();
		this.passiveView = new ArrayList<NodeID>();
	}
	
	public void fillActiveView(Collection<NodeID> c) {
		this.activeView.addAll(c);
	}
	
	public void fillPassiveView(Collection<NodeID> c) {
		this.passiveView.addAll(c);
	}
	
	public int getActiveViewSize() {
		return this.activeView.size();
	}
	
	public int getPassiveViewSize() {
		return this.passiveView.size();
	}
	
	public Iterator<NodeID> getActiveViewIterator() {
		return this.activeView.iterator();
	}
	
	public Iterator<NodeID> getPassiveViewIterator() {
		return this.passiveView.iterator();
	}

	public void fillPassiveView(NodeID[] passivePeersPresentInfo) {
		for(NodeID n : passivePeersPresentInfo) {
			if(n != null)
				this.passiveView.add(n);
		}
		
	}
	
	@Override
	public String toString() {
		return "Active view size: " + this.activeView.size() + " Passive view size: " +  this.passiveView.size();
	}
}

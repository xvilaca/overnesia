package gsd.appl.control.experiences.data;

import java.io.Serializable;
import java.util.UUID;

public class FiliationRequest extends Request implements Serializable {
	
	private static final long serialVersionUID = -7810694106370586273L;

	public final static short operCode = 0;
	
	public FiliationRequest(int nodeIndex, UUID experienceID) {
		super(nodeIndex, experienceID, FiliationRequest.operCode);
	}

}

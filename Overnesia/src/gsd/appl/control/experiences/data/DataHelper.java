package gsd.appl.control.experiences.data;

import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.UUID;

public class DataHelper {

	public static ByteBuffer[] encode(DataBroadcastRequest item) {
		ByteBuffer[] msg = new ByteBuffer[2];	
		msg[0] = ByteBuffer.allocate(2+4+16+4);
		msg[0].putShort(DataBroadcastRequest.operCode);
		//System.err.println("Manual serialization - in goes: " + DataBroadcastRequest.operCode + " (short)");
		msg[0].putInt(item.getNodeIndex());
		//System.err.println("Manual serialization - in goes: " + item.getNodeIndex() + " (int)");
		UUIDs.writeUUIDToBuffer(msg[0], item.getExperinceID());
		//System.err.println("Manual serialization - in goes: " + item.getExperinceID() + " (UUID)");
		msg[1] = item.getData().asReadOnlyBuffer();
		msg[0].putInt(msg[1].capacity());
		//System.err.println("Manual serialization - in goes: " + msg[1].capacity()+ " (int)");
		msg[0].flip();
		//msg[1].flip();
		return msg;
	}
	
	private static DataBroadcastRequest decodeDBR(ByteBuffer[] buffer) {
		ByteBuffer b = Buffers.sliceCompact(buffer, 4+16+4);
		int index = b.getInt();
		//System.err.println("Manual serialization - out comes: " + index + " (int)");
		UUID id = UUIDs.readUUIDFromBuffer(b);
		//System.err.println("Manual serialization - out comes: " + id + " (UUID)");
		int size = b.getInt();
		//System.err.println("Manual serialization - out comes: " + size + " (int)");
		return new DataBroadcastRequest(index, id, Buffers.sliceCompact(buffer, size));
	}
	
	public static Request decode(ByteBuffer[] buffer) {
		short s = Buffers.sliceCompact(buffer, 2).getShort(); 
		//System.err.println("Manual serialization - out comes: " + s + " (short)");
		switch (s) {
			case DataBroadcastRequest.operCode:
				return DataHelper.decodeDBR(buffer);
			default:
				return null;
		}	
	}
	
	
}

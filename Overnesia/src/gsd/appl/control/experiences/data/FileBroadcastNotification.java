package gsd.appl.control.experiences.data;

import gsd.common.NodeID;

import java.io.Serializable;
import java.util.UUID;

public class FileBroadcastNotification extends DataRecord implements Serializable {

	private static final long serialVersionUID = -1708717998451733792L;
	private String filename;
	private long filesize;
	
	public FileBroadcastNotification(UUID eid, NodeID nid, int nodeIndex, String filename, long filesize) {
		super(eid, nid, nodeIndex);
		this.filename = filename;
		this.filesize = filesize;
	}

	@Override
	public String contentToString() {
		return this.filename + " (" + this.filesize + " bytes)";
	}
	
}

package gsd.appl.control.experiences.data;

import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

public class BroadcastStatistics extends DataRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3571816452343569684L;

	private ArrayList<String> records;
	
	public BroadcastStatistics(UUID eID, NodeID nodeID, int nodeIndex) {
		super(eID, nodeID, nodeIndex);
		this.records = new ArrayList<String>();
	}
	
	public BroadcastStatistics(UUID eID, NodeID nodeID, int nodeIndex, ArrayList<String> records) {
		super(eID, nodeID, nodeIndex);
		this.records = records;
	}
	
	public void registerBCastStart(String msgID) {
		this.records.add("I " + msgID + " " + DateTimeRepresentation.timeStamp());
	}
	
	public static void registerBCastStart(String msg_id, PrintStream out) {
		out.println("I " + msg_id + " " + DateTimeRepresentation.timeStamp());
	}

	public void registerBCastReceive(String msgID) {
		this.records.add("R " + msgID + " " + DateTimeRepresentation.timeStamp());
	}

	public static void registerBCastReceive(String msgID, PrintStream out) {
		out.println("R " + msgID + " " + DateTimeRepresentation.timeStamp());
	}
	
	public synchronized void dumpRecord() throws FileNotFoundException{
		File out = new File(this.getExperienceID() + "-" + this.getSource() + ".bcast.log");
		PrintStream ps = new PrintStream(new FileOutputStream(out, true));
		while(this.records.size() > 0)
			ps.println(this.records.remove(0));
		ps.close();
	}

	@Override
	public String contentToString() {
		return "Contains " + this.records + " individual records for node " + this.getSource().toString();
	}

	public static Object create(UUID experienceID, NodeID gossipNodeID,
			int node_ex_index, String register) {
		ArrayList<String> records = new ArrayList<String>();
		records.add(register);
		return new BroadcastStatistics(experienceID, gossipNodeID, node_ex_index, records);
	}
	
	public static Object create(UUID experienceID, NodeID gossipNodeID,
			int node_ex_index, File logFile) {
		ArrayList<String> records = new ArrayList<String>();
		try {
			Scanner sc = new Scanner(logFile);
			while(sc.hasNextLine())
				records.add(sc.nextLine());
			sc.close();
			logFile.delete();
		} catch (FileNotFoundException e) {
			//nothing to do... keep the file
		}
		return new BroadcastStatistics(experienceID, gossipNodeID, node_ex_index, records);
	}

	public static Object create(UUID experienceID, NodeID gossipNodeID,
			int node_ex_index) {
		return new BroadcastStatistics(experienceID, gossipNodeID, node_ex_index, new ArrayList<String>());
	}

	public int RegistersCount() {
		if(this.records != null)
			return this.records.size();
		else
			return 0;
	}

	public void mergeRegisters(BroadcastStatistics r) {
		if(this.records == null)
			this.records = r.records;
		else
			for(int i = 0; i < r.records.size(); i++)
				this.records.add(r.records.remove(0));
	}
}

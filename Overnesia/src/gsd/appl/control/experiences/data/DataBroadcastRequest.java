package gsd.appl.control.experiences.data;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.UUID;

public class DataBroadcastRequest extends Request implements Serializable {

	private static final long serialVersionUID = -6724278225771518811L;

	public final static short operCode = 1;
	
	private byte[] data;
	
	public DataBroadcastRequest(int nodeIndex, UUID experienceID, ByteBuffer data) {
		super(nodeIndex, experienceID, DataBroadcastRequest.operCode);
		System.err.println("Capacity of data buffer: " + data.remaining());
		this.data = new byte[data.remaining()];
		data.get(this.data);
	}
	
	public ByteBuffer getData() {
		return this.data == null? null : ByteBuffer.wrap(this.data);
	}

}

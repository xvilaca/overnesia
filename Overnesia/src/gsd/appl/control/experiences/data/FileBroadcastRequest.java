package gsd.appl.control.experiences.data;

import java.io.Serializable;
import java.util.UUID;

public class FileBroadcastRequest extends Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4665807419732248650L;

	public final static short operCode = 2;
	
	private String source;
	private String dest;
	
	public FileBroadcastRequest(UUID experienceID, String source, String dest) {
		super(0, experienceID, FileBroadcastRequest.operCode);
		this.source = source;
		this.dest = dest;
	
	}
	
	public String getSourceFilename() {
		return this.source;
	}
	
	public String getDestFilename() {
		return this.dest;
	}
	
	@Override
	public String toString() {
		return this.source+":"+this.dest+":"+this.getExperinceID();
	}
}

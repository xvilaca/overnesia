package gsd.appl.control.experiences.data;

import gsd.common.NodeID;

import java.io.Serializable;
import java.util.UUID;

public class FiliationDataRecord extends DataRecord implements Serializable {

	private static final long serialVersionUID = 1003899143808243345L;
	public Filiation data;
	
	public FiliationDataRecord(UUID eid, NodeID nid, int nodeIndex) {
		super(eid, nid, nodeIndex);
		this.data = new Filiation();
	}

	@Override
	public String contentToString() {
		return data.toString();
	}
	
	
}

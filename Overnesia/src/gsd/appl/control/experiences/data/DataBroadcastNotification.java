package gsd.appl.control.experiences.data;

import gsd.common.NodeID;

import java.io.Serializable;
import java.util.UUID;

public class DataBroadcastNotification extends DataRecord implements Serializable {

	private static final long serialVersionUID = -1708717998451733792L;
	
	
	
	public DataBroadcastNotification(UUID eid, NodeID nid, int nodeIndex) {
		super(eid, nid, nodeIndex);
	}

	@Override
	public String contentToString() {
		return "";
	}
	
}

package gsd.appl.control.experiences.data;

import gsd.common.NodeID;

import java.io.Serializable;
import java.util.UUID;

public class StartBroadcastExperienceReply extends DataRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4574077183768409970L;

	public StartBroadcastExperienceReply (UUID eID, NodeID nID, int index) {
		super(eID, nID, index);
	}
	
	@Override
	public String contentToString() {
		return this.getSource() + " [" + this.getNodeIndex() + "] has acknoledged experience " + this.getExperienceID();
	}

}

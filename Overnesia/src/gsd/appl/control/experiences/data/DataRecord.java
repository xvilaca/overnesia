package gsd.appl.control.experiences.data;

import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;

import java.io.Serializable;
import java.util.UUID;

public abstract class DataRecord  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7192257504597894170L;
	private UUID experienceID;
	private NodeID source;
	private int nodeIndex;
	private transient long receivedTime;
	
	public DataRecord(UUID eID, NodeID nID, int nodeIndex) {
		this.experienceID = eID;
		this.source = nID;
		this.nodeIndex = nodeIndex;
	}
	
	public NodeID getSource() {
		return this.source;
	}
	
	public UUID getExperienceID() {
		return this.experienceID;
	}

	public void timestamp() {
		this.receivedTime = System.currentTimeMillis();
	}
	
	public long getReceptionTimestamp() {
		return this.receivedTime;
	}
	
	public int getNodeIndex() {
		return nodeIndex;
	}
	
	@Override
	public final String toString() {
		return this.getClass().getName() + " Source: " + this.source + " experienceID: " + this.experienceID + " nodeIndes: " + this.nodeIndex + " " + DateTimeRepresentation.convertTimeStamp(DateTimeRepresentation.convert(this.receivedTime)) + "\n" + this.contentToString();
	}
	
	public abstract String contentToString();
}

package gsd.appl.control.experiences.data;

import java.io.Serializable;
import java.util.UUID;

public class StartBroadcastExperienceRequest extends Request implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8643979319956501609L;

	public final static short operCode = 3;
	public double p;
	public long d;
	public long r;
	public short port;
	
	public StartBroadcastExperienceRequest(int nodeIndex, UUID experienceID, double p, long d, long r, short port) {
		super(nodeIndex, experienceID, StartBroadcastExperienceRequest.operCode);
		this.p = p;
		this.d = d;
		this.r = r;
		this.port = port;
	}
	
	

}

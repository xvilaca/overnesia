package gsd.appl;

import gsd.api.HyParViewPLabP2PChannel;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.impl.Config;
import gsd.impl.Connection;
import gsd.impl.Periodic;
import gsd.impl.hyparview.ImmortalHyParView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class NonInteractiveHyParView extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private HyParViewPLabP2PChannel channel;
	
	private Periodic aliveOperation;
	
	public long epoch;
	
	private PrintStream out; 
	private PrintStream overlay;
	
	private Thread cmdLine;
	
	public short filiationRequestID = 1;
	
	private Timer timer;
	
	public NonInteractiveHyParView(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException, FileNotFoundException {
		System.setErr(System.out);
		
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new HyParViewPLabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		//Use this for one hour period: 60 * 60 * 1000
		//Use this for 10 minute period: 60 * 10 * 1000
		//Use this for 5 minute period: 60 * 5 * 1000
		//Use this for 1 minute period: 60 * 1000
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		

		this.initializeFiles();
	}
	
	public NonInteractiveHyParView(InetSocketAddress myId) throws UnknownHostException, FileNotFoundException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			System.out.println("Before");
			this.channel = new HyParViewPLabP2PChannel(this.myId);
			System.out.println("After");
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.aliveOperation = new Periodic(channel.getRand(), channel.getTransport(), 60 * 60 * 1000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.aliveOperation.start();
		
		this.epoch = 0;		
	
		this.initializeFiles();
	}
	
	private void initializeFiles() throws FileNotFoundException {
		this.out = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_alive.log"));
		this.overlay = new PrintStream(new FileOutputStream("./" + this.myId.getHostName() + "_overlay.log"));
		this.addToOutLog("ACTIV", myId.getHostName() + " " + myId.getAddress() + ":" + myId.getPort());
	}

	public void addToOutLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.out.println(line);
	}
	
	
	public void addToOverlayLog(String oper, String msg) {
		String line = oper + " " + DateTimeRepresentation.timeStamp() + " " + msg;
		//System.out.println(line);
		this.overlay.println(line);
	}
	
	public boolean debugOperation() {
		this.addToOutLog("ALIVE", channel.getOverlayInstance().getNodeIdentifier() + " " + epoch);
		this.out.flush();
		this.overlay.flush();
		return true;
	}
	
	private void bcastFiliationRequest() {
		addToOutLog("BCSND", "Filiation Request");
		ByteBuffer msg = ByteBuffer.allocate(2 + 38 + 8);
		msg.putShort(filiationRequestID);
		NodeID.writeNodeIdToBuffer(msg, channel.getOverlayInstance().getNodeIdentifier());
		msg.putLong(epoch);
		msg.flip();
		channel.broadcast(new ByteBuffer[]{msg});
	}	
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		channel.connect(this.contactId);
		if(this.contactId == null) {
			cmdLine = new Thread() {
				@Override
				public void run() {
					BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
					String cmd;
					try {
						while(true) {
							cmd = r.readLine();
							if(cmd != null) {
								if(cmd.equalsIgnoreCase("bcast")) {
									bcastFiliationRequest();
								} else if(cmd.equalsIgnoreCase("start")) {
									if(timer == null) {
										timer = new Timer();
										timer.schedule(new TimerTask(){
											@Override
											public void run() {
												bcastFiliationRequest();
											}
										}, 0, 5 * 60 * 1000);
									} else 
										System.out.println("Current application is already in 'Started' state.");
								} else if(cmd.equalsIgnoreCase("stop")) {
									if(timer != null) {
										timer.cancel();
										timer = null;
									} else 
										System.out.println("Current application is already in 'Stopped' state.");
								} else
									System.out.println("Unrecognized Command: " + cmd);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("Input cmd line is off...");
					}
				}

				
			};
			cmdLine.start();
		}
		
		try {
			while(true) {
				ByteBuffer bb = ByteBuffer.allocate(1024);
				channel.read(bb);
				bb.rewind();
				short op_code = bb.getShort();
				if(op_code == this.filiationRequestID) {
					NodeID sender = NodeID.readNodeIDFromBuffer(bb);
					long recv_epoch = bb.getLong();
					recv_epoch++;
					if(this.epoch < recv_epoch)
						this.epoch = recv_epoch;
					this.addToOutLog("BCRCV", "Filiation Request " + sender );
					this.printFiliation(sender);
				} else {
					this.addToOutLog("ERROR", "Unrecognized operation code received: " + op_code);
				}
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	private void printFiliation(NodeID sender) {
		ImmortalHyParView ovlay = channel.getOverlayInstance();
		Set<NodeID> activeView = ovlay.getNeighbors().keySet(); 
		NodeID[] passiveView = ovlay.getPassivePeersPresentInfo();
		
		int passiveSize = 0;
		for(NodeID id: passiveView)
			if(id != null)
				passiveSize++;
		
		String line = this.epoch + "\n" + ovlay.getNodeIdentifier() + "\n" 
			+ activeView.size() + " " + passiveSize + "\n";
		for(NodeID id: activeView)
			line = line + id + "\n";
		for(NodeID id: passiveView)
			if(id != null) line = line + id + "\n";
		line = line + ovlay.connections().length + " " + ovlay.network().connections().length + " " + ovlay.pendingConns();
		if(ovlay.connections().length != (ovlay.network().connections().length - 1))
			for(Connection c: ovlay.network().connections())
				line = line + "\n    -- " + c.id + " " + c.getPeer();
		this.addToOverlayLog("OVFIL", line);

	}

	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), 28371);
		InetSocketAddress contact = new InetSocketAddress("146.193.41.16",28371);
	
		try {
			if(local.getAddress().equals(contact.getAddress())) {
				System.out.println("Primary node is on-line.");
				new NonInteractiveHyParView(local).execute();
			} else {
				System.out.println("Node is on-line.");
				new NonInteractiveHyParView(local, contact).execute();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
	}

}

package gsd.appl;

import gsd.api.P2PTestChannel;
import gsd.impl.Config;
import gsd.impl.Periodic;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class NonInteractiveTestApplication extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private P2PTestChannel channel;
	
	private Periodic DebugOperation;
	
	public NonInteractiveTestApplication(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException {
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new P2PTestChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
					
	}
	
	public NonInteractiveTestApplication(InetSocketAddress myId) throws UnknownHostException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new P2PTestChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
	
	}
	
	public boolean debugOperation() {
		System.out.println("Alive...");
		
		return true;
	}
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		if(this.contactId != null)
			System.out.println("Contact id: " + this.contactId.toString());
		else
			System.out.println("Contact id: [NONE]");
		
		//Initialize the overlay network level
		channel.connect(this.contactId);
		
		try {
			while(true) {
				byte[] buf = new byte[1000];
				ByteBuffer bb = ByteBuffer.wrap(buf);
				channel.read(bb);
				System.out.println((new String(buf)).trim());
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), Integer.parseInt(args[0]));
		InetSocketAddress contact = new InetSocketAddress(InetAddress.getLocalHost(), 10000);
		
		if(local.getPort() == contact.getPort()) {
			System.out.println("Primary node is on-line.");
			new NonInteractiveTestApplication(local).execute();
		} else {
			System.out.println("Node is on-line.");
			new NonInteractiveTestApplication(local, contact).execute();
		}
		
	}

}

package gsd.appl;

import gsd.api.HyParViewPLabP2PChannel;
import gsd.common.NodeID;
import gsd.impl.Config;
import gsd.impl.Connection;
import gsd.impl.OverlayNetwork;
import gsd.impl.Periodic;
import gsd.impl.hyparview.HyParView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class NonInteractiveProtoBCastApplication extends Config{
	
	private InetSocketAddress myId;
	private InetSocketAddress contactId;
	
	private HyParViewPLabP2PChannel channel;
	
	private Periodic DebugOperation;
	
	public NonInteractiveProtoBCastApplication(InetSocketAddress myId, InetSocketAddress contact) throws UnknownHostException {
		this.contactId = contact;
		this.myId = myId;
	
		try {
			this.channel = new HyParViewPLabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
					
	}
	
	public NonInteractiveProtoBCastApplication(InetSocketAddress myId) throws UnknownHostException {
		this.contactId = null;
		this.myId = myId;
	
		try {
			this.channel = new HyParViewPLabP2PChannel(this.myId);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		this.DebugOperation = new Periodic(channel.getRand(), channel.getTransport(), 10000, 0) {
			public void run() {
				debugOperation();
			}
		};
		this.DebugOperation.start();
	
	}
	
	public boolean debugOperation() {
		OverlayNetwork overlay = channel.getOverlayInstance();
		HashMap<NodeID, Connection> activeView = overlay.getNeighbors();
		NodeID[] passiveView = ((HyParView)overlay).getPassivePeersPresentInfo();
		
		System.err.println("ID: " + overlay.getNodeIdentifier());
		System.err.println("Active View Size: " + activeView.size());
		System.err.println("Passive View Size: " + passiveView.length);
		System.err.println("Active View:");
		for(NodeID id: activeView.keySet())
			System.err.println("  " + id);
		System.err.println("Passive View:");
		for(NodeID id: passiveView)
			System.err.println("  " + id);
		System.err.println("Number of TCP Connections: " + overlay.connections().length + " [Network layer: " + overlay.network().connections().length + "]");
		return true;
	}
	
	public void execute() {
		System.out.println("My id: " + this.myId.toString());
		if(this.contactId != null)
			System.out.println("Contact id: " + this.contactId.toString());
		else
			System.out.println("Contact id: [NONE]");
		
		//Initialize the overlay network level
		channel.connect(this.contactId);
		
		try {
			while(true) {
				byte[] buf = new byte[1000];
				ByteBuffer bb = ByteBuffer.wrap(buf);
				channel.read(bb);
				System.out.println((new String(buf)).trim());
			}
		} catch(Exception e) { e.printStackTrace(); }
		
		channel.close();
		
	}
	
	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {
	
		InetSocketAddress local = new InetSocketAddress(InetAddress.getLocalHost(), 30000);
		InetSocketAddress contact = new InetSocketAddress("192.16.125.11",30000);
		
		if(local.getAddress().equals(contact.getAddress())) {
			System.out.println("Primary node is on-line.");
			new NonInteractiveProtoBCastApplication(local).execute();
		} else {
			System.out.println("Node is on-line.");
			new NonInteractiveProtoBCastApplication(local, contact).execute();
		}
		
	}

}

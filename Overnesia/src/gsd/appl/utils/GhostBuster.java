package gsd.appl.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.StringTokenizer;

public class GhostBuster {

	private Scanner sc;
	private HashSet<String> known_ips;
	private HashMap<String, Integer> repeat;
	
	public GhostBuster(String filename, String ipList) throws FileNotFoundException {
		this.sc = new Scanner(new File(filename));
		this.known_ips = new HashSet<String>();
		this.repeat = new HashMap<String, Integer>();
		Scanner scan_ips = new Scanner(new File(ipList));
		while(scan_ips.hasNextLine())
			this.known_ips.add(scan_ips.nextLine().trim());
		scan_ips.close();
	}
	
	public void run() {
		String line = null;
		//Locate the correct line in the file to start scanning
		while(sc.hasNextLine()) {
			line = sc.nextLine();
			if(line.trim().equalsIgnoreCase("--- translation table ---"))
				break;
		}
		sc.nextLine(); //white line
		
		try {
			while(sc.hasNextLine()) {
				line = sc.nextLine();
				StringTokenizer st1 = new StringTokenizer(line,":");
				st1 = new StringTokenizer(st1.nextToken(),"/");;
				if(st1.countTokens() == 2) {
					st1.nextToken();
				}
				String ip = st1.nextToken();
				if(ip.equals("null")) continue;
				
				//System.err.println("Testing ip: " + ip);
				if(!this.known_ips.contains(ip))
					System.out.println(ip);
				if(this.repeat.containsKey(ip)) {
					this.repeat.put(ip, new Integer(this.repeat.remove(ip).intValue() + 1));
				} else
					this.repeat.put(ip, new Integer(1));
				
			}
		} catch (Exception e) {
			System.err.println(line);
			e.printStackTrace();
		}
		for(String ip: this.repeat.keySet())
			if(this.repeat.get(ip).intValue() > 1) {
				System.out.println("IP " + ip + " is repeated: " + this.repeat.get(ip).intValue());
			}
		System.out.println("Found: " + this.repeat.keySet().size() + "unique ips");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 2) {
			System.err.println("usage: java " + GhostBuster.class.getCanonicalName() + " filename filename.ips");
			System.exit(1);
		}
		
		try {
			GhostBuster pfer = new GhostBuster(args[0], args[1]);
			pfer.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

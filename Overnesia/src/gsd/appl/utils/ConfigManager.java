package gsd.appl.utils;

import java.io.File;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ConfigManager {

	public final static String defaultScriptFile = "config";
	public final static String defaultExtension = "cfg";
	
	/**
	 * Strings used in the configuration file to identify parameters
	 */
	public final static String IDNUMBER = "id";
	public final static String STARTTIME = "startTime";
	public final static String PERIODIC = "periodicTimer";
	public final static String CONTACTID = "contactSocket";
	public final static String MYID = "localSocket";
	
	
	private File configFile;
	private HashMap<String,String> configLines;
	private InetSocketAddress localID;
	private InetSocketAddress contactID;
	private String hostname;
	
	public ConfigManager() throws Exception {
		this.configFile = new File(ConfigManager.defaultScriptFile + "." + ConfigManager.defaultExtension);
		this.configLines = new HashMap<String,String>();
		this.loadConfigFile();
		this.generateNodeIDs();
		this.hostname = InetAddress.getLocalHost().getHostName();
	}
	
	public ConfigManager(String configFile) throws Exception {
		this.configFile = new File(configFile);
		this.configLines = new HashMap<String,String>();
		this.loadConfigFile();
		this.generateNodeIDs();
	}
	
	private void generateNodeIDs() throws Exception {
		this.localID = null;
		this.contactID = null;
		
		StringTokenizer st = new StringTokenizer(this.getParameter(ConfigManager.MYID));
		StringTokenizer st2 = new StringTokenizer(this.getParameter(ConfigManager.CONTACTID));
	
		if (st.countTokens() != 2 || st2.countTokens() != 2)
			throw new Exception("Invalid configuration line: unexpected number of tokens for node id.");
		
		this.localID = new InetSocketAddress(st.nextToken(),Integer.parseInt(st.nextToken()));
		this.contactID = new InetSocketAddress(st2.nextToken(),Integer.parseInt(st2.nextToken()));
	}
	
	private void loadConfigFile() throws Exception {
		String line = null;
		String component = null;
		StringTokenizer st = null;
		Scanner scan = new Scanner(this.configFile);
		while(scan.hasNext()) {
			line = scan.next();
			st = new StringTokenizer(line);
			if(st.countTokens() != 2) throw new Exception("Parse error at line: '" + line + "' Invalid number of tokens: " + st.countTokens());
			if(st.nextToken().equalsIgnoreCase("begin")) throw new Exception("Invalid format in line '" + line + "', expected token: begin");
			component = st.nextToken();
			if(component.equalsIgnoreCase(this.hostname) || component.equalsIgnoreCase("global")) {
				readConfigArea(component, scan);
			} else {
				skipConfigArea(component, scan);
			}
		}
	}
	
	private void readConfigArea(String area, Scanner sc) throws Exception {
		String line = null;
		StringTokenizer st = null;
		while(sc.hasNext()) {
			line = sc.next();
			st = new StringTokenizer(line,":");
			if(st.countTokens() == 1 && st.nextToken().equalsIgnoreCase("end"))
				return;
			else if (st.countTokens() == 0)
				throw new Exception("Parse error at line: '" + line + "' Invalid number of tokens: 1.");
			else if (st.countTokens() == 2)
				this.configLines.put(st.nextToken(), st.nextToken());
			else
				throw new Exception("Parse error at line: '" + line + "' Invalid number of tokens: " + st.countTokens());
		}
		throw new Exception("Unexpected end of file. Waiting 'end' for matching 'begin " + area + "'");
	}
	
	private void skipConfigArea(String area, Scanner sc) throws Exception {
		String line = null;
		StringTokenizer st = null;
		while(sc.hasNext()) {
			line = sc.next();
			st = new StringTokenizer(line,":");
			if(st.countTokens() == 1 && st.nextToken().equalsIgnoreCase("end"))
				return;
			else if (st.countTokens() == 1)
				throw new Exception("Parse error at line: '" + line + "' Invalid number of tokens.");
		}
		throw new Exception("Unexpected end of file. Waiting 'end' for matching 'begin " + area + "'");
	}
	
	private String getParameter(String named) throws Exception{
		if(this.configLines.containsKey(named)) {
			return configLines.get(named);
		} else {
			throw new Exception("There is not such parameter: " + named);
		}
	}
	
	public long getMyIdNumber() throws Exception {
		return Long.parseLong(this.getParameter(ConfigManager.IDNUMBER));
	}
	
	public long getStartTime() throws Exception  {
		return Long.parseLong(this.getParameter(ConfigManager.STARTTIME));
	}

	public long getPeriodicInterval() throws Exception {
		return Long.parseLong(this.getParameter(ConfigManager.PERIODIC));
	}

	public InetSocketAddress getContactId() {
		return this.contactID;
	}

	public InetSocketAddress getMyId() {
		return this.localID;
	}
}

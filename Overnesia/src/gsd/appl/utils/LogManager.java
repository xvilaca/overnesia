package gsd.appl.utils;

import java.io.FileOutputStream;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.WriterAppender;

public class LogManager{

	private long nodeID;
	private Logger global;
	private HashMap<String,Logger> others;
	private static LogManager globalInstance = null;
	
	
	private LogManager(long myId) {
		this.nodeID = myId;
		this.global = Logger.getRootLogger();
		this.setGlobalLoggerConfig();
		this.others = new HashMap<String,Logger>();
	}
	
	private void setGlobalLoggerConfig() {
	    WriterAppender appender = null;  
		try {
	         appender = new WriterAppender(new PatternLayout("%d{DATE}: %m %n"),new FileOutputStream("global_" + this.nodeID + ".txt"));
	    } catch(Exception e) {}
		
	    this.global.addAppender(appender);
	    this.global.setLevel(Level.ALL);
	}
	
	public static LogManager getInstance(long myId) {
		if (LogManager.globalInstance == null)
			LogManager.globalInstance = new LogManager(myId);
		return LogManager.globalInstance;
	}
	
	public Logger createNewLogger(String desc) {
		if(!this.others.containsKey(desc))
			this.others.put(desc, Logger.getLogger(desc));
		return this.others.get(desc);
	}

	public static void logDebug(String info) {
		LogManager.globalInstance.global.debug(info);
	}
	
	public static void logInfo(String info) {
		LogManager.globalInstance.global.info(info);
	}
	
	public static void logWarn(String info) {
		LogManager.globalInstance.global.warn(info);
	}

	public static void logException(Object printStackTrace) {
		LogManager.globalInstance.global.error(printStackTrace);
	}

	public static void logMessageReception(BroadcastMessage bm) {
		LogManager.globalInstance.global.info("Received Message: " + bm.getSenderID() + " " + bm.getSequenceNumber() + " " + bm.getPeer().toString());
	}
	
	public static void logMessageBroadcast(BroadcastMessage bm) {
		LogManager.globalInstance.global.info("Sent Message: " + bm.getSenderID() + " " + bm.getSequenceNumber() + " " + bm.getPeer().toString());
	}

	public static void logError(String string) {
		LogManager.globalInstance.global.error(string);		
	}
	
}

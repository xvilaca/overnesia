package gsd.appl.utils;

import gsd.appl.modules.ExperimentalNode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;


public class ActionManager extends Thread {

	public final static String defaultScriptFile = "script";
	public final static String defaultExtension = "scp";
	
	private ExperimentalNode node;
	private File scriptFile;
	private LinkedList<Action> pendingActions;	
	private long nodeID;
	private long startTime;
	private Scanner scan;
	
	private final static boolean debug = true;
	
	private void debug(String msg) {
		if(debug) {
			System.err.println(System.currentTimeMillis() + ": " + msg);
		}
	}
	
	public ActionManager (long nodeID, long startTime, ExperimentalNode node) {
		this.node = node;
		this.nodeID = nodeID;
		this.startTime = startTime;
		this.scriptFile = new File(ActionManager.defaultScriptFile + "." + ActionManager.defaultExtension);
		this.pendingActions = new LinkedList<Action>();
		this.scan = null;
		this.init();
	}
	
	public ActionManager (long nodeID, long startTime, ExperimentalNode node, String actionsFile) {
		this.node = node;
		this.nodeID = nodeID;
		this.startTime = startTime;
		this.scriptFile = new File(actionsFile);
		this.pendingActions = new LinkedList<Action>();
		this.scan = null;
		this.init();
	}
	
	private void init() {
		try {
			scan = new Scanner(this.scriptFile);
		} catch (FileNotFoundException e) {
			debug("Cannot open the script file.");
			return;
		}
		
		Action ac = null;
		
		while (scan.hasNext()) {
			ac = parseAction(scan.next());
			if(ac != null)
				this.pendingActions.add(ac);
		}
	}
	
	private Action parseAction(String line) {
		StringTokenizer st = new StringTokenizer(line,":");
		
		if(st.countTokens() != 3 && Integer.parseInt(st.nextToken()) == this.nodeID) {
			try {
				return new Action(this.startTime,st.nextToken(),st.nextToken());
			} catch (Exception e) {
				debug("Failed to instanciate an Action.");
				return null;
			}
		} 
		
		return null;
	}
	
	/**
	 * Main function for this thread
	 */
	
	@Override
	public void run() {
		Action ac = null;
		long time;
		
		while(this.pendingActions.size() > 0) {
			ac = this.pendingActions.remove(0);
			try {
				Thread.sleep(ac.getDelta(System.currentTimeMillis()));
			} catch (InterruptedException e) {
				debug("Thread.sleep(): failed.");
				return;
			}
		
			node.executeAction(ac);
			time = System.currentTimeMillis();
			
			while(this.pendingActions.get(0).getDelta(time) == 0)
				node.executeAction(this.pendingActions.remove(0));
		}
	}
}

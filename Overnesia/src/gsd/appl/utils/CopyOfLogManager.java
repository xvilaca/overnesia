package gsd.appl.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

public class CopyOfLogManager extends Thread{

	public final static String logExtension = "log";
	public final static String oldLogExtension = "old";
	public final static String defaultLogFile = "activity";
	
	private final static boolean debug = true;
	
	private void debug(String msg) {
		if(debug) {
			System.err.println(System.currentTimeMillis() + ": " + msg);
		}
	}
	
	private long nodeID;
	private File logFile;
	private boolean logOpen;
	
	private PrintStream out;
		
	public CopyOfLogManager(long myId) {
		this.nodeID = myId;
		this.logFile = openLogFile(CopyOfLogManager.defaultLogFile);
		this.logOpen = false;
		this.out = null;
	}
	
	public CopyOfLogManager(long myId, String filename) {
		this.nodeID = myId;
		this.logFile = openLogFile(filename);
		this.logOpen = false;
		this.out = null;
	}
	
	public CopyOfLogManager(long myId, String filename, int logIndex) {
		this.nodeID = myId;
		this.logFile = openLogFile(filename+logIndex);
		this.logOpen = false;
		this.out = null;
	}

	private File openLogFile(String name) {
		File tmp = new File(name + "." + CopyOfLogManager.logExtension);
		if(tmp.exists()) {
			tmp.renameTo(new File(name + "." + CopyOfLogManager.oldLogExtension));
			tmp = new File(name + "." + CopyOfLogManager.logExtension);
		} 
		try {
			tmp.createNewFile();
		} catch (IOException e) {
			tmp.delete();
		}
		return tmp;
	}
	
	public boolean openLog() {
		try {
			out = new PrintStream(this.logFile);
		} catch (FileNotFoundException e) {
			return this.logOpen = false;
		}
		return this.logOpen = true;
	}
	
	public void flushLog() {
		if(this.logOpen) {
			out.flush();
		} else {
			debug("Cannot flush: logOpen = false.");
		}
	}
	
	public void appendToLog(String msg) {
		this.addToLog(this.nodeID + " " + System.currentTimeMillis() + " " + msg);
	}
	
	public void addToLog(String msg) {
		if(this.logOpen) {
			out.println(msg);
		} else {
			debug("Cannot write ' " + msg + "': logOpen = false.");
		}
	}
	
	public void timeStamp() {
		if(this.logOpen) {
			out.println(System.currentTimeMillis());
		} else {
			debug("Cannot write timestamp: logOpen = false.");
		}
	}
	
	public void lineFeed() {
		if(this.logOpen) {
			out.println();
		} else {
			debug("Cannot add lineFeed: logOpen = false.");
		}
	}
	
	public void closeLog() {
		if(this.logOpen) {
			this.out.close();
			this.logOpen = false;
		}
	}
}

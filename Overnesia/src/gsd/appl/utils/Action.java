package gsd.appl.utils;

import java.util.StringTokenizer;

public class Action {
	
	private long execTime;
	private String action;
	private long msgID;
	
	public Action (long startTime, String time, String action) throws Exception {
		this.execTime = startTime + Long.parseLong(time);
		StringTokenizer stk = new StringTokenizer(action);
		if(stk.countTokens() == 2) {
			this.action = stk.nextToken();
			this.msgID = Integer.parseInt(stk.nextToken());
		} else if(stk.countTokens() == 1) {
			this.action = stk.nextToken();
		} else
			throw new Exception("Invalid Action");
	}
	
	public long getTimeToExec() {
		return this.execTime;
	}
	
	public String getAction() {
		return this.action;
	}
	
	public long getMsgID() {
		return this.msgID;
	}
	
	public long getDelta(long currTimeMillis) {
		long time = this.execTime - currTimeMillis;
		if(time < 0)
			return 0;
		else 
			return time;
	}
}

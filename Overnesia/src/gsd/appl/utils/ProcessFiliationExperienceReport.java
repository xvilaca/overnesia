package gsd.appl.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.UUID;

public class ProcessFiliationExperienceReport {

	private Scanner sc;
	private HashMap<UUID, ArrayList<String>> entries;
	
	public ProcessFiliationExperienceReport(String filename) throws FileNotFoundException {
		this.sc = new Scanner(new File(filename));
		this.entries = new HashMap<UUID, ArrayList<String>>();
	}
	
	private int unique;
	
	public void run() {
		this.unique = 0;
		String line = null;
		//Locate the correct line in the file to start scanning
		while(sc.hasNextLine()) {
			line = sc.nextLine();
			if(line.trim().equalsIgnoreCase("--- translation table ---"))
				break;
		}
		sc.nextLine(); //white line
		
		try {
			while(sc.hasNextLine()) {
				line = sc.nextLine();
				StringTokenizer st1 = new StringTokenizer(line);
				String node = st1.nextToken();
				if(node.equals("null")) {
					continue;
				}
				st1 = new StringTokenizer(node, ":");
				st1.nextToken();
				st1.nextToken();
				UUID id = UUID.fromString(st1.nextToken());
				if(this.entries.containsKey(id)) {
					ArrayList<String> item = this.entries.remove(id);
					item.add(node);
					this.entries.put(id, item);
				} else {
					ArrayList<String> item = new ArrayList<String>();
					item.add(node);
					this.entries.put(id, item);
					unique++;
				}
			}
		} catch (Exception e) {
			System.err.println(line);
			e.printStackTrace();
		}
		
		for(UUID id: this.entries.keySet()) {
			if(this.entries.get(id).size() > 1) {
				System.out.println("ID: " + id);
				for(String s: this.entries.get(id)) 
					System.out.println("\t" + s);
			}
		}
		System.out.println(unique + " unique nodes found.");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.err.println("usage: java " + ProcessFiliationExperienceReport.class.getCanonicalName() + " filename");
			System.exit(1);
		}
		
		try {
			ProcessFiliationExperienceReport pfer = new ProcessFiliationExperienceReport(args[0]);
			pfer.run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

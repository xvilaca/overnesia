package gsd.appl.utils;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;



public class BroadcastMessage {

	private OvernesiaNodeID peer;
	private long senderID;
	private long sequenceNumber;
	private long payload;

	public BroadcastMessage(OvernesiaNodeID overnesiaNodeID, long senderID, long sequenceNumber) {
		this.peer = overnesiaNodeID;
		this.senderID = senderID;
		this.sequenceNumber = sequenceNumber;
		this.payload = System.currentTimeMillis();
	}
	
	public BroadcastMessage(OvernesiaNodeID peer, long senderID, long sequenceNumber, long payload) {
		this.peer = peer;
		this.senderID = senderID;
		this.sequenceNumber = sequenceNumber;
		this.payload = payload;
	}
	
	public OvernesiaNodeID getPeer() {
		return this.peer;
	}
	
	public long getSenderID() {
		return this.senderID;
	}
	
	public OvernesiaNodeID getNodeID() {
		return this.peer;
	}
	
	public long getSequenceNumber() {
		return this.sequenceNumber;
	}
	
	public long getPayload() {
		return this.payload;
	}
	
	public void setNodeID(OvernesiaNodeID id) {
		this.peer = id;
	}
	
	static public ByteBuffer writeBroadcastMessageToBuffer(BroadcastMessage request) {
		ByteBuffer msg = ByteBuffer.allocate(8 + 8 + 38);

		msg.putLong(request.getSequenceNumber());
		msg.putLong(request.getSenderID());
		msg.putLong(request.payload);
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.peer);
		
		msg.flip();
		return msg;
	}
	
	public static BroadcastMessage readBroadcastMessageFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 8 + 8 + 38);
		
		long sequenceNumber = buf.getLong();
		long senderID = buf.getLong();
		long payload = buf.getLong();

        return new BroadcastMessage(OvernesiaNodeID.readNodeIDFromBuffer(buf), senderID, sequenceNumber, payload);
	}
	
}

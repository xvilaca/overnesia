package gsd.appl.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeRepresentation {

	public static String timeStamp() {
		return new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss.SSS").format(new Date());
	}
	
	public static Date dateTimeStamp() {
		return new Date();
	}
	
	public static long currentTime() {
		return System.currentTimeMillis();
	}
	
	public static String convertTimeStamp(Date d) {
		return new SimpleDateFormat("EEE, dd-MMM-yyyy HH:mm:ss.SSS").format(d);
	}
	
	public static long timeElapsed(long start, long end) {
		return end-start;
	}
	
	public static Date convert(long date) {
		return new Date(date);
	}
	
}

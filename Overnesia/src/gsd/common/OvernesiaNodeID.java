package gsd.common;

import gsd.impl.Buffers;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.UUID;


public class OvernesiaNodeID extends ID{

	private UUID id;
	private UUID nesos_id;
	private InetSocketAddress listenPort;
	
	public OvernesiaNodeID(InetSocketAddress socketAddr) {
		this.id = UUID.randomUUID();
		this.nesos_id = null;
		this.listenPort = socketAddr;
	}
	
	public OvernesiaNodeID(InetSocketAddress socketAddr, UUID id) {
		this.id = id;
		this.nesos_id = null;
		this.listenPort = socketAddr;
	}
	
	public OvernesiaNodeID(InetSocketAddress socketAddr, UUID id, UUID nesos_id) {
		this.id = id;
		this.nesos_id = nesos_id;
		this.listenPort = socketAddr;
	}
	
	public OvernesiaNodeID(OvernesiaNodeID nodeID) {
		this.id = nodeID.getUUID();
		this.nesos_id = nodeID.getNesosUUID();
		this.listenPort = nodeID.getSocketAddress();
	}

	public UUID getUUID() {
		return this.id;
	}
	
	public UUID getNesosUUID() {
		return this.nesos_id;
	}
	
	public InetSocketAddress getSocketAddress() {
		return this.listenPort;
	}
	
	static public ByteBuffer writeNodeIdToBuffer(OvernesiaNodeID id) {
		InetSocketAddress isa = id.getSocketAddress();
		UUID uuid = id.getUUID();
		UUID nesos = id.getNesosUUID();
		ByteBuffer msg = ByteBuffer.allocate(38);
		
		msg.put(isa.getAddress().getAddress());
		msg.putShort((short) isa.getPort());
		
		msg.putLong(uuid.getMostSignificantBits());
		msg.putLong(uuid.getLeastSignificantBits());
		
		if(nesos != null) {
			msg.putLong(nesos.getMostSignificantBits());
			msg.putLong(nesos.getLeastSignificantBits());
		} else {
			msg.putLong(0);
			msg.putLong(0);
		}
		
		msg.flip();
		return msg;
	}
	
	static public void writeNodeIdToBuffer(ByteBuffer buf, OvernesiaNodeID node) {
		InetSocketAddress isa = node.getSocketAddress();
		UUID uuid = node.getUUID();
		UUID nesos = node.getNesosUUID();
		
		buf.put(isa.getAddress().getAddress());
		buf.putShort((short) isa.getPort());
		
		buf.putLong(uuid.getMostSignificantBits());
		buf.putLong(uuid.getLeastSignificantBits());
		
		if(nesos != null) {
			buf.putLong(nesos.getMostSignificantBits());
			buf.putLong(nesos.getLeastSignificantBits());
		} else {
			buf.putLong(0);
			buf.putLong(0);
		}
	}
	
	public static OvernesiaNodeID readNodeIDFromBuffer(ByteBuffer[] msg) {
		int port = 0;
		byte[] dst = null;
		InetAddress ia = null;

		ByteBuffer buf = Buffers.sliceCompact(msg, 38);
		dst = new byte[4];
		buf.get(dst, 0, dst.length);
		port = (buf.getShort()) & 0xffff;
		try {
			ia = InetAddress.getByAddress(dst);
		} catch (UnknownHostException e) {
			// We are sure that this does not happen, as the
			// byte array is created with the correct size.
		}
		
		//node id
		long msb = buf.getLong();
		long lsb = buf.getLong();
		
		//nesos id
		long n_msb = buf.getLong();
		long n_lsb = buf.getLong();
		
		return new OvernesiaNodeID(new InetSocketAddress(ia, port), new UUID(msb, lsb), (n_msb == 0 && n_lsb == 0 ? null : new UUID(n_msb, n_lsb)));
		
	}
	
	public static OvernesiaNodeID readNodeIDFromBuffer(ByteBuffer buf) {
		int port = 0;
		byte[] dst = new byte[4];
		InetAddress ia = null;
		
		buf.get(dst, 0, dst.length);
		port = (buf.getShort()) & 0xffff;
		try {
			ia = InetAddress.getByAddress(dst);
		} catch (UnknownHostException e) {
			// We are sure that this does not happen, as the
			// byte array is created with the correct size.
		}
		
		//node id
		long msb = buf.getLong();
		long lsb = buf.getLong();
		
		//nesos id
		long n_msb = buf.getLong();
		long n_lsb = buf.getLong();
		
		return new OvernesiaNodeID(new InetSocketAddress(ia, port), new UUID(msb, lsb), (n_msb == 0 && n_lsb == 0 ? null : new UUID(n_msb, n_lsb)));
		
        
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null && o instanceof OvernesiaNodeID) {
			OvernesiaNodeID nid = (OvernesiaNodeID) o;
			return this.listenPort.equals(nid.listenPort) && this.id.equals(nid.id);
		} else {
			return false;
		}		
	}
	
	@Override
	public String toString() {
		return this.listenPort.toString() + "::" + this.id.toString()+ " " + (this.nesos_id == null ? "null" : this.nesos_id);
	}

	public void setUUID(UUID id) {
		this.id = id;
	}
	
	public void setNesosUUID(UUID id) {
		this.nesos_id = id;
	}
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

	public static void writeNodeIdToBufferWithoutNesos(ByteBuffer msg, OvernesiaNodeID id) {
		InetSocketAddress isa = id.getSocketAddress();
		UUID uuid = id.getUUID();
			
		msg.put(isa.getAddress().getAddress());
		msg.putShort((short) isa.getPort());
		
		msg.putLong(uuid.getMostSignificantBits());
		msg.putLong(uuid.getLeastSignificantBits());
	}

	public static OvernesiaNodeID readNodeIDFromBufferWithoutNesos(ByteBuffer buf, UUID nesosID) {
		int port = 0;
		byte[] dst = new byte[4];
		InetAddress ia = null;
		
		buf.get(dst, 0, dst.length);
		port = (buf.getShort()) & 0xffff;
		try {
			ia = InetAddress.getByAddress(dst);
		} catch (UnknownHostException e) {
			// We are sure that this does not happen, as the
			// byte array is created with the correct size.
		}
		
		//node id
		long msb = buf.getLong();
		long lsb = buf.getLong();
				
		return new OvernesiaNodeID(new InetSocketAddress(ia, port), new UUID(msb, lsb), nesosID);        
	}
}

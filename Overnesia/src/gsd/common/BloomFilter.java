package gsd.common;

import java.util.BitSet;

public class BloomFilter {

	private int numHash, filterSize;
	private BitSet filter;

	public BloomFilter(int size, int nHash){
		filter = new BitSet(size);
		numHash = nHash;
	}

	public BloomFilter(BitSet set, int nHash){
		filter = set;
		numHash = nHash;
	}
	
	public BitSet getFilter(){
		return filter;
	}

	public int getnumHash(){
		return numHash;
	}

	public boolean constains(byte[] byteArray) {
		return accessFilterPositions(byteArray, false);
	}

	public void put(byte[] byteArray) {
		this.accessFilterPositions(byteArray, true);
	}
	
	private boolean accessFilterPositions(byte[] elementId, boolean set) {
		int hash1 = murmurHash(elementId, elementId.length, 0);
		int hash2 = murmurHash(elementId, elementId.length, hash1);
		for (int i = 0; i < numHash; i++){
			if(set)
				filter.set(Math.abs((hash1 + i * hash2) % filterSize), true);
			else
				if(!filter.get(Math.abs((hash1 + i * hash2) % filterSize)))
					return false;
		}
		return true;
	}

	private int murmurHash(byte[] data, int length, int seed) {
		int m = 0x5bd1e995;
		int r = 24;
			int h = seed ^ length;
			int len_4 = length >> 2;
			for (int i = 0; i < len_4; i++) {
			int i_4 = i << 2;
			int k = data[i_4 + 3];
			k = k << 8;
			k = k | (data[i_4 + 2] & 0xff);
			k = k << 8;
			k = k | (data[i_4 + 1] & 0xff);
			k = k << 8;
			k = k | (data[i_4 + 0] & 0xff);
			k *= m;
			k ^= k >>> r;
			k *= m;
			h *= m;
			h ^= k;
		}

		// avoid calculating modulo
		int len_m = len_4 << 2;
		int left = length - len_m;
			if (left != 0) {
			if (left >= 3) {
				h ^= data[length - 3] << 16;
			}
			if (left >= 2) {
				h ^= data[length - 2] << 8;
			}
			if (left >= 1) {
				h ^= data[length - 1];
			}
				h *= m;
		}
			h ^= h >>> 13;
				h *= m;
				h ^= h >>> 15;
				return h;
	}
}

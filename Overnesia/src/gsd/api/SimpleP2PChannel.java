/*
 * NeEM - Network-friendly Epidemic Multicast
 * Copyright (c) 2005-2006, University of Minho
 * All rights reserved.
 *
 * Contributors:
 *  - Pedro Santos <psantos@gmail.com>
 *  - Jose Orlando Pereira <jop@di.uminho.pt>
 * 
 * Partially funded by FCT, project P-SON (POSC/EIA/60941/2004).
 * See http://pson.lsd.di.uminho.pt/ for more information.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  - Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 *  - Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 * 
 *  - Neither the name of the University of Minho nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gsd.api;

import gsd.impl.*;

import java.net.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;


/**
 * Channel interface to a NeEM epidemic multicast group. This interface is
 * thread and aliasing safe, and thus can be used by multiple threads and all
 * buffers used as parameters can immediately be reused by the application.
 * <p>
 * After creating the channel, it must be connected to at least one previously
 * known peer in the desired group. Such peer must be determined by the
 * application. A good strategy is to maintain a list of known peers and connect
 * to them all to cope with transient failures.
 */
public class SimpleP2PChannel extends Config implements InterruptibleChannel,
        ReadableByteChannel, WritableByteChannel {

	/**
     * Creates a new instance of a peer-to-peer channel.
     * 
     * @param local
     *            the local address to bind to
     */
    public SimpleP2PChannel(InetSocketAddress local) throws IOException {
    	rand = new Random();
    	net = new Transport(rand, local);
        t = new Thread(net);
        t.setDaemon(true);
        t.start();
    }

    /**
     * Get the address that is being advertised to peers.
     * 
     * @return the address being advertised to peers
     */
    public InetSocketAddress getLocalSocketAddress() {
        return this.net.id();
    }

    /* Random instance */
    Random rand = null;
    
    /* Transport layer */
    Transport net = null;

    private Thread t;

	public Transport getTransport() {
		return this.net;
	}

	public Random getRand() {
		return this.rand;
	}

	public void close() throws IOException {
		this.net.close();
	}

	public boolean isOpen() {
		return this.t.isAlive();
	}

	public int read(ByteBuffer dst) throws IOException {
		return 0;
	}

	public int write(ByteBuffer src) throws IOException {
		return 0;
	}

}


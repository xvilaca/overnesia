package gsd.api;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFactory {

	private Properties prop;
	
	
	public PropertiesFactory(int expN){

		prop = new Properties();
		InputStream input;
		try {
			input = new FileInputStream("../config/config" + expN + ".properties");
			//input = new FileInputStream("config.properties");
			
			// load a properties file
			prop.load(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getProperty(String key){
		return this.prop.getProperty(key);
	}
	
	public boolean withChallenge(){
		boolean b = this.getBoolProperty("withChallenge");
		long delay = this.getLongProperty("challengeDelay");
		if(!b)
			return false;
		
		return (delay > 0);
	}
	
	public void setProperty(String key, String value) {
		this.prop.setProperty(key, value);
	}
	
	public long getLongProperty(String key){
		return Long.valueOf(this.prop.getProperty(key));
	}
	
	public int getIntProperty(String key){
		return Integer.valueOf(this.prop.getProperty(key));
	}
	
	public short getShortProperty(String key){
		return ((short) Integer.valueOf(this.prop.getProperty(key)).intValue());
	}
	
	public Double getDoubleProperty(String key){
		return Double.valueOf(prop.getProperty(key));
	}
	
	public Double getBFP(){
		return Double.valueOf(prop.getProperty("bfp"))/10;
	}
	
	private short getViewSize(String s){
		boolean withWhite = Boolean.valueOf(prop.getProperty("withWhite"));
		return (withWhite)? Short.valueOf(prop.getProperty("whiteView")) :
			Short.valueOf(prop.getProperty(s));
	}
	
	public short getFreeRideActiveViewBaseSize(){
		return getViewSize("activeViewBaseSize");
	}
	
	public short getFreeRideActiveViewMaxSize(){
		return getViewSize("activeViewMaxSize");
	}
	
	public short getFreeRidePassiveViewSize(){
		return getViewSize("activeViewMaxSize");
	}
	
	public Boolean getBoolProperty(String key){
		Boolean bool= true ;
		String value = prop.getProperty(key);
		bool = (value.equals("false")) ? false : true;
		return bool;
	}
	
	
}

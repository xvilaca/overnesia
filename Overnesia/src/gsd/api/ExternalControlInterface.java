package gsd.api;

import java.io.IOException;

public interface ExternalControlInterface {

	public void sendMessage(Object m) throws IOException;
	
}

package gsd.pings.messages;

import gsd.impl.Buffers;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class Pong {

	private InetSocketAddress sender;
	private long time;
		
	public Pong(InetSocketAddress me) {
		this.sender = me;
		this.time = 0;
	}
	
	private Pong(InetSocketAddress me, long time) {
		this.sender = me;
		this.time = time;
	}
	
	public long getTime() {
		return time;
	}
	
	public InetSocketAddress getSender() {
		return this.sender;
	}
	
	static public ByteBuffer writeJoinReplyToBuffer(Pong request) {
		ByteBuffer msg = ByteBuffer.allocate(4+2+8);
		
		msg.put(request.sender.getAddress().getAddress());
		msg.putShort((short) request.sender.getPort());
		
		msg.putLong(System.nanoTime());
		
		msg.flip();
		return msg;
	}
	
	public static Pong readJoinReplyFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 4 + 2 + 8);
		
		InetSocketAddress isa = null;
		byte[] dst = new byte[4];
		buf.get(dst, 0, dst.length);
		int port = (buf.getShort()) & 0xffff;
		try {
		 isa = new InetSocketAddress(InetAddress.getByAddress(dst), port);
		} catch (UnknownHostException e) {
			// We are sure that this does not happen, as the
			// byte array is created with the correct size.
		}
		
		long time = buf.getLong();
		
		return new Pong(isa, time);
	}

}

package gsd.impl;

import gsd.api.TestResults;
import gsd.common.NodeID;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.spi.DirStateFactory.Result;

public class TestCollect {
	
	private ObjectInputStream commands;
	private ObjectOutputStream replies;
	private TestResults results;
	
	public TestCollect(ObjectInputStream commands, ObjectOutputStream replies ,TestResults test_results) {
		this.commands = commands;
		this.replies = replies;
		this.results = test_results;
	}

	public void collectData() throws IOException {
		if(results.isIsolated()){
			System.out.println(results.getID() + ": ISOLATED!!!");
		}
		replies.writeObject(results);
		replies.flush();
		//replies.close();
	}

	public void sendTest_collection_signal() throws IOException{
		System.out.println("");
		replies.writeObject('r'); // redundancy signal
		replies.flush();
		try{
			this.results = (TestResults) commands.readObject();
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
	}
		
		public TestResults getTestResults(){
			return this.results;
		}	

		public boolean wasKicked() {
			// TODO Auto-generated method stub
			return this.results.wasKicked();
		}
	}
	

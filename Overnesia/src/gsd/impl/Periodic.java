/*
 * NeEM - Network-friendly Epidemic Multicast
 * Copyright (c) 2005-2006, University of Minho
 * All rights reserved.
 *
 * Contributors:
 *  - Pedro Santos <psantos@gmail.com>
 *  - Jose Orlando Pereira <jop@di.uminho.pt>
 * 
 * Partially funded by FCT, project P-SON (POSC/EIA/60941/2004).
 * See http://pson.lsd.di.uminho.pt/ for more information.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  - Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 *  - Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 * 
 *  - Neither the name of the University of Minho nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gsd.impl;

import java.util.Random;

/**
 * A stopable and restartable periodic activity. This uses the scheduling
 * mechanism in the transport layer.
 */
public abstract class Periodic implements Runnable {	
	private boolean running;
	private int interval;
	private int hysteresis;
	private Transport trans;
	private Runnable runnable;
	private Random rand;
	
	public Periodic(Random rand, Transport trans, int interval) {
		this.interval=interval;
		this.hysteresis=-1;
		this.trans=trans;
		this.rand=rand;
		runnable=new Runnable() {
			public void run() {
				doIt();
			}
		};
	}
	
	public Periodic(Random rand, Transport trans, int interval, int hysteresis) {
		this(rand, trans, interval);
		this.hysteresis=hysteresis;
	}
	
	public void start() {
		if (running)
			return;
		running=true;	
		if(this.hysteresis == -1)
			trans.schedule(runnable, rand.nextInt(interval*2));
		else 
			trans.schedule(runnable, interval + (this.hysteresis == 0 ? 0 : rand.nextInt(this.hysteresis)));
	}
	
	public void stop() {
		running=false;
	}
	
	public void doIt() {
		if (running)
			run();
		if (running)
			if(this.hysteresis == -1)
				trans.schedule(runnable, rand.nextInt(interval*2));
			else 
				trans.schedule(runnable, interval + (this.hysteresis == 0 ? 0 : rand.nextInt(this.hysteresis)));
	}
	
	public int getInterval() {
		return interval;
	}
	
	public int getHysteresis() {
		return hysteresis;
	}
	
	public void setInterval(int interval) {
		this.interval=interval;
	}
	
	public void setHysteresis(int hysteresis) {
		this.hysteresis = hysteresis;
	}

	public boolean isRunning() {
		return this.running;
	}
}


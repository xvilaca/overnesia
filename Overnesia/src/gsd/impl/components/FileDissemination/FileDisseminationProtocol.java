package gsd.impl.components.FileDissemination;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;

import gsd.api.ExternalControlInterface;
import gsd.appl.control.experiences.data.FileBroadcastNotification;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.impl.Bloomfilter;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;
import gsd.impl.UUIDs;
import gsd.impl.components.GossipBroadcastProtocol;
import gsd.impl.components.FileDissemination.messages.FileDisseminationUnit;

/**
 * 
 * @author jleitao
 * This is the implementation of a module that is responsible for supporting 
 * large-scale epidemic file broadcast by relying on the HyParView p2p
 * overlay network and on a push-broadcast strategy.
 */

public class FileDisseminationProtocol extends Thread implements DataListener {

	/**
	 * Logging object to this module.
	 */
	public static Logger log = Logger.getLogger(GossipBroadcastProtocol.class);
	
	/**
     * ExternalControlInterface which might be useful to export data 
     * (THE LOCATION OF THIS INTERFACE HERE IS TEMPTATIVE)
     */
	private ExternalControlInterface eiface;
	
	/**
     * TTL
     */
    private short ttl;
    private short maxHistoric;
	
	public static final short maxPayload = 4000;//1380; //This is the maximum size (in bytes) of single message disseminated by this component.
	
	@SuppressWarnings("unused")
	private Random rand;
	private OverlayNetwork overlay;
	private Transport net;
	public final short dataPort;
	public final short abortPort;
	public final short senderDataPort;
	private HashMap<UUID,FileDisseminationSession> sessions;
	private ArrayList<UUID> terminatedSessions;
	
    /**
     * Message queue with application issued requests to start broadcasts.
     */
    private ArrayList<String> queue;
    
    /**
     * Message queue with contents received from the network to be processed...
     * These messages are processed by a thread that might block while forwarding 
     * these messages to neighbors.
     */
    private ArrayList<FileDisseminationUnit> input;
    private Dispatcher dispatcher;
    
    public void addToProcessQueue(FileDisseminationUnit unit) {
    	synchronized (this.input) {
			this.input.add(unit);
			this.input.notify();
		}
    }
    
    public void addRequest(String message) {
    	synchronized (this.queue) {
			this.queue.add(message);
			this.queue.notify();
		}
    }
	
	public FileDisseminationProtocol(Random rand, Transport net, OverlayNetwork overlay, short[] ports) {
		this.rand = rand;
		this.net = net;
		this.overlay = overlay;
		
		this.ttl = 7;
		this.maxHistoric = 20;
		
		this.queue = new ArrayList<String>();
		this.sessions = new HashMap<UUID, FileDisseminationSession>();
		this.terminatedSessions = new ArrayList<UUID>();
		
		this.input = new ArrayList<FileDisseminationUnit>();
		this.dispatcher = new Dispatcher(this);
		this.dispatcher.start();
		
		this.dataPort = ports[0];
		this.abortPort = ports[1];
		this.senderDataPort = ports[2];
		net.setDataListener(this, this.dataPort);
		net.setDataListener(this, this.abortPort);
		net.setDataListener(this, this.senderDataPort);
		
		net.setDiscardableData(this.dataPort);
		
	}
	
	public void setExternalControlInterface(ExternalControlInterface iface) {
	    	this.eiface = iface;
	    }
	
	public void run() {
		while(true) {
    		synchronized (this.queue) {
    			if(this.queue.size() > 0) {
    				try {
    					String req = this.queue.remove(0);
    					if(req != null) {
    						String[] vector = req.split(":");
    						File source = new File(vector[0]);
    						File dest = new File(vector[1]);
    						UUID experience_id = null;
    						if(vector.length == 3) {
    							experience_id = UUID.fromString(vector[2]);
    						}
    						if(!source.exists()) {
    							FileDisseminationProtocol.log.error("Cannot broadcast file '" + vector[0] + "': file does not exist.");
    							continue;
    						}
    						if(!source.canRead()) {
    							FileDisseminationProtocol.log.error("Cannot broadcast file '" + vector[0] + "': cannot read the source file.");
    							continue;
    						}
    						if(dest.exists() && !dest.canWrite()) {
    							FileDisseminationProtocol.log.error("Cannot broadcast file '" + vector[0] + "': destination file (" + vector[1] + ") cannot be written.");
    							continue;
    						}
    						
    						if(experience_id == null)
    							FileDisseminationSession.createNewSession(this, this.net, source, dest);
    						else
    							FileDisseminationSession.createNewSession(this, this.net, source, dest, experience_id);
    					}
    				} catch (Exception e) {
    					FileDisseminationProtocol.log.error("An exception occured during main operation. " + e.getClass().getName() + ": " + e.getMessage());
    					System.err.println(DateTimeRepresentation.timeStamp());
    					e.printStackTrace(System.err);
    				}
    			} else {
    				try {
    					this.queue.wait();
    				} catch (InterruptedException e) {
    					;
    				}
    			}
    		}
    	}
	}
	
	public void addSession(UUID id, FileDisseminationSession session) {
		synchronized (this.sessions) {
			this.sessions.put(id, session);
		}
	}
	
	public void failedSession(UUID id) {
		FileDisseminationSession session = null;
		synchronized (this.sessions) {
			if(this.sessions.containsKey(id)) {
				session = this.sessions.remove(id);
				this.recordPastSession(id);
			} 
		}
		
		if (session != null && session.hasStartedTransmission()) {
			this.relay(null, id, (short) 0, this.abortPort, null, new Bloomfilter());
		}
	}
	
	public synchronized void relay(ByteBuffer[] payload, UUID session_id, short ttl, short port, Connection source, Bloomfilter visited) {
		
		if(ttl >= this.ttl)
			return;
		
		try { 
			synchronized (this.net) {
				if(net.areQueuesFilled()) {
					net.wait();						
				}
			}		
		} catch (InterruptedException e) { 
			FileDisseminationProtocol.log.error("Interruption received while waiting for resource...");
    		System.err.println(DateTimeRepresentation.timeStamp());
    		e.printStackTrace(System.err);
		}
		
		visited.addElement(this.overlay.getNodeIdentifier().getUUID());
		
		ByteBuffer[] msg = null;
		if (payload == null) 
			msg = new ByteBuffer[] {ByteBuffer.allocate(16+2), Bloomfilter.writeUUIDToBuffer(visited)};
		else {
			msg = new ByteBuffer[payload.length + 2]; 
			msg[0] = ByteBuffer.allocate(16+2);
			msg[1] = Bloomfilter.writeUUIDToBuffer(visited);
			System.arraycopy(payload, 0, msg, 2, payload.length);
		}
		UUIDs.writeUUIDToBuffer(msg[0], session_id);
		msg[0].putShort(ttl);
		msg[0].flip();
		
        for(Connection link: this.overlay.connections()) {
        	if(link.id != null && !visited.containsElement(((NodeID)link.id).getUUID())) {
        		//System.err.println("GFB [DEBUG] forwarding message to neighbor: " + link.id);
            	try {
            		link.send(Buffers.clone(msg), port);    
            	} catch (Exception e) {
            		FileDisseminationProtocol.log.error("Failed to send message to: " + link.id + ". " + e.getClass().getName() + ": " + e.getMessage());
            		System.err.println(DateTimeRepresentation.timeStamp());
            		e.printStackTrace(System.err);
            	}
            } else {
            	System.err.println("GFB [DEBUG] omitting message forward to neighbor: " + link.id);
            }
        }
    }
	
	public void receive(final ByteBuffer[] msg, final Connection info, short port) {
		
		if(port == this.dataPort || port == this.senderDataPort) {	
		
			synchronized (input) {
				input.add(new FileDisseminationUnit(msg, info));
				input.notify();
			}
			
		} else if(port == this.abortPort) {
			
			ByteBuffer buf = Buffers.sliceCompact(msg, 16+2);
			UUID session_id = UUIDs.readUUIDFromBuffer(buf);
			final short ttl = buf.getShort();
			Bloomfilter visited = Bloomfilter.readUUIDFromBuffer(msg);
			
			synchronized (this.sessions) {
				if(this.sessions.containsKey(session_id)) {
					this.sessions.get(session_id).abort();
				}
				
				this.relay(null, session_id, (short) (ttl + 1), port, info, visited);
			}
			
		} else {
			FileDisseminationProtocol.log.error("Received an unknown message type from " + info.id + ": " + port);
		}
	}
	
	public void sucessfullSession(UUID id) {
		
		FileDisseminationSession session = null;
		synchronized (this.sessions) {
			session = this.sessions.get(id);
		}
			
		if(this.eiface != null)
			try {
				String filename = "";
				long filesize = 0;
				if(session != null) {
					filename = session.getDestinationFilename();
					filesize = session.getDestinationFileSize();
				}
				this.eiface.sendMessage(new FileBroadcastNotification(id, this.overlay.getNodeIdentifier(), 0, filename, filesize));
			} catch (Exception e) {
				FileDisseminationProtocol.log.error("Error while sending file broadcast notification to console (session id: " + id + "). " + e.getClass().getName() + ": " + e.getMessage());
        		System.err.println(DateTimeRepresentation.timeStamp());
        		e.printStackTrace(System.err);
			}
		this.cleanUpSession(id);
	}
	
	private void cleanUpSession(UUID id) {
		synchronized (this.sessions) {
			if(this.sessions.containsKey(id)) {
				this.sessions.remove(id);
			}
			this.recordPastSession(id);
		}
	}
	
	private void recordPastSession(UUID id) {
		if(this.terminatedSessions.size() == this.maxHistoric) {
			this.terminatedSessions.remove(0);
		}
		this.terminatedSessions.add(id);
	}
	
	private class Dispatcher extends Thread {
		
		private FileDisseminationProtocol parent;
		
		public Dispatcher(FileDisseminationProtocol parent) {
			this.parent = parent;
		}
		
		public void run() {
			FileDisseminationUnit fdu = null;
			
			while(true) {
				try {
				    fdu = null;
					synchronized (input) {
						if(input.size() > 0) {
							fdu = input.remove(0);
						} else {
							input.wait();
							if(input.size() > 0)
								fdu = input.remove(0);
						}
					}
					
					if(fdu != null) {
						ByteBuffer[] msg = fdu.getPayload();
						Connection peer = fdu.getPeer();
						fdu = null;
						
						ByteBuffer buf = Buffers.sliceCompact(msg, 16+2);
						UUID session_id = UUIDs.readUUIDFromBuffer(buf);
						short ttl = buf.getShort();
					
						Bloomfilter visited = Bloomfilter.readUUIDFromBuffer(msg);
						
						FileDisseminationSession session = null;
						synchronized(sessions) {
							session  = sessions.containsKey(session_id) ? sessions.get(session_id) : FileDisseminationSession.instanciateSession(parent, net, session_id);
						}
						
						session.deliver(msg, ttl, peer, visited);
					}
					
				} catch (Exception e) {
					FileDisseminationProtocol.log.error("Exception in thread Dispatcher. " + e.getClass().getName() + ": " + e.getMessage());
					System.err.println(DateTimeRepresentation.timeStamp());
					e.printStackTrace(System.err);
				}
			}
			
		}
		
	}

	public String getSessionStatus(UUID sessionID) {
		synchronized(sessions) {
			if(sessions.containsKey(sessionID)) {
				return this.sessions.get(sessionID).getStatus();
			} else {
				return "Session '" + sessionID + "' does not exist.";
			}
		}
	}
}

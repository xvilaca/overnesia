package gsd.impl.components.FileDissemination.messages;

import gsd.impl.Connection;

import java.nio.ByteBuffer;

public class FileDisseminationUnit {
	
	private ByteBuffer[] payload;
	private Connection sender;
	
	public FileDisseminationUnit(ByteBuffer[] msg, Connection peer) {
		this.payload = msg;
		this.sender = peer;
	}
	
	public ByteBuffer[] getPayload() {
		return this.payload;
	}
	
	public Connection getPeer() {
		return this.sender;
	}
}

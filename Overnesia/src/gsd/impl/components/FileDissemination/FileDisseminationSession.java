package gsd.impl.components.FileDissemination;

import gsd.appl.utils.DateTimeRepresentation;
import gsd.impl.Bloomfilter;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.Transport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.UUID;

public class FileDisseminationSession extends Thread {
	
	//Main protocol control component...
	private FileDisseminationProtocol protocol;
	private Transport net;
	
	private UUID id;
	private File dest;
	private File source;
	private File tmp;
	
	/**
	 * Information about the file being transfered and associated messages 
	 */
	private long size;
	private int redundant;
	private boolean[] received;
	private int missing;
	private int nextToSend;
	private RandomAccessFile file_input;
	private RandomAccessFile file_output;
	
	
	private FileDisseminationSession(FileDisseminationProtocol protocol, Transport net, File source, File dest) {
		this.protocol = protocol;
		this.net = net;
		this.id = UUID.randomUUID();
		this.source = source;
		this.dest = dest;
		this.tmp = new File(this.id.toString() + ".tmp");
		this.missing = Integer.MAX_VALUE;
		this.redundant = 0;
	}
	
	private FileDisseminationSession(FileDisseminationProtocol protocol, Transport net, File source, File dest, UUID id) {
		this.protocol = protocol;
		this.net = net;
		this.id = id;
		this.source = source;
		this.dest = dest;
		this.tmp = new File(this.id.toString() + ".tmp");
		this.missing = Integer.MAX_VALUE;
		this.redundant = 0;
	}
	
	private FileDisseminationSession(FileDisseminationProtocol protocol, Transport net, UUID id) {
		this.protocol = protocol;
		this.net = net;
		this.id = id;
		this.tmp = new File(this.id.toString() + ".tmp");
		this.missing = Integer.MAX_VALUE;
		this.redundant = 0;
	}
	
	public void run() {
		if(this.source == null) {
			return;
		}
		
		if(this.tmp.exists()) {
			this.tmp.delete();
		}
		try {
			this.tmp.createNewFile();
			this.file_output = new RandomAccessFile(this.tmp, "rw");
		} catch (Exception e) {
			FileDisseminationProtocol.log.error("Can not create temporary file. Aborting.");
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		}
		
		this.size = source.length();
		int payloadpermessage = FileDisseminationProtocol.maxPayload - 20;
		int payloadoffirstmessage = payloadpermessage - (this.dest.toString().length() + 2) + 8;
		if(this.size < payloadoffirstmessage)
			payloadoffirstmessage = (int) this.size;
		int payloadoflastmessage = (int) ((this.size - payloadoffirstmessage) % payloadpermessage);
		if(payloadoflastmessage == 0)
			payloadoflastmessage = payloadpermessage;
		
		this.missing = (int) (1 + ((this.size - payloadoffirstmessage) / payloadpermessage) + ((this.size - payloadoffirstmessage) % payloadpermessage == 0 ? 0 : 1)); 
		this.received = new boolean[missing];
		this.nextToSend = 0;
		
		try {
			this.file_input = new RandomAccessFile(this.source, "r");
		} catch (FileNotFoundException e) {
			FileDisseminationProtocol.log.error("Cannot open source file: " + this.source.toString());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
			this.protocol.failedSession(id);
		}
		
		
		while(this.received != null && this.nextToSend < this.received.length) {
			try {
				synchronized (this.net) {
					if(net.areQueuesFilled()) {
						net.wait();
					}
				}	
				
				ByteBuffer msg = null;
				if(this.nextToSend == 0 ) {
					msg = ByteBuffer.allocate(payloadoffirstmessage + 14 + this.dest.toString().length());
				} else if(this.nextToSend != this.received.length - 1) {
					msg = ByteBuffer.allocate(FileDisseminationProtocol.maxPayload);
				} else {	
					msg = ByteBuffer.allocate(payloadoflastmessage + 20);
				}
				
				msg.putInt(this.nextToSend);			//Write int - current msg index
				msg.putInt(this.received.length);		//Write int - max msg index
				
				byte[] local_buffer = new byte[payloadpermessage];
				
				if(this.nextToSend == 0) {
					//First message
					msg.putShort((short) this.dest.toString().length());
					msg.put(this.dest.toString().getBytes());
					msg.putInt(payloadoffirstmessage);
					this.file_input.seek(0);
					this.file_input.read(local_buffer, 0, payloadoffirstmessage);
					msg.put(local_buffer, 0, payloadoffirstmessage);
				} else {
					long seek = payloadoffirstmessage + ((this.nextToSend - 1) * payloadpermessage);
					msg.putLong(seek);
					if(this.nextToSend == this.received.length - 1) {
						//lastMessage
						msg.putInt(payloadoflastmessage);
						this.file_input.seek(seek);
						this.file_input.read(local_buffer, 0, payloadoflastmessage);
						msg.put(local_buffer, 0, payloadoflastmessage);
					} else {
						//NormalMessage
						msg.putInt(payloadpermessage);
						
						this.file_input.seek(seek);
						this.file_input.read(local_buffer, 0, payloadpermessage);
						msg.put(local_buffer, 0, payloadpermessage);
					}
				}
				msg.flip();
				synchronized (this) {
					this.nextToSend++;
				}
				
				this.deliver(new ByteBuffer[]{msg}, (short) -1, null, new Bloomfilter());
				//this.protocol.relay(msg, this.id, (short) 0, this.protocol.dataPort, null);
				
			} catch (IOException e) {
				FileDisseminationProtocol.log.error("Error while disseminating file. Aborting...");
				System.err.println(DateTimeRepresentation.timeStamp());
				e.printStackTrace(System.err);
				this.abort();
			} catch (Exception e) {
				FileDisseminationProtocol.log.error("Error while disseminating file. Aborting...");
				System.err.println(DateTimeRepresentation.timeStamp());
				e.printStackTrace(System.err);
				this.abort();
			}
		}
		
	}
	
	/**
	 * 
	 * @param msg ByteBuffer[] that contains the payload of a message for the file broadcast protocol to be processed by this session.
	 * @return true if this payload has never been seen before, false otherwise.
	 */
	public synchronized void deliver(ByteBuffer[] msg, short ttl, Connection info, Bloomfilter visited) {
		if(this.missing <= 0) {
			this.redundant++;
			return; //This session already terminated... this is a duplicate message received before closing the session.
		}
		
		int msg_index, max_index;
		ByteBuffer[] toProcess;
		
		try {	
			toProcess = Buffers.clone(msg);
			ByteBuffer buf = Buffers.sliceCompact(toProcess, 8);
			msg_index = buf.getInt();
			max_index = buf.getInt();
		} catch (Exception e) {
			FileDisseminationProtocol.log.error("Error while processing data message for file stream: " + this.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
			return;
		}
			
		long seek_position;
		
		try {
		
			if(this.received != null && this.received[msg_index]) {
				this.redundant++;
			} else {
				if(this.received == null) {
					this.received = new boolean[max_index];
					this.file_output = new RandomAccessFile(this.tmp, "rw");
					this.missing = max_index;
				}			
				
				this.received[msg_index] = true;
				this.missing--;
				
				//Forward the message...
				this.protocol.relay(msg, this.id, ++ttl, ttl == -1? this.protocol.senderDataPort : this.protocol.dataPort, info, visited);
				
				//Process the remaining of the message.
				if(msg_index == 0) {
					byte[] dst = new byte[Buffers.sliceCompact(toProcess, 2).getShort()];
					Buffers.sliceCompact(toProcess, dst.length).get(dst);
					this.dest = new File(new String(dst));
					seek_position = 0;
				} else {
					seek_position = Buffers.sliceCompact(toProcess, 8).getLong();
				}
				
				byte[] payload = new byte[Buffers.sliceCompact(toProcess, 4).getInt()];
				Buffers.sliceCompact(toProcess, payload.length).get(payload);
				
				this.file_output.seek(seek_position);
				this.file_output.write(payload);	
				
				if(this.missing == 0) {
					this.receptionComplete();
				}
			}
		} catch (FileNotFoundException e) {
			FileDisseminationProtocol.log.error("Error while processing data message for file stream: " + this.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		} catch (IOException e) {
			FileDisseminationProtocol.log.error("Error while processing data message for file stream: " + this.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		} catch (Exception e) {
			FileDisseminationProtocol.log.error("Error while processing data message for file stream: " + this.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		}
		
		
	}
	
	
	private void receptionComplete() {
		try {
			this.file_output.close();
			if(this.dest.exists())
				this.dest.delete();
			this.tmp.renameTo(this.dest);
			this.tmp.delete();
			this.received = null;
			this.protocol.sucessfullSession(this.id);
			this.protocol = null;
		} catch (Exception e) {
			FileDisseminationProtocol.log.error("Error while finishing the file broadcast session: " + this.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
			this.protocol.failedSession(this.id);
		}
	}

	public synchronized void abort() {
		this.protocol.failedSession(this.id);
		this.protocol = null;
	}
	
	public synchronized boolean hasStartedTransmission() {
		return this.nextToSend > 0;
	}
	
	public static final UUID createNewSession(FileDisseminationProtocol protocol, Transport net, File source, File dest) {
		FileDisseminationSession session = new FileDisseminationSession(protocol, net, source, dest);
		protocol.addSession(session.id, session);
		session.start();
		return session.id;
		
	}
	
	public static final UUID createNewSession(FileDisseminationProtocol protocol, Transport net, File source, File dest, UUID experienceID) {
		FileDisseminationSession session = new FileDisseminationSession(protocol, net, source, dest, experienceID);
		protocol.addSession(session.id, session);
		session.start();
		return session.id;
		
	}

	public static FileDisseminationSession instanciateSession(FileDisseminationProtocol protocol, Transport net, UUID session_id) {
		FileDisseminationSession session = new FileDisseminationSession(protocol, net, session_id);
		protocol.addSession(session_id, session);
		return session;
	}

	public String getDestinationFilename() {
		return this.dest.getName();
	}

	public long getDestinationFileSize() {
		if (this.dest.exists())
			return this.dest.length();
		else 
			return 0;
	}

	public synchronized String getStatus() {
		int total = (this.received != null ? this.received.length : Integer.MAX_VALUE);
		double received = total - this.missing;
		return "Total pieces: " + total  + " Missing: " + this.missing + " Received: " + ((int) received) + " Redundant messages received: " + this.redundant + " " + (received / total) * 100 + "%";
	}
	
}

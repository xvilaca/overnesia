package gsd.impl.components.BroadcastMonitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.UUID;

import gsd.api.ExternalControlInterface;
import gsd.api.HyParViewChannel;
import gsd.appl.control.experiences.data.BroadcastStatistics;
import gsd.appl.control.experiences.data.StartBroadcastExperienceRequest;
import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.UUIDs;
import gsd.impl.components.GossipBroadcastProtocol;

public class BroadcastMonitor implements DataListener{

	public final UUID experienceID;
	private HyParViewChannel instance;
	private GossipBroadcastProtocol gossip;
	private double p;
	private long r;
	private long d;
	private short port;
	private long gossipSent;
	private File logFile;
	private PrintStream out;
	private int node_ex_index;
	
	private static final long wait_time = 180*1000; //5 minutes
	private static final long final_wait_time = 20*60*1000;//20 min
	/**
     * ExternalControlInterface which might be useful to export data 
     * (THE LOCATION OF THIS INTERFACE HERE IS TEMPTATIVE)
     */
	private ExternalControlInterface eiface;
	
	public void setExternalControlInterface(ExternalControlInterface iface) {
    	this.eiface = iface;
    }
	
	public BroadcastMonitor(HyParViewChannel hyParViewChannel,
			GossipBroadcastProtocol gossip, StartBroadcastExperienceRequest params) {
		this.experienceID = params.getExperinceID();
		this.instance = hyParViewChannel;
		this.gossip = gossip;
		this.p = params.p;
		this.r = params.r;
		this.d = params.d;
		this.port = params.port;
		this.node_ex_index = params.getNodeIndex();
		
		this.gossipSent = 0;
		
		this.gossip.setDataListener(this, this.port);
		
		this.logFile = new File(this.experienceID.toString()+"-"+this.instance.getGossipNodeID().getSocketAddress().getHostName().toString()+".log");
		try {
			this.out = new PrintStream(logFile);
		} catch (FileNotFoundException e) {
			this.out = System.err;
		}
		
		this.instance.getTransport().schedule(new Runnable(){
			public void run() {
				executeRound();
			}
		}, wait_time);
	}

	public synchronized void executeRound() {
		if(this.instance.getRand().nextDouble() <= this.p) {
			//Start a gossip
			ByteBuffer msg = ByteBuffer.allocate(16+22+8);
			UUIDs.writeUUIDToBuffer(msg, this.experienceID);
			NodeID.writeNodeIdToBuffer(msg, this.instance.getGossipNodeID());
			msg.putLong(this.gossipSent);
			msg.flip();
			String msg_id = this.experienceID + " " + this.instance.getGossipNodeID().toString() + " " + this.gossipSent;
			
			this.gossip.sendMessage(new ByteBuffer[]{msg}, this.port);
			BroadcastStatistics.registerBCastStart(msg_id, out); 
		
			this.gossipSent++;
		}
		
		
		this.r--;
		if(this.r >= 0) {
			this.instance.getTransport().schedule(new Runnable(){
				public void run() {
					executeRound();
				}
			}, this.d);
		} else {
			this.instance.getTransport().schedule(new Runnable(){
				public void run() {
					terminate();
				}
			}, final_wait_time);
		}
	}
	
	public void terminate() {
		
		if(!out.equals(System.err))
			out.close();
		try {
			eiface.sendMessage(BroadcastStatistics.create(this.experienceID, this.instance.getGossipNodeID(), this.node_ex_index));
		} catch (IOException e) {
			System.err.println("Error while sending final report to control, for broadcast experience " + this.experienceID);
		}
		this.gossip.removeDataListener(port);
		this.instance.removeBroadcastMonitor(this.experienceID);
	}
	
	public void receive(ByteBuffer[] msg, Connection info, short port) {
		ByteBuffer b = Buffers.sliceCompact(msg, 16+22+8);
		
		UUID ex_id = UUIDs.readUUIDFromBuffer(b);
		NodeID sender = NodeID.readNodeIDFromBuffer(b);
		long index = b.getLong();
	
		if(ex_id.equals(this.experienceID)) {
			BroadcastStatistics.registerBCastReceive(ex_id.toString() + " " + sender.toString() + " " + index, this.out);
		}
	}

}

package gsd.impl.components;

import gsd.appl.HyParViewLabTest;
import gsd.appl.control.components.EnvironmentControl;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import org.apache.log4j.Logger;

public class GossipBasedUpdater extends Thread implements DataListener {

	private Logger log = Logger.getLogger(GossipBasedUpdater.class);
	private boolean isUpdating;
	private boolean isUpdated;
	private boolean isServing;
	private boolean errorFlag;
	private double newVersionNumber;
	private static final long executionPeriod = 2 * 60 * 1000; //5 minutes
	private Random r;
	private OverlayNetwork ovlay;
	private Transport net;
	private short gossipPort;
	private short gossipReplyPort;
	private short requestUpdatePort;
	private short codeUpdatePort;
	private short failedCodeUpdatePort;
	private Connection updateMaster;
	
	public GossipBasedUpdater(Random rand, Transport net, OverlayNetwork overlay, short[] ports) {
		this.isUpdating = false;
		this.isUpdated = false;
		this.isServing = false;
		this.errorFlag = false;
		this.newVersionNumber = 0;
		this.r = rand;
		this.ovlay = overlay;
		this.net = net;
		this.gossipPort = ports[0];
		this.gossipReplyPort = ports[1];
		this.requestUpdatePort = ports[2];
		this.codeUpdatePort = ports[3];
		this.failedCodeUpdatePort = ports[4];
		this.updateMaster = null;
	
		net.setDataListener(this, this.gossipPort);
		net.setDataListener(this, this.gossipReplyPort);
		net.setDataListener(this, this.requestUpdatePort);
		net.setDataListener(this, this.codeUpdatePort);
		net.setDataListener(this, this.failedCodeUpdatePort);
		
		net.setDiscardableData(this.gossipPort);
		net.setDiscardableData(this.gossipReplyPort);
	}
	
	/**
	 * Thread main loop
	 */
	@Override
	public void run() {
		try{Thread.sleep(60*1000);} catch (Exception e) {;} //Wait one minute before starting operation.
		
		while(true) {
			try {
				synchronized(this) {
					if(this.isUpdating && this.isUpdated) {
						this.isUpdating = false;
						this.disseminateCurrentVersion();
					} else if(!this.isUpdating && this.isUpdated && !this.isServing) {
						log.info("Gossip-Updater Module: Reseting node.");
						EnvironmentControl.resetOperation(this.ovlay, this.net);
					} else if(this.isUpdating && !this.isUpdated) {
						if(this.errorFlag || this.updateMaster == null || this.updateMaster.id == null || !this.updateMaster.isActive()) {
							this.errorFlag = false;
							this.errorWhileUpdatingCode();
						} else {
							this.errorFlag = true;
						}
					} else if(!this.isUpdating)  {
						this.disseminateCurrentVersion(); 
					}
					
					this.wait(GossipBasedUpdater.executionPeriod + (Math.abs(this.r.nextLong()) % (3*60*1000)));
				}	
			} catch (java.lang.InterruptedException e) {
				log.error("Interruption at the gossip-based updater module.");
				System.err.println(DateTimeRepresentation.timeStamp() + " Interrupt at the gossip-based updater.");
			} catch (Exception e) {
				log.error("Error during gossip-updater module operation. " + e.getClass().getName() + ": " + e.getMessage());
				System.err.println(DateTimeRepresentation.timeStamp() + " interrupt at the gossip-based updater.");
			}
		}
	}

	/**
	 * This code is here mostly for debug of the operation of this protocol.
	 */
	public synchronized String getInternalStatus() {
		return "isUpdating = " + this.isUpdating + "; isUpdated = " + this.isUpdated + "; isServing = " + this.isServing + "; servant = " + (this.updateMaster == null ? "none" : this.updateMaster.id) + "; currentVersion = " + this.currentVersionNumber() + "[var " + this.newVersionNumber + "]";
	}
	
	
	private void disseminateCurrentVersion() throws Exception {
		ByteBuffer[] buffer = new ByteBuffer[]{ByteBuffer.allocate(8)};
		double cvn = this.currentVersionNumber();
		buffer[0].putDouble(cvn);
		buffer[0].flip();
		for(Connection link: this.ovlay.connections()) {
			link.send(Buffers.clone(Buffers.clone(buffer)), this.gossipPort);
		}
	}

		
	/**
	 * DataListener Interface
	 */
	public synchronized void receive(ByteBuffer[] msg, final Connection info, short port) {
		try {
			if(port == this.gossipPort) {
				double pv = Buffers.sliceCompact(msg, 8).getDouble();
				if(pv > this.currentVersionNumber() && !this.isUpdating) {
					this.requestCodeUpdate(info, pv);
				} else if(this.currentVersionNumber() > pv) {
					ByteBuffer[] reply = new ByteBuffer[]{ByteBuffer.allocate(8)};
					reply[0].putDouble(this.currentVersionNumber());
					reply[0].flip();
					info.send(reply, this.gossipReplyPort);
				}
			} else if(port == this.gossipReplyPort) {
				double pv = Buffers.sliceCompact(msg, 8).getDouble();
				if(pv > this.currentVersionNumber() && !this.isUpdating) {
					this.requestCodeUpdate(info, pv);
				}
			} else if(port == this.requestUpdatePort) {
				double desiredVersion = Buffers.sliceCompact(msg, 8).getDouble();
				if(this.currentVersionNumber() == desiredVersion) {
					this.isServing = true;
					this.sendCode(info);
				} else {
					ByteBuffer[] reply = new ByteBuffer[]{ByteBuffer.allocate(8)};
					reply[0].putDouble(this.currentVersionNumber());
					reply[0].flip();
					info.send(reply, this.failedCodeUpdatePort);
				}
			} else if(port == this.codeUpdatePort) {
				byte[] data = new byte[(int) Buffers.sliceCompact(msg, 8).getLong()];
				Buffers.sliceCompact(msg, data.length).get(data);
				this.updateMyCodeVersion(data);				
			} else if(port == this.failedCodeUpdatePort) {
				log.info("Received failed code update notication from " + info.id);
				this.errorWhileUpdatingCode();				
			} 
			 else {
				this.log.info("Received a message for unknown data port: " + port);
			}
		} catch (Exception e) {
			log.error("Error while receiving message " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		}
	}
	
	private void errorWhileUpdatingCode() {
		log.info("Gossip-Updater Module: reseting internal state due to error while updating code" + (this.updateMaster == null ? "" : " with node " + this.updateMaster.id));
		this.isUpdating = false;
		this.errorFlag = false;
		this.updateMaster = null;
		this.newVersionNumber = 0;
	}
	
	private void updateMyCodeVersion(byte[] data) {
		try {
			
			File temp = new File("Overnesia.jar.temp");
			if(temp.exists())
				temp.delete();
			DataOutputStream out = new DataOutputStream(new FileOutputStream(temp));
			out.write(data);
			out.flush();
			out.close();
			data = null;
			this.isUpdated = true;
			this.updateMaster = null;
			this.errorFlag = false;
			try { this.interrupt(); } catch (Exception e) {;}
		} catch (FileNotFoundException e) {
			this.isUpdating = false;
			this.updateMaster = null;
			this.log.error("Error while writting updated code: " + e.getMessage());
		} catch (IOException e) {
			this.isUpdating = false;
			this.updateMaster = null;
			this.log.error("Error while writting updated code. " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp() + ": Error while finishing update.");
		}
	}

	private void requestCodeUpdate(Connection peer, double peerVersion) {
		this.isUpdating = true;
		this.updateMaster = peer;
		this.newVersionNumber = peerVersion;
		try {
			ByteBuffer[] msg = new ByteBuffer[]{ByteBuffer.allocate(8)};
			msg[0].putDouble(peerVersion);
			msg[0].flip();
			this.updateMaster.send(msg, this.requestUpdatePort);	
		} catch (Exception e) {
			log.error("Error while requesting code update to " + peer.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		}
	}
	
	private double currentVersionNumber() {
		synchronized (this) {
			if(this.isUpdated)
				return this.newVersionNumber;
			else
				return HyParViewLabTest.version_numb;
		}	
	}
	
	private void sendCode(Connection peer) {
		boolean exception = false;
		
		try {
			File source = this.isUpdated ? new File("Overnesia.jar.temp") : new File("Overnesia.jar");
			DataInputStream in = new DataInputStream(new FileInputStream(source));
			byte[] file_contents = new byte[(int) source.length()];
			in.read(file_contents);
			ByteBuffer[] data = new ByteBuffer[]{ByteBuffer.allocate(8), ByteBuffer.wrap(file_contents)};
			data[0].putLong(file_contents.length);
			data[0].flip();
			//data[1].flip();
			file_contents = null;
			peer.send(data, this.codeUpdatePort);
		} catch (FileNotFoundException e) {
			this.log.error("Error while sending updated code to " + peer.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			exception = true;
		} catch (IOException e) {
			this.log.error("Error while sending updated code to " + peer.id + ". " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			exception = true;
		}
		
		if(exception) {
			try {
				ByteBuffer[] reply = new ByteBuffer[]{ByteBuffer.allocate(8)};
				reply[0].putDouble(this.currentVersionNumber());
				reply[0].flip();
				peer.send(reply, this.failedCodeUpdatePort);
			} catch (Exception e) {
				this.log.error("Error while notifying " + peer.id + " of a failed update" +
						"" +
						". " + e.getClass().getName() + ": " + e.getMessage());
				System.err.println(DateTimeRepresentation.timeStamp());
				e.printStackTrace(System.err);
			}
		}
		
		
		this.isServing = false;
	}
}

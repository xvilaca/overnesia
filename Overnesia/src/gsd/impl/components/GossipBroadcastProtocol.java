package gsd.impl.components;

import gsd.api.ExternalControlInterface;
import gsd.appl.control.experiences.data.DataBroadcastNotification;
import gsd.appl.control.experiences.data.DataBroadcastRequest;
import gsd.appl.control.experiences.data.DataHelper;
import gsd.appl.control.experiences.data.Request;
import gsd.impl.Application;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;
import gsd.impl.UUIDs;

import java.util.*;
import java.io.IOException;
import java.nio.*;

import org.apache.log4j.Logger;


/**
 * Implementation of gossip. Like bimodal, combines a forward
 * retransmission phase with a repair phase. However, the
 * optimistic phase is also gossip bases. UUIDs, instead of
 * sequence numbers are used to identify and discard duplicates.  
 */
public class GossipBroadcastProtocol extends Thread implements DataListener {
    
	private Logger log = Logger.getLogger(GossipBroadcastProtocol.class);
	
	/**
     * ExternalControlInterface which might be useful to export data 
     * (THE LOCATION OF THIS INTERFACE HERE IS TEMPTATIVE)
     */
	private ExternalControlInterface eiface;
	
	/**
     * ConnectionListener management module.
     */
    private OverlayNetwork memb;

    /**
     *  Represents the class to which messages must be delivered.
     */
    private Application handler;

    /**
     *  The Transport port used by the Gossip class instances to exchange messages. 
     */
    private short bcastPort;
    private short randomWalkPort;
    
    /**
     * 	Set containing the identifiers of delivered messages.
     */
    private LinkedList<UUID> delivered;
 
    /**
     * Maximum number of stored message identifiers
     */
    private short maxIds;
    
    /**
     * TTL
     */
    private short ttl;
    
    /**
     * Handlers for external clients
     */
    
    private HashMap<Short, DataListener> handlers;
    
    /**
     * Message queue with messages (ByteBuffer) to gossip
     */
    private ArrayList<QElement> queue;
    
    private class QElement {
    	public short port;
    	public ByteBuffer[] msg;
    	
    	public QElement(ByteBuffer[] message, short port) {
    		this.port = port;
    		this.msg = message;
    	}
    }
    
    public void sendMessage(ByteBuffer[] message) {
    	synchronized (this.queue) {
			this.queue.add(new QElement(message, (short) 0));
			this.queue.notify();
		}
    }
    
    public void sendMessage(ByteBuffer[] message, short port) {
    	synchronized (this.queue) {
			this.queue.add(new QElement(message, port));
			this.queue.notify();
		}
    }
       
	/**
     *  Creates a new instance of Gossip.
     */
    public GossipBroadcastProtocol(Random rand, Transport net, OverlayNetwork overlay, short[] ports) {
    	this.memb = overlay;
        this.bcastPort = ports[0];
        this.randomWalkPort = ports[1];

        this.ttl = 7;
        this.maxIds = 1000;
        this.delivered = new LinkedList<UUID>();
            
        this.queue = new ArrayList<QElement>();
        
        net.setDataListener(this, this.bcastPort);
        net.setDataListener(this, this.randomWalkPort);
        
        //net.canDiscard(this.bcastPort); 
        handlers = new HashMap<Short, DataListener>();
    }
    
    /**
     * Add a reference to a message handler.
     */
    public synchronized boolean setDataListener(DataListener handler, short port) {
    	if(port > 0 && !this.handlers.containsKey(new Short(port))) {
    		this.handlers.put(new Short(port), handler);
    		return true;
    	} else
    		return false;
    }
    
    /**
     * Removes a reference to a message handler.
     */
    public synchronized boolean removeDataListener(short port) {
        if(this.handlers.containsKey(new Short(port))) {
        	this.handlers.remove(new Short(port));
        	return true;
        } else
        	return false;
        	
    }
    
    public void setExternalControlInterface(ExternalControlInterface iface) {
    	this.eiface = iface;
    }
    
    public void handler(Application handler) {
        this.handler = handler;
    }
       
    @Override
	public void run() {
    	while(true) {
    		synchronized (this.queue) {
    			if(this.queue.size() > 0) {
    				short port = this.queue.get(0).port;
    				ByteBuffer[] msg = this.queue.remove(0).msg;
    				if(msg != null)
    					try {
    						this.log.debug("I am initiating the broadcast of a message now...");
    						this.broadcast(msg, port);
    					} catch (Exception e) {
    						e.printStackTrace(System.err);
    					}
    			} else {
    				try {
    					this.queue.wait();
    				} catch (InterruptedException e) {
    					;
    				}
    			}
    		}
    	}
    }
    
    private void broadcast(ByteBuffer[] msg, short port) {
    	ByteBuffer[] msg2 = new ByteBuffer[msg.length+1];
    	System.arraycopy(msg, 0, msg2, 1, msg.length);
    	msg2[0] = ByteBuffer.allocate(2);
    	msg2[0].putShort(port);
    	msg2[0].flip();
    	handleData(msg2, UUID.randomUUID(), (short) 0, null);
    }
    
    public void receive(ByteBuffer[] msg, Connection info, short port) {  	
    	if (port == this.bcastPort) {
    		ByteBuffer buf = Buffers.sliceCompact(msg, 16 + 2);
        	UUID uuid = UUIDs.readUUIDFromBuffer(buf);	
        	short hops = buf.getShort();
    		handleData(msg, uuid, ++hops, info);
    	} 
	}
        
    private void handleData(ByteBuffer[] msg, UUID uuid, short hops, Connection info) {
		synchronized (this.delivered) {
			if (delivered.contains(uuid)) {
				this.log.info("Rejecting redundant broadcast message: " + uuid);
				return;
			}
			
			this.registerMesssageID(uuid);
		}
    		
		try {
			this.log.info("Delivering broadcast message: " + uuid);
			this.deliver(Buffers.clone(msg), info);
		} catch (Exception e) {
			this.log.error("Exception while delivering data. (" + e.getClass().getName() + ": " + e.getMessage() +")");
			e.printStackTrace(System.err);
			return;
		} 
		
		if (hops>ttl)
			return;
		
		ByteBuffer[] out = null;
		
		try {
			out = new ByteBuffer[msg.length + 1];
			out[0] = ByteBuffer.allocate(16 + 2);
			UUIDs.writeUUIDToBuffer(out[0], uuid);
			out[0].putShort(hops);
			out[0].flip();
			System.arraycopy(msg, 0, out, 1, msg.length);
		} catch (Exception e) {
			this.log.error("Exception while preaparing data. (" + e.getClass().getName() + ": " + e.getMessage() +")");
			e.printStackTrace(System.err);
			return;
		} 
		
		try {
			this.relay(out, this.bcastPort, memb.connections(), info);
		} catch (Exception e) {
			this.log.error("Exception while forwarding data. (" + e.getClass().getName() + ": " + e.getMessage() +")");
			e.printStackTrace(System.err);
		} 
		
    }
    
    private void registerMesssageID(UUID uuid) {
		while(this.delivered.size() >= this.maxIds)
			this.delivered.removeLast();
		this.delivered.addFirst(uuid);
    }

	private void relay(ByteBuffer[] msg, short port, Connection[] conns, Connection source) {
        
        for(Connection link: this.memb.connections()) {
        	if(source == null || !source.equals(link)) {
            	try {
            		this.log.debug(memb.getNodeIdentifier() + " forwarding gossip message to " + link.id + " (" + link.getPeer() + ")");
            		link.send(Buffers.clone(msg), port);    
            	} catch (Exception e) {
            		System.err.println(e);
            		this.log.error(memb.getNodeIdentifier() + "forward failed due to " + e.getClass().getName() + ": " + e.getMessage());
            	}
            	this.log.debug(memb.getNodeIdentifier() + " forward complete...");
        	}
        }
    }
    
    public int getMaxIds() {
        return maxIds;
    }

    public void setMaxIds(short maxIds) {
        this.maxIds = maxIds;
    }

	public int getTtl() {
		return ttl;
	}

	public void setTtl(short ttl) {
		this.ttl = ttl;
	}
	
	private synchronized void deliver(ByteBuffer[] msg, Connection info) {
		short port = Buffers.sliceCompact(msg, 2).getShort();
		if(port == 0) 
			this.deliverRequest(DataHelper.decode(msg));
		else if(this.handlers.containsKey(new Short(port))) {
			this.handlers.get(new Short(port)).receive(msg, info, port);
		} else {
			System.err.println("Gossip protocol: Received message from unregistered application (port " + port + ").");
		}
	}
	
	private void deliverRequest(Request r) {
		System.err.println("Initiating deliver operation. " + (eiface == null ? "eiface is null" : "eiface is not null"));
		Object reply = null;
		
		switch(r.getOperationCode()) {
		case DataBroadcastRequest.operCode:
			reply = new DataBroadcastNotification(r.getExperinceID(), this.memb.getNodeIdentifier(), r.getNodeIndex());
			break;
		default:
			reply = new String("Received a request with an invalid operation code: " + r.getOperationCode() + ". Experience ID: " + r.getExperinceID());
		}
		
		if(eiface != null) {
			try {
				eiface.sendMessage(reply);
			} catch (IOException e) {
				e.printStackTrace(System.err);
				//Logger.getLogger(this.getClass()).error("Error sending message to control station. " + e.getClass().getName() + ": " + e.getMessage());
			}
		} else {
			handler.deliver(new ByteBuffer[]{ByteBuffer.wrap(reply.toString().getBytes())});
		}

			
	}
	
}


package gsd.impl;

import java.io.IOException;

public interface GenericConnection {

	void handleClose();

	void handleRead();

	void handleAccept() throws IOException;

	void handleConnect() throws IOException;

	void handleWrite();
	
	int queueSize();

}

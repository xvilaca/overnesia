package gsd.impl;

import gsd.common.NodeID;

public interface OverlayAware {

	public void neighborUp(NodeID neighbor, Connection info);
	
	public void neighborDown(NodeID neighbor);
	
}

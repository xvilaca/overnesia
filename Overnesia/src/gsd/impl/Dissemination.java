package gsd.impl;


public class Dissemination {

	private int quantity;

	public Dissemination(int number){
		this.quantity = number;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	

	
}

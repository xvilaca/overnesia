package gsd.impl;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Set;

public class Bloomfilter  {
    
    private OpenBitSet filter;
  
    public static final int numHash = 7;
    public static final int filterSize = 256;
    
    public Bloomfilter() {
    	filter = new OpenBitSet(filterSize);
    }
    
    public Bloomfilter(Set<Object> set){
		
		filter = new OpenBitSet(filterSize);
		
		Iterator<Object> iterator = set.iterator();
		while(iterator.hasNext())
		    accessFilterPositions(iterator.next().toString().getBytes(), true);
	
    }

    public Bloomfilter(OpenBitSet bitset){
    	filter = bitset;
	}

	public OpenBitSet getFilter(){
    	return filter;
    }

    public int getnumHash(){
    	return numHash;
    }

   
    /* (non-Javadoc)
     * @see xtramy.protocols.certificationreadwriteset.Bloomfilter#containsElement(byte[])
     */
    public boolean containsElement(Object item) {
    	return accessFilterPositions(item.toString().getBytes(), false);
    }

    public void addElement(Object item) {
    	accessFilterPositions(item.toString().getBytes(), true);
    }
   
    private boolean accessFilterPositions(byte[] elementId, boolean set) {
	int hash1 = murmurHash(elementId, elementId.length, 0);
	int hash2 = murmurHash(elementId, elementId.length, hash1);
	for (int i = 0; i < numHash; i++){
	    if(set)
		filter.set(Math.abs((hash1 + i * hash2) % filterSize), true);
	    else
		if(!filter.get(Math.abs((hash1 + i * hash2) % filterSize)))
		    return false;
	}
	return true;
    }

    private int murmurHash(byte[] data, int length, int seed) {
	int m = 0x5bd1e995;
	int r = 24;

	int h = seed ^ length;

	int len_4 = length >> 2;

	for (int i = 0; i < len_4; i++) {
	    int i_4 = i << 2;
	    int k = data[i_4 + 3];
	    k = k << 8;
	    k = k | (data[i_4 + 2] & 0xff);
	    k = k << 8;
	    k = k | (data[i_4 + 1] & 0xff);
	    k = k << 8;
	    k = k | (data[i_4 + 0] & 0xff);
	    k *= m;
	    k ^= k >>> r;
	    k *= m;
	    h *= m;
	    h ^= k;
	}

	// avoid calculating modulo
	int len_m = len_4 << 2;
	int left = length - len_m;

	if (left != 0) {
	    if (left >= 3) {
		h ^= (int) data[length - 3] << 16;
	    }
	    if (left >= 2) {
		h ^= (int) data[length - 2] << 8;
	    }
	    if (left >= 1) {
		h ^= (int) data[length - 1];
	    }

	    h *= m;
	}

	h ^= h >>> 13;
		h *= m;
		h ^= h >>> 15;

	    return h;
    }
    
    /**
	 * Write a BloomFiler to a ByteBuffer.
     * @param bf The BloomFiler to be written.
     * @return The Buffer with the BloomFiler written into.
     */
    public static ByteBuffer writeUUIDToBuffer(Bloomfilter bf) {
        long[] bits = bf.filter.getData();
    	ByteBuffer buf = ByteBuffer.allocate(4 + (8*bits.length));
        buf.putInt(bits.length);
        for(int i = 0; i < bits.length; i++)
        	buf.putLong(bits[i]);
        buf.flip();
        return buf;
    }
    
    /** Read an BloomFiler from an array of ByteBuffers into an BloomFiler.
     * @param msg The buffer from which to read the BloomFiler from.
     * @return The BloomFiler read.
     */
    public static Bloomfilter readUUIDFromBuffer(ByteBuffer[] msg) {
    	long[] bits = new long[Buffers.sliceCompact(msg, 4).getInt()];
    	ByteBuffer buf = Buffers.sliceCompact(msg, 8*bits.length);
    	for(int i = 0; i < bits.length; i++)
    		bits[i] = buf.getLong();
        return new Bloomfilter(new OpenBitSet(bits));
    }
}
package gsd.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Frame {

	private int frameID = -1;
	private byte[] frame;

	public Frame(int id){
		this.frameID = id;

	}

	public Frame(int id, byte[] frameObj){
		this.frameID = id;
		frame = frameObj;	
	}

	public int getFrameID(){
		return this.frameID;
	}

	public byte[] getFrame(){
		return this.frame;
	}


	public static String getPackName(int i, int port){
		return "package"+i+"_"+ port;
	}


	public static boolean wantThis(int packId, int port){
		String file = getPackName(packId, port);
		File pack = new File("results/"+file);
		if(pack.exists()) {
			FileWriter writer;
			try {
				writer = new FileWriter(pack);
				writer.write("redundant\n"); 
				writer.flush();
				writer.close();
			} catch (IOException e) {
				System.out.println("Cannot write to file");
				e.printStackTrace();
			} 
		}
		return !pack.exists();
	}



	private static void storeFrame(int packID,int port) {
		boolean sucess = false;
		File file= null;
		try {
			File dir = new File("results/");
			dir.mkdir();
			file = new File("results/"+Frame.getPackName(packID, port));
			sucess = file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(sucess)
			System.out.println("Was file " + file.getPath() + " created ? : " + sucess);
	}


	public void storeFrameFromPeer(int port) {
		storeFrame(this.frameID, port);
	}


	public static void storeFrameFromServer(int packID, int port) {
		storeFrame(packID, port);
	}


	public static int getAllocationSize() {
		return 4;
	}


	public static void writeFrameToBuffer(ByteBuffer msg, Frame frame) {
		msg.putInt(frame.frameID);
	}


	public static Frame readFrameFromBuffer(ByteBuffer msg) {
		return new Frame(msg.getInt());
	}
}

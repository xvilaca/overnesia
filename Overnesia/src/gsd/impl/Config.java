package gsd.impl;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public abstract class Config {
	
	final static protected boolean modelnet = false;
	
	protected InetSocketAddress toModelnetAddr(InetSocketAddress peer) {
		if(Config.modelnet) {
			byte[] myAddr = peer.getAddress().getAddress();
			myAddr[1]|=0x80;
			try {
				return new InetSocketAddress(InetAddress.getByAddress(myAddr),peer.getPort());
			} catch (UnknownHostException e) {
				System.err.println("Cannot convert addr to modelnet format: " + peer);
				System.exit(1);
				return null;
			}
		} else {
			return peer;
		}
	}
}

package gsd.impl;

import gsd.appl.utils.DateTimeRepresentation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;

/**
 * Connection with a peer. This class provides event handlers
 * for a connection. It also implements multiplexing and queueing.
 */
public class DatagramConnection implements GenericConnection {
	
	private boolean pendingOperation;

	/**
	 * Create a new connection.
	 * 
	 * @param net transport object
	 * @param bind local address to bind to, if any
	 * @param conn allow simultaneous outgoing connection
     * @param rand random generator
	 * @throws IOException 
	 */
	DatagramConnection(Transport trans, InetSocketAddress bind) throws IOException {
    	this.transport = trans;
		
		sock = DatagramChannel.open();
		sock.configureBlocking(false);
		if (bind != null) {
			sock.socket().setReuseAddress(true);
		}
		sock.socket().bind(bind);
		sock.socket().setSendBufferSize(transport.getBufferSize());
        sock.socket().setReceiveBufferSize(transport.getBufferSize());
		
        queue = new Queue(transport.getQueueSize(), transport.rand, transport);
        connected=true;
        
		key = sock.register(transport.selector,	SelectionKey.OP_READ);
		key.attach(this);
				
		this.pendingOperation = false;
	}
    

    /**
     * Send message to peers
     * @param msg The message to be sent.
     * @param port Port, at transport layer, where the message must be delivered.
     */
    public void send(ByteBuffer[] msg, short port, InetSocketAddress addr) {
    	if (key==null)
            return;
    	synchronized (queue) {
    		 UDPQueued b = new UDPQueued(msg, new Short(port), addr);
    	        queue.push(b);
    	        handleWrite();
		}
    }

    public void close() {
    	handleClose();
    }

    // --- Event handlers
    
	void handleGC() {
		if (!dirty && outgoing == null) {
		    handleClose();
		} else {
		    dirty = false;
		}
	}
    
   /** Write event handler.
     * There's something waiting to be written.
     */
    public void handleWrite() {
        synchronized (queue) {
			
	    	if (queue.isEmpty() && outgoing == null) {
	            key.interestOps(SelectionKey.OP_READ);
	            return;
	        }      
	        
	        try {
	           UDPQueued b = (UDPQueued) queue.pop();
	           ByteBuffer[] msg = b.getMsg();
	           if (msg == null) {
	              return;
	           }
	           short port = b.getPort();
	           int size = 0;
	
	           for (int i = 0; i < msg.length; i++) {
	               size += msg[i].remaining();
	           }
	           ByteBuffer header = ByteBuffer.allocate(6);
	
	           header.putInt(size);
	           header.putShort(port);
	           header.flip();
	           ByteBuffer[] outgoings = new ByteBuffer[msg.length + 1];
	           outgoings[0] = header;
	           System.arraycopy(msg, 0, outgoings, 1, msg.length);
	           outgoing = Buffers.compact(outgoings);
	           int n = sock.send(this.outgoing, b.getDest());
	           if(n == 0) {
	        	   queue.push(b);
	        	   key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
	           } else {
	        	   transport.bytesOut+=n;
	        	   key.interestOps(SelectionKey.OP_READ);
	           }
	                    
	        } catch (java.lang.IndexOutOfBoundsException e) {
	        	System.err.println("Error while accessing outgoing Queue element in the datagram socket.");
	        	if (queue.isEmpty() && outgoing == null) 
		            key.interestOps(SelectionKey.OP_READ);
	        	else 
	        		key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
	        	return;
		    } catch (IOException e) {
	            handleClose();
	            //return;
	        } catch (CancelledKeyException cke) {
	            transport.notifyClose(null);
	        }
	        
        }
        
    }

    public void handleRead() {	
        ByteBuffer incoming = ByteBuffer.allocate(transport.getBufferSize());
        ByteBuffer copy = incoming.asReadOnlyBuffer();
        
        try {       
        	InetSocketAddress peer = (InetSocketAddress) sock.receive(incoming);
            if (peer == null) {
                return;
            } 
        } catch (IOException e) {
            handleClose();
            return;
        }
            
        try {
	        int n = incoming.capacity() - incoming.remaining();
	        transport.bytesIn += n;
	        copy.limit(n);
	        msgsize = copy.getInt();
	        port = copy.getShort();
	        
	        final Short prt = new Short(port);
	       	transport.pktIn++;
	       	copy = copy.slice();
	       	copy.limit(msgsize);
	        final ByteBuffer[] msg = new ByteBuffer[]{copy};
	        transport.deliver(null, prt, msg);  
        } catch(IllegalArgumentException e) {
        	System.err.println(DateTimeRepresentation.timeStamp() + ": Received bad format UDP packet. (Triggered an illegar argument type in code, msgsize="+msgsize+")");
    	} catch (Exception e) {
        	e.printStackTrace(); //TODO: review this portion of the code...
        }
        
        key.interestOps(SelectionKey.OP_READ);
    }
    
    /**
     * Closed connection event handler.
     * Either by hyParView management or death of peer.
     */
    public void handleClose() {
    	if (key!=null) {
    		try {
    			connected=false;
				key.channel().close();
				key.cancel();
				sock.close();
			} catch (IOException e) {
				// Don't care, we're cleaning up anyway...
			}
			key = null;
			sock = null;
    	}
    }

	public InetSocketAddress getPeer() {
		return null;
	}

	public InetSocketAddress getLocal() {
		if (sock!=null)
			return (InetSocketAddress) sock.socket().getLocalSocketAddress();
		return null;
	}
    
    public InetAddress getRemoteAddress() {
        return ((InetSocketAddress) this.sock.socket().getRemoteSocketAddress()).getAddress();
    }
    
    private Transport transport;
    protected DatagramChannel sock;
    private SelectionKey key;

    //private ByteBuffer incoming, copy;
    //private ArrayList<ByteBuffer> incomingmb;
    private int msgsize;

    private ByteBuffer outgoing;
    //private int outremaining;
    private short port;
 
    private boolean dirty, connected;

    /** Message queue
     */
    public Queue queue;

    /**
     * A protocol can associate to this connection a node identity (in the form of an object)
     */
    public Object id;
        
    public void clearPendingOperation() {
    	this.pendingOperation = false;
    }
    
    public boolean requiresCleaning() {
    	return this.pendingOperation;
    }

    /**
     * Verifies if the TCP connection associated with this instance is active.
     * @return
     */
    
	public boolean isActive() {
		return this.connected;
	}


	public void handleAccept() throws IOException {
		//No Operation;
	}


	public void handleConnect() throws IOException {
		//No Operation
	}
	
	public int queueSize() {
		synchronized (queue) {
			return this.queue.getSize();
		}
	}
}


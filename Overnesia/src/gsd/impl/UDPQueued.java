package gsd.impl;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public class UDPQueued extends Queued {

	private InetSocketAddress addr;
	
	public UDPQueued(ByteBuffer[] msg, short port, InetSocketAddress addr) {
		super(msg, port);
		this.addr = addr;
	}

	public InetSocketAddress getDest() {
		return this.addr;
	}
}

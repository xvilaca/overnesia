package gsd.impl.hyparview;

import gsd.api.ConstValues;
import gsd.api.PropertiesFactory;
import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.ConnectionListener;
import gsd.impl.DataListener;
import gsd.impl.OverlayAware;
import gsd.impl.OverlayNetwork;
import gsd.impl.Periodic;
import gsd.impl.RandomSamples;
import gsd.impl.Transport;
import gsd.impl.hyparview.messages.ForwardedJoinRequest;
import gsd.impl.hyparview.messages.JoinReply;
import gsd.impl.hyparview.messages.JoinRequest;
import gsd.impl.hyparview.messages.JoinWalkerRequest;
import gsd.impl.hyparview.messages.NeighborReply;
import gsd.impl.hyparview.messages.NeighborRequest;
import gsd.impl.hyparview.messages.ShuffleReply;
import gsd.impl.hyparview.messages.ShuffleRequest;
import gsd.impl.hyparview.utils.NodeIDHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class BCK_ImmortalHyParView implements OverlayNetwork, ConnectionListener, DataListener {
	
	private Logger log = Logger.getLogger(ImmortalHyParView.class);
	
	/**
	 * Key attributes (passed by the constructor)
	 */
	private Random rand;
	private Transport net;
		
	/**
	 * Protocol parameters
	 */
	protected short activeViewSize;
	protected short passiveViewSize;
	protected short activeShuffleLenght;
	protected short passiveShuffleLenght;
	protected short randomWalkLenght;
	protected short passiveRandomWalkLenght;
	
	/**
	 * Message ports
	 */
	private short joinRequestPort;
	private short forwardJoinRequestPort;
	private short joinWalkerPort;
	private short neighborRequestPort;
	private short neighborReplyPort;
	private short shuffleRequestPort;
	private short shuffleReplyPort;
	private short disconnectRequestPort;
	private short joinReplyPort;
	
	/**
	 * Other protocol state parameters
	 */
	protected NodeID myId;
	private HashMap<InetSocketAddress,Operation> operations;
	private Periodic shuffle;
	private Periodic fillActiveView;
	private NodeID[] lastShuffleSent;
	private HashMap<NodeID,Connection>  activeView;
	private LinkedList<NodeID> passiveView;
	
	//To process neighboring
	short pendingNeighboringRequests;
	int neighboringUniversePosition;
	int[] neighboringUniverse;
	
	//The shutdown variable...
	private boolean shutdown;
	
	/**
	 * For handling overlay aware protocols
	 */
	private ArrayList<OverlayAware> overlayAwareProtocols;
	
	/**
	 * 
	 * @param rand
	 * @param net
	 * @param ports
	 */
	public BCK_ImmortalHyParView(Random rand, Transport net, short[] ports, InetSocketAddress myId) {
		//Key atributes atribution;
		this.rand = rand;
		this.net = net;
	
		//We want to use udp so...
		this.net.activateUDP();
		
		//Message ports definition
		this.joinRequestPort = ports[0]; //103
		this.forwardJoinRequestPort = ports[1]; //104
		this.joinReplyPort = ports[2]; //105
		this.neighborRequestPort = ports[3]; //106
		this.neighborReplyPort = ports[4]; //107
		this.shuffleRequestPort = ports[5]; //108
		this.shuffleReplyPort = ports[6]; //109
		this.joinWalkerPort = ports[7]; //110
		this.disconnectRequestPort = ports[8]; //111
		
		//Protocol parameters initialization [Default values]
		this.activeViewSize = 5;
		this.passiveViewSize = 30;
		this.activeShuffleLenght = 3;
		this.passiveShuffleLenght = 4;
		this.randomWalkLenght = 6;
		this.passiveRandomWalkLenght = 3;
		
		//Other protocol state variables
		//ChangeLog: JLeitao - 18 August 2010 - I am updating this to enforce each node to use a single UUID
		this.myId = NodeIDHelper.getMyNodeID(myId);
	
		this.shutdown = false;
		
		this.shuffle = new Periodic(rand, net, 10000) { //10 Second
			public void run() {
				executeShuffle();
			}
		};
		this.shuffle.start();
		this.lastShuffleSent = null;
		
		this.fillActiveView = new Periodic(rand, net, 3000) { //3 Second
			public void run() {
				startNeighboring();
				
			}
		};
		this.neighboringUniverse = RandomSamples.mkUniverse(this.passiveViewSize);
		this.neighboringUniversePosition = this.passiveViewSize;
		this.fillActiveView.start();
		
		this.activeView = new HashMap<NodeID, Connection>();
		this.passiveView = new LinkedList<NodeID>();
		this.operations = new HashMap<InetSocketAddress,Operation>();
		this.pendingNeighboringRequests = 0;
		
		//Prepare callbacks
        net.setDataListener(this, this.joinRequestPort);
        net.setDataListener(this, this.forwardJoinRequestPort);
        net.setDataListener(this, this.joinReplyPort);
        net.setDataListener(this, this.neighborRequestPort);
        net.setDataListener(this, this.neighborReplyPort);
        net.setDataListener(this, this.shuffleRequestPort);
        net.setDataListener(this, this.shuffleReplyPort);
        net.setDataListener(this, this.joinWalkerPort);
        net.setDataListener(this, this.disconnectRequestPort);
        net.setConnectionListener(this);
	}

	/**
	 * Public methods to support gossip protocol
	 */
		
	public synchronized Connection[] connections() {
		return activeView.values().toArray(new Connection[this.activeView.size()]);
	}
	
	public synchronized NodeID[] getPeers() {
		return this.activeView.keySet().toArray(new NodeID[this.activeView.size()]);
	}
	
	public synchronized InetSocketAddress[] getPeerAddresses() {
		InetSocketAddress[] list = new InetSocketAddress[this.activeView.size()];
		int i = 0;
		for(NodeID peer: this.activeView.keySet())
			list[i++] = peer.getSocketAddress();
		
		return list;
	}
	
	//Old interface
	public int getFanout() {
		return this.getActiveMembershipSize() - 1;
	}
	
	//Old interface
	public void setFanout(short fanout) {
		this.setActiveMembershipSize((short) (fanout+1));
	}
	
	/**
	 * This method handles the opening of TCP connections in the underlying layer...
	 */
	
	private void openConnection(final InetSocketAddress contact, Object info, short operationCode) {
		if(!operations.containsKey(contact)) {
			Operation oper = new Operation(operationCode, info);
			log.info("Opening connection to " + contact + " for " + oper + " with info: " + info);
			this.operations.put(contact, oper);
			this.net.queue(new Runnable() {
	            public void run() {
	                net.add(contact);
	            }
	        });
		} else {
			log.info("Rejected the creation of a new TCP Connection to: " + contact);
			log.info("Current Operation was: " + new Operation(operationCode));
			log.info("Existing Operation is: " + operations.get(contact));
		}
	}
	
	/**
	 * initialization interface for the overlay network
	 */
	
	private ArrayList<String> cached;
	public static final int cache_size = 100;
	public static final String cache_filename = "node_cache.hyparview";
	public static final String recovery_filaname = "node_recovery.hyparview"; 
	public static final long cache_update_interval = 15 * 60 * 1000; //15 Min
	
	private int cache_counter = -1;
	private File recovery;
	
	private ArrayList<String> readCache() {
		ArrayList<String> c = new ArrayList<String>();
	Scanner sc = null;
		try {
			sc = new Scanner(new File(ImmortalHyParView.cache_filename));
			while(sc.hasNextLine()) {
				String host = sc.nextLine();
				if(!c.contains(host) && !host.equals(myId.getSocketAddress().getAddress().getHostAddress()))
					c.add(host);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		sc.close();
		
		return c;
	}
	
	private Thread aliveControl;
	private int randomTries = 5;
	
	public synchronized void init() {
		this.cached = this.readCache();
		PropertiesFactory prop = new PropertiesFactory(1); //TODO @author Daniel Quinta
		if(this.cached.size() == 0 && !myId.getSocketAddress().getAddress().getHostAddress().equals(prop.getProperty("serverIP")))			
			this.cached.add(prop.getProperty("serverIP")); 
		
		recovery = new File(ImmortalHyParView.recovery_filaname);
		if(!recovery.exists()) 
			recovery = null;
		
		this.aliveControl = new Thread() {
			
			@Override
			public void run() {
				while(true) {
					synchronized (cached) {
						if(shutdown)
							return;
						
						if(recovery != null) {
							Scanner scan = null;
							try {
								scan = new Scanner(recovery);
								while(scan.hasNextLine()) {
									openConnection(new InetSocketAddress(scan.nextLine(), myId.getSocketAddress().getPort()), null, Operation.JOIN_WALKER);
								}
								recovery.delete();
								recovery = null;
							} catch (Exception e) {
								System.err.println(DateTimeRepresentation.timeStamp());
								e.printStackTrace(System.err);
								continue;
							}  finally{
								try{
									scan.close();
								}
								catch(Exception es){}
							}
						} else if(activeView.size() == 0) {
							if(cached.size() > 0) {
								if(randomTries <= 0) {
									cache_counter++;
									if(cache_counter >= cached.size())
										cache_counter = 0;
									init(new InetSocketAddress(cached.get(cache_counter), myId.getSocketAddress().getPort()));
								} else {
									init(new InetSocketAddress(cached.get(rand.nextInt(cached.size())), myId.getSocketAddress().getPort()));
									randomTries--;
								}
								try { cached.wait(3 * 60 * 1000); } catch (InterruptedException e) {} //3 min to connect
								continue;
							}
						} else {
							updateCache();
						}
						
						try { cached.wait(ImmortalHyParView.cache_update_interval); } catch (InterruptedException e) {
							//nothing to do...
						}
					}
				}
			}
			
		};
		
		this.aliveControl.start();
	}
	
	public synchronized void updateCache() {
		ArrayList<NodeID> localCopy = new ArrayList<NodeID>(activeView.keySet());
		boolean dirty = false;
		for( NodeID id: localCopy ) {
			if(!cached.contains(id.getSocketAddress().getAddress().getHostAddress())) {
				if(cached.size() == ImmortalHyParView.cache_size) {
					cached.remove(rand.nextInt(cached.size() - 1) + 1);
				}
				cached.add(id.getSocketAddress().getAddress().getHostAddress());
				dirty = true;
			}
		}
		if(dirty) {
			try {
				File temp = new File(ImmortalHyParView.cache_filename + ".temp");
				PrintStream out = new PrintStream(temp);
				for(String s: cached) 
					out.println(s);
				out.close();
				temp.renameTo(new File(ImmortalHyParView.cache_filename));
			} catch (FileNotFoundException e) {
				//Nothing to do
			}
		}
	}
	
	public synchronized void init(final InetSocketAddress contact) {
		log.info("Attempring to start a join operation with: " + contact);
		this.openConnection(contact, null, Operation.JOIN);
	}
	
	/**
	 * Private methods to handle protocol specific messages
	 */
	
	protected void handleJoinRequest(ByteBuffer[] msg, Connection info) {
		NodeID newPeer = JoinRequest.readJoinRequestFromBuffer(msg).getNewNode();
		if(this.activeView.size() > 0) {
			//First of all it is required to check if there is space in the active view, and make it if required
			if(this.activeView.size() == this.activeViewSize) {
				//Drop a random element using a disconnect request
				this.disconnectRandomPeer();
			}
			
			//Prepare a forwardedJoinRequestBuffer	
			ByteBuffer[]forwardJoinRequest = {ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(new ForwardedJoinRequest(this.randomWalkLenght,newPeer))};
			
			//Forward request for all neighbors in the active view
			for(Connection link: this.activeView.values()) {
				link.send(Buffers.clone(forwardJoinRequest), this.forwardJoinRequestPort);
			}
		}
		
		if(!this.activeView.containsKey(newPeer)) {
			info.id = newPeer;
			info.send(new ByteBuffer[]{JoinReply.writeJoinReplyToBuffer(new JoinReply(this.myId))},this.joinReplyPort);
			this.addToActiveView(newPeer,info);
		} else {
			//Terminate connection... [this should never happen...]
			info.id = null;
			info.close();
		}
		
	}
	
	protected void handleForwardJoinRequest(ByteBuffer[] msg, Connection info) {
		ForwardedJoinRequest request = ForwardedJoinRequest.readForwardedJoinRequestFromBuffer(msg);
		Connection[] candidatesToForward = forwardingCandidates(request.getNewNode(), info);
			
		if(request.getTTL() == 0 || candidatesToForward.length == 0) {
			if(this.canAddToActiveView(request.getNewNode()))
				this.openConnection(request.getNewNode().getSocketAddress(), request.getNewNode(), Operation.ACCEPTJOIN);
		} else {
			if (request.getTTL() == this.passiveRandomWalkLenght) {
				if(this.canAddToPassiveView(request.getNewNode()))	
					this.addToPassiveView(request.getNewNode());
			}
			request.decrementTTL();
			candidatesToForward[this.rand.nextInt(candidatesToForward.length)].send(new ByteBuffer[]{ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(request)}, this.forwardJoinRequestPort);			
		}
		
	}

	protected void handleJoinWalker(ByteBuffer[] msg, Connection info) {
		JoinWalkerRequest request = JoinWalkerRequest.readJoinWalkerRequestFromBuffer(msg);
		Connection[] candidatesToForward = forwardingCandidates(request.getNewNode(),  info);

		boolean accepted = false;
		short initialTTL = request.getTTL();
		
		if(initialTTL == 0 || candidatesToForward.length == 0) {
			if(this.canAddToActiveView(request.getNewNode())) { 
				accepted = true;
				if(initialTTL == this.randomWalkLenght) {
					info.id = request.getNewNode();
					this.executeAcceptJoin(info.getPeer(), info);
				} else {
					this.openConnection(request.getNewNode().getSocketAddress(), request.getNewNode(), Operation.ACCEPTJOIN);
				}
			}
		} else {
			if(initialTTL == this.passiveRandomWalkLenght) {
				if(this.canAddToPassiveView(request.getNewNode()))
					this.addToPassiveView(request.getNewNode());
			}
			request.decrementTTL();
			candidatesToForward[this.rand.nextInt(candidatesToForward.length)].send(new ByteBuffer[]{JoinWalkerRequest.writeJoinWalkerRequestToBuffer(request)}, this.joinWalkerPort);
		}
		
		if(!accepted && initialTTL == this.randomWalkLenght)
			info.close(); //in this case clean up the state of the temporary connection.
	}
	
	protected void handleJoinReply(ByteBuffer[] msg, Connection info) {
		JoinReply jr = JoinReply.readJoinReplyFromBuffer(msg);
		info.id = jr.getPeer();
		this.addToActiveView(jr.getPeer(), info);
	}
	
	protected void handleNeighborRequest(ByteBuffer[] msg, Connection info) {
		NeighborRequest nReq = NeighborRequest.readNeighborRequestFromBuffer(msg);
		if(this.activeView.containsKey(nReq.getPeer())) {
			//This is also our neighbor so we reject him... (this should never happen!!)
			info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(false))}, this.neighborReplyPort);	
			log.info("Received a neighborin request from a node that is already my neighbor: " + nReq.getPeer());
		} else {
			//This is a possible candidate to add to active view... evaluate the situation
			if(nReq.hasHighPriority() || this.activeView.size() < this.activeViewSize) {
				//We welcome this element even if we have to disconnect someone in the addToActiveView method
				info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(true))}, this.neighborReplyPort);
				info.id = nReq.getPeer();
				this.addToActiveView(nReq.getPeer(), info);
			} else {
				//We reject this element
				info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(false))}, this.neighborReplyPort);
			}
		}
	}
	
	protected void handleNeighborReply(ByteBuffer[] msg, Connection info) {
		NeighborReply nReply = NeighborReply.readNeighborReplyFromBuffer(msg);
		this.pendingNeighboringRequests--;
	
		NodeID candidate = (NodeID) info.id;
		
		if(nReply.isAccepted() && this.canAddToActiveView(candidate)) {
			if(this.activeView.size() < this.activeViewSize) {
				this.addToActiveView(candidate, info);
			} else {
				//We don't require this element anymore... so we disconnect and close
				this.disconnectPeer(info);
			}
		} else {
			//Close the connection with the peer
			info.id = null;
			info.close();
		}
		
		if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
			this.nextNeighboringCandidate();
		}
	}
	
	protected void handleShuffleRequest(ByteBuffer[] msg, Connection info) {
		ShuffleRequest shfReq = ShuffleRequest.readShuffleRequestFromBuffer(msg);
		Connection[] candidatesToForward = forwardingCandidates(shfReq.getOriginator(), info);
		NodeID[] listToSend = null;
		
		if(shfReq.getTTL() == 0 || candidatesToForward.length == 0) {
			//Get a list of peers from passiveView with at most number of peers received		
			int peersToSend = (this.passiveView.size() <= shfReq.getListSize() + 1 ? this.passiveView.size() : shfReq.getListSize() + 1);
			
			if(peersToSend > 0) {
				listToSend = new NodeID[peersToSend];
				NodeID[] passivePeers = this.passiveView.toArray(new NodeID[this.passiveView.size()]);
				int[] selectionUniverse = RandomSamples.mkUniverse(peersToSend);
				RandomSamples.uniformSample(peersToSend, selectionUniverse, this.rand);
				
				for(int i = 0; i < peersToSend; i++) 
					listToSend[i] = passivePeers[selectionUniverse[i]];
				
				ShuffleReply shfReply = new ShuffleReply(listToSend);
				
				if(this.activeView.containsKey(shfReq.getOriginator())) //Use the TCP connection
					this.activeView.get(shfReq.getOriginator()).send(new ByteBuffer[]{ShuffleReply.writeShuffleReplyToBuffer(shfReply)}, this.shuffleReplyPort);
				else //Use UDP to transmit this message
					this.net.udpSend(new ByteBuffer[]{ShuffleReply.writeShuffleReplyToBuffer(shfReply)}, this.shuffleReplyPort, shfReq.getOriginator().getSocketAddress());
			}
			
			this.mergeView(shfReq.generateList(myId,activeView,passiveView), listToSend == null ? new NodeID[0] : listToSend);
		} else {
			shfReq.decrementTTL();
			candidatesToForward[this.rand.nextInt(candidatesToForward.length)].send(new ByteBuffer[]{ShuffleRequest.writeShuffleRequestToBuffer(shfReq)}, this.shuffleRequestPort);			
		}
	}
	
	protected void handleShuffleReply(ByteBuffer[] msg, Connection info) {
		ShuffleReply shfReply = ShuffleReply.readShuffleReplyFromBuffer(msg);
		//Notice that this is never a temporary connection therefore we don't close it :)
	
		if(shfReply != null) //Now the shuffle reply can be null due to use of UDP and missed packets.
			this.mergeView(shfReply.generateList(myId,activeView,passiveView) , (this.lastShuffleSent == null ? new NodeID[0]:this.lastShuffleSent));
	}
	
	protected void handleShuffleReplyTemp(ByteBuffer[] msg, Connection info) {
		ShuffleReply shfReply = ShuffleReply.readShuffleReplyFromBuffer(msg);
		//If this is a temporary connection close it...
		info.id = null;
		info.close();
		
		if(shfReply != null) //Now the shuffle reply can be null due to use of UDP and missed packets.
			this.mergeView(shfReply.generateList(myId,activeView,passiveView) , (this.lastShuffleSent == null ? new NodeID[0]:this.lastShuffleSent));
	}
	
	protected void handleDisconnectRequest(ByteBuffer[] msg, Connection info) {
		NodeID sender = NodeID.readNodeIDFromBuffer(msg);
		
		if(this.activeView.containsKey(sender)) {
			this.activeView.remove(sender);
			this.addToPassiveView(sender);
			this.notifyOverlayAwareProtocolNeighborDown(sender);
			this.fillActiveView.start();
		}		
		
		//Close connection
		info.id = null;
		info.close();
					
	}
	
	/**
	 * Other protocol operations
	 */

	private void executeJoin(InetSocketAddress listenAddr, Connection info) {
		info.send(new ByteBuffer[]{JoinRequest.writeJoinRequestToBuffer(new JoinRequest(this.myId))}, this.joinRequestPort);
	}
	
	private void executeJoinWalker(InetSocketAddress listenAddr, Connection info) {
		info.send(new ByteBuffer[]{JoinWalkerRequest.writeJoinWalkerRequestToBuffer(new JoinWalkerRequest(this.randomWalkLenght,this.myId))}, this.joinWalkerPort);
	}
	
	protected void executeAcceptJoin(InetSocketAddress listenAddr, Connection info) {
		info.send(new ByteBuffer[]{JoinReply.writeJoinReplyToBuffer(new JoinReply(this.myId))},this.joinReplyPort);
		this.addToActiveView((NodeID) info.id, info);
	}
	
	public void executeShuffle() {
		if(this.activeView.size() > 0) {
			NodeID[] activePeers = this.activeView.keySet().toArray(new NodeID[this.activeView.size()]);
			int[] activeUniverse = RandomSamples.mkUniverse(activePeers.length);
			
//			We want 1 node to whom to send the shuffle and actuveShuffleLenght peers to send that node
			int actShuffleSize = (activePeers.length <= this.activeShuffleLenght+1 ? activePeers.length:this.activeShuffleLenght+1);
			RandomSamples.uniformSample(actShuffleSize, activeUniverse, this.rand);
						
			//Create the shuffleMessage :) starting with our peer id
			ShuffleRequest shfMsg = new ShuffleRequest(this.myId,this.passiveRandomWalkLenght);
			
//			Add elements from the active view
			int shuffleTarget = this.rand.nextInt(actShuffleSize);
			for(int i = 0; i < actShuffleSize; i++) {
				if(i != shuffleTarget)
					shfMsg.addPeer(activePeers[activeUniverse[i]]);
			}
			
			
			
			if(this.passiveView.size() > 0) {
//				Add some elements from the passive view
				NodeID[] passivePeers = this.passiveView.toArray(new NodeID[this.passiveView.size()]);
				int[] passiveUniverse = RandomSamples.mkUniverse(passivePeers.length); 
		
				int passShuffleSize = (this.passiveShuffleLenght <= passivePeers.length ? this.passiveShuffleLenght:passivePeers.length);
				
				RandomSamples.uniformSample(passShuffleSize, passiveUniverse, this.rand);
				
				for(int i = 0; i < passShuffleSize; i++)
					shfMsg.addPeer(passivePeers[passiveUniverse[i]]);
			}
		
//			Send the shuffle
			this.lastShuffleSent = shfMsg.toArray();
			this.activeView.get(activePeers[activeUniverse[shuffleTarget]]).send(new ByteBuffer[]{ShuffleRequest.writeShuffleRequestToBuffer(shfMsg)}, this.shuffleRequestPort);
		}
	}

	private synchronized boolean nextNeighboringCandidate() {
		while(true) {
			if(this.neighboringUniversePosition >= this.passiveViewSize)
				return false; //End of line...
	
			if(this.neighboringUniverse[this.neighboringUniversePosition] < this.passiveView.size() && !this.operations.containsKey(this.passiveView.get(this.neighboringUniverse[this.neighboringUniversePosition]).getSocketAddress()))
				break; //We found a candidate
			
			this.neighboringUniversePosition++; //Try next one...
		}
				
		NodeID tryOut = this.passiveView.get(this.neighboringUniverse[this.neighboringUniversePosition]);
		this.neighboringUniversePosition++;
		this.pendingNeighboringRequests++;
		this.openConnection(tryOut.getSocketAddress(), tryOut, Operation.NEIGHBORING);
		
		return true;
		
	}
		
	protected void startNeighboring() {
		//this.net.detectBlockedConnections();
		
		if(this.passiveView.size() == 0)
			return; //Don't do anything if you don't have anyone in the passive view...
		
		if(this.activeView.size() == this.activeViewSize) {
			this.fillActiveView.stop();
			return;
		} else if (this.neighboringUniversePosition >= this.passiveViewSize) {
			//Restart the random sample
			this.neighboringUniversePosition = 0;
			RandomSamples.uniformSample(this.passiveViewSize / 2, this.neighboringUniverse, this.rand);
		}
		
		while(this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
			if(!this.nextNeighboringCandidate())
				break; //As there are not any more elements to try from the passive view
		}
	}
	
	private void executeNeighboring(Connection info) {
		//Check if in the meanwhile this node has become our neighbor or our membership is full
		if(this.activeView.size() < this.activeViewSize && !this.activeView.containsKey(info.id))
			info.send(new ByteBuffer[]{NeighborRequest.writeNeighborRequestToBuffer(new NeighborRequest(this.myId,this.activeView.size()==0))}, this.neighborRequestPort);
		else {
			this.pendingNeighboringRequests--;
			info.id = null;
			info.close(); //This request is not logic anymore... close connection.
		}
	}

	private boolean canAddToActiveView(NodeID peer) {
		boolean ans = !this.myId.equals(peer) && !this.activeView.containsKey(peer);
		return ans;
	}
	
	private boolean canAddToPassiveView(NodeID peer) {
		return !this.myId.equals(peer) && !this.activeView.containsKey(peer) && !this.passiveView.contains(peer);
	}
	
	private void addToActiveView(NodeID peer, Connection link) {
		this.passiveView.remove(peer);
		
		if(this.activeView.size() == this.activeViewSize) {
			this.disconnectRandomPeer();
		}	
		
		this.activeView.put(peer,link);
		this.notifyOverlayAwareProtocolNeighborUp(peer, link);
	}

	private void addToPassiveView(NodeID peer) {
		if(this.passiveView.size() == this.passiveViewSize)
			this.passiveView.remove(this.rand.nextInt(this.passiveViewSize));
		
		this.passiveView.add(peer);
	}
	
	protected Connection[] forwardingCandidates(NodeID newNode, Connection info) {
		ArrayList<Connection> temp = new ArrayList<Connection>();
		for(NodeID neighbor: this.activeView.keySet()) {
			if(!neighbor.equals(newNode) && !neighbor.equals(info.id)) {
				temp.add(this.activeView.get(neighbor));
			}
		}
		return temp.toArray(new Connection[temp.size()]);
	}
	
	protected void disconnectRandomPeer() {
		NodeID[] peers = new NodeID[this.activeView.size()];
		this.activeView.keySet().toArray(peers);
		NodeID selected = peers[this.rand.nextInt(peers.length)];
		Connection info = this.activeView.remove(selected);
		
		info.send(new ByteBuffer[]{NodeID.writeNodeIdToBuffer(this.myId)}, this.disconnectRequestPort);
		this.notifyOverlayAwareProtocolNeighborDown(selected);
		
		this.addToPassiveView(selected);
		info.id = null;
	}
	
	protected void disconnectPeer(Connection info) {
		info.send(new ByteBuffer[]{NodeID.writeNodeIdToBuffer(this.myId)}, this.disconnectRequestPort);
		this.activeView.remove(info.id);
		this.addToPassiveView((NodeID) info.id);
		this.notifyOverlayAwareProtocolNeighborDown((NodeID) info.id);
		info.id = null;
	}
	
	/**
	 * Auxiliary Functions
	 */
	
	private void mergeView(NodeID[] peerList, NodeID[] listToSend) {
		NodeID[] passiveViewList = this.passiveView.toArray(new NodeID[this.passiveView.size()]); 
		int peerListPosition = 0;
		int myListPosition = 0;
		
		while (this.passiveView.size() < this.passiveViewSize && peerListPosition < peerList.length) {
			this.passiveView.add(peerList[peerListPosition]);
			peerListPosition++;
		}
		
		while (myListPosition < listToSend.length && peerListPosition < peerList.length) {
			while(myListPosition < listToSend.length && !this.passiveView.contains(listToSend[myListPosition]))
				myListPosition++;
			
			if(myListPosition < listToSend.length) {
				this.passiveView.remove(myListPosition);
				myListPosition++;
				this.passiveView.add(peerList[peerListPosition]);
				peerListPosition++;
			} else {
				break;
			}
				
		}
		
		if(peerListPosition < peerList.length) {
			int[] universe = RandomSamples.mkUniverse(passiveViewList.length);
			RandomSamples.uniformSample(universe.length, universe, this.rand);
			
			while (peerListPosition < peerList.length) {
				int trashPosition = 0;
				while(trashPosition < universe.length && !this.passiveView.remove(passiveViewList[universe[trashPosition]]))
					trashPosition++;
				trashPosition++;
				this.passiveView.add(peerList[peerListPosition]);
				peerListPosition++;
			}
		}
	}

	
	/**
	 * Methods used to manipulate protocol parameters
	 */
	public int getActiveMembershipSize() {
		return this.activeViewSize;
	}
	
	public void setActiveMembershipSize(short value) {
		this.activeViewSize = value;
	}
	
	public int getPassiveMembershipSize() {
		return this.passiveViewSize;
	}
	
	public void setPassiveMembershipSize(short value) {
		this.passiveViewSize = value;
	}
	
	public int getActiveRandomWalkLenght() {
		return this.randomWalkLenght;
	}
	
	public void setActiveRandomWalkLenght(short value) {
		this.randomWalkLenght = value;
	}
	
	public int getPassiveRandomWalkLenght() {
		return this.passiveRandomWalkLenght;
	}
	
	public void setPassiveRandomWalkLenght(short value) {
		this.passiveRandomWalkLenght = value;
	}
	
	public int getKa() {
		return this.activeShuffleLenght;
	}
	
	public void setKa(short value) {
		this.activeShuffleLenght = value;
	}
	
	public int getKp() {
		return this.passiveShuffleLenght;
	}
	
	public void setKp(short value) {
		this.passiveShuffleLenght = value;
	}
	
	public int getShuffleLenght() {
		return this.activeShuffleLenght + this.passiveShuffleLenght + 1;
	}
	
	//This one is a implicit parameter that is the shuffle interval
	public int getShufflePeriod() {
		return this.shuffle.getInterval();
	}
	
	public void setShufflePeriod(int period) {
		this.shuffle.setInterval(period);
	}
	
	/**
	 * Methods from the interface: ConnectionListener
	 */
	
	public synchronized void failed(InetSocketAddress info) {
		log.info("Connection open timetou: " + info);
		
		if(this.shutdown) return;
		
		Operation pending = this.operations.remove(info);
		if(pending != null) {
			log.info("Pending Operation: " + pending);
			switch (pending.getOperationId()) {
				case Operation.NEIGHBORING:
					this.pendingNeighboringRequests--;
					this.passiveView.remove(pending.getInfo());
					if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
						this.nextNeighboringCandidate();
					}
					break;
				case Operation.JOIN:
					this.passiveView.remove(pending.getInfo());
					synchronized (this.cached) {
						cached.notify();
					}
					break;
				default:
					this.passiveView.remove(pending.getInfo());
					break;
			} 
		}
	}
	
	/**
	 * 
	 * @param info
	 */
	
	public synchronized void close(Connection info) {
		log.info("Connection closed: " + info.id + " " + info.getPeer());
		
		if(this.shutdown) return;
		
		if(info.id != null) {
			if(this.activeView.containsKey(info.id)) {
				this.activeView.remove(info.id);
				this.notifyOverlayAwareProtocolNeighborDown((NodeID) info.id);
				this.fillActiveView.start();
			}		
		} else {
			Operation pending = this.operations.remove(info.getPeer());
			if(pending != null) {
				log.info("Pending Operation: " + pending);
				switch (pending.getOperationId()) {
				case Operation.JOIN:
					this.passiveView.remove(pending.getInfo());
					synchronized (cached) {
						cached.notify(); //Notifies the immortal thread...
					}
					break;
				case Operation.ACCEPTJOIN:
					this.passiveView.remove(pending.getInfo());
					break;
				case Operation.ANSWERSHUFFLE:
					this.passiveView.remove(pending.getInfo());
					break;
				case Operation.NEIGHBORING:
					this.pendingNeighboringRequests--;
					this.passiveView.remove(pending.getInfo());
					if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
						this.nextNeighboringCandidate();
					}
					break;
				default:
					this.passiveView.remove(pending.getInfo());
					break;
				} 
			}
		}
	}

	/**
	 *
	 * @param info
	 */
	
	public synchronized void open(Connection info) {
		log.info("Connection openned: " + info.id + " " + info.getPeer());
		
		if(this.shutdown) {
			info.close();
			return;
		}
		
		//Verify if we were waiting for this connection to execute some operation
		Operation pending = this.operations.remove(info.getPeer());
		if(pending != null) {
			log.info("Pending Operation: " + pending);
			switch (pending.getOperationId()) {
				case Operation.NEIGHBORING:
					info.id = pending.getInfo();
					this.executeNeighboring(info);
					break;	
				case Operation.JOIN:
					this.executeJoin(info.getPeer(), info);
					break;
				case Operation.JOIN_WALKER:
					this.executeJoinWalker(info.getPeer(), info);
					break;
				case Operation.ACCEPTJOIN:
					info.id = pending.getInfo();
					this.executeAcceptJoin(info.getPeer(), info);
					break;
				default:
					break;
			} 
		}
	}

	/**
	 * Methods from the interface: DataListener
	 */
	
	/**
	 * 
	 * @param msg
	 * @param info
	 * @param port
	 */
	
	//TODO: Remove This... :)
	private String translatePort(short port) {
		switch(port) {
		case 103:
			return "Join Request";
		case 104:
			return "Forward Join Request";
		case 105:
			return "Join Reply";
		case 106:
			return "Neighbor Request";
		case 107:
			return "Neighbor Reply";
		case 108:
			return "Shuffle Request";
		case 109: 
			return "Shuffle Reply";
		case 110:
			return "Join Walker Request";
		case 111:
			return "Disconnect";
		default:
			return "Unknown";
		}
	}
	
	public void receive(ByteBuffer[] msg, Connection info, short port) {
		if(port != this.shuffleReplyPort && port != this.shuffleRequestPort)
			log.info("Received message on port " + translatePort(port) + " from " + (info==null?"UDP":info.id) + " (" + (info==null?"UDP":info.getPeer()) + ")");
		
		if(this.shutdown) return;
		
		if(this.joinRequestPort == port)
			this.handleJoinRequest(msg, info);
		else if(this.forwardJoinRequestPort == port)
			this.handleForwardJoinRequest(msg, info);
		else if(this.joinWalkerPort == port)
			this.handleJoinWalker(msg, info);
		else if(this.joinReplyPort == port) 
			this.handleJoinReply(msg, info);
		else if(this.neighborRequestPort == port)
			this.handleNeighborRequest(msg, info);
		else if(this.neighborReplyPort == port)
			this.handleNeighborReply(msg, info);
		else if(this.shuffleRequestPort == port)
			this.handleShuffleRequest(msg, info);
		else if(this.shuffleReplyPort == port)
			this.handleShuffleReply(msg, info);
		else /*if(this.disconnectRequestPort == port) */
			this.handleDisconnectRequest(msg, info);
	}
	
	public synchronized void leave() {
		short missing = this.activeViewSize;
		this.shutdown = true;
		this.updateCache();
		
		try {
			PrintStream out = new PrintStream(new File(ImmortalHyParView.recovery_filaname));
			ByteBuffer[] disconnectReq = new ByteBuffer[]{};
			for(NodeID peer: new ArrayList<NodeID>(this.activeView.keySet())) {
				out.println(peer.getSocketAddress().getAddress().getHostAddress());
				this.activeView.get(peer).send(Buffers.clone(disconnectReq), this.disconnectRequestPort);
				this.notifyOverlayAwareProtocolNeighborDown(peer);
				missing--;
			}
			this.activeView.clear();
			while(missing > 0 && passiveView.size() > 0) {
				NodeID peer = this.passiveView.remove(this.rand.nextInt(this.passiveView.size()));
				out.println(peer.getSocketAddress().getAddress().getHostAddress());
				missing--;
			}
			out.close();
		} catch (Exception e) {
			this.log.error("Unable to complete the leave operation. " + e.getClass().getName() + ": " + e.getMessage());
			System.err.println(DateTimeRepresentation.timeStamp());
			e.printStackTrace(System.err);
		}
	}
	
	protected HashMap<NodeID,Connection> getActiveView() {
		return this.activeView;
	}
	
	protected LinkedList<NodeID> getPassiveView() {
		return this.passiveView;
	}
	
	public NodeID[] getActivePeersPresentInfo() {
		return this.activeView.keySet().toArray(new NodeID[this.activeViewSize]);
	}
	
	public NodeID[] getPassivePeersPresentInfo() {
		return this.passiveView.toArray(new NodeID[this.passiveViewSize]);
	}
	
	protected boolean isFillViewProcessRunning() {
		return this.fillActiveView.isRunning();
	}
	
	protected synchronized HashMap<InetSocketAddress,Operation> getOperations() {
		return this.operations;
	}
	
	public void resetCounters() {
		return;
	}

	@SuppressWarnings("unchecked")
	public HashMap<NodeID, Connection> getNeighbors() {
		return (HashMap<NodeID, Connection>) this.activeView.clone();
	}

	public NodeID getNodeIdentifier() {
		return this.myId;
	}

	public Transport network() {
		return this.net;
	}

	public int pendingConns() {
		return this.operations.size();
	}

	public void setOverlayAware(OverlayAware proto) {
		if(this.overlayAwareProtocols == null)
			this.overlayAwareProtocols = new ArrayList<OverlayAware>();
		if(!this.overlayAwareProtocols.contains(proto))
			this.overlayAwareProtocols.add(proto);
	}
	
	private void notifyOverlayAwareProtocolNeighborDown(NodeID peer) {
		if(this.overlayAwareProtocols != null) {
			for(OverlayAware proto: this.overlayAwareProtocols) {
				proto.neighborDown(peer);
			}
		}
	}
	
	private void notifyOverlayAwareProtocolNeighborUp(NodeID peer, Connection info) {
		if(this.overlayAwareProtocols != null) {
			for(OverlayAware proto: this.overlayAwareProtocols) {
				proto.neighborUp(peer, info);
			}
		}
	}
	
}

package gsd.impl.hyparview.utils;

import gsd.common.NodeID;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.UUID;

import org.apache.log4j.Logger;

public class NodeIDHelper {

	public static final String filename = "uuid.hyparview";
	
	public static NodeID getMyNodeID(InetSocketAddress myId) {
		File f = new File(NodeIDHelper.filename);
		NodeID id = null;		
		
		if(f.exists()) {
			UUID uuid = null;
			try {
				Scanner sc = new Scanner(f);
				uuid = UUID.fromString(sc.nextLine());
				sc.close();
			} catch (FileNotFoundException e) {
				Logger.getLogger(NodeIDHelper.class).error("Cannot recover UUID from file: " + NodeIDHelper.filename);
			}
			if(uuid != null) 
				id = new NodeID(myId, uuid);
			else {
				id = new NodeID(myId);
				try {
					PrintStream out = new PrintStream(f);
					out.println(id.getUUID().toString());
					out.close();
				} catch (FileNotFoundException e) {
					Logger.getLogger(NodeIDHelper.class).error("Cannot store UUID in file: " + NodeIDHelper.filename);
				}
			}
		} else {
			id = new NodeID(myId);
			try {
				PrintStream out = new PrintStream(f);
				out.println(id.getUUID().toString());
				out.close();
			} catch (FileNotFoundException e) {
				Logger.getLogger(NodeIDHelper.class).error("Cannot store UUID in file: " + NodeIDHelper.filename);
			}
			
		}
		
		return id; 
	}

}

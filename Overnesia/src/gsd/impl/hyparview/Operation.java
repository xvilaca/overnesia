package gsd.impl.hyparview;

public class Operation {
	
	public static final short JOIN = 1;
	public static final short ACCEPTJOIN = 2;
	public static final short ANSWERSHUFFLE = 3;
	public static final short NEIGHBORING = 4;
	public static final short JOIN_WALKER = 5;
	public static final short CHALLENGE = 6;
	public static final short DISSEMINATION = 7;
	
	private short operId;
	private Object info;
	
	public Operation(short id) {
		this.operId = id;
		this.info = null;
	}
	
	public Operation(short id, Object info)	{
		this.operId = id;
		this.info = info;
		
	}
	
	public boolean pendingJoin() {
		return this.operId == JOIN;
	}

	public boolean pendingAcceptJoin() {
		return this.operId == ACCEPTJOIN;
	}
	
	public boolean pendingAnswerShuffle() {
		return this.operId == ANSWERSHUFFLE;
	}
	
	public boolean pendingNeighboring() {
		return this.operId == NEIGHBORING;
	}
	
	public boolean pendingJoiningWalker() {
		return this.operId == JOIN_WALKER;
	}
	
	public short getOperationId() {
		return this.operId;
	}
	
	public Object getInfo() {
		return this.info;
	}

	@Override
	public String toString() {
		switch(this.operId) {
		case JOIN:
			return "Execute Join";
		case ACCEPTJOIN:
			return "Accept join";
		case ANSWERSHUFFLE:
			return "Answer shuffle";
		case NEIGHBORING:
			return "Neighboring";
		case JOIN_WALKER:
			return "Join Walker";
		default:
			return "Unknown (default)";
		}
	}
}

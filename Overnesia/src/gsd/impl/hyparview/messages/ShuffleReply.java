package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class ShuffleReply {
	private ArrayList <NodeID> peerList;
	
	public ShuffleReply() {
		this.peerList = new ArrayList<NodeID>();
	}
	
	public ShuffleReply(NodeID[] listToSend) {
		this.peerList = new ArrayList<NodeID>();
		for(int i = 0; i < listToSend.length; i++)
			this.peerList.add(listToSend[i]);
	}
	
	public void addPeer(NodeID peer) {
		this.peerList.add(peer);
	}
	
	public Iterator<NodeID> getPeerIterator() {
		return this.peerList.iterator();
	}
	
	public int getListSize() {
		return this.peerList.size();
	}
	
	static public ByteBuffer writeShuffleReplyToBuffer(ShuffleReply request) {
		ByteBuffer msg = ByteBuffer.allocate(2+(22*request.getListSize()));
		msg.putShort((short) request.getListSize());
		Iterator<NodeID> iterador = request.getPeerIterator();
		while(iterador.hasNext())
			NodeID.writeNodeIdToBuffer(msg, iterador.next());
		msg.flip();
		return msg;
	}
		
	public static ShuffleReply readShuffleReplyFromBuffer(ByteBuffer[] msg) {
		try {
		
			ByteBuffer buf = Buffers.sliceCompact(msg, 2);
			short peerListLenght = buf.getShort();
			ShuffleReply ev = new ShuffleReply();
					
			buf = Buffers.sliceCompact(msg, 22 * peerListLenght);
			
			for(int i = 0; i < peerListLenght; i++)
				ev.addPeer(NodeID.readNodeIDFromBuffer(buf));
			
	        return ev;
		} catch (Exception e) {
			System.err.println("Error while reading ShuffleReply from buffer.");
			return null;
		}
	}

	public NodeID[] toArray() {
		return this.peerList.toArray(new NodeID[this.peerList.size()]);
	}

	public NodeID[] generateList(NodeID myId, HashMap<NodeID, Connection> activeView, LinkedList<NodeID> passiveView) {
		ArrayList<NodeID> temp = new ArrayList<NodeID>();
		
		for (NodeID peer: this.peerList)
			if(!peer.equals(myId) && !activeView.containsKey(peer) && !passiveView.contains(peer))
				temp.add(peer);
		
		return temp.toArray(new NodeID[temp.size()]);
	}
	
}

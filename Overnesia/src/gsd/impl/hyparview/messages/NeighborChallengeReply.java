package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;

public class NeighborChallengeReply {

	private boolean value;
	private NodeID peer;
	
	public NeighborChallengeReply(NodeID node, final boolean b) {
		this.peer = node;
		this.value = b;
	}

	public NodeID getPeer(){
		return this.peer;
	}
	
	public static ByteBuffer writeNeighborReplyChallengeToBuffer(NeighborChallengeReply challengeReply) {
		
		ByteBuffer msg = ByteBuffer.allocate(1+22);
		if(challengeReply.value)
			msg.put((byte)0xf);
		else
			msg.put((byte)0x0);
		
		NodeID.writeNodeIdToBuffer(msg, challengeReply.peer);
		msg.flip();
		
		return msg;
	}

	public boolean getResult() {
		return this.value;
	}

	public static NeighborChallengeReply readNeighborReplyChallengeFromBuffer( ByteBuffer[] msg) {
		
		ByteBuffer buf = Buffers.sliceCompact(msg, 1+22);
		boolean value = (buf.get() == 0xf);
				
		return new NeighborChallengeReply(NodeID.readNodeIDFromBuffer(buf), value);
	}
	
	

}

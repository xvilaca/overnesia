package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;


public class JoinReply {

	private NodeID peer;

	public JoinReply(NodeID peer) {
		this.peer = peer;
	}
	
	public NodeID getPeer() {
		return this.peer;
	}
	
	static public ByteBuffer writeJoinReplyToBuffer(JoinReply request) {
		ByteBuffer msg = ByteBuffer.allocate(22);
		
		NodeID.writeNodeIdToBuffer(msg, request.peer);
		
		msg.flip();
		return msg;
	}
	
	public static JoinReply readJoinReplyFromBuffer(ByteBuffer[] msg) {
        return new JoinReply(NodeID.readNodeIDFromBuffer(Buffers.slice(msg, 22)));
	}
	
}

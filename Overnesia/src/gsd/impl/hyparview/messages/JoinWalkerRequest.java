package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;

public class JoinWalkerRequest {
	
	private short ttl;
	private NodeID newNode;
	
	public JoinWalkerRequest(short ttl, NodeID newNode) {
		this.ttl = ttl;
		this.newNode = newNode;
	}
	
	public short getTTL() {
		return this.ttl;
	}
	
	public NodeID getNewNode() {
		return this.newNode;
	}
	
	public void decrementTTL() {
		this.ttl--;
	}
	
	static public ByteBuffer writeJoinWalkerRequestToBuffer(JoinWalkerRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(22 + 2);
		
		msg.putShort(request.ttl);
		NodeID.writeNodeIdToBuffer(msg, request.newNode);
		
		msg.flip();
		return msg;
	}
	
	public static JoinWalkerRequest readJoinWalkerRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 22 + 2);
        return new JoinWalkerRequest(buf.getShort() , NodeID.readNodeIDFromBuffer(buf));
	}

}

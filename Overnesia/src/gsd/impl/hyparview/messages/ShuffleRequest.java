package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;



public class ShuffleRequest {
	private short ttl;
	private NodeID originator;
	private ArrayList <NodeID> peerList;
	
	public ShuffleRequest(NodeID originator, short ttl) {
		this.ttl = ttl;
		this.originator = originator;
		this.peerList = new ArrayList<NodeID>();
	}
	
	public void addPeer(NodeID peer) {
		this.peerList.add(peer);
	}
	
	public short getTTL() {
		return this.ttl;
	}
	
	public NodeID getOriginator() {
		return this.originator;
	}
	
	public Iterator<NodeID> getPeerIterator() {
		return this.peerList.iterator();
	}
	
	public void decrementTTL() {
		this.ttl--;
	}
	
	public int getListSize() {
		return this.peerList.size();
	}
	
	static public ByteBuffer writeShuffleRequestToBuffer(ShuffleRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(2+22+2+(22*request.getListSize()));
		msg.putShort(request.ttl);
		NodeID.writeNodeIdToBuffer(msg, request.originator);
		msg.putShort((short)request.getListSize());
		Iterator<NodeID> iterador = request.getPeerIterator();
		while(iterador.hasNext())
			NodeID.writeNodeIdToBuffer(msg, iterador.next());
		msg.flip();
		return msg;
	}
		
	public static ShuffleRequest readShuffleRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 2+22+2);
		short ttl = buf.getShort();
		NodeID peer = NodeID.readNodeIDFromBuffer(buf);

		ShuffleRequest ev = new ShuffleRequest(peer,ttl);
		
		short peerListLenght = buf.getShort();
	
		buf = Buffers.sliceCompact(msg, 22 * peerListLenght);
		for(int i = 0; i < peerListLenght; i++)
			ev.addPeer(NodeID.readNodeIDFromBuffer(buf));
		
        return ev;
	}

	public NodeID[] generateList(NodeID myId, HashMap<NodeID, Connection> activeView, LinkedList<NodeID> passiveView) {
		ArrayList<NodeID> temp = new ArrayList<NodeID>();
		if(!this.originator.equals(myId) && !activeView.containsKey(this.originator) && !passiveView.contains(this.originator))
			temp.add(this.originator);
		
		for (NodeID peer: this.peerList)
			if(!peer.equals(myId) && !activeView.containsKey(peer) && !passiveView.contains(peer))
				temp.add(peer);
		
		return temp.toArray(new NodeID[temp.size()]);
	}

	public NodeID[] toArray() {		
		return this.peerList.toArray(new NodeID[this.peerList.size()]);
	}
	
}

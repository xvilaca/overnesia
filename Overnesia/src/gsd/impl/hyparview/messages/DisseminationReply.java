package gsd.impl.hyparview.messages;

import gsd.impl.Buffers;

import java.nio.ByteBuffer;

public class DisseminationReply {

	
	private boolean isWanted;
	private int frameID;
	
	public DisseminationReply(final boolean isWanted,final int frameID) {
		this.isWanted = isWanted;
		this.frameID = frameID;
	}

	public static ByteBuffer writeDisseminationReplyToBuffer(
			DisseminationReply dissReply) {
		ByteBuffer msg = ByteBuffer.allocate(1+4);
		
		if(dissReply.isWanted)
			msg.put((byte)0xf);
		else
			msg.put((byte)0x0);
	
		msg.putInt(dissReply.frameID);
		msg.flip();
		
		return msg;
	}

	public static DisseminationReply disseminationReplyReadFromBuffer(
			ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 1+4);
		boolean value = (buf.get() == 0xf);
		int newFrameID = buf.getInt();
		
		return new DisseminationReply(value, newFrameID);
	}

	public boolean getIsWanted() {
		return this.isWanted;
	}

	public int getFrameID() {
		return this.frameID;
	}

	
	//read
	
	

}

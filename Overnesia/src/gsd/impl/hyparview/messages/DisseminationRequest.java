package gsd.impl.hyparview.messages;

import gsd.impl.Buffers;
import gsd.impl.Frame;

import java.nio.ByteBuffer;

public class DisseminationRequest {

	private int frameID;
	private Frame frame;
	
	public DisseminationRequest(int id){
		this.frameID = id;
		
	}


	
	public static ByteBuffer writeDisseminationRequestFromBuffer(DisseminationRequest dissReq){
		
		ByteBuffer msg = ByteBuffer.allocate(4);
		msg.putInt(dissReq.frameID);
		msg.flip();
		return msg;
	}

	public static DisseminationRequest readDisseminationRequestFromBuffer(
			ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg,4);// (/8 to get bytes)
		
		int newFrameID = buf.getInt();
		
		return new DisseminationRequest(newFrameID);
	}

	public int getFrameID() {
		return this.frameID;
	}
	
	
}

package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Frame;

import java.nio.ByteBuffer;

public class DisseminationSendFrame {

	private Frame frame;
	
	public DisseminationSendFrame(final Frame f){
		this.frame = f;
	}
	
	public static ByteBuffer DisseminationSendFrameToBuffer(
			DisseminationSendFrame disseminationSendFrame) {
		
		ByteBuffer msg = ByteBuffer.allocate(Frame.getAllocationSize()); 
		Frame.writeFrameToBuffer(msg, disseminationSendFrame.getFrame());
		msg.flip();
		
		return msg;
	}

	public Frame getFrame() {
		return this.frame;
	}

	public static DisseminationSendFrame disseminationSendFrameFromBuffer(
			ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, Frame.getAllocationSize());
		Frame f = Frame.readFrameFromBuffer(buf);
		
		
		return new DisseminationSendFrame(f);
	}
	

}

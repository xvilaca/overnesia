package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;


public class NeighborRequest {

	private boolean highPriority;
	private NodeID peer;

	public NeighborRequest(NodeID peer, boolean highPriority) {
		this.peer = peer;
		this.highPriority  = highPriority;
	}
	
	public NodeID getPeer() {
		return this.peer;
	}
	
	public boolean hasHighPriority() {
		return this.highPriority;
	}
	
	static public ByteBuffer writeNeighborRequestToBuffer(NeighborRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(1 + 22);
		
		if(request.highPriority) {
			msg.put((byte)0xf);
		} else {
			msg.put((byte)0x0);
		}
					
		NodeID.writeNodeIdToBuffer(msg, request.peer);
		
		msg.flip();
		return msg;
	}
	
	public static NeighborRequest readNeighborRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 1+22);
		
		boolean pValue = (buf.get() == 0xf);			
        return new NeighborRequest(NodeID.readNodeIDFromBuffer(buf), pValue);
	}
	
}

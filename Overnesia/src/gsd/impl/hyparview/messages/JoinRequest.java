package gsd.impl.hyparview.messages;

import gsd.common.NodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;


public class JoinRequest {

	private NodeID newNode;

	public JoinRequest(NodeID nn) {
		this.newNode = nn;
	}
	
	public NodeID getNewNode() {
		return this.newNode;
	}
	
	static public ByteBuffer writeJoinRequestToBuffer(JoinRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(22);

		NodeID.writeNodeIdToBuffer(msg, request.newNode);
		
		msg.flip();
		return msg;
	}
	
	public static JoinRequest readJoinRequestFromBuffer(ByteBuffer[] msg) {
		return new JoinRequest(NodeID.readNodeIDFromBuffer(Buffers.sliceCompact(msg, 22)));
	}
	
}

package gsd.impl.hyparview.messages;

import gsd.impl.Buffers;

import java.nio.ByteBuffer;


public class NeighborReply {

	private boolean accept;

	public NeighborReply(boolean accept) {
		this.accept  = accept;
	}
	
	public boolean isAccepted() {
		return this.accept;
	}
	
	static public ByteBuffer writeNeighborReplyToBuffer(NeighborReply reply) {
		ByteBuffer msg = ByteBuffer.allocate(1);
		
		if(reply.accept) {
			msg.put((byte)0xf);
		} else {
			msg.put((byte)0x0);
		}
						
		msg.flip();
		return msg;
	}
	
	public static NeighborReply readNeighborReplyFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 1);
		
		boolean accepted = (buf.get() == 0xf);
				
       return new NeighborReply(accepted);
	}
	
}

package gsd.impl.hyparview.messages;

import gsd.api.ConstValues;
import gsd.api.PropertiesFactory;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.CryptoPuzzle;
import gsd.impl.hyparview.ImmortalHyParView;

import java.math.BigInteger;
import java.nio.ByteBuffer;

public class NeighborChallengeRequest {
	CryptoPuzzle puzzle;
	Connection info;
	
	public NeighborChallengeRequest(CryptoPuzzle puzzle) {
		this.puzzle = puzzle;
	}

	public static ByteBuffer writeNeighborChallengeRequestToBuffer(NeighborChallengeRequest request) {
			byte [] puzzle = request.puzzle.toByteArray();
			ByteBuffer msg = ByteBuffer.allocate(puzzle.length); //capacity - The new buffer's capacity, in bytes
			//ImmortalHyParView.arrayToString(hash);
			msg.put(puzzle);			
			msg.flip();
			return msg;
	}

	
	public static NeighborChallengeRequest readNeighborChallengeRequestFromBuffer(int hashSize, 
			int hintSize, int numBitsWindow, ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, hashSize + hintSize + 8);
		byte [] arr = new byte[buf.limit()];
		buf.get(arr);
		
		return new NeighborChallengeRequest(CryptoPuzzle.fromByteArray(arr, 
				hashSize, hintSize, numBitsWindow));
	}

	

	public int getProblem() {
		int value = 4;// random value;
		return value;
	}

	public CryptoPuzzle getPuzzle() {
		return this.puzzle;
	}
}

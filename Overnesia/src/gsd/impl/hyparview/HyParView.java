package gsd.impl.hyparview;

import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.ConnectionListener;
import gsd.impl.DataListener;
import gsd.impl.OverlayAware;
import gsd.impl.OverlayNetwork;
import gsd.impl.Periodic;
import gsd.impl.RandomSamples;
import gsd.impl.Transport;
import gsd.impl.hyparview.messages.ForwardedJoinRequest;
import gsd.impl.hyparview.messages.JoinReply;
import gsd.impl.hyparview.messages.JoinRequest;
import gsd.impl.hyparview.messages.NeighborReply;
import gsd.impl.hyparview.messages.NeighborRequest;
import gsd.impl.hyparview.messages.ShuffleReply;
import gsd.impl.hyparview.messages.ShuffleRequest;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class HyParView implements OverlayNetwork, ConnectionListener, DataListener {
	
	/**
	 * Key attributes (passed by the constructor)
	 */
	private Random rand;
	private Transport net;
		
	/**
	 * Protocol parameters
	 */
	protected short activeViewSize;
	protected short passiveViewSize;
	protected short activeShuffleLenght;
	protected short passiveShuffleLenght;
	protected short randomWalkLenght;
	protected short passiveRandomWalkLenght;
	
	/**
	 * Message ports
	 */
	private short joinRequestPort;
	private short forwardJoinRequestPort;
	private short neighborRequestPort;
	private short neighborReplyPort;
	private short shuffleRequestPort;
	private short shuffleReplyPort;
	private short disconnectRequestPort;
	private short joinReplyPort;
	
	/**
	 * Other protocol state parameters
	 */
	protected NodeID myId;
	private HashMap<InetSocketAddress,Operation> operations;
	private Periodic shuffle;
	private Periodic fillActiveView;
	private NodeID[] lastShuffleSent;
	private HashMap<NodeID,Connection>  activeView;
	private LinkedList<NodeID> passiveView;
	
	//To process neighboring
	short pendingNeighboringRequests;
	int neighboringUniversePosition;
	int[] neighboringUniverse;
	
	/**
	 * For handling overlay aware protocols
	 */
	private ArrayList<OverlayAware> overlayAwareProtocols;
	
	/**
	 * 
	 * @param rand
	 * @param net
	 * @param ports
	 */
	public HyParView(Random rand, Transport net, short[] ports, InetSocketAddress myId) {
		//Key atributes atribution;
		this.rand = rand;
		this.net = net;
	
		//We want to use udp so...
		this.net.activateUDP();
		
		//Message ports definition
		this.joinRequestPort = ports[0]; //103
		this.forwardJoinRequestPort = ports[1]; //104
		this.joinReplyPort = ports[2]; //105
		this.neighborRequestPort = ports[3]; //106
		this.neighborReplyPort = ports[4]; //107
		this.shuffleRequestPort = ports[5]; //108
		this.shuffleReplyPort = ports[6]; //109
		//110 - old temporary shufle reply port (to use with temporary TCP connections. Now this is deprecated
		this.disconnectRequestPort = ports[8]; //111
	
		//Protocol parameters initialization [Default values]
		this.activeViewSize = 5;
		this.passiveViewSize = 30;
		this.activeShuffleLenght = 3;
		this.passiveShuffleLenght = 4;
		this.randomWalkLenght = 6;
		this.passiveRandomWalkLenght = 3;
		
		//Other protocol state variables
		this.myId = new NodeID(myId);
		this.shuffle = new Periodic(rand, net, 10000) { //10 Second
			public void run() {
				executeShuffle();
			}
		};
		this.shuffle.start();
		this.lastShuffleSent = null;
		
		this.fillActiveView = new Periodic(rand, net, 3000) { //3 Second
			public void run() {
				startNeighboring();
			}
		};
		this.neighboringUniverse = RandomSamples.mkUniverse(this.passiveViewSize);
		this.neighboringUniversePosition = this.passiveViewSize;
		this.fillActiveView.start();
		
		this.activeView = new HashMap<NodeID, Connection>();
		this.passiveView = new LinkedList<NodeID>();
		this.operations = new HashMap<InetSocketAddress,Operation>();
		this.pendingNeighboringRequests = 0;
		
		//Prepare callbacks
        net.setDataListener(this, this.joinRequestPort);
        net.setDataListener(this, this.forwardJoinRequestPort);
        net.setDataListener(this, this.joinReplyPort);
        net.setDataListener(this, this.neighborRequestPort);
        net.setDataListener(this, this.neighborReplyPort);
        net.setDataListener(this, this.shuffleRequestPort);
        net.setDataListener(this, this.shuffleReplyPort);
        net.setDataListener(this, this.disconnectRequestPort);
        net.setConnectionListener(this);
	}

	/**
	 * Public methods to support gossip protocol
	 */
		
	public synchronized Connection[] connections() {
		return activeView.values().toArray(new Connection[this.activeView.size()]);
	}
	
	public synchronized NodeID[] getPeers() {
		return this.activeView.keySet().toArray(new NodeID[this.activeView.size()]);
	}
	
	public synchronized InetSocketAddress[] getPeerAddresses() {
		InetSocketAddress[] list = new InetSocketAddress[this.activeView.size()];
		int i = 0;
		for(NodeID peer: this.activeView.keySet())
			list[i++] = peer.getSocketAddress();
		
		return list;
	}
	
	//Old interface
	public int getFanout() {
		return this.getActiveMembershipSize() - 1;
	}
	
	//Old interface
	public void setFanout(short fanout) {
		this.setActiveMembershipSize((short) (fanout+1));
	}
	
	/**
	 * This method handles the opening of TCP connections in the underlying layer...
	 */
	
	private void openConnection(final InetSocketAddress contact, Object info, short operationCode) {
		if(!operations.containsKey(contact)) {
			Operation oper = new Operation(operationCode, info);
			System.err.println("Opening connection to " + contact + " for " + oper + " with info: " + info);
			this.operations.put(contact, oper);
			this.net.queue(new Runnable() {
	            public void run() {
	                net.add(contact);
	            }
	        });
		} else {
			System.err.println("Rejected the creation of a new TCP Connection to: " + contact);
			System.err.println("Current Operation was: " + new Operation(operationCode));
			System.err.println("Existing Operation is: " + operations.get(contact));
		}
	}
	
	/**
	 * initialization interface for the overlay network
	 */
	
	public void init() {
		//TODO: Should timers be only started here?!?!
	}
	
	public synchronized void init(final InetSocketAddress contact) {
		this.openConnection(contact, null, Operation.JOIN);
	}
	
	/**
	 * Private methods to handle protocol specific messages
	 */
	
	protected void handleJoinRequest(ByteBuffer[] msg, Connection info) {
		NodeID newPeer = JoinRequest.readJoinRequestFromBuffer(msg).getNewNode();
		if(this.activeView.size() > 0) {
			//First of all it is required to check if there is space in the active view, and make it if required
			if(this.activeView.size() == this.activeViewSize) {
				//Drop a random element using a disconnect request
				this.disconnectRandomPeer();
			}
			
			//Prepare a forwardedJoinRequestBuffer	
			ByteBuffer[]forwardJoinRequest = {ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(new ForwardedJoinRequest(this.randomWalkLenght,newPeer))};
			
			//Forward request for all neighbors in the active view
			for(Connection link: this.activeView.values()) {
				link.send(Buffers.clone(forwardJoinRequest), this.forwardJoinRequestPort);
			}
		}
		
		if(!this.activeView.containsKey(newPeer)) {
			info.id = newPeer;
			info.send(new ByteBuffer[]{JoinReply.writeJoinReplyToBuffer(new JoinReply(this.myId))},this.joinReplyPort);
			this.addToActiveView(newPeer,info);
		} else {
			//Terminate connection... [this should never happen...]
			info.id = null;
			info.close();
		}
		
	}
	
	protected void handleForwardJoinRequest(ByteBuffer[] msg, Connection info) {
		ForwardedJoinRequest request = ForwardedJoinRequest.readForwardedJoinRequestFromBuffer(msg);
		Connection[] candidatesToForward = forwardingCandidates(request.getNewNode(), info);
			
		if(request.getTTL() == 0 || candidatesToForward.length == 0) {
			if(this.canAddToActiveView(request.getNewNode()))
				this.openConnection(request.getNewNode().getSocketAddress(), request.getNewNode(), Operation.ACCEPTJOIN);
		} else {
			if (request.getTTL() == this.passiveRandomWalkLenght) {
			if(this.canAddToPassiveView(request.getNewNode()))	
				this.addToPassiveView(request.getNewNode());
			}
			request.decrementTTL();
			candidatesToForward[this.rand.nextInt(candidatesToForward.length)].send(new ByteBuffer[]{ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(request)}, this.forwardJoinRequestPort);			
		}
		
	}

	protected void handleJoinReply(ByteBuffer[] msg, Connection info) {
		JoinReply jr = JoinReply.readJoinReplyFromBuffer(msg);
		info.id = jr.getPeer();
		this.addToActiveView(jr.getPeer(), info);
	}
	
	protected void handleNeighborRequest(ByteBuffer[] msg, Connection info) {
		NeighborRequest nReq = NeighborRequest.readNeighborRequestFromBuffer(msg);
		if(this.activeView.containsKey(nReq.getPeer())) {
			//This is also our neighbor so we reject him... (this should never happen!!)	
			info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(false))}, this.neighborReplyPort);
			System.err.println("Received a neighborin request from a node that is already my neighbor: " + nReq.getPeer());
		} else {
			//This is a possible candidate to add to active view... evaluate the situation
			if(nReq.hasHighPriority() || this.activeView.size() < this.activeViewSize) {
				//We welcome this element even if we have to disconnect someone in the addToActiveView method
				info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(true))}, this.neighborReplyPort);
				info.id = nReq.getPeer();
				this.addToActiveView(nReq.getPeer(), info);
			} else {
				//We reject this element
				info.send(new ByteBuffer[]{NeighborReply.writeNeighborReplyToBuffer(new NeighborReply(false))}, this.neighborReplyPort);
			}
		}
	}
	
	protected void handleNeighborReply(ByteBuffer[] msg, Connection info) {
		NeighborReply nReply = NeighborReply.readNeighborReplyFromBuffer(msg);
		this.pendingNeighboringRequests--;
	
		NodeID candidate = (NodeID) info.id;
		
		if(nReply.isAccepted() && this.canAddToActiveView(candidate)) {
			if(this.activeView.size() < this.activeViewSize) {
				this.addToActiveView(candidate, info);
			} else {
				//We don't require this element anymore... so we disconnect and close
				this.disconnectPeer(info);
			}
		} else {
			//Close the connection with the peer
			info.id = null;
			info.close();
		}
		
		if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
			this.nextNeighboringCandidate();
		}
	}
	
	protected void handleShuffleRequest(ByteBuffer[] msg, Connection info) {
		ShuffleRequest shfReq = ShuffleRequest.readShuffleRequestFromBuffer(msg);
		Connection[] candidatesToForward = forwardingCandidates(shfReq.getOriginator(), info);
		NodeID[] listToSend = null;
		
		if(shfReq.getTTL() == 0 || candidatesToForward.length == 0) {
			//Get a list of peers from passiveView with at most number of peers received		
			int peersToSend = (this.passiveView.size() <= shfReq.getListSize() + 1 ? this.passiveView.size() : shfReq.getListSize() + 1);
			
			if(peersToSend > 0) {
				listToSend = new NodeID[peersToSend];
				NodeID[] passivePeers = this.passiveView.toArray(new NodeID[this.passiveView.size()]);
				int[] selectionUniverse = RandomSamples.mkUniverse(peersToSend);
				RandomSamples.uniformSample(peersToSend, selectionUniverse, this.rand);
				
				for(int i = 0; i < peersToSend; i++) 
					listToSend[i] = passivePeers[selectionUniverse[i]];
				
				ShuffleReply shfReply = new ShuffleReply(listToSend);
				
				if(this.activeView.containsKey(shfReq.getOriginator())) //Use the TCP connection
					this.activeView.get(shfReq.getOriginator()).send(new ByteBuffer[]{ShuffleReply.writeShuffleReplyToBuffer(shfReply)}, this.shuffleReplyPort);
				else //Use UDP to transmit this message
					this.net.udpSend(new ByteBuffer[]{ShuffleReply.writeShuffleReplyToBuffer(shfReply)}, this.shuffleReplyPort, shfReq.getOriginator().getSocketAddress());
			}
			
			this.mergeView(shfReq.generateList(myId,activeView,passiveView), listToSend == null ? new NodeID[0] : listToSend);
		} else {
			shfReq.decrementTTL();
			candidatesToForward[this.rand.nextInt(candidatesToForward.length)].send(new ByteBuffer[]{ShuffleRequest.writeShuffleRequestToBuffer(shfReq)}, this.shuffleRequestPort);			
		}
	}
	
	protected void handleShuffleReply(ByteBuffer[] msg, Connection info) {
		ShuffleReply shfReply = ShuffleReply.readShuffleReplyFromBuffer(msg);
		//Notice that this is never a temporary connection therefore we don't close it :)
		
		if(shfReply != null) //Now the shuffle reply can be null due to use of UDP and missed packets.
			this.mergeView(shfReply.generateList(myId,activeView,passiveView) , (this.lastShuffleSent == null ? new NodeID[0]:this.lastShuffleSent));
	}
	
	protected void handleShuffleReplyTemp(ByteBuffer[] msg, Connection info) {
		ShuffleReply shfReply = ShuffleReply.readShuffleReplyFromBuffer(msg);
		//If this is a temporary connection close it...
		info.id = null;
		info.close();
		
		if(shfReply != null) //Now the shuffle reply can be null due to use of UDP and missed packets.
			this.mergeView(shfReply.generateList(myId,activeView,passiveView) , (this.lastShuffleSent == null ? new NodeID[0]:this.lastShuffleSent));
	}
	
	protected void handleDisconnectRequest(ByteBuffer[] msg, Connection info) {
		NodeID sender = NodeID.readNodeIDFromBuffer(msg);
		
		if(this.activeView.containsKey(sender)) {
			this.activeView.remove(sender);
			this.addToPassiveView(sender);
			this.notifyOverlayAwareProtocolNeighborDown(sender);
			this.fillActiveView.start();
		}		
		
		//Close connection
		info.id = null;
		info.close();
					
	}
	
	/**
	 * Other protocol operations
	 */

	private void executeJoin(InetSocketAddress listenAddr, Connection info) {
		info.send(new ByteBuffer[]{JoinRequest.writeJoinRequestToBuffer(new JoinRequest(this.myId))}, this.joinRequestPort);
	}
	
	protected void executeAcceptJoin(InetSocketAddress listenAddr, Connection info) {
		info.send(new ByteBuffer[]{JoinReply.writeJoinReplyToBuffer(new JoinReply(this.myId))},this.joinReplyPort);
		this.addToActiveView((NodeID) info.id, info);
	}
	
	public void executeShuffle() {
		if(this.activeView.size() > 0) {
			NodeID[] activePeers = this.activeView.keySet().toArray(new NodeID[this.activeView.size()]);
			int[] activeUniverse = RandomSamples.mkUniverse(activePeers.length);
			
//			We want 1 node to whom to send the shuffle and actuveShuffleLenght peers to send that node
			int actShuffleSize = (activePeers.length <= this.activeShuffleLenght+1 ? activePeers.length:this.activeShuffleLenght+1);
			RandomSamples.uniformSample(actShuffleSize, activeUniverse, this.rand);
						
			//Create the shuffleMessage :) starting with our peer id
			ShuffleRequest shfMsg = new ShuffleRequest(this.myId,this.passiveRandomWalkLenght);
			
//			Add elements from the active view
			int shuffleTarget = this.rand.nextInt(actShuffleSize);
			for(int i = 0; i < actShuffleSize; i++) {
				if(i != shuffleTarget)
					shfMsg.addPeer(activePeers[activeUniverse[i]]);
			}
			
			
			
			if(this.passiveView.size() > 0) {
//				Add some elements from the passive view
				NodeID[] passivePeers = this.passiveView.toArray(new NodeID[this.passiveView.size()]);
				int[] passiveUniverse = RandomSamples.mkUniverse(passivePeers.length); 
		
				int passShuffleSize = (this.passiveShuffleLenght <= passivePeers.length ? this.passiveShuffleLenght:passivePeers.length);
				
				RandomSamples.uniformSample(passShuffleSize, passiveUniverse, this.rand);
				
				for(int i = 0; i < passShuffleSize; i++)
					shfMsg.addPeer(passivePeers[passiveUniverse[i]]);
			}
		
//			Send the shuffle
			this.lastShuffleSent = shfMsg.toArray();
			this.activeView.get(activePeers[activeUniverse[shuffleTarget]]).send(new ByteBuffer[]{ShuffleRequest.writeShuffleRequestToBuffer(shfMsg)}, this.shuffleRequestPort);
		}
	}

	private synchronized boolean nextNeighboringCandidate() {
		while(true) {
			if(this.neighboringUniversePosition >= this.passiveViewSize)
				return false; //End of line...
	
			if(this.neighboringUniverse[this.neighboringUniversePosition] < this.passiveView.size() && !this.operations.containsKey(this.passiveView.get(this.neighboringUniverse[this.neighboringUniversePosition]).getSocketAddress()))
				break; //We found a candidate
			
			this.neighboringUniversePosition++; //Try next one...
		}
				
		NodeID tryOut = this.passiveView.get(this.neighboringUniverse[this.neighboringUniversePosition]);
		this.neighboringUniversePosition++;
		this.pendingNeighboringRequests++;
		this.openConnection(tryOut.getSocketAddress(), tryOut, Operation.NEIGHBORING);
		
		return true;
		
	}
		
	protected void startNeighboring() {
		if(this.passiveView.size() == 0)
			return; //Don't do anything if you don't have anyone in the passive view...
		
		if(this.activeView.size() == this.activeViewSize) {
			this.fillActiveView.stop();
			return;
		} else if (this.neighboringUniversePosition >= this.passiveViewSize) {
			//Restart the random sample
			this.neighboringUniversePosition = 0;
			RandomSamples.uniformSample(this.passiveViewSize / 2, this.neighboringUniverse, this.rand);
		}
		
		while(this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
			if(!this.nextNeighboringCandidate())
				break; //As there are not any more elements to try from the passive view
		}
	}
	
	private void executeNeighboring(Connection info) {
		//Check if in the meanwhile this node has become our neighbor or our membership is full
		if(this.activeView.size() < this.activeViewSize && !this.activeView.containsKey(info.id))
			info.send(new ByteBuffer[]{NeighborRequest.writeNeighborRequestToBuffer(new NeighborRequest(this.myId,this.activeView.size()==0))}, this.neighborRequestPort);
		else {
			this.pendingNeighboringRequests--;
			info.id = null;
			info.close(); //This request is not logic anymore... close connection.
		}
	}

	private boolean canAddToActiveView(NodeID peer) {
		boolean ans = !this.myId.equals(peer) && !this.activeView.containsKey(peer);
		return ans;
	}
	
	private boolean canAddToPassiveView(NodeID peer) {
		return !this.myId.equals(peer) && !this.activeView.containsKey(peer) && !this.passiveView.contains(peer);
	}
	
	private void addToActiveView(NodeID peer, Connection link) {
		this.passiveView.remove(peer);
		
		if(this.activeView.size() == this.activeViewSize) {
			this.disconnectRandomPeer();
		}	
		
		this.activeView.put(peer,link);
		this.notifyOverlayAwareProtocolNeighborUp(peer, link);
	}
	
	private void addToPassiveView(NodeID peer) {
		if(this.passiveView.size() == this.passiveViewSize)
			this.passiveView.remove(this.rand.nextInt(this.passiveViewSize));
		
		this.passiveView.add(peer);
	}
	
	protected Connection[] forwardingCandidates(NodeID newNode, Connection info) {
		ArrayList<Connection> temp = new ArrayList<Connection>();
		for(NodeID neighbor: this.activeView.keySet()) {
			if(!neighbor.equals(newNode) && !neighbor.equals(info.id)) {
				temp.add(this.activeView.get(neighbor));
			}
		}
		return temp.toArray(new Connection[temp.size()]);
	}
	
	protected void disconnectRandomPeer() {
		NodeID[] peers = new NodeID[this.activeView.size()];
		this.activeView.keySet().toArray(peers);
		NodeID selected = peers[this.rand.nextInt(peers.length)];
		Connection info = this.activeView.remove(selected);
		
		info.send(new ByteBuffer[]{NodeID.writeNodeIdToBuffer(this.myId)}, this.disconnectRequestPort);
		this.notifyOverlayAwareProtocolNeighborDown(selected);
		
		this.addToPassiveView(selected);
		info.id = null;
	}
	
	protected void disconnectPeer(Connection info) {
		info.send(new ByteBuffer[]{NodeID.writeNodeIdToBuffer(this.myId)}, this.disconnectRequestPort);
		this.activeView.remove(info.id);
		this.notifyOverlayAwareProtocolNeighborDown((NodeID) info.id);
		this.addToPassiveView((NodeID) info.id);
		info.id = null;
	}
	
	/**
	 * Auxiliary Functions
	 */
	
	private void mergeView(NodeID[] peerList, NodeID[] listToSend) {
		NodeID[] passiveViewList = this.passiveView.toArray(new NodeID[this.passiveView.size()]); 
		int peerListPosition = 0;
		int myListPosition = 0;
		
		while (this.passiveView.size() < this.passiveViewSize && peerListPosition < peerList.length) {
			this.passiveView.add(peerList[peerListPosition]);
			peerListPosition++;
		}
		
		while (myListPosition < listToSend.length && peerListPosition < peerList.length) {
			while(myListPosition < listToSend.length && !this.passiveView.contains(listToSend[myListPosition]))
				myListPosition++;
			
			if(myListPosition < listToSend.length) {
				this.passiveView.remove(myListPosition);
				myListPosition++;
				this.passiveView.add(peerList[peerListPosition]);
				peerListPosition++;
			} else {
				break;
			}
				
		}
		
		if(peerListPosition < peerList.length) {
			int[] universe = RandomSamples.mkUniverse(passiveViewList.length);
			RandomSamples.uniformSample(universe.length, universe, this.rand);
			
			while (peerListPosition < peerList.length) {
				int trashPosition = 0;
				while(trashPosition < universe.length && !this.passiveView.remove(passiveViewList[universe[trashPosition]]))
					trashPosition++;
				trashPosition++;
				this.passiveView.add(peerList[peerListPosition]);
				peerListPosition++;
			}
		}
	}

	
	/**
	 * Methods used to manipulate protocol parameters
	 */
	public int getActiveMembershipSize() {
		return this.activeViewSize;
	}
	
	public void setActiveMembershipSize(short value) {
		this.activeViewSize = value;
	}
	
	public int getPassiveMembershipSize() {
		return this.passiveViewSize;
	}
	
	public void setPassiveMembershipSize(short value) {
		this.passiveViewSize = value;
	}
	
	public int getActiveRandomWalkLenght() {
		return this.randomWalkLenght;
	}
	
	public void setActiveRandomWalkLenght(short value) {
		this.randomWalkLenght = value;
	}
	
	public int getPassiveRandomWalkLenght() {
		return this.passiveRandomWalkLenght;
	}
	
	public void setPassiveRandomWalkLenght(short value) {
		this.passiveRandomWalkLenght = value;
	}
	
	public int getKa() {
		return this.activeShuffleLenght;
	}
	
	public void setKa(short value) {
		this.activeShuffleLenght = value;
	}
	
	public int getKp() {
		return this.passiveShuffleLenght;
	}
	
	public void setKp(short value) {
		this.passiveShuffleLenght = value;
	}
	
	public int getShuffleLenght() {
		return this.activeShuffleLenght + this.passiveShuffleLenght + 1;
	}
	
	//This one is a implicit parameter that is the shuffle interval
	public int getShufflePeriod() {
		return this.shuffle.getInterval();
	}
	
	public void setShufflePeriod(int period) {
		this.shuffle.setInterval(period);
	}
	
	/**
	 * Methods from the interface: ConnectionListener
	 */
	
	public synchronized void failed(InetSocketAddress info) {
		System.err.println("Connection open timetou: " + info);
		
		Operation pending = this.operations.remove(info);
		if(pending != null) {
			System.err.println("Pending Operation: " + pending);
			switch (pending.getOperationId()) {
				case Operation.NEIGHBORING:
					this.pendingNeighboringRequests--;
					this.passiveView.remove(pending.getInfo());
					if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
						this.nextNeighboringCandidate();
					}
					break;
				default:
					this.passiveView.remove(pending.getInfo());
					break;
			} 
		}
	}
	
	/**
	 * 
	 * @param info
	 */
	
	public synchronized void close(Connection info) {
		System.err.println("Connection closed: " + info.id + " " + info.getPeer());
		
		if(info.id != null) {
			if(this.activeView.containsKey(info.id)) {
				this.activeView.remove(info.id);
				this.notifyOverlayAwareProtocolNeighborDown((NodeID) info.id);
				this.fillActiveView.start();
			}		
		} else {
			Operation pending = this.operations.remove(info.getPeer());
			if(pending != null) {
				System.err.println("Pending Operation: " + pending);
				switch (pending.getOperationId()) {
				case Operation.JOIN:
					this.passiveView.remove(pending.getInfo());
					break;
				case Operation.ACCEPTJOIN:
					this.passiveView.remove(pending.getInfo());
					break;
				case Operation.ANSWERSHUFFLE:
					this.passiveView.remove(pending.getInfo());
					break;
				case Operation.NEIGHBORING:
					this.pendingNeighboringRequests--;
					this.passiveView.remove(pending.getInfo());
					if (this.activeView.size() + this.pendingNeighboringRequests < this.activeViewSize) {
						this.nextNeighboringCandidate();
					}
					break;
				default:
					this.passiveView.remove(pending.getInfo());
					break;
				} 
			}
		}
	}

	/**
	 *
	 * @param info
	 */
	
	public synchronized void open(Connection info) {
		System.err.println("Connection openned: " + info.id + " " + info.getPeer());
		
		//Verify if we were waiting for this connection to execute some operation
		Operation pending = this.operations.remove(info.getPeer());
		if(pending != null) {
			System.err.println("Pending Operation: " + pending);
			switch (pending.getOperationId()) {
				case Operation.JOIN:
					this.executeJoin(info.getPeer(), info);
					break;
				case Operation.ACCEPTJOIN:
					info.id = pending.getInfo();
					this.executeAcceptJoin(info.getPeer(), info);
					break;
				case Operation.NEIGHBORING:
					info.id = pending.getInfo();
					this.executeNeighboring(info);
					break;
				default:
					break;
			} 
		}
	}

	/**
	 * Methods from the interface: DataListener
	 */
	
	/**
	 * 
	 * @param msg
	 * @param info
	 * @param port
	 */
	
	//TODO: Remove This... :)
	private String translatePort(short port) {
		switch(port) {
		case 103:
			return "Join Request";
		case 104:
			return "Forward Join Request";
		case 105:
			return "Join Reply";
		case 106:
			return "Neighbor Request";
		case 107:
			return "Neighbor Reply";
		case 108:
			return "Shuffle Request";
		case 109: 
			return "Shuffle Reply";
		case 110:
			return "Shuffle Reply (Temp)";
		case 111:
			return "Disconnect";
		default:
			return "Unknown";
		}
	}
	
	public void receive(ByteBuffer[] msg, Connection info, short port) {
		System.err.println("Received message on port " + translatePort(port) + " from " + (info==null?"UDP":info.id) + " (" + (info==null?"UDP":info.getPeer()) + ")");
		
		if(this.joinRequestPort == port)
			this.handleJoinRequest(msg, info);
		else if(this.forwardJoinRequestPort == port)
			this.handleForwardJoinRequest(msg, info);
		else if(this.joinReplyPort == port) 
			this.handleJoinReply(msg, info);
		else if(this.neighborRequestPort == port)
			this.handleNeighborRequest(msg, info);
		else if(this.neighborReplyPort == port)
			this.handleNeighborReply(msg, info);
		else if(this.shuffleRequestPort == port)
			this.handleShuffleRequest(msg, info);
		else if(this.shuffleReplyPort == port)
			this.handleShuffleReply(msg, info);
		else /*if(this.disconnectRequestPort == port) */
			this.handleDisconnectRequest(msg, info);
	}
	
	protected HashMap<NodeID,Connection> getActiveView() {
		return this.activeView;
	}
	
	protected LinkedList<NodeID> getPassiveView() {
		return this.passiveView;
	}
	
	public NodeID[] getActivePeersPresentInfo() {
		return this.activeView.keySet().toArray(new NodeID[this.activeViewSize]);
	}
	
	public NodeID[] getPassivePeersPresentInfo() {
		return this.passiveView.toArray(new NodeID[this.passiveViewSize]);
	}
	
	protected boolean isFillViewProcessRunning() {
		return this.fillActiveView.isRunning();
	}
	
	protected synchronized HashMap<InetSocketAddress,Operation> getOperations() {
		return this.operations;
	}
	
	public void resetCounters() {
		return;
	}

	@SuppressWarnings("unchecked")
	public HashMap<NodeID, Connection> getNeighbors() {
		return (HashMap<NodeID, Connection>) this.activeView.clone();
	}

	public NodeID getNodeIdentifier() {
		return this.myId;
	}

	public Transport network() {
		return this.net;
	}

	public int pendingConns() {
		return this.operations.size();
	}

	public synchronized void leave() {
		//Nothing to do
	}
	
	public void setOverlayAware(OverlayAware proto) {
		if(this.overlayAwareProtocols == null)
			this.overlayAwareProtocols = new ArrayList<OverlayAware>();
		if(!this.overlayAwareProtocols.contains(proto))
			this.overlayAwareProtocols.add(proto);
	}
	
	private void notifyOverlayAwareProtocolNeighborDown(NodeID peer) {
		if(this.overlayAwareProtocols != null) {
			for(OverlayAware proto: this.overlayAwareProtocols) {
				proto.neighborDown(peer);
			}
		}
	}
	
	private void notifyOverlayAwareProtocolNeighborUp(NodeID peer, Connection info) {
		if(this.overlayAwareProtocols != null) {
			for(OverlayAware proto: this.overlayAwareProtocols) {
				proto.neighborUp(peer, info);
			}
		}
	}
}

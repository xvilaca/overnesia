package gsd.impl.plumtree;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;

import gsd.common.NodeID;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataBroadcaster;
import gsd.impl.DataListener;
import gsd.impl.GossipProtocol;
import gsd.impl.OverlayAware;
import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;
import gsd.impl.UUIDs;

public class Plumtree implements GossipProtocol, DataListener, OverlayAware {

	/**
	 * Key attributes (passed by the constructor)
	 */
	private Random rand;
	private Transport net;
	private OverlayNetwork overlay;
	
	/**
	 * Internal status variables
	 */
	private HashMap<NodeID, Connection> eagerPushSet;
	private HashMap<NodeID, Connection> lazyPushSet;
	private HashMap<UUID, ArrayList<DataAnnouncement>> iHaveSet;
	private ArrayList<UUID> deliveredSet;
	
	private ArrayList<ByteBuffer[]> pendingsIHave;
	
	private HashMap<Short, DataBroadcaster> broadcasters;
	
	//Parameters
	public final short ttl = 7;
	public final long graftTimeout = 60000;
	public final long graftRequestTimeout = 30000;
	
	/**
	 * Message ports
	 */
	private short dataPort;
	private short iHavePort;
	private short prunePort;
	private short graftPort;
	
	private static final Logger log = Logger.getLogger(Plumtree.class);
	
	public Plumtree(Random rand, Transport net, short[] ports, OverlayNetwork overlay) {
		this.rand = rand;
		this.net = net;
		this.overlay = overlay;
	
		this.dataPort = ports[0];
		this.iHavePort = ports[1];
		this.prunePort = ports[2];
		this.graftPort = ports[3];
		
		this.net.setDataListener(this, this.dataPort);
		this.net.setDataListener(this, this.iHavePort);
		this.net.setDataListener(this, this.prunePort);
		this.net.setDataListener(this, this.graftPort);
		
		this.overlay.setOverlayAware(this);
		
		this.eagerPushSet = new HashMap<NodeID, Connection>();
		this.lazyPushSet = new HashMap<NodeID, Connection>();
		this.iHaveSet = new HashMap<UUID, ArrayList<DataAnnouncement>>();
		this.deliveredSet = new ArrayList<UUID>();
		
		this.pendingsIHave = new ArrayList<ByteBuffer[]>();
		
		this.broadcasters = new HashMap<Short, DataBroadcaster>();
	}
	
	public synchronized void receive(ByteBuffer[] msg, Connection info, short port) {
		if(port == this.dataPort) {
			this.handleDataMessage(msg, info, port);
		} else if(port == this.iHavePort) { 
			this.handleIHaveMessage(msg, info, port);
		} else if(port == this.prunePort) {
			this.handlePruneMessage(msg, info, port);
		} else if(port == this.graftPort) {
			this.handleGraftMessage(msg, info, port);
		} else {
			log.warn("Reveived message of unknown type in data port: " + port); 
		}
		
		
	}
	
	private void handleDataMessage(ByteBuffer[] msg, Connection info, short port) {
		ByteBuffer header = Buffers.sliceCompact(msg, 22);
		UUID message_u_id = UUIDs.readUUIDFromBuffer(header);
		if(!this.deliveredSet.contains(message_u_id)) {
			this.deliveredSet.add(message_u_id);
			
			short current_ttl = header.getShort();
			short session_id = header.getShort();
			short key_size = header.getShort();
			ByteBuffer key = Buffers.sliceCompact(msg, key_size);
			ByteBuffer payload[] = {Buffers.compact(msg)};
			
			if(this.broadcasters.containsKey(session_id)) {
				this.broadcasters.get(session_id).receive(key.asReadOnlyBuffer(), Buffers.clone(payload));
				if(this.iHaveSet.containsKey(message_u_id)) {
					this.iHaveSet.remove(message_u_id);
				}
				
				if(current_ttl < this.ttl) {
					current_ttl++;
					msg = new ByteBuffer[] {ByteBuffer.allocate(22), key, payload[0]};
					UUIDs.writeUUIDToBuffer(msg[0], message_u_id);
					msg[0].putShort((short) 0);
					msg[0].putShort(session_id);
					msg[0].putShort((short) key.capacity());
					msg[0].flip();
					
					for(NodeID peer: this.eagerPushSet.keySet()) {
						this.eagerPushSet.get(peer).send(Buffers.clone(msg), this.dataPort);
					}
					
					ByteBuffer[] iHave = new ByteBuffer[2];
					System.arraycopy(msg, 0, iHave, 0, 2);
					this.pendingsIHave.add(Buffers.clone(iHave));
				}
			} else {
				log.error("Received a broadcast message issued by a broadcast session that is not currently registered.");
			}
		} else {
			//This is a redundant message...
			if(this.eagerPushSet.containsKey(info.id)) {
				info.send(new ByteBuffer[] {ByteBuffer.allocate(0)}, this.prunePort);	
				this.lazyPushSet.put((NodeID) info.id, this.eagerPushSet.remove(info.id));
			} 
		}
	}

	private void handleIHaveMessage(ByteBuffer[] msg, Connection info, short port) {
		ByteBuffer header = Buffers.sliceCompact(msg, 22);
		final UUID message_u_id = UUIDs.readUUIDFromBuffer(header);
		if(!this.deliveredSet.contains(message_u_id)) {
		
			short current_ttl = header.getShort();
			short session_id = header.getShort();
			short key_size = header.getShort();
			ByteBuffer key = Buffers.sliceCompact(msg, key_size);
		
			if(this.iHaveSet.containsKey(message_u_id)) {
				this.iHaveSet.get(message_u_id).add(new DataAnnouncement(key, session_id, current_ttl, info));
			} else {
				ArrayList<DataAnnouncement> list = new ArrayList<DataAnnouncement>();
				list.add(new DataAnnouncement(key, session_id, current_ttl, info));
				net.schedule(new Runnable() { public void run() { executeGraft(message_u_id); } } , this.graftTimeout);
			}
		}
	}
	
	protected synchronized void executeGraft(final UUID message_u_id) {
		if(this.iHaveSet.containsKey(message_u_id)) {
			ArrayList<DataAnnouncement> list = this.iHaveSet.get(message_u_id);
			DataAnnouncement info = list.remove(0);
			
			ByteBuffer[] graft = new ByteBuffer [] {ByteBuffer.allocate(22), info.retrieve_key};
			UUIDs.writeUUIDToBuffer(graft[0], message_u_id);
			graft[0].putShort(info.session_id);
			graft[0].putShort(info.ttl);
			graft[0].putShort((short) info.retrieve_key.capacity());
			graft[0].flip();
			
			info.info.send(graft, this.graftPort);
			
			if(this.lazyPushSet.containsKey(info.info.id)) {
				this.eagerPushSet.put((NodeID) info.info.id, this.lazyPushSet.remove(info.info.id));
			}
			
			if(list.size() == 0) {
				this.iHaveSet.remove(message_u_id);
			} else {
				net.schedule(new Runnable() { public void run() { executeGraft(message_u_id); } } , this.graftRequestTimeout);
			}
		}
	}
	
	private void handlePruneMessage(ByteBuffer[] msg, Connection info, short port) {
		if(this.eagerPushSet.containsKey(info.id)) {
			this.lazyPushSet.put((NodeID) info.id, this.eagerPushSet.remove(info.id));
		}
	}

	private void handleGraftMessage(ByteBuffer[] msg, Connection info, short port) {
		if(this.lazyPushSet.containsKey(info.id)) {
			this.eagerPushSet.put((NodeID) info.id, this.lazyPushSet.remove(info.id));
		}
		
		ByteBuffer buf = Buffers.sliceCompact(msg, 22);
		
		UUID message_u_id = UUIDs.readUUIDFromBuffer(buf);
		short session_id = buf.getShort();
		short ttl = buf.getShort();
		short key_size = buf.getShort();
		ByteBuffer key = Buffers.sliceCompact(msg, key_size);
		
		if(this.broadcasters.containsKey(session_id)) {
			ByteBuffer[] payload = this.broadcasters.get(session_id).retrievePayload(key.asReadOnlyBuffer());
			if(payload != null) {
			
				msg = new ByteBuffer[payload.length + 2];
				msg[0] = ByteBuffer.allocate(22);
				msg[1] = key;
				System.arraycopy(payload, 0, msg, 2, payload.length);
				
				
				UUIDs.writeUUIDToBuffer(msg[0], message_u_id);
				msg[0].putShort((short) ttl);
				msg[0].putShort(session_id);
				msg[0].putShort(key_size);
				msg[0].flip();
				
				info.send(msg, this.dataPort);
			}
		} else {
			log.error("Received a graft request for a broadcast session " + session_id + " that is not instantiated.");
		}
	}
	
	public synchronized void broadcast(ByteBuffer key, ByteBuffer[] payload, Short session_id) {
		if(!this.broadcasters.containsKey(session_id)) {
			log.error("Received broadcast request from a broadcast session that is not currently registered.");
			return;
		}
		
		ByteBuffer[] msg = new ByteBuffer[payload.length + 2];
		msg[0] = ByteBuffer.allocate(22);
		msg[1] = key;
		System.arraycopy(payload, 0, msg, 2, payload.length);
		UUID message_u_id = UUID.randomUUID();
		
		UUIDs.writeUUIDToBuffer(msg[0], message_u_id);
		msg[0].putShort((short) 0);
		msg[0].putShort(session_id);
		msg[0].putShort((short) key.capacity());
		msg[0].flip();
		
		for(NodeID peer: this.eagerPushSet.keySet()) {
			this.eagerPushSet.get(peer).send(Buffers.clone(msg), this.dataPort);
		}
		
		ByteBuffer[] iHave = new ByteBuffer[2];
		System.arraycopy(msg, 0, iHave, 0, 2);
		this.pendingsIHave.add(Buffers.clone(iHave));
		
		this.deliveredSet.add(message_u_id);
		this.broadcasters.get(session_id).receive(key, payload);
	}

	public synchronized void neighborDown(NodeID neighbor) {
		if(this.eagerPushSet.containsKey(neighbor))
			this.eagerPushSet.remove(neighbor);
		else if(this.lazyPushSet.containsKey(neighbor))
			this.lazyPushSet.remove(neighbor);
		else
			log.warn("Received a neighbor down notification for " + neighbor + " which is not part of any of my sets.");
		
		for(UUID id: this.iHaveSet.keySet()) {
			ArrayList<DataAnnouncement> list = this.iHaveSet.get(id);
			for(int i = 0; i < list.size(); i++) {
				if(list.get(i).info.id == null || neighbor.equals(list.get(i).info.id)) {
					list.remove(i);
					i--;
				}
			}
			if(list.size() == 0)
				this.iHaveSet.remove(id);
		}
	}

	public synchronized void neighborUp(NodeID neighbor, Connection info) {
		if(this.eagerPushSet.containsKey(neighbor) || this.lazyPushSet.containsKey(neighbor)) {
			log.warn("Received a neighbor up notification for " + neighbor + " which is already in one of my sets.");
		} else {
			this.eagerPushSet.put(neighbor, info);
		}
	}
	
	public synchronized void setBroadcasterSession(DataBroadcaster session, Short session_id) {
		if(!this.broadcasters.containsKey(session_id))
			this.broadcasters.put(session_id, session);
		else
			log.warn("Broadcaster session " + session_id + " is already registered.");
	}

	public synchronized void releaseBroascasterSession(Short session_id) {
		if(this.broadcasters.containsKey(session_id)) {
			this.broadcasters.remove(session_id);
		} else {
			log.warn("Broadcaster session " + session_id + " does not exists.");
		}
	}
}

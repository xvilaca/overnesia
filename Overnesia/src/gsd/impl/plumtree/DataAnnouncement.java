package gsd.impl.plumtree;

import gsd.impl.Connection;

import java.nio.ByteBuffer;

public class DataAnnouncement {

	public ByteBuffer retrieve_key;
	public short session_id;
	public short ttl;
	public Connection info;
	
	public DataAnnouncement(ByteBuffer retrieve_key, short session_id, short ttl, Connection info) {
		this.retrieve_key = retrieve_key;
		this.session_id = session_id;
		this.ttl = ttl;
		this.info = info;
	}
}

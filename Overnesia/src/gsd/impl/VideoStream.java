package gsd.impl;

import java.util.Random;

import gsd.api.ConstValues;
import gsd.api.PropertiesFactory;

public class VideoStream{

	
	private int videoMaxFrame;
	private int videoSourceCurrentFrame = 1;
	private int quality = 1 ; // can change the size of packages
	
	public VideoStream(int max){
		this.videoMaxFrame = max;
	}
	
	private VideoStream(){}
	
	
	public int getVideoMaxFrame() {
		return videoMaxFrame;
	}
	public void setvideoMaxFrame(int videoSourceMax) {
		this.videoMaxFrame = videoSourceMax;
	}
	public int getVideoSourceCurrentFrame() {
		return videoSourceCurrentFrame;
	}
	public void setVideoSourceCurrentFrame(int videoSourceCurrentFrame) {
		this.videoSourceCurrentFrame = videoSourceCurrentFrame;
	}
	public int getQuality() {	
		return quality;
	}
	public void setQuality(int quality) {
		this.quality = quality;
	}


	public void incVideoCurrentFrame(int incCurrent) {
		this.videoSourceCurrentFrame += incCurrent;
		videoSourceCurrentFrame =(videoSourceCurrentFrame > videoMaxFrame) ? videoMaxFrame + 1 : videoSourceCurrentFrame ;		  		
	}


	/*
	 * FAKE getFrame
	 * 
	 * */
	public static byte[] getFrame(int size) {
		byte[] b = new byte[size];//new byte[quality * 128]; //quality * 1024b
		new Random().nextBytes(b);		
		return b;
	}	
	
}

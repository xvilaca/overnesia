package gsd.impl.simpleFBCastGossip;

import gsd.common.NodeID;
import gsd.impl.Application;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.OverlayNetwork;
import gsd.impl.Transport;
import gsd.impl.UUIDs;

import java.util.*;
import java.nio.*;

/**
 * Implementation of gossip. Like bimodal, combines a forward
 * retransmission phase with a repair phase. However, the
 * optimistic phase is also gossip bases. UUIDs, instead of
 * sequence numbers are used to identify and discard duplicates.  
 */
public class SimpleFBCastGossip implements DataListener {
    /**
     * ConnectionListener management module.
     */
    private OverlayNetwork memb;

    /**
     *  Represents the class to which messages must be delivered.
     */
    private Application handler;

    /**
     *  The Transport port used by the Gossip class instances to exchange messages. 
     */
    private short bcastPort;
    private short randomWalkPort;
    
    /**
     * 	Set containing the identifiers of delivered messages.
     */
    private LinkedList<UUID> delivered;
 
    /**
     * Maximum number of stored message identifiers
     */
    private short maxIds;
    
    /**
     * TTL
     */
    private short ttl;
   
	/**
     *  Creates a new instance of Gossip.
     */
    public SimpleFBCastGossip(Random rand, Transport net, OverlayNetwork overlay, short[] ports) {
    	this.memb = overlay;
        this.bcastPort = ports[0];
        this.randomWalkPort = ports[1];

        this.ttl = 6;
        this.maxIds = 30;
        this.delivered = new LinkedList<UUID>();
                
        net.setDataListener(this, this.bcastPort);
        net.setDataListener(this, this.randomWalkPort);
    }
    
    public void handler(Application handler) {
        this.handler = handler;
    }
        
    public void broadcast(ByteBuffer[] msg) {
    	handleData(msg, UUID.randomUUID(), (short) 0, null);
    }
        
	public void broadcast(short filiationRequestID, NodeID id) {
		ByteBuffer b = ByteBuffer.allocate(2+22);
		b.putShort(filiationRequestID);
		NodeID.writeNodeIdToBuffer(b,id);
		b.flip();
		broadcast(new ByteBuffer[]{b});
	}
    
    public void receive(ByteBuffer[] msg, Connection info, short port) {  	
    	if (port == this.bcastPort) {
    		ByteBuffer buf = Buffers.sliceCompact(msg, 16 + 2);
        	UUID uuid = UUIDs.readUUIDFromBuffer(buf);	
        	short hops = buf.getShort();
    		handleData(msg, uuid, ++hops, info);
    	} 
	}
        
    private void handleData(ByteBuffer[] msg, UUID uuid, short hops, Connection info) {
		if (delivered.contains(uuid))
			return;
		
		this.registerMesssageID(uuid);
		this.handler.deliver(Buffers.clone(msg));
		
		if (hops>ttl)
			return;
		
		ByteBuffer[] out = new ByteBuffer[msg.length + 1];
		out[0] = ByteBuffer.allocate(16 + 2);
		UUIDs.writeUUIDToBuffer(out[0], uuid);
		out[0].putShort(hops);
		out[0].flip();
		System.arraycopy(msg, 0, out, 1, msg.length);
		
		relay(out, this.bcastPort, memb.connections(), info);
    }
    
    private void registerMesssageID(UUID uuid) {
		if(this.delivered.size() == this.maxIds)
			this.delivered.removeLast();
		this.delivered.addFirst(uuid);
    }

	private void relay(ByteBuffer[] msg, short port, Connection[] conns, Connection source) {
        
        for(Connection link: this.memb.connections()) {
            if(source == null || !source.equals(link)) {
            	link.send(Buffers.clone(msg), port);    
            }
        }
    }
    
    public int getMaxIds() {
        return maxIds;
    }

    public void setMaxIds(short maxIds) {
        this.maxIds = maxIds;
    }

	public int getTtl() {
		return ttl;
	}

	public void setTtl(short ttl) {
		this.ttl = ttl;
	}
	
}


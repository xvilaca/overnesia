package gsd.impl.testoverlay;

import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.ConnectionListener;
import gsd.impl.DataListener;
import gsd.impl.Periodic;
import gsd.impl.Transport;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class TestOverlay implements ConnectionListener, DataListener {
	
	/**
	 * Key attributes (passed by the constructor)
	 */
	private Random rand;
	private Transport net;
		
	/**
	 * Message ports
	 */
	private short gossipPort;
	private short helloPort;
	private short idPort;
	
	/**
	 * Protocol state
	 */
	private InetSocketAddress myId;
	private HashMap<InetSocketAddress,Connection> view;
		
	/**
	 * 
	 * @param rand
	 * @param net
	 * @param ports
	 */
	public TestOverlay(Random rand, Transport net, short[] ports, InetSocketAddress myId) {
		this.rand = rand;
		this.net = net;
	
		//We want to use udp so...
		this.net.activateUDP();
		
		//Message ports definition
		this.gossipPort = ports[0]; //101
		this.helloPort = ports[1]; //201
		this.idPort = ports[2]; //501
	
		//Protocol state variables
		this.myId = myId;
		this.view = new HashMap<InetSocketAddress,Connection>();
		
		//Prepare call back for network.
        net.setDataListener(this, this.gossipPort);
        net.setDataListener(this, this.helloPort);
        net.setDataListener(this, this.idPort);
        net.setConnectionListener(this);
	}
	
	/**
	 * Initialization Method for the Overlay Network. 
	 * This will open a connection to the contact node and then send a join request.
	 */
	
	public synchronized void init(final InetSocketAddress contact) {
		this.net.queue(new Runnable() {
			public void run() {
				net.add(contact);
			}
	    });
		//this.startPeriodicOperations();
	}
	
	/**
	 * Initialization Method for the Overlay Network. 
	 * This is mandatory for the first node in the overlay.
	 */
	public void init() {
		this.startPeriodicOperations();
	}
	
	Periodic p = null;
	
	private void startPeriodicOperations() {
		this.p = new Periodic(rand, net, 10000, 0) {
			public void run() {
				managementTask();
			}
		};
		p.start();
		System.out.println("Test overlay is now running...");
	}
		
	public final void managementTask() {
		System.out.println("Initiaing Periodic Management Task...");
		
		ByteBuffer msg = ByteBuffer.allocate(6 + 2 + (this.view.size() * 6));
		msg.put(myId.getAddress().getAddress());
		msg.putShort((short) myId.getPort());
	
		msg.putShort((short) this.view.size());
		
		for(Connection c: this.view.values()) {
			InetSocketAddress isa = (InetSocketAddress) c.id;
			if(isa != null) {
				msg.put(isa.getAddress().getAddress());
				msg.putShort((short) isa.getPort());
			}
			
		}
		msg.flip();
				
		for(Connection c: this.view.values()) {
			System.out.println("Sending Gossip message to " + c.getPeer());
			//c.send(new ByteBuffer[]{msg.duplicate()}, this.gossipPort);
		}
		
		ByteBuffer hello = ByteBuffer.allocate(6);
		hello.put(myId.getAddress().getAddress());
		hello.putShort((short) myId.getPort());
		hello.flip();
		
		if(view.size() > 0) {
			InetSocketAddress dest = new ArrayList<InetSocketAddress>(this.view.keySet()).get(rand.nextInt(this.view.keySet().size()));
			if(dest != null) {
				System.out.println("Sending A Hello Message to " + dest); 
				net.udpSend(new ByteBuffer[]{hello}, this.helloPort, dest);
			}
		}
	}
	
	
	/*******************************************************
	 * Methods from the interface: ConnectionListener
	 *******************************************************/
	public void failed(InetSocketAddress info) {
		//No-OP
	}
	
	/**
	 * 
	 * @param info
	 */
	
	public synchronized void close(Connection info) {
		if(info == null) {
			System.out.println("UDP local socket error...");
			net.deactivateUDP();
			return;
		}
		
		System.out.println("Connection closed: " + info.getPeer() + " <" + info.id + ">");
	
		this.view.remove(info);
	}

	/**
	 *
	 * @param info
	 */
	
	public synchronized void open(Connection info) {
		//Verify if we were waiting for this connection to execute some operation
		System.out.println("New connection available: " + info.getPeer());	
		
		ByteBuffer msg = ByteBuffer.allocate(6);
		msg.put(myId.getAddress().getAddress());
		msg.putShort((short) myId.getPort());
		msg.flip();
		
		info.send(new ByteBuffer[]{msg}, this.idPort);
		System.out.println("Sent my id");
	}

	/**
	 * Methods from the interface: DataListener
	 */
	
	/**
	 * 
	 * @param msg
	 * @param info
	 * @param port
	 */
	
	public synchronized void receive(ByteBuffer[] msg, Connection info, short port) {
		System.out.println("Message received in port " + port);
		if(this.gossipPort == port)
			handleMessage(msg, info);	
		else if(this.helloPort == port)
			handleUDPMessage(msg); //info in this case is null
		else if(this.idPort == port) {
			info.id = readInetSocketAddress(Buffers.sliceCompact(msg, 6));
			if(!this.view.containsKey(info.id))
				this.view.put((InetSocketAddress) info.id, info);
			else
				info.close();
			System.out.println("Updated id in connection: " + info.id);
		} else
			System.out.println("Unknown port: " + port);
	}

	private void handleMessage(ByteBuffer[] msg, Connection info) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 6 + 2);
		InetSocketAddress isa = readInetSocketAddress(buf);
		
		System.out.println("Received a Gossip message from: " + isa);
		System.out.println("Message list contains the following elements:");
		
		short size = buf.getShort();
		ArrayList<InetSocketAddress> list = new ArrayList<InetSocketAddress>(size);
		buf = Buffers.sliceCompact(msg, size * 6);
		
		for(int i = 0; i < size; i++)
			list.add(readInetSocketAddress(buf));
		
		for(final InetSocketAddress i: list) {
			System.out.println("  " + i);
			if(!this.view.containsKey(i) && !this.myId.equals(i)) {
				this.net.queue(new Runnable() {
					public void run() {
						net.add(i);
					}
			    });
			}
		}
			
	}
	
	private void handleUDPMessage(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 6);
		InetSocketAddress isa = readInetSocketAddress(buf);
		//debug
		System.out.println("Received a (UDP) Hello message from: " + isa);
	}

	private InetSocketAddress readInetSocketAddress(ByteBuffer buf) {
		
		byte[] dst = new byte[4];
		buf.get(dst, 0, dst.length);
		int port = (buf.getShort()) & 0xffff;
		try {
			return new InetSocketAddress(InetAddress.getByAddress(dst), port);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**********************************************************
	 * Methods for interaction with the P2PChannel Class
	 **********************************************************/
	
	public Connection[] connections() {
		return net.connections();
 	}

	/*************************************************************
	 * Methods for interaction with the Protocol Management Class
	 *************************************************************/
	
	public void resetCounters() {
		return; //What should this do?!?!
	}
}

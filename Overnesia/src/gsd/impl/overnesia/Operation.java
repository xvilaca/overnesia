package gsd.impl.overnesia;

public class Operation {
	
	public static final short JOIN = 1;
	public static final short ACCEPTJOIN = 2;
	public static final short ANSWERSHUFFLE = 3;
	public static final short SEND_EXTERNAL_NEIGHBORING = 4;
	public static final short NESOS_NEIGHBORING = 5;
	public static final short REPLY_EXTERNAL_NEIGHBORING = 6;
	public static final short SEND_REALLOCATE_REQUEST = 7;
	public static final short REPLY_REALLOCATE_REQUEST = 8;
	
	private short operId;
	private Object intel;
	
	public Operation(short id) {
		this.operId = id;
		this.intel = null;
	}
	
	public Operation(short id, Object info)	{
		this.operId = id;
		this.intel = info;
	}
	
	public short getOperationId() {
		return this.operId;
	}
	
	public Object getAdditionalInformation() {
		return this.intel;
	}

	@Override
	public String toString() {
		switch(this.operId) {
		case JOIN:
			return "Execute Join";
		case ACCEPTJOIN:
			return "Accept join";
		case ANSWERSHUFFLE:
			return "Answer shuffle";
		case SEND_EXTERNAL_NEIGHBORING:
			return "Send External Neighboring";
		case NESOS_NEIGHBORING:
			return "Nesos Neighboring";
		case REPLY_EXTERNAL_NEIGHBORING:
			return "Reply to External Neighboring Request";
		case SEND_REALLOCATE_REQUEST:
			return "Send a Reallocate Request";
		case REPLY_REALLOCATE_REQUEST:
			return "Reply to Reallocate Resquest";
		default:
			return "Unknown (default)";
		}
	}
}

package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;



public class NesosNeighboringRequest {

	private OvernesiaNodeID peer;

	public NesosNeighboringRequest(OvernesiaNodeID peer) {
		this.peer = peer;
	}
	
	public OvernesiaNodeID getPeer() {
		return this.peer;
	}
	
	
	static public ByteBuffer writeNesosNeighboringRequestToBuffer(NesosNeighboringRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(38);
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.peer);
		msg.flip();
		return msg;
	}
	
	public static NesosNeighboringRequest readNesosNeighboringRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38);
		return new NesosNeighboringRequest(OvernesiaNodeID.readNodeIDFromBuffer(buf));
	}
	
}

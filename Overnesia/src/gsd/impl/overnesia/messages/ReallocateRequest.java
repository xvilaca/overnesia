package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;

public class ReallocateRequest {

	private OvernesiaNodeID originalSender;
	private ArrayList<UUID> knownNesoi;
	private short ttl;

	public ReallocateRequest(OvernesiaNodeID peer, short ttl) {
		this.originalSender = peer;
		this.knownNesoi = new ArrayList<UUID>();
		this.knownNesoi.add(peer.getNesosUUID());
		this.ttl = ttl;
	}
	
	public ReallocateRequest(OvernesiaNodeID peer, ArrayList<UUID> knownNesoi, short ttl) {
		this.originalSender = peer;
		this.knownNesoi = knownNesoi;
		this.ttl = ttl;
	}

	public OvernesiaNodeID getOriginalSender() {
		return originalSender;
	}

	public void setOriginalSender(OvernesiaNodeID originalSender) {
		this.originalSender = originalSender;
	}

	public ArrayList<UUID> getKnownNesoi() {
		return knownNesoi;
	}

	public void setKnownNesoi(ArrayList<UUID> knownNesoi) {
		this.knownNesoi = knownNesoi;
	}

	public short getTTL() {
		return ttl;
	}

	public void setTTL(short ttl) {
		this.ttl = ttl;
	}

	
	static public ByteBuffer writeReallocateRequestToBuffer(ReallocateRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + 2 + (request.knownNesoi.size() * 16));
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.originalSender);
		msg.putShort(request.ttl);
			
		msg.putShort((short) request.knownNesoi.size());
		
		for(UUID id: request.knownNesoi)
			UUIDs.writeUUIDToBuffer(msg, id);
		
		msg.flip();
		return msg;
	}
	
	public static ReallocateRequest readReallocateRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 2 + 2);
		
		OvernesiaNodeID nid = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short ttl = buf.getShort();
		short size = buf.getShort();
		
		buf = Buffers.sliceCompact(msg, size * 16);
		ArrayList<UUID> knownNesoi = new ArrayList<UUID>(size);
		
		for(int i = 0; i < size; i++)
			knownNesoi.add(UUIDs.readUUIDFromBuffer(buf));
		
		return new ReallocateRequest(nid, knownNesoi, ttl);
	}

	public void decreaseTTL() {
		this.ttl--;
	}

	public void addNesosToKnownNesoiSet(UUID nesosUUID) {
		this.knownNesoi.add(nesosUUID);
	}
	
}

package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;

import java.nio.ByteBuffer;

public class JoinRequest {

	private OvernesiaNodeID newNode;

	public JoinRequest(OvernesiaNodeID nn) {
		this.newNode = nn;
	}
	
	public OvernesiaNodeID getNewNode() {
		return this.newNode;
	}
	
	static public ByteBuffer writeJoinRequestToBuffer(JoinRequest request) {
		return OvernesiaNodeID.writeNodeIdToBuffer(request.newNode);
	}
	
	public static JoinRequest readJoinRequestFromBuffer(ByteBuffer[] msg) {
        return new JoinRequest(OvernesiaNodeID.readNodeIDFromBuffer(msg));
	}
	
}

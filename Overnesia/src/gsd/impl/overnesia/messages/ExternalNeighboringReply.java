package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.UUID;


public class ExternalNeighboringReply {

	private OvernesiaNodeID sender;
	private UUID remoteNesosID;
	
	public ExternalNeighboringReply(OvernesiaNodeID sender, UUID remoteNesosID) {
		this.sender = sender;
		this.remoteNesosID = remoteNesosID;
	}
	
	
	
	public OvernesiaNodeID getSender() {
		return sender;
	}



	public void setSender(OvernesiaNodeID sender) {
		this.sender = sender;
	}



	public UUID getRemoteNesosID() {
		return remoteNesosID;
	}



	public void setRemoteNesosID(UUID remoteNesosID) {
		this.remoteNesosID = remoteNesosID;
	}



	static public ByteBuffer writeExternalNeighboringReplyToBuffer(ExternalNeighboringReply request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 16);
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg,request.sender);
		UUIDs.writeUUIDToBuffer(msg, request.remoteNesosID);				
		
		msg.flip();
		return msg;
	}
	
	public static ExternalNeighboringReply readNExternalNeighboringReplyFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 16);		
		return new ExternalNeighboringReply(OvernesiaNodeID.readNodeIDFromBuffer(buf), UUIDs.readUUIDFromBuffer(buf));
	}
	
}

package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.UUID;

public class NesosDivision implements Comparable<NesosDivision> {

	private OvernesiaNodeID sender;
	private UUID nesosA;
	private UUID nesosB;
	private ArrayList<OvernesiaNodeID> filiationNesosA;
	private ArrayList<OvernesiaNodeID> filiationNesosB;
		
	public NesosDivision(OvernesiaNodeID sender, UUID nesosA, UUID nesosB, ArrayList<OvernesiaNodeID> filiationNesosA, ArrayList<OvernesiaNodeID> filiationNesosB) {
		this.sender = sender;
		this.nesosA = nesosA;
		this.nesosB = nesosB;
		this.filiationNesosA = filiationNesosA;
		this.filiationNesosB = filiationNesosB;
	}
	
	public OvernesiaNodeID getSender() {
		return sender;
	}

	public void setSender(OvernesiaNodeID sender) {
		this.sender = sender;
	}

	public UUID getNesosA() {
		return nesosA;
	}

	public void setNesosA(UUID nesosA) {
		this.nesosA = nesosA;
	}

	public UUID getNesosB() {
		return nesosB;
	}

	public void setNesosB(UUID nesosB) {
		this.nesosB = nesosB;
	}

	public ArrayList<OvernesiaNodeID> getFiliationNesosA() {
		return filiationNesosA;
	}

	public void setFiliationNesosA(ArrayList<OvernesiaNodeID> filiationNesosA) {
		this.filiationNesosA = filiationNesosA;
	}

	public ArrayList<OvernesiaNodeID> getFiliationNesosB() {
		return filiationNesosB;
	}

	public void setFiliationNesosB(ArrayList<OvernesiaNodeID> filiationNesosB) {
		this.filiationNesosB = filiationNesosB;
	}

	static public ByteBuffer writeNesosDivisionToBuffer(NesosDivision request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 16 + 16 + 2 + 2 + ((request.filiationNesosA.size() + request.filiationNesosB.size()) * 22));
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.sender);
		UUIDs.writeUUIDToBuffer(msg, request.nesosA);
		UUIDs.writeUUIDToBuffer(msg, request.nesosB);
		msg.putShort((short) request.filiationNesosA.size());
		msg.putShort((short) request.filiationNesosB.size());
		for(OvernesiaNodeID id: request.filiationNesosA)
			OvernesiaNodeID.writeNodeIdToBufferWithoutNesos(msg, id);
		for(OvernesiaNodeID id: request.filiationNesosB)
			OvernesiaNodeID.writeNodeIdToBufferWithoutNesos(msg, id);
		msg.flip();
		
		return msg;
	}
	
	public static NesosDivision readNesosDivisionFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 16 + 16 + 2 + 2);		
        
		OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(buf);
        UUID nesosA = UUIDs.readUUIDFromBuffer(buf);
        UUID nesosB = UUIDs.readUUIDFromBuffer(buf);
        short nesosASize = buf.getShort();
        short nesosBSize = buf.getShort();
		buf = Buffers.sliceCompact(msg, (nesosASize + nesosBSize) * 22);
		ArrayList<OvernesiaNodeID> filiationNesosA = new ArrayList<OvernesiaNodeID>();
		ArrayList<OvernesiaNodeID> filiationNesosB = new ArrayList<OvernesiaNodeID>();
		for(int i = 0; i < nesosASize; i++)
			filiationNesosA.add(OvernesiaNodeID.readNodeIDFromBufferWithoutNesos(buf, nesosA));
		for(int i = 0; i < nesosBSize; i++)
			filiationNesosB.add(OvernesiaNodeID.readNodeIDFromBufferWithoutNesos(buf, nesosB));
		
		return new NesosDivision(sender, nesosA, nesosB, filiationNesosA, filiationNesosB);
	}

	public int compareTo(NesosDivision o) {
		return this.sender.getUUID().compareTo(o.sender.getUUID());
	}
	
	@Override
	public String toString() {
		return "Nesos Division Proposal\nIssued by: " + this.sender + "\nCluster A Proposed ID: " + this.nesosA + "\nFiliation:\n" + printFiliation(this.filiationNesosA) + "Cluster B proposed ID: " + this.nesosB + "\nFiliation:\n" + printFiliation(this.filiationNesosB);
	}

	public String printFiliation(ArrayList<OvernesiaNodeID> list) {
		String ret = "";
		Iterator<OvernesiaNodeID> i = new TreeSet<OvernesiaNodeID>(list).iterator();
		while(i.hasNext())
			ret = ret + "  " + i.next() + "\n";
		return ret;
	}
}

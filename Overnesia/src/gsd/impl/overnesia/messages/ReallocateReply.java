package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;

public class ReallocateReply {

	private OvernesiaNodeID sender;
	private Set<OvernesiaNodeID> set;
	
	public ReallocateReply(OvernesiaNodeID peer, Set<OvernesiaNodeID> filiation) {
		this.sender = peer;
		this.set = filiation;
	}
	
	public OvernesiaNodeID getSender() {
		return this.sender;
	}
	
	public Set<OvernesiaNodeID> getFiliation() {
		return this.set;
	}
	
	static public ByteBuffer writeReallocateReplyToBuffer(ReallocateReply request) {
		Set<OvernesiaNodeID> filiation = request.getFiliation();
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + (22 * filiation.size()));
		
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.getSender());
		msg.putShort((short) filiation.size());
		
		for(OvernesiaNodeID element: filiation) {
			OvernesiaNodeID.writeNodeIdToBufferWithoutNesos(msg, element);
		}
		
		msg.flip();
		return msg;
	}
	
	public static ReallocateReply readReallocateReplyFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 40);
		OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short filiationSize = buf.getShort();
		HashSet<OvernesiaNodeID> set = new HashSet<OvernesiaNodeID>(filiationSize);	
		buf = Buffers.sliceCompact(msg, 22 * filiationSize);
		for(int i = 0; i < filiationSize; i++)
			set.add(OvernesiaNodeID.readNodeIDFromBufferWithoutNesos(buf, sender.getNesosUUID()));
		
        return new ReallocateReply(sender, set);
	}
	
}

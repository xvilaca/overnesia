package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;



public class NesosGossip {
	private OvernesiaNodeID originator;
	private ArrayList <OvernesiaNodeID> filiation;
	private ArrayList <UUID> externalNesoi; 
	
	public NesosGossip(OvernesiaNodeID originator, Set<OvernesiaNodeID> filiation, Set<OvernesiaNodeID> externalNeighbors) {
		this.originator = originator;
		this.filiation = new ArrayList<OvernesiaNodeID>(filiation);
		this.externalNesoi = new ArrayList<UUID>();
		for(OvernesiaNodeID id: externalNeighbors) {
			if(!this.externalNesoi.contains(id.getNesosUUID()))
				this.externalNesoi.add(id.getNesosUUID());
		}
	}
	
	public NesosGossip(OvernesiaNodeID originator, ArrayList<OvernesiaNodeID> filiation, Set<OvernesiaNodeID> externalNeighbors) {
		this.originator = originator;
		this.filiation = filiation;
		this.externalNesoi = new ArrayList<UUID>();
		for(OvernesiaNodeID id: externalNeighbors) {
			if(!this.externalNesoi.contains(id.getNesosUUID()))
				this.externalNesoi.add(id.getNesosUUID());
		}
	}
	
	public NesosGossip(OvernesiaNodeID originator, ArrayList<OvernesiaNodeID> filiation, ArrayList<UUID> externalNesoi) {
		this.originator = originator;
		this.filiation = filiation;
		this.externalNesoi = externalNesoi;
	}
	
	public OvernesiaNodeID getOriginator() {
		return originator;
	}

	public void setOriginator(OvernesiaNodeID originator) {
		this.originator = originator;
	}

	public ArrayList<OvernesiaNodeID> getFiliation() {
		return filiation;
	}

	public void setFiliation(ArrayList<OvernesiaNodeID> filiation) {
		this.filiation = filiation;
	}

	public ArrayList<UUID> getExternalNesoi() {
		return externalNesoi;
	}

	public void setExternalNesoi(ArrayList<UUID> externalNesoi) {
		this.externalNesoi = externalNesoi;
	}

	static public ByteBuffer writeNesosGossipToBuffer(NesosGossip request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + 2 + (request.filiation.size() * 22) + (request.externalNesoi.size() * 16));
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.originator);
		msg.putShort((short)request.filiation.size());
		msg.putShort((short)request.externalNesoi.size());
		for(OvernesiaNodeID id: request.filiation)
			OvernesiaNodeID.writeNodeIdToBufferWithoutNesos(msg, id);		
		for(UUID id: request.externalNesoi)
			UUIDs.writeUUIDToBuffer(msg, id);
		
		msg.flip();
		return msg;
	}
		
	public static NesosGossip readNesosGossipFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 2 + 2);
		OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short filiationSize = buf.getShort();
		short externalNesoiSize = buf.getShort();
		ArrayList<OvernesiaNodeID> filiation = new ArrayList<OvernesiaNodeID>(filiationSize);
		ArrayList<UUID> externalNesoi = new ArrayList<UUID>(externalNesoiSize);
		
		buf = Buffers.sliceCompact(msg, (filiationSize * 22) + (externalNesoiSize * 16));
		
		for(int i = 0; i < filiationSize; i++)
			filiation.add(OvernesiaNodeID.readNodeIDFromBufferWithoutNesos(buf, sender.getNesosUUID()));
			
		for(int i = 0; i < externalNesoiSize; i++)
			externalNesoi.add(UUIDs.readUUIDFromBuffer(buf));
        
		return new NesosGossip(sender, filiation, externalNesoi);
	}

}

package gsd.impl.overnesia.messages;

import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.UUID;

public class DisconnectRequest {

	private UUID currentNesosID;
	private UUID targetNesosID;
	private boolean nesosDisconnect; //TODO: Remove this if this is indeed unecessary as it appears...
	
	
	public UUID getCurrentNesosID() {
		return currentNesosID;
	}

	public void setCurrentNesosID(UUID currentNesosID) {
		this.currentNesosID = currentNesosID;
	}

	public UUID getTargetNesosID() {
		return targetNesosID;
	}

	public void setTargetNesosID(UUID targetNesosID) {
		this.targetNesosID = targetNesosID;
	}

	public boolean isNesosDisconnect() {
		return nesosDisconnect;
	}

	public void setNesosDisconnect(boolean nesosDisconnect) {
		this.nesosDisconnect = nesosDisconnect;
	}
	
	public DisconnectRequest(UUID currentNesosID, UUID targetNesosID, boolean nesos) {
		this.currentNesosID = currentNesosID;
		this.targetNesosID = targetNesosID;
		this.nesosDisconnect = nesos;
	}
	
	static public ByteBuffer writeDisconnectRequestToBuffer(DisconnectRequest request) {
		
		ByteBuffer msg = ByteBuffer.allocate(32 + 1);
		UUIDs.writeUUIDToBuffer(msg, request.currentNesosID);
		UUIDs.writeUUIDToBuffer(msg, request.targetNesosID);
		if(request.nesosDisconnect)
			msg.put((byte) 0xf);
		else
			msg.put((byte) 0x0);
		msg.flip();
		return msg;
	}
	
	public static DisconnectRequest readDisconnectRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 32 + 1);		
        return new DisconnectRequest(UUIDs.readUUIDFromBuffer(buf), UUIDs.readUUIDFromBuffer(buf), buf.get() == (byte) 0xf);
	}
	
}

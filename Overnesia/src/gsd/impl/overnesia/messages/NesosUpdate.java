package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.UUID;

public class NesosUpdate {

	private OvernesiaNodeID sender;
	private UUID oldNesosID;
	private boolean nesosNeighbor; 
		
	public OvernesiaNodeID getSender() {
		return sender;
	}

	public void setSender(OvernesiaNodeID sender) {
		this.sender = sender;
	}

	public UUID getOldNesosID() {
		return this.oldNesosID;
	}
	
	public void setOldNesosID(UUID oldNesosID) {
		this.oldNesosID = oldNesosID;
	}
	
	public boolean isNesosNeighbor() {
		return nesosNeighbor;
	}

	public void setNesosNeighbor(boolean nesosNeighbor) {
		this.nesosNeighbor = nesosNeighbor;
	}

	public NesosUpdate(OvernesiaNodeID sender, UUID oldNesosID, boolean nesos) {
		this.sender = sender;
		this.oldNesosID = oldNesosID;
		this.nesosNeighbor = nesos;
	}
	
	static public ByteBuffer writeNesosUpdateToBuffer(NesosUpdate request) {
		
		ByteBuffer msg = ByteBuffer.allocate(38 + 16 + 1);
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.sender);
		UUIDs.writeUUIDToBuffer(msg, request.oldNesosID);
		if(request.nesosNeighbor)
			msg.put((byte) 0xf);
		else
			msg.put((byte) 0x0);
		msg.flip();
		return msg;
	}
	
	public static NesosUpdate readNesosUpdateFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 16 + 1);		
        return new NesosUpdate(OvernesiaNodeID.readNodeIDFromBuffer(buf), UUIDs.readUUIDFromBuffer(buf), buf.get() == (byte) 0xf);
	}
	
}

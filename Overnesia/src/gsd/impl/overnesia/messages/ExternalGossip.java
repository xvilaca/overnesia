package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class ExternalGossip {
	private OvernesiaNodeID originator;
	private ArrayList <OvernesiaNodeID> sample; 

	
	public ExternalGossip(OvernesiaNodeID originator, ArrayList<OvernesiaNodeID> sample) {
		this.originator = originator;
		this.sample = sample;
	}
	
	
	public OvernesiaNodeID getOriginator() {
		return originator;
	}

	public void setOriginator(OvernesiaNodeID originator) {
		this.originator = originator;
	}

	public ArrayList<OvernesiaNodeID> getSample() {
		return sample;
	}

	public void setFiliation(ArrayList<OvernesiaNodeID> sample) {
		this.sample = sample;
	}

	static public ByteBuffer writeExternalGossipToBuffer(ExternalGossip request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + (request.sample.size() * 22));
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.originator);
		msg.putShort((short)request.sample.size());
		for(OvernesiaNodeID id: request.sample)
			OvernesiaNodeID.writeNodeIdToBufferWithoutNesos(msg, id);		
		msg.flip();
		return msg;
	}
		
	public static ExternalGossip readExternalGossipFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 2);
		OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short sampleSize = buf.getShort();
	
		ArrayList<OvernesiaNodeID> sample = new ArrayList<OvernesiaNodeID>(sampleSize);
		buf = Buffers.sliceCompact(msg, sampleSize * 22);
		
		for(int i = 0; i < sampleSize; i++)
			sample.add(OvernesiaNodeID.readNodeIDFromBufferWithoutNesos(buf, sender.getNesosUUID()));
			   
		return new ExternalGossip(sender, sample);
	}

}

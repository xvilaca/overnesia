package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ExternalNeighboringRequest {

	private OvernesiaNodeID originalSender;
	private ArrayList<UUID> knownNesoi;
	private short externalTTL;
	private short internalTTL;
	private boolean disconnected;

	public ExternalNeighboringRequest(OvernesiaNodeID peer, Set<OvernesiaNodeID> externalNeighbors, boolean highPriority, short externalTTL, short internalTTL) {
		this.originalSender = peer;
		this.knownNesoi = new ArrayList<UUID>();
		for(OvernesiaNodeID id: externalNeighbors)
			if(!this.knownNesoi.contains(id.getNesosUUID()))
				this.knownNesoi.add(id.getNesosUUID());
		this.disconnected = highPriority;
		this.externalTTL = externalTTL;
		this.internalTTL = internalTTL;
	}
	
	public ExternalNeighboringRequest(OvernesiaNodeID peer, ArrayList<UUID> knownNesoi, boolean highPriority, short externalTTL, short internalTTL) {
		this.originalSender = peer;
		this.knownNesoi = knownNesoi;
		this.disconnected = highPriority;
		this.externalTTL = externalTTL;
		this.internalTTL = internalTTL;
	}

	public OvernesiaNodeID getOriginalSender() {
		return originalSender;
	}

	public void setOriginalSender(OvernesiaNodeID originalSender) {
		this.originalSender = originalSender;
	}

	public ArrayList<UUID> getKnownNesoi() {
		return knownNesoi;
	}

	public void setKnownNesoi(ArrayList<UUID> knownNesoi) {
		this.knownNesoi = knownNesoi;
	}

	public short getExternalTTL() {
		return externalTTL;
	}

	public void setExternalTTL(short externalTTL) {
		this.externalTTL = externalTTL;
	}

	public short getInternalTTL() {
		return internalTTL;
	}

	public void setInternalTTL(short internalTTL) {
		this.internalTTL = internalTTL;
	}

	public boolean isDisconnected() {
		return disconnected;
	}

	public void setDisconnected(boolean disconnected) {
		this.disconnected = disconnected;
	}
	
	static public ByteBuffer writeExternalNeighboringRequestToBuffer(ExternalNeighboringRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + 2 + 1 + 2 + (request.knownNesoi.size() * 16));
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.originalSender);
		msg.putShort(request.externalTTL);
		msg.putShort(request.internalTTL);
		
		if(request.disconnected) {
			msg.put((byte)0xf);
		} else {
			msg.put((byte)0x0);
		}
		
		msg.putShort((short) request.knownNesoi.size());
		
		for(UUID id: request.knownNesoi)
			UUIDs.writeUUIDToBuffer(msg, id);
		
		msg.flip();
		return msg;
	}
	
	public static ExternalNeighboringRequest readExternalNeighboringRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 2 + 2 + 1 + 2);
		
		OvernesiaNodeID nid = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short extTTL = buf.getShort();
		short intTTL = buf.getShort();
		boolean disconnect = buf.get() == 0xf;
		short size = buf.getShort();
		
		buf = Buffers.sliceCompact(msg, size * 16);
		ArrayList<UUID> knownNesoi = new ArrayList<UUID>();
		
		for(int i = 0; i < size; i++)
			knownNesoi.add(UUIDs.readUUIDFromBuffer(buf));
		
		return new ExternalNeighboringRequest(nid, knownNesoi, disconnect, extTTL, intTTL);
	}

	public void decreaseExternalTTL() {
		this.externalTTL--;
	}

	public void decreaseInternalTTL() {
		this.internalTTL--;
	}
	
}

package gsd.impl.overnesia.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class ForwardedJoinRequest {
	
	private short ttl;
	private OvernesiaNodeID newNode;
	private ArrayList<UUID> visitedNesoi;
	
	public ForwardedJoinRequest(OvernesiaNodeID newNode, short ttl, UUID currentNesos) {
		this.ttl = ttl;
		this.newNode = newNode;
		this.visitedNesoi = new ArrayList<UUID>();
		this.visitedNesoi.add(currentNesos);
	}
	
	public ForwardedJoinRequest(OvernesiaNodeID newNode, short ttl, ArrayList<UUID> visitedNesoi) {
		this.ttl = ttl;
		this.newNode = newNode;
		this.visitedNesoi = visitedNesoi;
	}
	
	public int getTTL() {
		return this.ttl;
	}
	
	public OvernesiaNodeID getNewNode() {
		return this.newNode;
	}
	
	public void decrementTTL() {
		this.ttl--;
	}
	
	static public ByteBuffer writeForwardedJoinRequestToBuffer(ForwardedJoinRequest request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 2 + 2 + (request.visitedNesoi.size() * 16));
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.newNode);
		msg.putShort(request.ttl);
		msg.putShort((short) request.visitedNesoi.size());
		for(UUID id: request.visitedNesoi)
			UUIDs.writeUUIDToBuffer(msg, id);
		msg.flip();
		return msg;
	}
	
	public static ForwardedJoinRequest readForwardedJoinRequestFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 2 + 2);
		OvernesiaNodeID nid = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		short ttl = buf.getShort();
		short size = buf.getShort();
		ArrayList<UUID> visited = new ArrayList<UUID>(size);
		buf = Buffers.sliceCompact(msg, size * 16);
		for(int i = 0; i < size; i++)
			visited.add(UUIDs.readUUIDFromBuffer(buf));
        return new ForwardedJoinRequest(nid, ttl, visited);
	}

	public Collection<UUID> getVisitedNesoi() {
		return this.visitedNesoi;
	}

	public void markNesosAsVisited(UUID nesosUUID) {
		if(!this.visitedNesoi.contains(nesosUUID))
			this.visitedNesoi.add(nesosUUID);
	}

}

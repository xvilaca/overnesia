package gsd.impl.overnesia;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Connection;
import gsd.impl.OverlayNetwork;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public interface Overnesia extends OverlayNetwork {
	
	public OvernesiaNodeID getId();

	/*************************************************************
	 * Methods for interaction with the DisseminationStrategy (//debug)
	 *************************************************************/
	
	public int getNesosSize();
	public int getExternalNeighborsSize();	
	public int getPassiveViewSize();

	public Set<OvernesiaNodeID> getNesosFiliation();
	public Set<OvernesiaNodeID> getExternalIDs();
	public Collection<OvernesiaNodeID> getPassiveView();

	public HashMap<OvernesiaNodeID, Connection> getOvernesiaExternalView();
	public HashMap<OvernesiaNodeID, Connection> getOvernesiaNesosView();
	public String pendingConns();

	public void flushStatistics(PrintStream printStream, long epoch);
	
}

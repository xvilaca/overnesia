package gsd.impl.overnesia;

import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.NodeID;
import gsd.common.OvernesiaNodeID;
import gsd.impl.Connection;
import gsd.impl.ConnectionListener;
import gsd.impl.DataListener;
import gsd.impl.OverlayAware;
import gsd.impl.Periodic;
import gsd.impl.Transport;
import gsd.impl.overnesia.messages.ExternalGossip;
import gsd.impl.overnesia.messages.NesosDivision;
import gsd.impl.overnesia.messages.NesosUpdate;
import gsd.impl.overnesia.messages.DisconnectRequest;
import gsd.impl.overnesia.messages.ExternalNeighboringReply;
import gsd.impl.overnesia.messages.ExternalNeighboringRequest;
import gsd.impl.overnesia.messages.JoinRequest;
import gsd.impl.overnesia.messages.ForwardedJoinRequest;
import gsd.impl.overnesia.messages.JoinReply;
import gsd.impl.overnesia.messages.NesosGossip;
import gsd.impl.overnesia.messages.NesosNeighboringRequest;
import gsd.impl.overnesia.messages.ReallocateReply;
import gsd.impl.overnesia.messages.ReallocateRequest;

import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class OvernesiaAPlus implements Overnesia, ConnectionListener, DataListener {
	
	/**
	 * Key attributes (passed by the constructor)
	 */
	private Random rand;
	private Transport net;
	
	/**
	 * Protocol parameters for nesoi size control
	 */
	protected int nesos_management_interval;
	protected int nesos_management_interval_additional;
	protected int ns;
	protected int ns_max;
	protected int ns_min;
	protected short join_ttl;
	
	/**
	 * Protocol parameters for managing external nesos neighbors
	 */
	protected int external_neighbors_management_interval;
	protected int external_neighbors;
	
	/**
	 * Protocol parameters for managing anti entropy
	 */
	protected int anti_entropy_interval;
	protected double anti_entropy_probability;
	
	/**
	 * Protocol parameters for managing passive view
	 */
	protected int passive_view_size;
	protected int shuffle_sample_size;
	
	/**
	 * Other Protocol parameters
	 */
	protected long timeout;
	
	/**
	 * Message ports
	 */
	private short JoinRequestPort;
	private short ForwardJoinRequestPort;
	private short JoinReplyPort;
	private short NesosNeighboringRequestPort;
	private short NesosUpdatePort;
	private short ExternalNeighboringRequestPort;
	private short ExternalNeighboringReplyPort;
	private short ExternalNeighboringRequestTempPort;
	private short NesosDivisionPort;
	private short NesosGossipPort;
	private short ExternalGossipRequestPort;
	private short ExternalGossipReplyPort;
	private short ReallocateRequestPort;
	private short ReallocateReplyPort;
	private short ReallocateRequestTempPort;
	private short DisconnectRequestPort;
	
	/**
	 * Protocol state
	 */
	private OvernesiaNodeID myId;
	private HashMap<InetSocketAddress,Operation> operations;	
	private HashMap<OvernesiaNodeID,Connection> nesos_view;
	private HashMap<OvernesiaNodeID,Connection> external_view;
	private LinkedList<OvernesiaNodeID> passive_view;
	
	/**
	 * Periodic Operations
	 */
	private Periodic NesosManagement;
	private Periodic ExternalManagement;
	private Periodic AntiEntropy;
	
	/**
	 * Additional status for performing nesos division
	 */
	private TreeSet<NesosDivision> pendingDivisionRequests;
	private ArrayList<UUID> lastReceivedNesosGossip;
	private OvernesiaNodeID lastExternalGossipDest;
	private ArrayList<OvernesiaNodeID> lastExternalGossipSample;
	
	/**
	 * This is used to recover nodes which Join fails...
	 */
	private InetSocketAddress contactNodeAddress;
	
	
	/**
	 * 
	 * @param rand
	 * @param net
	 * @param ports
	 */
	public OvernesiaAPlus(Random rand, Transport net, short[] ports, InetSocketAddress myId) {
		this.rand = rand;
		this.net = net;
	
		//We want to use udp so...
		this.net.activateUDP();
		
		//Message ports definition
		this.JoinRequestPort = ports[0]; //101
		this.ForwardJoinRequestPort = ports[1]; //102
		this.JoinReplyPort = ports[2]; //103
		this.NesosNeighboringRequestPort = ports[3]; //201 
		this.NesosUpdatePort = ports[4]; //202
		this.ExternalNeighboringRequestPort = ports[5]; //301
		this.ExternalNeighboringReplyPort = ports[6]; //302
		this.ExternalNeighboringRequestTempPort = ports[7]; //309
		this.NesosDivisionPort = ports[8]; //401
		this.NesosGossipPort = ports[9]; //501
		this.ExternalGossipRequestPort = ports[10]; //601
		this.ExternalGossipReplyPort = ports[11]; //602
		this.ReallocateRequestPort = ports[12]; //701
		this.ReallocateReplyPort = ports[13]; //702
		this.ReallocateRequestTempPort = ports[14]; //709
		this.DisconnectRequestPort = ports[15]; //901
	
		//Protocol parameters initialization [Default values]
		this.nesos_management_interval = 20000; //20 seconds
		this.nesos_management_interval_additional = 20000; //20 seconds
		this.ns = 5; 		//TODO: REVIEW THIS
		this.ns_max = 10; 	//TODO: REVIEW THIS
		this.ns_min = 5; 	//TODO: REVIEW THIS
		this.join_ttl = 10;
		
		this.external_neighbors_management_interval = 20000; //20 seconds
		this.external_neighbors = 3;
	
		this.anti_entropy_interval = 10000; //10 seconds
		this.anti_entropy_probability = 0.1;
		
		this.passive_view_size = 20;
		this.shuffle_sample_size = 8;
		
		this.timeout = 5000; //5 seconds 
		
		//Protocol state variables
		this.myId = new OvernesiaNodeID(myId);
		this.operations = new HashMap<InetSocketAddress,Operation>();
		this.nesos_view = new HashMap<OvernesiaNodeID, Connection>();
		this.external_view = new HashMap<OvernesiaNodeID, Connection>();
		this.passive_view = new LinkedList<OvernesiaNodeID>();
		
		//Setup periodic operations
		this.NesosManagement = new Periodic(rand, net, this.nesos_management_interval, this.nesos_management_interval_additional) {
			public void run() {
				nesosManagementTask();
			}
		};
		
		this.ExternalManagement = new Periodic(rand, net, this.external_neighbors_management_interval, 0) {
			public void run() {
				externalNeighboringManagementTask();
			}
		};
		
		this.AntiEntropy = new Periodic(rand, net, this.anti_entropy_interval, 0) {
			public void run() {
				antiEntropyTask();
			}
		};
		
		this.pendingDivisionRequests = new TreeSet<NesosDivision>();
		this.lastExternalGossipSample = new ArrayList<OvernesiaNodeID>();
		
		//Prepare call back for network.
        net.setDataListener(this, this.JoinRequestPort);
        net.setDataListener(this, this.ForwardJoinRequestPort);
        net.setDataListener(this, this.JoinReplyPort);
        net.setDataListener(this, this.NesosNeighboringRequestPort);
        net.setDataListener(this, this.NesosUpdatePort);
        net.setDataListener(this, this.ExternalNeighboringRequestPort);
        net.setDataListener(this, this.ExternalNeighboringReplyPort);
        net.setDataListener(this, this.ExternalNeighboringRequestTempPort);
        net.setDataListener(this, this.NesosDivisionPort);
        net.setDataListener(this, this.NesosGossipPort);
        net.setDataListener(this, this.ExternalGossipRequestPort);
        net.setDataListener(this, this.ExternalGossipReplyPort);
        net.setDataListener(this, this.ReallocateRequestPort);
        net.setDataListener(this, this.ReallocateReplyPort);
        net.setDataListener(this, this.ReallocateRequestTempPort);
        net.setDataListener(this, this.DisconnectRequestPort);
        net.setConnectionListener(this);
        
        this.startPeriodicOperations();
	}
	
	/**
	 * Debug method 
	 * TODO: REMOVE THIS
	 */
	private void debug(String s) {
		System.out.println("Overnesia:   " + s);
	}
	
	/**
	 * This method handles the opening of TCP connections in the underlying layer...
	 */
	
	private void openConnection(final InetSocketAddress contact, Object info, short operationCode) {
		if(!operations.containsKey(contact)) {
			this.operations.put(contact, new Operation(operationCode, info));
			this.net.queue(new Runnable() {
	            public void run() {
	                net.add(contact);
	            }
	        });
		} else {
			System.err.println("Rejected the creation of a new TCP Connection to: " + contact);
			System.err.println("Current Operation was: " + new Operation(operationCode));
			System.err.println("Existing Operation is: " + operations.get(contact));
		}
	}
	
	/**
	 * Initialization Method for the Overlay Network. 
	 * This will open a connection to the contact node and then send a join request.
	 */
	
	public synchronized void init(final InetSocketAddress contact) {
		this.contactNodeAddress = contact;
		this.openConnection(contact, null, Operation.JOIN);		
	}
	
	

	/**
	 * Initialization Method for the Overlay Network. 
	 * This is mandatory for the first node in the overlay.
	 */
	public void init() {
		this.myId.setNesosUUID(UUID.randomUUID());
		this.contactNodeAddress = null;
	}
	
	private void startPeriodicOperations() {
		this.NesosManagement.start();
		this.ExternalManagement.start();
		this.AntiEntropy.start();
	}

	private void executeJoin(final InetSocketAddress listenAddr, final Connection contact) {
		if(this.myId.getNesosUUID() == null) {
			JoinRequest jr = new JoinRequest(this.myId);
			this.msg_send++;
			contact.send(new ByteBuffer[]{JoinRequest.writeJoinRequestToBuffer(jr)}, this.JoinRequestPort);
			net.schedule(new Runnable() {
				public void run() {
					executeJoin(listenAddr, contact);
				}	
			}, this.timeout);			
		}
	}
	
	/**
	 * Methods to be executed by the Periodic managers
	 */
	
	private final void nesosManagementTask() {
		if(this.nesos_view.size() == 0 && this.external_view.size() == 0) {
			System.out.println("Exception: I am completely disconnected (passive view size: " + this.passive_view.size() + ").");
			if(this.passive_view.size() > 0) {
				this.init(this.passive_view.get(rand.nextInt(this.passive_view.size())).getSocketAddress());
				return;
			} else if(this.contactNodeAddress != null) {
				this.init(this.contactNodeAddress);
				return;
			}
		}
		
		if(this.myId.getNesosUUID() != null && this.pendingDivisionRequests.size() == 0) {
			if(this.nesosSize() >= this.ns_max && this.isNesosCoordinator()) {
				this.nesosAboveMaximumSize();
			} else if(this.nesosSize() < this.ns_min) {
				this.nesosBelowMinimumSize();
			}
		}
	}
	
	private void nesosAboveMaximumSize() {
		ArrayList<OvernesiaNodeID> nesosA = new ArrayList<OvernesiaNodeID>();
		nesosA.add(this.myId);
		ArrayList<OvernesiaNodeID> nesosB = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
		int toSwitch = nesosB.size() / 2;
		for(int i = 0; i < toSwitch; i++) {
			nesosA.add(nesosB.remove(rand.nextInt(nesosB.size())));
		}
		
		ByteBuffer myNesosDivisionProposal = NesosDivision.writeNesosDivisionToBuffer(new NesosDivision(this.myId, UUID.randomUUID(), UUID.randomUUID(), nesosA, nesosB));
		for(OvernesiaNodeID id: this.nesos_view.keySet()) {
			this.nesos_view.get(id).send(new ByteBuffer[]{myNesosDivisionProposal.duplicate()}, this.NesosDivisionPort);
			this.msg_send++;
		}

		this.pendingDivisionRequests.add(NesosDivision.readNesosDivisionFromBuffer(new ByteBuffer[]{myNesosDivisionProposal}));
		this.net.schedule(new Runnable() {
			public void run() {
				nesosDivisionTimerHandler();
			}
		}, this.timeout/2 + rand.nextInt((int) (this.timeout/2)) ); //TODO: This timeout use (5 seconds... is not the better one)
	}
	
	protected synchronized void nesosDivisionTimerHandler() {
		////debug("Handling the Nesos Division Timer");
		if(this.pendingDivisionRequests.size() == 0) return; //Nothing to do... no pending proposals... probably already did this!
		
		NesosDivision elected = this.pendingDivisionRequests.first(); //Select the proposal form the node with lowest id.
		this.pendingDivisionRequests.clear(); //Flush all remaining proposals.
		
		UUID myOldNesosID = this.myId.getNesosUUID();
		UUID complementaryNesosID = null;
		ArrayList<OvernesiaNodeID> myNewFiliation = null;
		ArrayList<OvernesiaNodeID> complementaryFiliation = null;
		
		if(elected.getFiliationNesosA().contains(this.myId)) {
			this.myId.setNesosUUID(elected.getNesosA());
			myNewFiliation = elected.getFiliationNesosA();
			complementaryNesosID = elected.getNesosB();
			complementaryFiliation = elected.getFiliationNesosB();
		} else if(elected.getFiliationNesosB().contains(this.myId)) {
			this.myId.setNesosUUID(elected.getNesosB());
			myNewFiliation = elected.getFiliationNesosB();
			complementaryNesosID = elected.getNesosA();
			complementaryFiliation = elected.getFiliationNesosA();
		} else {
			//debug("Applying a Nesos Division proposal where my ID does not appear: " + this.myId);
			//debug(elected.toString());
			//debug("Doing nothing...");
			return;
		}
		
		int mySetPosition = myNewFiliation.indexOf(this.myId);
		
		Iterator<OvernesiaNodeID> iterator = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet()).iterator();
		
		ByteBuffer nesosUpdateMessageforNesosNeighbors = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId ,myOldNesosID ,true));
		ByteBuffer disconnectMessage = DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, true));
		
		while(iterator.hasNext()) {
			OvernesiaNodeID id = iterator.next();
			Connection link = this.nesos_view.get(id);
			
			if(myNewFiliation.contains(id)) {
				//Update my nesos id with this neighbor and update also my current nesos id for him
				link.send(new ByteBuffer[]{nesosUpdateMessageforNesosNeighbors.duplicate()}, this.NesosUpdatePort);
				this.msg_send++;
				id.setNesosUUID(this.myId.getNesosUUID());
			} else {
				id.setNesosUUID(complementaryNesosID);
				if(mySetPosition != complementaryFiliation.indexOf(id)) {
					//Disconnect from this guy
					link.send(new ByteBuffer[]{disconnectMessage.duplicate()}, this.DisconnectRequestPort);		
					this.msg_send++;
					this.removeFromNesosView(id, link, false);
				} else {
					//Make this guy my new external neighbor even if I have to drop an existing neighbor to do that.
					this.nesos_view.remove(id);
					while(this.external_view.size() > this.external_neighbors) {
						OvernesiaNodeID toDrop = new ArrayList<OvernesiaNodeID>(this.external_view.keySet()).get(rand.nextInt(this.external_view.keySet().size()));
						Connection linkToDrop = this.external_view.get(toDrop);
						linkToDrop.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, false))}, this.DisconnectRequestPort);
						this.msg_send++;
						this.removeFromExternalNeighbors(toDrop, linkToDrop, false);
					}
					this.addToExternalNeighbors(id, link);
					//We don't need to send the nesos update message here because it will be (implicitly) sent in the next step of this handler
				}
			}
		}
		
		if(this.external_view.size() > 0) {
			ByteBuffer updateMessage = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId, myOldNesosID, false));
			for(OvernesiaNodeID id: this.external_view.keySet()) {
				this.external_view.get(id).send(new ByteBuffer[]{updateMessage.duplicate()}, this.NesosUpdatePort);
				this.msg_send++;
			}
		}
	}
	
	private void nesosBelowMinimumSize() {
		if(rand.nextDouble() <= (1 - (this.nesos_view.size() * (((double) 1) / this.ns_min)))) {
			Connection link = this.selectRandomNextHop();
			if((link == null || rand.nextDouble() <= 0.5) && this.passive_view.size() > 0 ) {
				OvernesiaNodeID peer = this.passive_view.get(rand.nextInt(this.passive_view.size()));
				this.openConnection(peer.getSocketAddress(), null, Operation.SEND_REALLOCATE_REQUEST);
			} else if(link != null)	{
				link.send(new ByteBuffer[]{ReallocateRequest.writeReallocateRequestToBuffer(new ReallocateRequest(this.myId,this.join_ttl))}, this.ReallocateRequestPort);
				this.msg_send++;
			}
		}
	}
	
	protected void executeSendReallocateRequest(Connection info) {
		if(this.nesos_view.size() < this.ns_min) {
			info.send(new ByteBuffer[]{ReallocateRequest.writeReallocateRequestToBuffer(new ReallocateRequest(this.myId,this.join_ttl))}, this.ReallocateRequestTempPort);
			this.msg_send++;
		} else {
			info.close();
		}
	}
	
	private final void externalNeighboringManagementTask() {
		if(this.myId.getNesosUUID() != null && this.external_view.size() < this.external_neighbors) {
			Connection link = this.selectRandomNextHop();
			if((link == null || rand.nextDouble() <= 0.5) && this.passive_view.size() > 0) {
				OvernesiaNodeID peer = this.passive_view.get(rand.nextInt(this.passive_view.size()));
				this.openConnection(peer.getSocketAddress(), null, Operation.SEND_EXTERNAL_NEIGHBORING);
			} else if(link != null)	{
				link.send(new ByteBuffer[]{ExternalNeighboringRequest.writeExternalNeighboringRequestToBuffer(new ExternalNeighboringRequest(this.myId, this.external_view.keySet(), this.external_view.size() == 0, this.join_ttl,(short) (this.ns / 2)))}, this.ExternalNeighboringRequestPort);
				this.msg_send++;
			}
		}
			
	}
	
	protected void executeSendExternalNeighboringRequest(Connection info) {
		if(this.external_view.size() < this.external_neighbors) {
			info.send(new ByteBuffer[]{ExternalNeighboringRequest.writeExternalNeighboringRequestToBuffer(new ExternalNeighboringRequest(this.myId, this.external_view.keySet(), this.external_view.size() == 0, this.join_ttl,(short) (this.ns / 2)))}, this.ExternalNeighboringRequestTempPort);
			this.msg_send++;
		} else {
			info.close();
		}
	}
	
	private final void antiEntropyTask() {
		if(this.nesos_view.size() >= 2 && this.pendingDivisionRequests.size() == 0 && rand.nextDouble() <= this.anti_entropy_probability) {
			ArrayList<OvernesiaNodeID> temp = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
			this.nesos_view.get(temp.remove(rand.nextInt(temp.size()))).send(new ByteBuffer[]{NesosGossip.writeNesosGossipToBuffer(new NesosGossip(this.myId, temp, this.external_view.keySet()))}, this.NesosGossipPort);		
			this.msg_send++;
		}
		
		//Do External gossiping maintenance operations
		if(this.lastExternalGossipDest != null && this.passive_view.contains(this.lastExternalGossipDest)) {
			this.removeFromPassiveView(this.lastExternalGossipDest);
			this.lastExternalGossipSample.clear();
		}
		
		if(this.passive_view.size() > 0 && rand.nextDouble() <= this.anti_entropy_probability) {
			ArrayList<OvernesiaNodeID> workingSet = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
			while(this.lastExternalGossipSample.size() < this.shuffle_sample_size / 2 && workingSet.size() > 0)
				this.lastExternalGossipSample.add(workingSet.remove(rand.nextInt(workingSet.size())));
			workingSet.clear();
			workingSet.addAll(this.passive_view);
			this.lastExternalGossipDest = workingSet.remove(rand.nextInt(workingSet.size()));
			while(this.lastExternalGossipSample.size() < this.shuffle_sample_size && workingSet.size() > 0)
				this.lastExternalGossipSample.add(workingSet.remove(rand.nextInt(workingSet.size())));
			
			net.udpSend(new ByteBuffer[]{ExternalGossip.writeExternalGossipToBuffer(new ExternalGossip(this.myId, this.lastExternalGossipSample))}, this.ExternalGossipRequestPort, this.lastExternalGossipDest.getSocketAddress());
			this.msg_send++;
		}
	}
	
	/**
	 * Methods to handle protocol specific messages
	 */
	
	private void handleJoinRequest(ByteBuffer[] msg, Connection info) {
		OvernesiaNodeID newPeer = JoinRequest.readJoinRequestFromBuffer(msg).getNewNode();
		//debug("Handling a Join Request from " + newPeer);
		if(this.nesosSize() < this.ns) {
			//Accept this node in my nesos 
			info.id = newPeer;
			this.executeAcceptJoin(newPeer, info);
		} else {
			//Initiate the propagation of a random walk
			Connection nextHop = this.selectRandomNextHop();
			//debug("Sending a Forward Join Request for node " + newPeer + " to " + nextHop.id);
			nextHop.send(new ByteBuffer[]{ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(new ForwardedJoinRequest(newPeer, this.join_ttl,this.myId.getNesosUUID()))}, this.ForwardJoinRequestPort);
			this.msg_send++;
			info.close();
		}
	}
	
	private void handleForwardJoinRequest(ByteBuffer[] msg, Connection info) {
		ForwardedJoinRequest request = ForwardedJoinRequest.readForwardedJoinRequestFromBuffer(msg);
		//debug("Handling a Forwarded Join Request for " + request.getNewNode() + " with TTL = " + request.getTTL());
		request.decrementTTL();
		boolean accept = false;		
		
		if(this.nesosSize() < this.ns || request.getTTL() <= 0) {
			accept = true;
		} else {
			Connection nextHop = this.selectRandomNextHop((OvernesiaNodeID)info.id, request.getVisitedNesoi());
			if(nextHop == null)
				accept = true;
			else {
				request.markNesosAsVisited(this.myId.getNesosUUID());
				nextHop.send(new ByteBuffer[]{ForwardedJoinRequest.writeForwardedJoinRequestToBuffer(request)}, this.ForwardJoinRequestPort);
				this.msg_send++;
			}
		} 
		
		if(accept) 
			this.openConnection(request.getNewNode().getSocketAddress(), request.getNewNode(), Operation.ACCEPTJOIN);		
	}

	protected void executeAcceptJoin(OvernesiaNodeID newPeer, Connection info) {
		//debug("Accepting the Join Request of " + newPeer);
		info.send(new ByteBuffer[]{JoinReply.writeJoinReplyToBuffer(new JoinReply(this.myId, this.nesos_view.keySet()))}, this.JoinReplyPort);
		this.msg_send++;
		newPeer.setNesosUUID(this.myId.getNesosUUID());
		this.addToNesosView(newPeer, info);
	}

	private void handleJoinReply(ByteBuffer[] msg, Connection info) {
		JoinReply jr = JoinReply.readJoinReplyFromBuffer(msg);
		//debug("Handling a Join Reply from " + jr.getSender());
		//debug("Filiation in Join Reply: ");
		
		
		this.myId.setNesosUUID(jr.getSender().getNesosUUID());
		info.id = jr.getSender();
		this.addToNesosView(jr.getSender(), info);
		for(OvernesiaNodeID ngh: jr.getFiliation()) {
			//debug("  " + ngh);
			this.openConnection(ngh.getSocketAddress(), ngh, Operation.NESOS_NEIGHBORING);
		}
	}
	
	protected void executeNesosNeighboringRequest(OvernesiaNodeID peer, Connection info) {
		//debug("Sending Nesos Neighboring Request to " + peer + " with nesos ID " + this.myId.getNesosUUID());
		if(!this.nesos_view.containsKey(peer)) {
			this.addToNesosView(peer, info);
			info.send(new ByteBuffer[]{NesosNeighboringRequest.writeNesosNeighboringRequestToBuffer(new NesosNeighboringRequest(this.myId))}, this.NesosNeighboringRequestPort);
			this.msg_send++;
		} else {
			//There was a crossed request... therefore this Connection instance is redundant.
			info.id = null;
			info.close();
		}
	}
	
	private void handleNesosNeighboringRequest(ByteBuffer[] msg, Connection info) {
		NesosNeighboringRequest request = NesosNeighboringRequest.readNesosNeighboringRequestFromBuffer(msg);
		//debug("Handling a Nesos Neighboring Request from " + request.getPeer() + " ( my current nesos is " + this.myId.getNesosUUID() + " )");
		if(request.getPeer().getNesosUUID().equals(this.myId.getNesosUUID())) {
			info.id = request.getPeer();
			this.addToNesosView(request.getPeer(), info);
		} else {
			info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), request.getPeer().getNesosUUID(), true))}, this.DisconnectRequestPort);
			this.msg_send++;
		}
	}
	
	private void handleNesosUpdate(ByteBuffer[] msg, Connection info) {
		NesosUpdate update = NesosUpdate.readNesosUpdateFromBuffer(msg);
		//debug("Handling a Cluster Update from peer " + update.getSender() + " NESOS FLAG IS: " + update.isNesosNeighbor());
	
		NesosDivision elected = null;
		UUID myOldNesosID = this.myId.getNesosUUID();
		ArrayList<OvernesiaNodeID> myNewFiliation = null;
		UUID complementaryNesosID = null;
		ArrayList<OvernesiaNodeID> complementaryFiliation = null;
		
		if(update.isNesosNeighbor()) {
			//Start by checking if we already updated this node nesos id and if there is an agreement.
			if(this.myId.getNesosUUID().equals(update.getSender().getNesosUUID()))
				return;
			
			//Verify if I should reject this neighbor
			boolean rejectNeighbor = this.pendingDivisionRequests.size() == 0 || !update.getOldNesosID().equals(this.myId.getNesosUUID());
			
			if(!rejectNeighbor) {
				elected = this.pendingDivisionRequests.first();
				if(elected.getFiliationNesosA().contains(this.myId)) {
					this.myId.setNesosUUID(elected.getNesosA());
					myNewFiliation = elected.getFiliationNesosA();
					complementaryNesosID = elected.getNesosB();
					complementaryFiliation = elected.getFiliationNesosB();
				} else if(elected.getFiliationNesosB().contains(this.myId)) {
					this.myId.setNesosUUID(elected.getNesosB());
					myNewFiliation = elected.getFiliationNesosB();
					complementaryNesosID = elected.getNesosA();
					complementaryFiliation = elected.getFiliationNesosA();
				} else {
					//debug("Elected a Nesos Division proposal where I am not present as a result of a Nesos Update message");
					rejectNeighbor = true;
				}
				
				//Verify if I agree with this node in terms of the new nesos identifier...
				if(!rejectNeighbor) 
					rejectNeighbor = !this.myId.getNesosUUID().equals(update.getSender().getNesosUUID()) || !myNewFiliation.contains(update.getSender());
			}
			
			if(rejectNeighbor) {
				info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, true))}, this.DisconnectRequestPort);
				this.msg_send++;
				this.removeFromNesosView(update.getSender(), info, false);
				this.myId.setNesosUUID(myOldNesosID);
				info.id = null;
			} else {
				//Trigger local division
				int mySetPosition = myNewFiliation.indexOf(this.myId);
				this.pendingDivisionRequests.clear();
				
				Iterator<OvernesiaNodeID> iterator = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet()).iterator();
				
				ByteBuffer nesosUpdateMessageforNesosNeighbors = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId ,myOldNesosID ,true));
				ByteBuffer disconnectMessage = DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, true));
				
				while(iterator.hasNext()) {
					OvernesiaNodeID id = iterator.next();
					Connection link = this.nesos_view.get(id);
					
					if(myNewFiliation.contains(id)) {
						//Update my nesos id with this neighbor and update also my current nesos id for him
						if(!id.equals(update.getSender())) {
							link.send(new ByteBuffer[]{nesosUpdateMessageforNesosNeighbors.duplicate()}, this.NesosUpdatePort);
							this.msg_send++;
						}
						id.setNesosUUID(this.myId.getNesosUUID());
					} else {
						id.setNesosUUID(complementaryNesosID);
						if(mySetPosition != complementaryFiliation.indexOf(id)) {
							//Disconnect from this guy
							link.send(new ByteBuffer[]{disconnectMessage.duplicate()}, this.DisconnectRequestPort);		
							this.msg_send++;
							this.removeFromNesosView(id, link, false);
							link.id = null;
						} else {
							//Make this guy my new external neighbor even if I have to drop an existing neighbor to do that.
							this.nesos_view.remove(id);
							while(this.external_view.size() >= this.external_neighbors) {
								OvernesiaNodeID toDrop = new ArrayList<OvernesiaNodeID>(this.external_view.keySet()).get(rand.nextInt(this.external_view.keySet().size()));
								Connection linkToDrop = this.external_view.get(toDrop);
								linkToDrop.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, false))}, this.DisconnectRequestPort);
								this.msg_send++;
								this.removeFromExternalNeighbors(toDrop, linkToDrop, false);
								linkToDrop.id = null;
							}
							this.addToExternalNeighbors(id, link);
							link.id = id;
							//We don't need to send the nesos update message here because it will be (implicitly) sent in the next step of this handler
						}
					}
				}
				
				if(this.external_view.size() > 0) {
					ByteBuffer updateMessage = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId, myOldNesosID, false));
					for(OvernesiaNodeID id: this.external_view.keySet()) {
						this.external_view.get(id).send(new ByteBuffer[]{updateMessage.duplicate()}, this.NesosUpdatePort);
						this.msg_send++;
					}
				}
			}
		} else {
			//Nesos Update Message has flag for Remote Neighboring update.
			
			//Start by checking if the sender is effectively (and currently) an external neighbor, if this is the case use the simple handling strategy
			if(this.external_view.containsKey(update.getSender())) {
				if(update.getSender().getNesosUUID().equals(this.myId.getNesosUUID()))
					this.nesos_view.put(update.getSender(), this.external_view.remove(update.getSender()));
				else
					this.external_view.put(update.getSender(), this.external_view.remove(update.getSender()));
				return; //No need for further operation.
			}
		
			boolean rejectNeighbor = !this.nesos_view.containsKey(update.getSender()) || this.pendingDivisionRequests.size() == 0;
			if(!rejectNeighbor) {
				elected = this.pendingDivisionRequests.first();
				if(elected.getFiliationNesosA().contains(this.myId)) {
					this.myId.setNesosUUID(elected.getNesosA());
					myNewFiliation = elected.getFiliationNesosA();
					complementaryNesosID = elected.getNesosB();
					complementaryFiliation = elected.getFiliationNesosB();
				} else if(elected.getFiliationNesosB().contains(this.myId)) {
					this.myId.setNesosUUID(elected.getNesosB());
					myNewFiliation = elected.getFiliationNesosB();
					complementaryNesosID = elected.getNesosA();
					complementaryFiliation = elected.getFiliationNesosA();
				} else {
					//debug("Elected a Nesos Division proposal where I am not present as a result of a Nesos Update message");
					rejectNeighbor = true;
				}
				
				if(!rejectNeighbor)
					rejectNeighbor = !complementaryFiliation.equals(update.getSender().getNesosUUID());
				
				if(rejectNeighbor) {
					info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, false))}, this.DisconnectRequestPort);
					this.msg_send++;
					this.removeFromNesosView(update.getSender(), info, false);
					this.myId.setNesosUUID(myOldNesosID);
					info.id = null;
				} else {
					this.pendingDivisionRequests.clear();
					
					//Commit the exchange of remote neighbor
					while(this.external_view.size() >= this.external_neighbors) {
						OvernesiaNodeID toDrop = new ArrayList<OvernesiaNodeID>(this.external_view.keySet()).get(rand.nextInt(this.external_view.keySet().size()));
						Connection linkToDrop = this.external_view.get(toDrop);
						linkToDrop.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, false))}, this.DisconnectRequestPort);
						this.msg_send++;
						this.removeFromExternalNeighbors(toDrop, linkToDrop, false);
						linkToDrop.id = null;
					}
					this.addToExternalNeighbors(update.getSender(), this.nesos_view.remove(update.getSender()));
					info.id = update.getSender();
					
					Iterator<OvernesiaNodeID> iterator = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet()).iterator();
					
					ByteBuffer nesosUpdateMessageforNesosNeighbors = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId ,myOldNesosID ,true));
					ByteBuffer disconnectMessage = DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), myOldNesosID, true));
					
					while(iterator.hasNext()) {
						OvernesiaNodeID id = iterator.next();
						Connection link = this.nesos_view.get(id);
						
						if(myNewFiliation.contains(id)) {
							//Update my nesos id with this neighbor and update also my current nesos id for him
							if(!id.equals(update.getSender())) {
								link.send(new ByteBuffer[]{nesosUpdateMessageforNesosNeighbors.duplicate()}, this.NesosUpdatePort);
								this.msg_send++;
							}
							id.setNesosUUID(this.myId.getNesosUUID());
						} else {
							id.setNesosUUID(complementaryNesosID);
							link.send(new ByteBuffer[]{disconnectMessage.duplicate()}, this.DisconnectRequestPort);		
							this.msg_send++;
							this.removeFromNesosView(id, link, false);
							link.id = null;
						} 
					}
					
					ByteBuffer updateMessage = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId, myOldNesosID, false));
					for(OvernesiaNodeID id: this.external_view.keySet()) {
						if(!id.equals(update.getSender())) { //This is to avoid a redundant message.
							this.external_view.get(id).send(new ByteBuffer[]{updateMessage.duplicate()}, this.NesosUpdatePort);
							this.msg_send++;
						}
					}
				}
			}
		}
	}
	
	private void handleExternalNeighboringRequest(ByteBuffer[] msg,	Connection info) {
		ExternalNeighboringRequest request = ExternalNeighboringRequest.readExternalNeighboringRequestFromBuffer(msg);
		//debug("Handling an External Neighboring Request received from " + info.id + " originated by " + request.getOriginalSender());
		
		Connection nextHop = null;
		
		if(request.getExternalTTL() > 0) {
			request.decreaseExternalTTL();
			nextHop = this.selectRandomNextHop((OvernesiaNodeID) info.id, request.getOriginalSender());
		} else if(request.getInternalTTL() > 0) {
			request.decreaseInternalTTL();
			if(!this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) //If random walk terminates in original nesos -> kill it
					&& !this.nesos_view.containsKey(request.getOriginalSender())) { //If originator is in current nesos -> kill random walk
				
				if(this.external_view.size() < this.external_neighbors &&
						!this.existsRemoteConnectionToNesos(request.getOriginalSender().getNesosUUID()) &&
						!this.existsRemoteConnectionToNesoi(request.getKnownNesoi()) &&
						!this.external_view.containsKey(request.getOriginalSender())) {
					
					//We can accept a new external connection with the original sender of the request
					OvernesiaNodeID newExternalNeighbor = request.getOriginalSender();
					this.openConnection(newExternalNeighbor.getSocketAddress(), newExternalNeighbor, Operation.REPLY_EXTERNAL_NEIGHBORING);
				} else {
					if (this.external_view.size() < this.external_neighbors) {
						//Try to select the following hop of the message from an external neighbor 
						//(there are probably connections between these two nesoi)
						nextHop = selectRandomHop((OvernesiaNodeID) info.id, request.getOriginalSender(), request.getKnownNesoi());
					} else {
						//Select the next hop from an element in the current nesos
						nextHop = selectRandomHop((OvernesiaNodeID) info.id);
					}
				}
			}
		} else {
			// attempt to integrate this guy and if it is not possible drop the random walk
			//(can drop an element to make space if remote peer is disconnected from other clusters) 
			if(!this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) &&
				!this.nesos_view.containsKey(request.getOriginalSender()) &&
				!this.external_view.containsKey(request.getOriginalSender()) &&
				!this.existsRemoteConnectionToNesos(request.getOriginalSender().getNesosUUID()) &&
				!this.existsRemoteConnectionToNesoi(request.getKnownNesoi())) {
			
				if(this.external_view.size() < this.external_neighbors || request.isDisconnected()) {
					//Accept this guy in our external view
					OvernesiaNodeID newExternalNeighbor = request.getOriginalSender();
					this.openConnection(newExternalNeighbor.getSocketAddress(), newExternalNeighbor, Operation.REPLY_EXTERNAL_NEIGHBORING);
				}
			}
		}
		
		if(nextHop != null) {
			nextHop.send(new ByteBuffer[]{ExternalNeighboringRequest.writeExternalNeighboringRequestToBuffer(request)},this.ExternalNeighboringRequestPort);
			this.msg_send++;
		} else if(!this.operations.containsKey(request.getOriginalSender().getSocketAddress())) //We simply dropped the random walk.
			this.addToPassiveView(request.getOriginalSender());
		
	}
	
	private void handleExternalNeighboringRequestTemp(ByteBuffer[] msg,	Connection info) {
		ExternalNeighboringRequest request = ExternalNeighboringRequest.readExternalNeighboringRequestFromBuffer(msg);
		//debug("Handling an External Neighboring Request (temp) received from " + request.getOriginalSender());
		
		Connection nextHop = null;
		
		if(request.getExternalTTL() > 0) {
			request.decreaseExternalTTL();
			nextHop = this.selectRandomNextHop((OvernesiaNodeID) info.id, request.getOriginalSender());
		} else if(request.getInternalTTL() > 0) {
			request.decreaseInternalTTL();
			if(!this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) //If random walk terminates in original nesos -> kill it
					&& !this.nesos_view.containsKey(request.getOriginalSender())) { //If originator is in current nesos -> kill random walk
				
				if(this.external_view.size() < this.external_neighbors &&
						!this.existsRemoteConnectionToNesos(request.getOriginalSender().getNesosUUID()) &&
						!this.existsRemoteConnectionToNesoi(request.getKnownNesoi()) &&
						!this.external_view.containsKey(request.getOriginalSender())) {
					
					//We can accept a new external connection with the original sender of the request
					info.id = request.getOriginalSender();
					this.executeReplyToExternalNeighboringRequest(request.getOriginalSender(), info);
					return;
				} else {
					if (this.external_view.size() < this.external_neighbors) {
						//Try to select the following hop of the message from an external neighbor 
						//(there are probably connections between these two nesoi)
						nextHop = selectRandomHop(request.getOriginalSender(), request.getOriginalSender(), request.getKnownNesoi());
					} else {
						//Select the next hop from an element in the current nesos
						nextHop = selectRandomHop((OvernesiaNodeID) info.id);
					}
				}
			}
		} else {
			// attempt to integrate this guy and if it is not possible drop the random walk
			//(can drop an element to make space if remote peer is disconnected from other clusters) 
			if(!this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) &&
				!this.nesos_view.containsKey(request.getOriginalSender()) &&
				!this.external_view.containsKey(request.getOriginalSender()) &&
				!this.existsRemoteConnectionToNesos(request.getOriginalSender().getNesosUUID()) &&
				!this.existsRemoteConnectionToNesoi(request.getKnownNesoi())) {
			
				if(this.external_view.size() < this.external_neighbors || request.isDisconnected()) {
					//Accept this guy in our external view
					info.id = request.getOriginalSender();
					this.executeReplyToExternalNeighboringRequest(request.getOriginalSender(), info);
					return;
				}
			}
		}
		
		if(nextHop != null) {
			nextHop.send(new ByteBuffer[]{ExternalNeighboringRequest.writeExternalNeighboringRequestToBuffer(request)},this.ExternalNeighboringRequestPort);
			this.msg_send++;
		} else if(!this.operations.containsKey(request.getOriginalSender().getSocketAddress())) //We simply dropped the random walk.
			this.addToPassiveView(request.getOriginalSender());
		
		info.close(); //Close this temporary TCP connection
		
	}
	
	private void executeReplyToExternalNeighboringRequest(OvernesiaNodeID originalSender, Connection info) {
		if(this.external_view.containsKey(originalSender)) { //This is to ensure that no double connections are created...
			info.close();
			return;
		}
		
		while(this.external_view.size() >= this.external_neighbors) {
			OvernesiaNodeID toDrop = new ArrayList<OvernesiaNodeID>(this.external_view.keySet()).get(rand.nextInt(this.external_view.keySet().size()));
			this.disconnectAndRemoveFromExternalNeighbors(toDrop, this.external_view.get(toDrop));
		}
		
		if(this.addToExternalNeighbors(originalSender, info)) {
			info.id = originalSender;
			info.send(new ByteBuffer[]{ExternalNeighboringReply.writeExternalNeighboringReplyToBuffer(new ExternalNeighboringReply(this.myId, originalSender.getNesosUUID()))}, this.ExternalNeighboringReplyPort);
			this.msg_send++;
		} else {
			this.addToPassiveView(originalSender);
			info.id = null;
			info.close();
		}		
	}

	private void handleExternalNeighboringReply(ByteBuffer[] msg, Connection info) {
		ExternalNeighboringReply reply = ExternalNeighboringReply.readNExternalNeighboringReplyFromBuffer(msg);
		//debug("Handling External Neighboring Reply from " + reply.getSender());
		
		if(this.external_view.containsKey(reply.getSender())) {
			info.close();
			this.removeFromExternalNeighbors(reply.getSender(), this.external_view.get(reply.getSender()), true);
			System.err.println("Exception: Received External Neighboring Reply from a node already in external view: " + reply.getSender() + ": Trying to enforce consistency.");
			return;
		}
		
		if(this.external_view.size() < this.external_neighbors && this.addToExternalNeighbors(reply.getSender(), info)) {
			info.id = reply.getSender();
			if(!this.myId.getNesosUUID().equals(reply.getRemoteNesosID())) {
				info.send(new ByteBuffer[]{NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId, reply.getRemoteNesosID(), false))}, this.NesosUpdatePort);
				this.msg_send++;
			}
		} else {
			info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), this.myId.getNesosUUID(), false))}, this.DisconnectRequestPort);
			this.msg_send++;
			this.addToPassiveView(reply.getSender());
		}
	}
	
	private void handleNesosDivision(ByteBuffer[] msg, Connection info) {
		NesosDivision proposal = NesosDivision.readNesosDivisionFromBuffer(msg);
		//debug("Handling Nesos Division proposal issued by " + proposal.getSender());
		if(proposal.getSender().getNesosUUID().equals(this.myId.getNesosUUID())) {
			if(this.pendingDivisionRequests.size() == 0) {
				this.net.schedule(new Runnable() {
					public void run() {
						nesosDivisionTimerHandler();
					}
				}, this.timeout/2 + rand.nextInt((int) (this.timeout/2)) ); //TODO: This timeout use (5 seconds) is not the better one
			}
			this.pendingDivisionRequests.add(proposal);
		}
	}
	
	private void handleNesosGossip(ByteBuffer[] msg, Connection info) {
		NesosGossip gossip = NesosGossip.readNesosGossipFromBuffer(msg);
		//debug("Handling a Nesos Gossip message from " + gossip.getOriginator());
		if(!this.myId.getNesosUUID().equals(gossip.getOriginator().getNesosUUID()))
			return;
		
		ArrayList<OvernesiaNodeID> updatedFiliation = gossip.getFiliation();
		
		for(OvernesiaNodeID id: updatedFiliation) {
			if(!this.nesos_view.containsKey(id)) {
				this.openConnection(id.getSocketAddress(), id, Operation.NESOS_NEIGHBORING);
			}
		}
		
		boolean detectedInconsistency = false;
		
		for(OvernesiaNodeID id: this.nesos_view.keySet()) {
			if(!id.equals(gossip.getOriginator()) && !updatedFiliation.contains(id)) {
				updatedFiliation.add(id);
				detectedInconsistency = true;
			}
		}
		
		if(detectedInconsistency) {
			updatedFiliation.add(this.myId);
			updatedFiliation.remove(gossip.getOriginator());
			info.send(new ByteBuffer[]{NesosGossip.writeNesosGossipToBuffer(new NesosGossip(this.myId, updatedFiliation, this.external_view.keySet()))}, this.NesosGossipPort);
			this.msg_send++;
		}
		
		//Management of the external neighboring relations
		if(this.lastReceivedNesosGossip != null) {
			for(OvernesiaNodeID id: new ArrayList<OvernesiaNodeID>(this.external_view.keySet())) {
				if(this.lastReceivedNesosGossip.contains(id.getNesosUUID()) && gossip.getExternalNesoi().contains(id.getNesosUUID()) && this.external_view.size() > 1) {
					Connection link = this.external_view.get(id);
					link.send(new ByteBuffer[]{ExternalNeighboringRequest.writeExternalNeighboringRequestToBuffer(new ExternalNeighboringRequest(this.myId, this.external_view.keySet(), false, this.join_ttl, (short) (this.ns / 2)))},this.ExternalNeighboringRequestPort);
					this.msg_send++;
					link.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), this.myId.getNesosUUID(), false))}, this.DisconnectRequestPort);
					this.msg_send++;
					link.id = null;
					this.external_view.remove(id);
					this.addToPassiveView(id);
				}
			}	
		}
		
		this.lastReceivedNesosGossip = gossip.getExternalNesoi();	
	}

	private void handleExternalGossipRequest(ByteBuffer[] msg) {
		ExternalGossip gossip = ExternalGossip.readExternalGossipFromBuffer(msg);
		//debug("Handling External Gossip Request issued by " + gossip.getOriginator());
	
		ArrayList<OvernesiaNodeID> toSend = new ArrayList<OvernesiaNodeID>(this.shuffle_sample_size);
		ArrayList<OvernesiaNodeID> workingSet = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
		while(toSend.size() < this.shuffle_sample_size / 2 && workingSet.size() > 0)
			toSend.add(workingSet.remove(rand.nextInt(workingSet.size())));
		workingSet.clear();
		workingSet.addAll(this.passive_view);
		workingSet.remove(gossip.getOriginator());
		while(toSend.size() < this.shuffle_sample_size && workingSet.size() > 0)
			toSend.add(workingSet.remove(rand.nextInt(workingSet.size())));
		
		net.udpSend(new ByteBuffer[]{ExternalGossip.writeExternalGossipToBuffer(new ExternalGossip(this.myId, toSend))}, this.ExternalGossipReplyPort, gossip.getOriginator().getSocketAddress());
		this.msg_send++;
		
		this.updatePassiveView(toSend, gossip.getSample(), gossip.getOriginator());
	}

	private void handleExternalGossipReply(ByteBuffer[] msg) {
		ExternalGossip gossip = ExternalGossip.readExternalGossipFromBuffer(msg);
		//debug("Handling External Gossip Reply issued by " + gossip.getOriginator());
		
		if(this.lastExternalGossipDest != null && this.lastExternalGossipDest.equals(gossip.getOriginator())) {
			this.updatePassiveView(this.lastExternalGossipSample, gossip.getSample(), this.lastExternalGossipDest);
			this.lastExternalGossipDest = null;
			this.lastExternalGossipSample.clear();
		} else {
			this.updatePassiveView(gossip.getSample(), gossip.getOriginator());
		}
	}
	
	private void handleReallocateRequest(ByteBuffer[] msg, Connection info) {
		ReallocateRequest request = ReallocateRequest.readReallocateRequestFromBuffer(msg);
		//debug("Handling Reallocate Request sent by " + request.getOriginalSender() + " received through " + info.id);
		
		request.decreaseTTL();
		
		if(this.myId.getNesosUUID() != null && !this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) && !this.nesos_view.containsKey(request.getOriginalSender()) && this.nesosSize() < this.ns_max) {
			OvernesiaNodeID peer = request.getOriginalSender();
			if(peer.equals(info.id)) {
				this.executeReplyToReallocateRequest(peer, info);
			} else {
				this.openConnection(peer.getSocketAddress(), peer, Operation.REPLY_REALLOCATE_REQUEST);
			}
		} else if(request.getTTL() > 0) {
			Connection link = this.selectRandomHop((OvernesiaNodeID) info.id, request.getOriginalSender(), request.getKnownNesoi());
			request.addNesosToKnownNesoiSet(this.myId.getNesosUUID());
			if(link != null) {
				link.send(new ByteBuffer[]{ReallocateRequest.writeReallocateRequestToBuffer(request)}, this.ReallocateRequestPort);
				this.msg_send++;
			}
		}	
	}

	private void handleReallocateReply(ByteBuffer[] msg, Connection info) {
		ReallocateReply reply = ReallocateReply.readReallocateReplyFromBuffer(msg);
		//debug("Handling a Reallocate Reply from " + reply.getSender());
		
		if(this.nesosSize() >= this.ns_min) {
			info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(reply.getSender().getNesosUUID(), this.myId.getNesosUUID(), true))}, this.DisconnectRequestPort);
			this.msg_send++;
			if(this.external_view.containsKey(reply.getSender())) {
				this.removeFromExternalNeighbors(reply.getSender(), info, false);
			}
		} else {
			//Disconnect from my old cluster
			ByteBuffer disconnect = this.nesos_view.size() > 0 ? DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(reply.getSender().getNesosUUID(), this.myId.getNesosUUID(), true)) : null;
			info.id = reply.getSender();
			UUID myPreviousNesos = this.myId.getNesosUUID();
			this.myId.setNesosUUID(reply.getSender().getNesosUUID());
			ByteBuffer nesosUpdate = NesosUpdate.writeNesosUpdateToBuffer(new NesosUpdate(this.myId,myPreviousNesos,false));
			
			if(this.nesos_view.size() > 0) {
				ArrayList<OvernesiaNodeID> oldNesos = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
				for(OvernesiaNodeID id: oldNesos) {
					Connection link = this.nesos_view.get(id);
					link.send(new ByteBuffer[]{disconnect.duplicate()}, this.DisconnectRequestPort);
					this.msg_send++;
					this.removeFromNesosView(id, link, false);
					link.id = null;
				}
			}
			
			this.addToNesosView(reply.getSender(), info);
			for(OvernesiaNodeID ngh: reply.getFiliation()) {
				if(!this.external_view.containsKey(ngh))
					this.openConnection(ngh.getSocketAddress(), ngh, Operation.NESOS_NEIGHBORING);
				else {
					this.nesos_view.put(ngh, this.external_view.remove(ngh));
					this.nesos_view.get(ngh).send(new ByteBuffer[]{nesosUpdate.duplicate()}, this.NesosUpdatePort);
					this.msg_send++;
				}
			}
			
			if(this.external_view.size() > 0) {
				for(Connection extNeigh: this.external_view.values()) {
					extNeigh.send(new ByteBuffer[]{nesosUpdate.duplicate()}, this.NesosUpdatePort);
					this.msg_send++;
				}
			}
		}
	}

	private void handleReallocateRequestTemp(ByteBuffer[] msg, Connection info) {
		ReallocateRequest request = ReallocateRequest.readReallocateRequestFromBuffer(msg);
		//debug("Handling Reallocate Request sent by " + request.getOriginalSender());
		
		request.decreaseTTL();
		
		if(this.myId.getNesosUUID() != null && !this.myId.getNesosUUID().equals(request.getOriginalSender().getNesosUUID()) && !this.nesos_view.containsKey(request.getOriginalSender()) && this.nesosSize() < this.ns_max) {
			//Accept this guy into our cluster
			this.executeReplyToReallocateRequest(request.getOriginalSender(), info);		
			return;
		} else if(request.getTTL() > 0) {
			Connection link = this.selectRandomHop((OvernesiaNodeID) info.id, request.getOriginalSender(), request.getKnownNesoi());
			request.addNesosToKnownNesoiSet(this.myId.getNesosUUID());
			if(link != null) {
				link.send(new ByteBuffer[]{ReallocateRequest.writeReallocateRequestToBuffer(request)}, this.ReallocateRequestPort);
				this.msg_send++;
			}
		}	
		
		info.close(); //Close the temporary TCP connection		
	}
	
	protected void executeReplyToReallocateRequest(OvernesiaNodeID peer, Connection info) {
		//debug("Sending Reallocate Reply to " + peer);
		peer.setNesosUUID(this.myId.getNesosUUID());
		info.id = peer;
		info.send(new ByteBuffer[]{ReallocateReply.writeReallocateReplyToBuffer(new ReallocateReply(this.myId, this.nesos_view.keySet()))}, this.ReallocateReplyPort);
		this.msg_send++;
		this.addToNesosView(peer, info);
	}
	
	private void handleDisconnectRequest(ByteBuffer[] msg, Connection info) {
		//TODO: IS this always true??!!?!?! Check the protocol state machine (someday...)
		if(info.id == null) {
			//We already sent a disconnect request to this so we can close it without further delays
			info.close();
			return;
		}
		
		OvernesiaNodeID peer = (OvernesiaNodeID) info.id;
		//debug("Handling a Disconnect Request from " + peer + " (Socket intel says: " + info.getPeer() + ")");
		
		if(this.nesos_view.containsKey(peer)) {
			this.nesos_view.remove(peer).close();

		} 
		if(this.external_view.containsKey(peer)) {
			this.external_view.remove(peer).close();
		}

		this.addToPassiveView(peer);
	}
	
	/**
	 * Other protocol operations
	 */
	
	/*******************************************************
	 * Internal State Manipulation Methods
	 *******************************************************/
	
	/**
	 * This method adds the peer (and associated connection info) to the nesos neighbors set.
	 * This peer information may be removed form other sets due to this operation.
	 */
	private boolean addToNesosView(OvernesiaNodeID peer, Connection info) {
		if(info == null) {
			try {
				throw new Exception("Trying to insert a null value in the connection of a nesos neighbor.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		if(peer == null) {
			try {
				throw new Exception("Trying to insert a null value in the peer of a nesos neighbor.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		if(this.myId.equals(peer))
			return false; //Our identifier should never be in this set...
		
		if(this.passive_view.contains(peer))
			this.passive_view.remove(peer);
		
		if(this.external_view.containsKey(peer))
			this.external_view.remove(peer);
		
		if(!this.nesos_view.containsKey(peer)) {
			this.nesos_view.put(peer, info);
		} else {
			this.nesos_view.remove(peer); //To eventually update the nesos id of the peer instance
			this.nesos_view.put(peer, info);
			info.id = peer;
		}
		
		return true;
	}
	
	/**
	 * This method tries to add the peer (and associated connection info) to the external neighbors set.
	 * This peer information may be removed from the passive set due to this operation.
	 */
	private boolean addToExternalNeighbors(OvernesiaNodeID peer, Connection info) {
		if(info == null) {
			try {
				throw new Exception("Trying to insert a null value in the connection of an external neighbor.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		if(peer == null) {
			try {
				throw new Exception("Trying to insert a null value in the peer of an external neighbor.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		if(!this.myId.equals(peer) && !this.nesos_view.containsKey(peer)) {
			if(this.passive_view.contains(peer))
				this.passive_view.remove(peer);
			
			if(!this.external_view.containsKey(peer)) {
				this.external_view.put(peer, info);
				return true;
			} else {
				this.external_view.put(peer, this.external_view.remove(peer)); //Update peer instance without changing connection instance
				return false;
			}		
		} else 
			return false;
	}
	
	/**
	 * This methods tries to add the node identifier id to the passive view.
	 */
	private boolean addToPassiveView(OvernesiaNodeID id) {
		if(id == null) {
			try {
				throw new Exception("Trying to insert a null value in the peer of a passive neighbor.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		if(!this.myId.equals(id) && !this.nesos_view.containsKey(id) && !this.external_view.containsKey(id) && !this.passive_view.contains(id)) {
			if(this.passive_view.size() == this.passive_view_size) 
				this.passive_view.remove(rand.nextInt(this.passive_view_size));
			this.passive_view.add(id);
			return true;
		} else 
			return false;
	}

	/**
	 * This method adds the peer (and associated connection info) to the nesos neighbors set.
	 * This peer information may be removed form other sets due to this operation.
	 */
	private boolean removeFromNesosView(OvernesiaNodeID peer, Connection info, boolean toClose) {
		if(this.nesos_view.containsKey(peer)) {
			this.nesos_view.remove(peer);
			if(info.isActive()) {
				this.addToPassiveView(peer);
			}
			if(toClose)
				info.close();
			return true;
		} else
			return false;
	}
	
	private boolean disconnectAndRemoveFromExternalNeighbors(OvernesiaNodeID peer, Connection info) {
		if(this.external_view.containsKey(peer)) {
			this.external_view.remove(peer);
			info.send(new ByteBuffer[]{DisconnectRequest.writeDisconnectRequestToBuffer(new DisconnectRequest(this.myId.getNesosUUID(), this.myId.getNesosUUID(), false))}, this.DisconnectRequestPort);
			this.msg_send++;
			info.id = null;
			this.addToPassiveView(peer);
			return true;
		} else
			return false;
	}
	
	/**
	 * This method tries to add the peer (and associated connection info) to the external neighbors set.
	 * This peer information may be removed from the passive set due to this operation.
	 */
	private boolean removeFromExternalNeighbors(OvernesiaNodeID peer, Connection info, boolean toClose) {
		if(this.external_view.containsKey(peer)) {
			this.external_view.remove(peer);
			info.id = null;
			if(info.isActive()) {
				this.addToPassiveView(peer);
			}
			if(toClose)
				info.close();
			return true;
		} else
			return false;
	}
	
	/**
	 * This methods tries to add the node identifier id to the passive view.
	 */
	private boolean removeFromPassiveView(OvernesiaNodeID id) {
		if(this.passive_view.contains(id)) {
			this.passive_view.remove();
			return true;
		} else
			return false;
	}
	
	private boolean removeFromPassiveView(InetSocketAddress addr) {
		OvernesiaNodeID toRemove = null;
		Iterator<OvernesiaNodeID> i = this.passive_view.iterator();
		while(toRemove == null && i.hasNext()) {
			OvernesiaNodeID c = i.next();
			if(c.getSocketAddress().equals(addr))
				toRemove = c;
		}
		
		if(toRemove != null)
			return this.removeFromPassiveView(toRemove);
		else
			return false;
	}
	
	private void updatePassiveView(ArrayList<OvernesiaNodeID> peerSample, OvernesiaNodeID peer) {
		peerSample.add(peer);
		ArrayList<OvernesiaNodeID> workingSet = new ArrayList<OvernesiaNodeID>(peerSample);
		for(OvernesiaNodeID id: workingSet) {
			if(this.myId.equals(id) || this.nesos_view.containsKey(id) || this.external_view.containsKey(id) || this.passive_view.contains(id))
				peerSample.remove(id);
		}
		
		for(OvernesiaNodeID id: peerSample) {
			if(this.passive_view.size() == this.passive_view_size)
				this.passive_view.remove(rand.nextInt(this.passive_view_size));
			this.passive_view.add(id);
		}
	}
	
	private void updatePassiveView(ArrayList<OvernesiaNodeID> mySample, ArrayList<OvernesiaNodeID> peerSample, OvernesiaNodeID peer) {
		peerSample.add(peer);
		ArrayList<OvernesiaNodeID> workingSet = new ArrayList<OvernesiaNodeID>(peerSample);
		for(OvernesiaNodeID id: workingSet) {
			if(this.myId.equals(id) || this.nesos_view.containsKey(id) || this.external_view.containsKey(id) || this.passive_view.contains(id))
				peerSample.remove(id);
			if(mySample.contains(id)) {
				peerSample.remove(id);
				mySample.remove(id);
			}
		}
		workingSet.clear();
		workingSet.addAll(mySample);
		for(OvernesiaNodeID id: workingSet)
			if(!this.passive_view.contains(id))
				mySample.remove(id);
		
		for(OvernesiaNodeID id: peerSample) {
			if(this.passive_view.size() == this.passive_view_size)
				if(mySample.size() > 0)
					this.passive_view.remove(mySample.remove(rand.nextInt(mySample.size())));
				else
					this.passive_view.remove(rand.nextInt(this.passive_view_size));
			this.passive_view.add(id);
		}
	}
	
	/**
	 * This method returns the Connection instance of a random neighbor giving preference to external neighbors
	 */
	private Connection selectRandomNextHop() {
		OvernesiaNodeID[] list = null;
		if(this.external_view.size() > 0) {
			list = this.external_view.keySet().toArray(new OvernesiaNodeID[this.external_view.keySet().size()]);	
			return this.external_view.get(list[rand.nextInt(list.length)]);
		} else if(nesos_view.size() > 0){
			list = this.nesos_view.keySet().toArray(new OvernesiaNodeID[this.nesos_view.keySet().size()]);
			return this.nesos_view.get(list[rand.nextInt(list.length)]);
		}	
		return null;
	}
	
	/**
	 * This method returns the Connection instance of a random neighbor giving preference to external neighbors.
	 * Also it will never return the connection to the exception node id or to an external neighbor in a nesos
	 * that is already referenced in the visitedNesoi set.
	 */
	private Connection selectRandomNextHop(OvernesiaNodeID exception, Collection<UUID> visitedNesoi) {
		Connection toReturn = null;
		if(this.external_view.size() > 0) {
			ArrayList<OvernesiaNodeID> temp = new ArrayList<OvernesiaNodeID>(this.external_view.keySet());
			if(exception != null) temp.remove(exception);
			for(int i = 0; i < temp.size(); i++)
				if(visitedNesoi.contains(temp.get(i).getNesosUUID())) {
					temp.remove(i);
					i--;
				}
			
			if(temp.size() > 0)
				toReturn = this.external_view.get(temp.get(rand.nextInt(temp.size())));
		}
		
		if(toReturn == null && this.nesos_view.keySet().size() > 0) {
			ArrayList<OvernesiaNodeID> temp = new ArrayList<OvernesiaNodeID>(this.nesos_view.keySet());
			if(exception != null) temp.remove(exception);
			
			if(temp.size() > 0)
				toReturn = this.nesos_view.get(temp.get(rand.nextInt(temp.size())));
		}
		
		return toReturn;
	}
	
	private Connection selectRandomNextHop(OvernesiaNodeID previousHop, OvernesiaNodeID originalSender) {
		ArrayList<OvernesiaNodeID> candidates = new ArrayList<OvernesiaNodeID>();
		
		for(OvernesiaNodeID id: this.external_view.keySet()) {
			if(!id.getNesosUUID().equals(originalSender.getNesosUUID()) && !id.equals(previousHop) && !id.equals(originalSender))
				candidates.add(id);
		}
		
		if(candidates.size() > 0)
			return this.external_view.get(candidates.get(rand.nextInt(candidates.size())));
		
		for(OvernesiaNodeID id: this.nesos_view.keySet()) {
			if(!id.equals(originalSender) && !id.equals(previousHop))
				candidates.add(id);
		}
		
		if(candidates.size() > 0)
			return this.nesos_view.get(candidates.get(rand.nextInt(candidates.size())));
		
		return null;
	}
	
	private Connection selectRandomHop(OvernesiaNodeID previousHop) {
		ArrayList<OvernesiaNodeID> candidates = new ArrayList<OvernesiaNodeID>();
		
		for(OvernesiaNodeID id: this.external_view.keySet()) {
			if(!id.equals(previousHop))
				candidates.add(id);
		}
		
		if(candidates.size() > 0)
			return this.external_view.get(candidates.get(rand.nextInt(candidates.size())));
		
		return null;
	}

	private Connection selectRandomHop(OvernesiaNodeID previousHop, OvernesiaNodeID originalSender, ArrayList<UUID> exceptionNesoi) {
		ArrayList<OvernesiaNodeID> candidates = new ArrayList<OvernesiaNodeID>();
		
		for(OvernesiaNodeID id: this.external_view.keySet()) {
			if(!id.getNesosUUID().equals(originalSender.getNesosUUID()) && !id.equals(previousHop) && !id.equals(originalSender) && !exceptionNesoi.contains(id.getNesosUUID()))
				candidates.add(id);
		}
		
		if(candidates.size() > 0)
			return this.external_view.get(candidates.get(rand.nextInt(candidates.size())));
		
		for(OvernesiaNodeID id: this.nesos_view.keySet()) {
			if(!id.equals(originalSender) && !id.equals(previousHop))
				candidates.add(id);
		}
		
		if(candidates.size() > 0)
			return this.nesos_view.get(candidates.get(rand.nextInt(candidates.size())));
		
		return null;
	}
	
	/*******************************************************
	 * Auxiliary state verification methods
	 *******************************************************/
	
	private boolean existsRemoteConnectionToNesoi(ArrayList<UUID> knownNesoi) {
		boolean found = false;
		
		ArrayList<OvernesiaNodeID> external_list = new ArrayList<OvernesiaNodeID>(this.external_view.keySet());
		
		for(int i = 0; !found && i < knownNesoi.size(); i++) {
			for(int j = 0; !found && j < external_list.size(); j++)
				found = knownNesoi.get(i).equals(external_list.get(j).getNesosUUID());
		}
		
		return found;
	}

	private boolean existsRemoteConnectionToNesos(UUID nesosUUID) {
		boolean found = false;
		
		ArrayList<OvernesiaNodeID> external_list = new ArrayList<OvernesiaNodeID>(this.external_view.keySet());
		
		for(int j = 0; !found && j < external_list.size(); j++)
			found = nesosUUID.equals(external_list.get(j).getNesosUUID());
		
		return found;
	}
	
	private boolean isNesosCoordinator() {
		boolean found = false;
		
		Iterator<OvernesiaNodeID> iterator = this.nesos_view.keySet().iterator();
		
		while(!found && iterator.hasNext())
			found = iterator.next().getUUID().compareTo(this.myId.getUUID()) < 0;
		
		return found;
	}
	
	private int nesosSize() {
		return this.nesos_view.size() + 1;
	}
	/*******************************************************
	 * Methods from the interface: ConnectionListener
	 *******************************************************/
	
	public synchronized void failed(InetSocketAddress info) {
		System.err.println("Exception: Failed opening a connection to " + info);
		if(this.operations.containsKey(info)) {
			System.err.println("Operation failed: " + this.operations.remove(info));
		}
	}
	
	/**
	 * 
	 * @param info
	 */
	
	public synchronized void close(Connection info) {
		if(info == null) {
			//debug("UDP local socket error...");
			if(!net.isUDPActive())
				net.deactivateUDP();
			return;
		}
		
		//debug("Connection closed: " + info.getPeer() + " <" + info.id + ">");
		
		if(this.operations.containsKey(info.getPeer())){
			Operation pending = this.operations.remove(info.getPeer());
			switch (pending.getOperationId()) {
				case Operation.JOIN:
					//Try again.
					this.init(info.getPeer());
					break;
				default:
					//No Operation
					break;
			}
			this.removeFromPassiveView(info.getPeer());
		}
		
		if(info.id != null) {
			OvernesiaNodeID id = (OvernesiaNodeID) info.id;
			if(this.nesos_view.containsKey(id))
				this.removeFromNesosView(id, info, false);
			if(this.external_view.containsKey(id))
				this.removeFromExternalNeighbors(id, info, false);
			if(this.passive_view.contains(id))
				this.removeFromPassiveView(id);
		}
	}

	/**
	 *
	 * @param info
	 */
	
	public synchronized void open(Connection info) {
		//Verify if we were waiting for this connection to execute some operation
		//debug("New connection available: " + info.getPeer());
		Operation pending = this.operations.remove(info.getPeer());
		if(pending != null) {
			switch (pending.getOperationId()) {
				case Operation.JOIN:
					//debug("Executing JOIN");
					this.executeJoin(info.getPeer(), info);
					break;
				case Operation.ACCEPTJOIN:
					//debug("Executing ACCEPT JOIN");
					info.id = pending.getAdditionalInformation();
					this.executeAcceptJoin((OvernesiaNodeID) info.id, info);
					break;
				case Operation.NESOS_NEIGHBORING:
					//debug("Executing NESOS NEIGHBORING");
					info.id = pending.getAdditionalInformation();
					this.executeNesosNeighboringRequest((OvernesiaNodeID) info.id, info);
					break;
				case Operation.SEND_EXTERNAL_NEIGHBORING:
					//debug("Executing SEND EXTERNAL NEIGHBORING");
					this.executeSendExternalNeighboringRequest(info);
					break;
				case Operation.REPLY_EXTERNAL_NEIGHBORING:
					//debug("Executing a REPLY TO EXTERNAL NEIGHBORING REQUEST");
					info.id = pending.getAdditionalInformation();
					this.executeReplyToExternalNeighboringRequest((OvernesiaNodeID) info.id, info);
					break;
				case Operation.SEND_REALLOCATE_REQUEST:
					//debug("Executing SEND REALLOCATE REQUEST");
					this.executeSendReallocateRequest(info);
					break;
				case Operation.REPLY_REALLOCATE_REQUEST:
					//debug("Executing a REPLY TO REALLOCATE REQUEST");
					info.id = pending.getAdditionalInformation();
					this.executeReplyToReallocateRequest((OvernesiaNodeID) info.id, info);
					break;
				default:
					//debug("No Operation Pending");
					break;
			} 
		}
	}

	/**
	 * Methods from the interface: DataListener
	 */
	
	/**
	 * 
	 * @param msg
	 * @param info
	 * @param port
	 */
	
	public synchronized void receive(ByteBuffer[] msg, Connection info, short port) {
		this.msg_rcv++;
		//debug("Message received in port " + port);
		if(this.JoinRequestPort == port)
			this.handleJoinRequest(msg, info);
		else if(this.ForwardJoinRequestPort == port)
			this.handleForwardJoinRequest(msg, info);
		else if(this.JoinReplyPort == port) 
			this.handleJoinReply(msg, info);
		else if(this.NesosNeighboringRequestPort == port)
			this.handleNesosNeighboringRequest(msg, info);
		else if(this.NesosUpdatePort == port)
			this.handleNesosUpdate(msg, info);
		else if(this.ExternalNeighboringRequestPort == port)
			this.handleExternalNeighboringRequest(msg, info);
		else if(this.ExternalNeighboringReplyPort == port)
			this.handleExternalNeighboringReply(msg, info);
		else if(this.ExternalNeighboringRequestTempPort == port)
			this.handleExternalNeighboringRequestTemp(msg, info);
		else if(this.NesosDivisionPort == port)
			this.handleNesosDivision(msg, info);
		else if(this.NesosGossipPort == port)
			this.handleNesosGossip(msg, info);
		else if(this.ExternalGossipRequestPort == port) 
			this.handleExternalGossipRequest(msg); //No conn info cause it is UDP
		else if(this.ExternalGossipReplyPort == port)
			this.handleExternalGossipReply(msg); //No conn info cause it is UDP
		else if(this.ReallocateRequestPort == port)
			this.handleReallocateRequest(msg, info);
		else if(this.ReallocateReplyPort == port)
			this.handleReallocateReply(msg, info);
		else if(this.ReallocateRequestTempPort == port)
			this.handleReallocateRequestTemp(msg, info);
		else if(this.DisconnectRequestPort == port) 
			this.handleDisconnectRequest(msg, info);
		else {
			debug("Unknown port: " + port);
			this.msg_rcv--;
		}
	}



	/**********************************************************
	 * Methods for interaction with the P2PChannel Class
	 **********************************************************/
	
	public Connection[] connections() {
		Connection[] ret = new Connection[this.external_view.keySet().size() + this.nesos_view.keySet().size()];
		int i = 0;
		for(Connection c: this.external_view.values()) {
			ret[i] = c;
			i++;
		}
		
		for(Connection c: this.nesos_view.values()) {
			ret[i] = c;
			i++;
		}
		
		return ret;
 	}

	public OvernesiaNodeID getId() {
		return this.myId;
	}

	/*************************************************************
	 * Methods for interaction with the Protocol Management Class
	 *************************************************************/
	
	public void resetCounters() {
		return; //What should this do?!?!
	}

	/*************************************************************
	 * Methods for interaction with the DisseminationStrategy (//debug)
	 *************************************************************/
	
	public synchronized int getNesosSize() {
		return this.nesosSize();
	}

	public synchronized int getExternalNeighborsSize() {
		return this.external_view.keySet().size();
	}
	
	public synchronized int getPassiveViewSize() {
		return this.passive_view.size();
	}

	public synchronized Set<OvernesiaNodeID> getNesosFiliation() {
		return new HashSet<OvernesiaNodeID>(this.nesos_view.keySet());
	}

	public synchronized Set<OvernesiaNodeID> getExternalIDs() {
		return new HashSet<OvernesiaNodeID>(this.external_view.keySet());
	}

	public synchronized Collection<OvernesiaNodeID> getPassiveView() {
		return new ArrayList<OvernesiaNodeID>(this.passive_view);
	}
	
	public Transport network() {
		return this.net;
	}

	@SuppressWarnings("unchecked")
	public synchronized HashMap<OvernesiaNodeID, Connection> getOvernesiaExternalView() {
		return (HashMap<OvernesiaNodeID, Connection>) this.external_view.clone();
	}

	@SuppressWarnings("unchecked")
	public synchronized HashMap<OvernesiaNodeID, Connection> getOvernesiaNesosView() {
		return (HashMap<OvernesiaNodeID, Connection>) this.nesos_view.clone();
	}

	public synchronized String pendingConns() {
		String ret = this.operations.size() + " ";
		for(InetSocketAddress s: this.operations.keySet())
			ret = ret + s + "::" + this.operations.get(s);
		return ret;
	}

	public void flushStatistics(PrintStream printStream, long label) {
		printStream.println("STATS " + DateTimeRepresentation.timeStamp() + " " + label + " " + msg_rcv + " " + msg_send);
		msg_rcv = msg_send = 0;
	}
	
	private int msg_rcv;
	private int msg_send;
	
	public HashMap<NodeID, Connection> getNeighbors() {
		return null;
	}

	public NodeID getNodeIdentifier() {
		return null;
	}
	
	public synchronized void leave() {
		//Nothing to do
	}
	
	public void setOverlayAware(OverlayAware proto) {
		//Nothing to do (for now)
	}
}

/*
 * NeEM - Network-friendly Epidemic Multicast
 * Copyright (c) 2005-2006, University of Minho
 * All rights reserved.
 *
 * Contributors:
 *  - Pedro Santos <psantos@gmail.com>
 *  - Jose Orlando Pereira <jop@di.uminho.pt>
 *
 * Partially funded by FCT, project P-SON (POSC/EIA/60941/2004).
 * See http://pson.lsd.di.uminho.pt/ for more information.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  - Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 * 
 *  - Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 * 
 *  - Neither the name of the University of Minho nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package gsd.impl;

import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Implementation of the network layer.
 */
public class Transport extends Config implements Runnable {
  	
	private Connection idinfo;

	public Transport(Random rand, InetSocketAddress local) throws IOException, BindException {
		this.rand=rand;
		
		timers = new TreeMap<Long, Runnable>();
        handlers = new HashMap<Short, DataListener>();
        discardable = new ArrayList<Short>();
        selector = SelectorProvider.provider().openSelector();
        connections = new HashSet<Connection>();
        idinfo = new Connection(this, local, false);
        connections.add(idinfo); 
        
        id = new InetSocketAddress(local.getAddress(), idinfo.getLocal().getPort());
    }
    
    /**
     * Get local id.
     */
    public InetSocketAddress id() {
        return id;
    }
  
	/**
     * Get all local addresses.
     */
    public InetSocketAddress[] getLocals() {
    	List<InetSocketAddress> addrs=new ArrayList<InetSocketAddress>();
    	for(Connection info: connections) {
    		InetSocketAddress addr=info.getLocal();
    		if (addr!=null)
    			addrs.add(addr);
    	}
    	return addrs.toArray(new InetSocketAddress[addrs.size()]);
    }
  
    /**
     * Get all connections.
     */
    public Connection[] connections() {
        return (Connection[]) connections.toArray(
                new Connection[connections.size()]);
    }

	/**
     * Get addresses of all connected peers.
     */
    public InetSocketAddress[] getPeers() {
    	List<InetSocketAddress> addrs=new ArrayList<InetSocketAddress>();
    	for(Connection info: connections) {
    		InetSocketAddress addr=info.getPeer();
    		if (addr!=null)
    			addrs.add(addr);
    	}
    	return addrs.toArray(new InetSocketAddress[addrs.size()]);
    }

    /**
     * Call periodically to garbage collect idle connections.
     */
    public synchronized void gc() {
        Iterator<Connection> i = connections.iterator();

        while (i.hasNext()) {
            Connection info = i.next();

            info.handleGC();
        }
    }
  
    /**
     * Close all socket connections and release polling thread.
     */
    public synchronized void close() {
        if (closed)
            return;
        closed=true;
        timers.clear();
        chandler=null;
        selector.wakeup();
        for(Connection info: connections){
        	System.err.println(Thread.currentThread().getStackTrace());
            info.handleClose();
            }
        idinfo.handleClose();
    }

    /**
     * Queue processing task.
     */
    public synchronized void queue(Runnable task) {
        schedule(task, 0);
    }

    /**
     * Schedule processing task.
     * @param delay delay before execution
     */
    public synchronized void schedule(Runnable task, long delay) {
        Long key = new Long(System.nanoTime() + delay*1000000);

        while(timers.containsKey(key))
        	key=key+1;
        timers.put(key, task);
        if (key == timers.firstKey()) {
            selector.wakeup();
        }
    }

    /**
     * Initiate connection to peer. This is effective only after the open callback.
     * JLeitao - 22-01-2010: I have added the close call to the Exception Handler... my hopes is that this allows me to
     * not be forever waiting for the creation of a connection. This however introduces the possibility of receiving a
     * close call back with a null Connection info.
     */
    public void add(InetSocketAddress addr) {
    	try {    
    	   Connection info = null;
           if(Transport.modelnet)
        	   info = new Connection(this, true, this.id.getAddress());
           else
        	   info = new Connection(this, null, true);
           info.connect(addr); 
        } catch (IOException e) {
        	e.printStackTrace();
        	this.chandler.failed(addr);
        }
    }

    /**
     * Add a reference to a message handler.
     */
    public void setDataListener(DataListener handler, short port) {
        this.handlers.put(new Short(port), handler);
    }
    
    /**
     * Add an indication that this type of message can be discarded from send buffer without being sent.
     */
    public void setDiscardableData(short port) {
    	if(!this.discardable.contains(port))
    		this.discardable.add(port);
    }
        
    /**
     * Sets the reference to the connection handler.
     */
    public void setConnectionListener(ConnectionListener handler) {
        this.chandler = handler;
    }

    /**
     * Main loop.
     */
    public void run() {
    	this.schedule(new Runnable() { public void run() { cleanupQueues(); } }, 10 * 1000);
    	
        while (true) {
            try {
                // Execute pending tasks.
                Runnable task = null;
                long delay = 0;

                synchronized (this) {
                    if (!timers.isEmpty()) {
                        long now = System.nanoTime();
                        Long key = timers.firstKey();

                        if (key <= now) {
                            task = timers.remove(key);
                        } else {
                            delay = key - now;
                        }
                    }
                }
            
                if (task != null) {
                    task.run();
                } else {    
                	if (delay>0 && delay<1000000)
                		delay=1;
                	else
                		delay/=1000000;

                	selector.select(delay);
                    if (closed)
                        break;
                            
                    // Execute pending event-handlers.
                    boolean canRead = this.canRead();
                    
                    for (Iterator<SelectionKey> j = selector.selectedKeys().iterator(); j.hasNext();) {
                        SelectionKey key = j.next();
                        GenericConnection info = (GenericConnection) key.attachment();

                        if (!key.isValid()) {
                        	System.err.println(Thread.currentThread().getStackTrace());
                            info.handleClose();
                            continue;
                        }
                        
                        if (key.isReadable() && (canRead || this.isConnectionBlocked(info) /*|| this.rand.nextFloat() < 0.01 */)) {
                        	info.handleRead();                            
                        } else if (key.isAcceptable()) {
                        	info.handleAccept();                            
                        } else if (key.isConnectable()) {
                        	info.handleConnect();
                        } else if (key.isWritable()) {
                        	info.handleWrite();
                        }
                    }
                    
                    synchronized (this) {
            			if(!this.areQueuesFilled()) {
            				this.notify();
            			} 
            		}
                }
                        
            } catch (IOException e) {
                System.err.println("Error in the Transport main thread: " + e.getClass().getName());
                e.printStackTrace(System.err);
            } catch (CancelledKeyException cke) {
            	System.err.println("Error in the Transport main thread: " + cke.getClass().getName());
                cke.printStackTrace(System.err);
            } catch (Exception e) {
            	System.err.println("Error in the Transport main thread: " + e.getClass().getName());
                e.printStackTrace(System.err);
            }
        }
    }

    synchronized void cleanupQueues() {
    	for(Connection c: this.connections) {
			Queue q = c.queue;
			if(q != null)
				q.discardOld(maxMessagesInAQueue);
		}
    	this.schedule(new Runnable() { public void run() { cleanupQueues(); } }, 55000);
    }
    
	void deliverSocket(SocketChannel sock) throws IOException, SocketException, ClosedChannelException {
		final Connection info = new Connection(this, sock);

		synchronized(this) {
			this.connections.add(info); // adiciona nova connection as connections conhecidas
		}
		queue(new Runnable() {
		    public void run() {
		    	chandler.open(info);
		    }
		});
		this.accepted++;
	}

	
	void notifyOpen(final Connection info) {
        synchronized(this) {
        	connections.add(info);
        }
        queue(new Runnable() {
			public void run() {
				if (chandler != null)
					chandler.open(info);
			}
		});
	}
    
	void notifyClose(final Connection info) {
		synchronized(this) {
			connections.remove(info);
		}
			
		queue(new Runnable() {
			public void run() {
				if (chandler!=null)
					chandler.close(info);
			}
		});
	}
	
	void notifyFail(final InetSocketAddress peer) {
		queue(new Runnable() {
			public void run() {
				if (chandler!=null)
					chandler.failed(peer);
			}
		});
	}

	void deliver(final Connection source, final Short prt, final ByteBuffer[] msg) {
		final DataListener handler = handlers.get(prt);
		if (handler==null) {
			// unknown handler
			return;
		}
		queue(new Runnable() {
			public void run() {
				try {
					handler.receive(msg, source, prt);
				} catch(BufferUnderflowException e) {
					source.close();
				}
			}
		});
	}

    /**
	 * Local id for each instance
	 */
    private InetSocketAddress id;
    
    /**
     * Selector for events
     */
    Selector selector;

    /** Storage for open connections to other group members
     * This variable can be queried by an external thread for JMX
     * management. Therefore, all sections of the code that modify it must
     * be synchronized. Sections that read it from the protocol thread need
     * not be synchronized.
     */
    private Set<Connection> connections;

    /** 
     * Queue for tasks
     */
    private SortedMap<Long, Runnable> timers;

    /** 
     * Storage for DataListener protocol events handlers
     */
    private Map<Short, DataListener> handlers;

    /**
     * Information about which messages types can be discarded from send buffers
     */
    private ArrayList<Short> discardable;
    
    public boolean canDiscard(short port) {
    	return this.discardable.contains(port);
    }
    
    /** 
     * Reference for ConnectionListener events handler
     */
    private ConnectionListener chandler; // bing!

    /**
     * If we're not responding any more
     */
    private boolean closed;
        
    Random rand;
    
    /**
     * For having a UDP socket
     */
    DatagramConnection udpSocket;
    
    public void activateUDP() {
    	if(udpSocket == null) {
    		try {
				udpSocket = new DatagramConnection(this, this.id);
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
    
    public void deactivateUDP() {
    	if(udpSocket != null) {
    		udpSocket.close();
    		udpSocket = null;
    	}
    }
    
    public boolean isUDPActive() {
		return this.udpSocket != null && this.udpSocket.isActive();
	}
    
    public void udpSend(ByteBuffer[] msg, short port, InetSocketAddress dest) {
    	if(udpSocket != null)
    		udpSocket.send(msg, port, dest);
    	else 
    		System.err.println("Cannot send UPD Message (port " + port + ") to " + dest +": UDP comm not available");
    }
    
    // Configuration parameters
    
    /**
     * Execution queue size
     */
    private int queueSize = 10;
    private int bufferSize = 4000; //1024

    public int getQueueSize() {
		return queueSize;
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	// Statistics
	
    public int accepted, connected;
    public int pktOut, pktIn;
    public int bytesOut, bytesIn;

    public void resetCounters() {
        accepted=connected=pktOut=pktIn=bytesOut=bytesIn=0;
	}

    public static final int maxMessagesInAQueue = 15;
    
	public synchronized boolean areQueuesFilled() {
		for(Connection c: this.connections)
			if(c.id != null && c.queueSize() > maxMessagesInAQueue)
				return true;
		return false;
	}
	
	public synchronized boolean canRead() {
		for(Connection c: this.connections) 
			if(c.id != null && c.queueSize() > maxMessagesInAQueue)
				return false;
		return true;
	}
    
	public synchronized boolean isConnectionBlocked(GenericConnection info) {
		return info instanceof DatagramConnection || info.queueSize() > maxMessagesInAQueue;
	}

	public synchronized void purgeOverloadedConnection() {
		Connection target = null;
		int queueSize = -2;
		
		for(Connection c: this.connections()) {
			if(c.queueSize() > queueSize) {
				target = c;
				queueSize = c.queueSize();
			}		
		}
		
		if(target != null)
			target.close();
		
		System.gc();
	}
}


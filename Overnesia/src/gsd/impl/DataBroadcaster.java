package gsd.impl;

import java.nio.ByteBuffer;

public interface DataBroadcaster {

	public void receive(ByteBuffer key, ByteBuffer[] payload);
	
	public ByteBuffer[] retrievePayload(ByteBuffer key);
	
}

package gsd.impl;

import gsd.common.NodeID;

import java.net.InetSocketAddress;
import java.util.HashMap;

public interface OverlayNetwork {

	/**
	 * Initialization Method for the Overlay Network. 
	 * This will open a connection to the contact node and then send a join request.
	 */
	
	public void init(final InetSocketAddress contact);
	
	/**
	 * Initialization Method for the Overlay Network. 
	 * This is mandatory for the first node in the overlay.
	 */
	public void init();

	/**********************************************************
	 * Methods for interaction with the P2PChannel Class
	 **********************************************************/
	
	public Connection[] connections();
	
	public NodeID getNodeIdentifier();
	
	public Transport network();
	
	/*************************************************************
	 * Methods for interaction with the Protocol Management Class
	 *************************************************************/
	
	public void resetCounters(); 
	
	/********************************************************************
	 * Methods for exposing the views that can be used for communication
	 ********************************************************************/
	
	public HashMap<NodeID,Connection> getNeighbors();
	
	/********************************************************************
	 * This method is invoked before terminating the process...
	 ********************************************************************/
	
	public void leave();
	
	/********************************************************************
	 * This method is used by a OverlayAware protocol to register
	 * its interested in being notified of modifications to the overlay
	 * membership.
	 ********************************************************************/
	
	public void setOverlayAware(OverlayAware proto);
}

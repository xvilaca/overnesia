package gsd.impl.queryDissemination.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class InternalRandomWalk {

	private ArrayList<OvernesiaNodeID> visited;
	private short ttl;
	private short missing_transmissions;
	
	public InternalRandomWalk(OvernesiaNodeID peer, short ttl, short missing) {
		this.visited = new ArrayList<OvernesiaNodeID>();
		this.visited.add(peer);
		this.ttl = ttl;
		this.missing_transmissions = missing;
	}
	
	public InternalRandomWalk(ArrayList<OvernesiaNodeID> visited, short ttl, short missing) {
		this.visited = visited;
		this.ttl = ttl;
		this.missing_transmissions = missing;
	}
	
	public short decreaseTT() {
		this.ttl--;
		return ttl;
	}
	
	public ArrayList<OvernesiaNodeID> getVisited() {
		return this.visited;
	}
	
	public short getMissing() {
		return this.missing_transmissions;
	}
	
	public void decreaseMissingTransmissions() {
		this.missing_transmissions--;
	}
	
	static public ByteBuffer writeInternalRandomWalkToBuffer(InternalRandomWalk request) {
		ByteBuffer msg = ByteBuffer.allocate(2 + 2 + 2 + (38 * request.visited.size()));
		
		msg.putShort(request.ttl);
		msg.putShort(request.missing_transmissions);
		msg.putShort((short) request.visited.size());
		
		for(OvernesiaNodeID id: request.visited) {
			OvernesiaNodeID.writeNodeIdToBuffer(msg, id);
		}
		
		msg.flip();
		return msg;
	}
	
	public static InternalRandomWalk readInternalRandomWalkFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 2 + 2 + 2);
		
		short ttl = buf.getShort();
		short missing = buf.getShort();
		
		short size = buf.getShort();
		
		buf = Buffers.sliceCompact(msg, size * 38);
		ArrayList<OvernesiaNodeID> visited = new ArrayList<OvernesiaNodeID>(size);
		
		for(int i = 0; i < size; i++)
			visited.add(OvernesiaNodeID.readNodeIDFromBuffer(buf));
		
		return new InternalRandomWalk(visited, ttl, missing);
	}

	public void addVisited(OvernesiaNodeID id) {
		if(!this.visited.contains(id))
			this.visited.add(id);
	}
	
}

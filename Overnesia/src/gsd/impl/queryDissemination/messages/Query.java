package gsd.impl.queryDissemination.messages;

import gsd.common.OvernesiaNodeID;
import gsd.impl.Buffers;
import gsd.impl.UUIDs;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;

public class Query {

	private OvernesiaNodeID originalSender;
	private OvernesiaNodeID sender;
	private UUID queryID;
	private ArrayList<UUID> knownNesoi;
	private short ttl;

	public Query(OvernesiaNodeID peer, short ttl) {
		this.originalSender = peer;
		this.sender = peer;
		this.queryID = UUID.randomUUID();
		this.knownNesoi = new ArrayList<UUID>();
		this.knownNesoi.add(peer.getNesosUUID());
		this.ttl = ttl;
	}
	
	public Query(OvernesiaNodeID originator, OvernesiaNodeID sender, UUID query_id, ArrayList<UUID> knownNesoi, short ttl) {
		this.originalSender = originator;
		this.sender = sender;
		this.queryID = query_id;
		this.knownNesoi = knownNesoi;
		this.ttl = ttl;
	}
	
	public Query(OvernesiaNodeID peer, ArrayList<UUID> knownNesoi, short ttl) {
		this.originalSender = peer;
		this.knownNesoi = knownNesoi;
		this.ttl = ttl;
	}

	public OvernesiaNodeID getOriginalSender() {
		return originalSender;
	}

	public void setOriginalSender(OvernesiaNodeID originalSender) {
		this.originalSender = originalSender;
	}

	public ArrayList<UUID> getKnownNesoi() {
		return knownNesoi;
	}

	public void setKnownNesoi(ArrayList<UUID> knownNesoi) {
		this.knownNesoi = knownNesoi;
	}

	public short getTTL() {
		return ttl;
	}
	
	public short decreaseTTL() {
		this.ttl--;
		return this.ttl;
	}
	
	public UUID getQueryID() {
		return this.queryID;
	}
	
	public void addNesosToKnownNesoiSet(UUID nesosUUID) {
		this.knownNesoi.add(nesosUUID);
	}
	
	static public ByteBuffer writeQueryToBuffer(Query request) {
		ByteBuffer msg = ByteBuffer.allocate(38 + 38 + 16 + 2 + 2 + (request.knownNesoi.size() * 16));
		
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.originalSender);
		OvernesiaNodeID.writeNodeIdToBuffer(msg, request.sender);
		UUIDs.writeUUIDToBuffer(msg, request.queryID);
		msg.putShort(request.ttl);
			
		msg.putShort((short) request.knownNesoi.size());
		
		for(UUID id: request.knownNesoi)
			UUIDs.writeUUIDToBuffer(msg, id);
		
		msg.flip();
		return msg;
	}
	
	public static Query readQueryFromBuffer(ByteBuffer[] msg) {
		ByteBuffer buf = Buffers.sliceCompact(msg, 38 + 38 + 16 + 2 + 2);
		
		OvernesiaNodeID originator = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		OvernesiaNodeID sender = OvernesiaNodeID.readNodeIDFromBuffer(buf);
		UUID qid = UUIDs.readUUIDFromBuffer(buf);
		short ttl = buf.getShort();
		short size = buf.getShort();
		ArrayList<UUID> knownNesoi = new ArrayList<UUID>(size);

		if(size > 0) {
			buf = Buffers.sliceCompact(msg, size * 16);
			for(int i = 0; i < size; i++)
				knownNesoi.add(UUIDs.readUUIDFromBuffer(buf));
		}
		
		return new Query(originator, sender, qid, knownNesoi, ttl);
	}

	public Object getSender() {
		return this.sender;
	}

	public void removeFromKnownNesos(UUID nesosUUID) {
		while(this.knownNesoi.contains(nesosUUID))
			this.knownNesoi.remove(nesosUUID);
	}

	public void setSender(OvernesiaNodeID id) {
		this.sender = id;
	}	
	
}

package gsd.impl.queryDissemination;

import gsd.appl.utils.DateTimeRepresentation;
import gsd.common.OvernesiaNodeID;
import gsd.impl.Application;
import gsd.impl.Buffers;
import gsd.impl.Connection;
import gsd.impl.DataListener;
import gsd.impl.Transport;
import gsd.impl.UUIDs;
import gsd.impl.overnesia.Overnesia;
import gsd.impl.queryDissemination.messages.InternalRandomWalk;
import gsd.impl.queryDissemination.messages.Query;

import java.util.*;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.nio.*;

/**
 * Implementation of gossip. Like bimodal, combines a forward
 * retransmission phase with a repair phase. However, the
 * optimistic phase is also gossip bases. UUIDs, instead of
 * sequence numbers are used to identify and discard duplicates.  
 */
public class OvernesiaQueryDissemination implements DataListener {
	/**
     *  Creates a new instance of Gossip.
     */
	   /**
     * ConnectionListener management module.
     */
    private Overnesia memb;

    /**
     *  Represents the class to which messages must be delivered.
     */
    private Application handler;

    /**
     *  The Transport port used by the Gossip class instances to exchange messages. 
     */
    private short queryPort, replyPort, disseminationPort, internalWalkPort;
    
    /**
     * Random number generator for selecting targets.
     */
    private Random rand;

    /**
     *Configurations of the dissemination strategy
     */
    private short fanout;
    private short ttl;
    private short int_ttl;
    private boolean initial_flood;

    /**
     * Maximum number of stored ids.
     */
    private short maxIds;

    /**
     * Processed Queries
     */
    private LinkedList<UUID> processedQueries;
	
    /**
     * Network instance 
     */
    private Transport net;
    
    public OvernesiaQueryDissemination(Random rand, Transport net, Overnesia overlay, short[] ports) {
        this.memb = overlay;
        this.queryPort = ports[0];
        this.replyPort = ports[1];
        this.disseminationPort = ports[2];
        this.internalWalkPort = ports[3];
        this.rand = rand;

        this.fanout = 3;
        this.ttl = 3;
        this.int_ttl = 2;
        this.initial_flood = false;
        
        this.maxIds = 10;
        this.processedQueries = new LinkedList<UUID>();
        
        this.net = net;
          
        net.setDataListener(this, this.queryPort);
        net.setDataListener(this, this.replyPort);
        net.setDataListener(this, this.disseminationPort);
        net.setDataListener(this, this.internalWalkPort);
    }
    
    public void handler(Application handler) {
        this.handler = handler;
    }
        
    public void reply(ByteBuffer[] msg, InetSocketAddress dest) {
    	net.udpSend(msg, this.replyPort, dest);
    	this.reply_send++;
    }
    
	public void multicast(short queryRequestID, OvernesiaNodeID id, UUID uuid) {
		ByteBuffer[] msg = new ByteBuffer[]{ByteBuffer.allocate(2).putShort(queryRequestID), OvernesiaNodeID.writeNodeIdToBuffer(id), UUIDs.writeUUIDToBuffer(uuid)};
		for(ByteBuffer b: msg)
			b.flip();
		multicast(msg);
	}
    
    public void multicast(ByteBuffer[] msg) {
    	HashMap<OvernesiaNodeID, Connection> external = this.memb.getOvernesiaExternalView();
    	HashMap<OvernesiaNodeID, Connection> internal = this.memb.getOvernesiaNesosView();
    	
    	Query query = new Query(this.memb.getId(),this.ttl);
    	ArrayList<OvernesiaNodeID> externalDests = new ArrayList<OvernesiaNodeID>(external.keySet());
    	for(int i = 0; i < externalDests.size(); i++) {
    		for(int j = i + 1; j < externalDests.size(); j++) {
    			if(externalDests.get(i).getNesosUUID().equals(externalDests.get(j).getNesosUUID())) {
    				if(rand.nextDouble() < 0.5) {
    					externalDests.remove(j);
    					j--;
    					continue;
    				} else {
    					externalDests.remove(i);
    					i--;
    					break;
    				}
    			}
    		}
    	}
    	
    	while(externalDests.size() > this.fanout) {
    		externalDests.remove(rand.nextInt(externalDests.size()));
    	}
    	
    	for(OvernesiaNodeID id: externalDests) {
    		query.addNesosToKnownNesoiSet(id.getNesosUUID());
    	}
    	
    	short missing = this.fanout;
    	
    	this.deliver(query.getQueryID(), Buffers.clone(msg));
    	
    	ByteBuffer[] query_msg = new ByteBuffer[msg.length + 1];
    	query_msg[0] = Query.writeQueryToBuffer(query);
    	System.arraycopy(msg, 0, query_msg, 1, msg.length);
    	
    	for(OvernesiaNodeID id: externalDests) {
    		Connection link = external.get(id);
    		if(link != null && link.isActive()) {
    			link.send(Buffers.clone(query_msg), this.queryPort);
    			this.msg_send++;
    			missing--;
    		}	
    	}
    	
    	if(this.initial_flood) {
	    	for(Connection link: internal.values()) {
	    		link.send(Buffers.clone(query_msg), this.disseminationPort);
	    		this.msg_send++;
	    	}
    	} else if(missing > 0 && internal.size() > 0){
    		Connection link = internal.get(new ArrayList<OvernesiaNodeID>(internal.keySet()).get(rand.nextInt(internal.keySet().size())));
    		ByteBuffer[] internalWalker = new ByteBuffer[msg.length + 2];
    		internalWalker[0] = InternalRandomWalk.writeInternalRandomWalkToBuffer(new InternalRandomWalk(this.memb.getId(),this.int_ttl,missing));
    		internalWalker[1] = Query.writeQueryToBuffer(query);
    		System.arraycopy(msg, 0, internalWalker, 2, msg.length);
    		link.send(internalWalker, this.internalWalkPort);
    		this.msg_send++;
    	}
    	
    }
    
    private void deliver(UUID qid, ByteBuffer[] msg) {
    	if(this.processedQueries.size() == this.maxIds)
			this.processedQueries.removeLast();
			
		this.processedQueries.addFirst(qid);
		
		this.msg_deliv++;
		handler.deliver(msg);
	}
    
    private void addQueryIdentifier(UUID qid) {
    	if(this.processedQueries.size() == this.maxIds)
			this.processedQueries.removeLast();
			
		this.processedQueries.addFirst(qid);
	}

	public void receive(ByteBuffer[] msg, Connection info, short port) { 
		if(this.queryPort == port) {
			this.msg_rcv++;
			handleQuery(msg, info);
    	} else if(this.disseminationPort == port) {
    		this.msg_rcv++;
    		handleDissemination(msg, info);
    	} else if(this.internalWalkPort == port) {
    		this.msg_rcv++;
    		handleInternalRandomWalk(msg, info);
    	} else if(this.replyPort == port) {
    		handleReply(msg, info);
    	} else {
    		System.out.println(this.getClass().getCanonicalName() + ": Unrecognized message port " + port);  
    	}
	}

    private void handleReply(ByteBuffer[] msg, Connection info) {
    	this.reply_recv++;
    	this.handler.deliver(msg);
	}

	private void handleInternalRandomWalk(ByteBuffer[] msg, Connection info) {
		InternalRandomWalk internalWalker = InternalRandomWalk.readInternalRandomWalkFromBuffer(msg);
		Query query = Query.readQueryFromBuffer(msg);
		ByteBuffer payload = Buffers.compact(msg);
		
		if(this.processedQueries.contains(query.getQueryID()))
			return; //Nothing to do
		
		this.addQueryIdentifier(query.getQueryID());
		
		HashMap<OvernesiaNodeID, Connection> external = this.memb.getOvernesiaExternalView();
    	
    	ArrayList<OvernesiaNodeID> externalDests = new ArrayList<OvernesiaNodeID>(external.keySet());
    	ArrayList<UUID> noValidNesoi = query.getKnownNesoi();
    	
    	for(int i = 0; i < externalDests.size(); i++) {
    		if(noValidNesoi.contains(externalDests.get(i).getNesosUUID()) || externalDests.get(i).equals(query.getSender())) {
    			externalDests.remove(i);
    			i--;
    			continue;
    		}
    		for(int j = i + 1; j < externalDests.size(); j++) {
    			if(externalDests.get(i).getNesosUUID().equals(externalDests.get(j).getNesosUUID())) {
    				if(rand.nextDouble() < 0.5) {
    					externalDests.remove(j);
    					j--;
    					continue;
    				} else {
    					externalDests.remove(i);
    					i--;
    					break;
    				}
    			}
    		}
    	}
    	
    	while(externalDests.size() > internalWalker.getMissing()) {
    		externalDests.remove(rand.nextInt(externalDests.size()));
    	}
    		    	
    	for(OvernesiaNodeID id: externalDests) {
    		query.addNesosToKnownNesoiSet(id.getNesosUUID());
    	}
    	
    	query.setSender(this.memb.getId());
    	ByteBuffer[] query_msg = new ByteBuffer[]{Query.writeQueryToBuffer(query),payload};
    	
    	for(OvernesiaNodeID id: externalDests) {
    		Connection link = external.get(id);
    		if(link != null && link.isActive()) {
    			link.send(Buffers.clone(query_msg), this.queryPort);
    			this.msg_send++;
    			internalWalker.decreaseMissingTransmissions();
    		} else {
    			query.removeFromKnownNesos(id.getNesosUUID());
    		}
    	}
    	
    	if(internalWalker.getMissing() > 0 && internalWalker.decreaseTT() > 0) {
    		HashMap<OvernesiaNodeID, Connection> internal = this.memb.getOvernesiaNesosView();
    		ArrayList <OvernesiaNodeID> potentialTargets = new ArrayList<OvernesiaNodeID>(internal.keySet());
    		for(OvernesiaNodeID id: internalWalker.getVisited())
    			potentialTargets.remove(id);
    		if(potentialTargets.size() > 0) {
    			internalWalker.addVisited(this.memb.getId());
	    		Connection link = internal.get(potentialTargets.get(rand.nextInt(potentialTargets.size())));
	    		link.send(new ByteBuffer[]{InternalRandomWalk.writeInternalRandomWalkToBuffer(internalWalker),Query.writeQueryToBuffer(query),payload}, this.internalWalkPort);
	    		this.msg_send++;
    		}
    	}
	}

	private void handleDissemination(ByteBuffer[] msg, Connection info) {
		Query query = Query.readQueryFromBuffer(msg);
				
		if(this.processedQueries.contains(query.getQueryID()))
			return; //nothing to do
		
		this.addQueryIdentifier(query.getQueryID());
			
		HashMap<OvernesiaNodeID, Connection> external = this.memb.getOvernesiaExternalView();
    	
    	ArrayList<OvernesiaNodeID> externalDests = new ArrayList<OvernesiaNodeID>(external.keySet());
    	ArrayList<UUID> noValidNesoi = query.getKnownNesoi();
    	
    	for(int i = 0; i < externalDests.size(); i++) {
    		if(noValidNesoi.contains(externalDests.get(i).getNesosUUID()) || externalDests.get(i).equals(query.getSender())) {
    			externalDests.remove(i);
    			i--;
    			continue;
    		}
    		for(int j = i + 1; j < externalDests.size(); j++) {
    			if(externalDests.get(i).getNesosUUID().equals(externalDests.get(j))) {
    				if(rand.nextDouble() < 0.5) {
    					externalDests.remove(j);
    					j--;
    					continue;
    				} else {
    					externalDests.remove(i);
    					i--;
    					break;
    				}
    			}
    		}
    	}
    	
    	while(externalDests.size() > this.fanout) {
    		externalDests.remove(rand.nextInt(externalDests.size()));
    	}
    		    	
    	for(OvernesiaNodeID id: externalDests) {
    		query.addNesosToKnownNesoiSet(id.getNesosUUID());
    	}
    	
    	query.setSender(this.memb.getId());
    	ByteBuffer[] query_msg = new ByteBuffer[msg.length + 1];
    	query_msg[0] = Query.writeQueryToBuffer(query);
    	System.arraycopy(msg, 0, query_msg, 1, msg.length);
    	
    	for(OvernesiaNodeID id: externalDests) {
    		Connection link = external.get(id);
    		if(link != null && link.isActive()) {
    			link.send(Buffers.clone(query_msg), this.queryPort);
    			this.msg_send++;
    		}
    	}
	}

	

	private void handleQuery(ByteBuffer[] msg, Connection info) {
		Query query = Query.readQueryFromBuffer(msg);
		if(!this.processedQueries.contains(query.getQueryID())) {
			this.deliver(query.getQueryID(), Buffers.clone(msg));
			
			if(query.decreaseTTL() > 0) {
			
		    	HashMap<OvernesiaNodeID, Connection> external = this.memb.getOvernesiaExternalView();
		    		    	
		    	ArrayList<OvernesiaNodeID> externalDests = new ArrayList<OvernesiaNodeID>(external.keySet());
		    	ArrayList<UUID> noValidNesoi = query.getKnownNesoi();
		    	
		    	for(int i = 0; i < externalDests.size(); i++) {
		    		if(noValidNesoi.contains(externalDests.get(i).getNesosUUID()) || externalDests.get(i).equals(query.getSender())) {
		    			externalDests.remove(i);
		    			i--;
		    			continue;
		    		}
		    		for(int j = i + 1; j < externalDests.size(); j++) {
		    			if(externalDests.get(i).getNesosUUID().equals(externalDests.get(j))) {
		    				if(rand.nextDouble() < 0.5) {
		    					externalDests.remove(j);
		    					j--;
		    					continue;
		    				} else {
		    					externalDests.remove(i);
		    					i--;
		    					break;
		    				}
		    			}
		    		}
		    	}
		    	
		    	while(externalDests.size() > this.fanout) {
		    		externalDests.remove(rand.nextInt(externalDests.size()));
		    	}
		    		    	
		    	for(OvernesiaNodeID id: externalDests) {
		    		query.addNesosToKnownNesoiSet(id.getNesosUUID());
		    	}
		    	
		    	short missing = fanout;
		    	
		    	query.setSender(this.memb.getId());
		    	ByteBuffer[] query_msg = new ByteBuffer[msg.length + 1];
		    	query_msg[0] = Query.writeQueryToBuffer(query);
		    	System.arraycopy(msg, 0, query_msg, 1, msg.length);
		    	
		    	for(OvernesiaNodeID id: externalDests) {
		    		Connection link = external.get(id);
		    		if(link != null && link.isActive()) {
		    			link.send(Buffers.clone(query_msg), this.queryPort);
		    			this.msg_send++;
		    			missing--;
		    		} else {
		    			query.removeFromKnownNesos(id.getNesosUUID());
		    		}
		    	}
		    	
		    	if(missing > 0) {
		    		HashMap<OvernesiaNodeID, Connection> internal = this.memb.getOvernesiaNesosView();
		    		if(internal.size() > 0) {
			    		Connection link = internal.get(new ArrayList<OvernesiaNodeID>(internal.keySet()).get(rand.nextInt(internal.keySet().size())));
			    		ByteBuffer[] internalWalker = new ByteBuffer[msg.length + 2];
			    		internalWalker[0] = InternalRandomWalk.writeInternalRandomWalkToBuffer(new InternalRandomWalk(this.memb.getId(),int_ttl,missing));
			    		internalWalker[1] = Query.writeQueryToBuffer(query);
			    		System.arraycopy(msg, 0, internalWalker, 2, msg.length);
			    		link.send(internalWalker, this.internalWalkPort);
			    		this.msg_send++;
		    		}
		    	}
			}
		}
	}

	public void setFanout(short value) {
		this.fanout = value;
	}

	public void setTTL(short value) {
		this.ttl = value;
	}

	public void setIntTTL(short value) {
		this.int_ttl = value;
	}

	public void setInitialFloodinf(short value) {
		this.initial_flood = value != 0;		
	}
	
	public void setMaxIds(short value) {
		this.maxIds = value;
		while(this.processedQueries.size() > this.maxIds)
			this.processedQueries.removeLast();
	}

	public void flushStatistics(PrintStream printStream) {
		printStream.println("STATS " + DateTimeRepresentation.timeStamp() + " " + msg_rcv + " " + msg_send + " " + msg_deliv + " " + reply_send + " " + reply_recv);
		msg_deliv = msg_rcv = msg_send = reply_send = reply_recv = 0;
	}
	
	private int msg_rcv;
	private int msg_send;
	private int msg_deliv;
	private int reply_send;
	private int reply_recv;
}


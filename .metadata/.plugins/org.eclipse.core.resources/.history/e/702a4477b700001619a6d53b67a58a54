package gsd.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import gsd.common.NodeID;

public class TestResults implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean kicked = false;
	private boolean freerider = false;
	private int lastFrame = 0;
	private Map<NodeID,List<Integer>> rankEvol = new HashMap<>();
	private Map<NodeID,Integer> numInters = new HashMap<>();
	private Map<NodeID,Integer> numKicks = new HashMap<>();
	private Map<Integer,Integer> redundancyCounter = new HashMap<Integer, Integer>();
	private List<List<NodeID>> actViews = new ArrayList<>();
	private int nchals = 0;	
	private NodeID id;
	private int numSent = 0;
	private int numChals = 0;
	
	public TestResults(NodeID id){
		this.id = new NodeID(id);
		this.id.clean();
	}

	public Map<Integer, Integer> getRedundancyCounter() {
		return this.redundancyCounter;
	}

	/*public void putRedundancyCounter(int frameID, int value) {
		synchronized(this.redundancyCounter){
			this.redundancyCounter.put(frameID, value);	
		}
	}*/

	public void addNewRank(NodeID node, int frame, int rank){
		synchronized(rankEvol){
			if(!rankEvol.containsKey(node))
				rankEvol.put(node, new ArrayList<>());

			rankEvol.get(node).add(rank);
		}
	}

	public void incNumChals(){
		this.nchals++;
	}

	public int getNumChals(){
		return this.nchals;
	}

	/**
	 * Adds to redundancy counter of frames
	 * @param Frame ID
	 * @param Active view ids
	 * @return true if already received, false otherwise
	 */
	public boolean addNewFrame(int frame, Set<NodeID> set){
		boolean flag = false;
		synchronized(redundancyCounter){
			if(!redundancyCounter.containsKey(frame)){
				System.out.println("Saved frame " + frame);
				lastFrame = ((lastFrame < frame) && !this.kicked)? frame : lastFrame;
				redundancyCounter.put(frame, 1);
				List<NodeID> actV = new ArrayList<NodeID>();
				actV.addAll(set);
				this.actViews.add(actV);
			}
			else{
				redundancyCounter.put(frame, redundancyCounter.get(frame) +1);
				flag = true;
			}
		}

		return flag;
	}

	public void kick(NodeID node){
		synchronized(this.numInters){
			if(this.numKicks.containsKey(node)){
				this.numKicks.put(node,this.numKicks.get(node) + 1);
			}
		}
	}

	public void kickMe(){
		this.kicked = true;
	}

	public void addRel(NodeID node){
		synchronized(this.numInters){			
			if(this.numInters.containsKey(node)){
				this.numInters.put(node, this.numInters.get(node) + 1);
			}
			else{
				this.numInters.put(node, 1);
				this.numKicks.put(node,0);
			}
		}
	}

	public Map<NodeID,Double> avgKicks(){
		Map<NodeID,Double> avgKicks = new HashMap<>();

		for(NodeID node : this.numInters.keySet()){
			avgKicks.put(node, 
					(double)this.numKicks.get(node)/(double)this.numInters.get(node));
		}

		return avgKicks;
	}

	public boolean isIsolated() {
		return (this.redundancyCounter.isEmpty());
	}

	public void setFreeRider(){
		this.freerider = true;
	}

	public int getNumFrames(){
		return this.redundancyCounter.size();
	}

	public boolean isFreeRider() {
		return this.freerider;
	}

	public List<Integer> getRanks(NodeID node) {
		return this.rankEvol.get(node);
	}

	public int getLastFrame(){
		return this.lastFrame;
	}

	public boolean wasKicked(){
		return this.kicked;
	}

	public Map<NodeID,List<Integer>> getRanks() {
		return this.rankEvol;
	}

	public double calculateRedundancy(int num_frames) {
		double total = 0;
		for(Integer counter : this.redundancyCounter.values()) {
			total = total + counter;
		}

		System.out.println("Total " + total);
		System.out.println("Number frames " + num_frames);
		total = (num_frames > 0)? (total / num_frames) : 1 ;
		System.out.println("Redundancy frames " + total);
		return total;
	}

	public List<List<NodeID>> getActViewEvol() {
		// TODO Auto-generated method stub
		return this.actViews;
	}

	public double calculateReliability(int numFrames) {
		System.out.print("Missing frames (" + this.id.getSocketAddress());
		for(int i = 1 ; i <= numFrames; i++){
			System.out.printf(" %d ", i);
		}
		System.out.println();
		
		return ((double)this.getNumFrames() / (double) numFrames);
	}

	public List<Integer> maxNumRanks(List<NodeID> freeRiders) {
		int max = -1;
		List<Integer> res = null;

		Map<NodeID,List<Integer>> rks = this.getRanks();
		for(Map.Entry<NodeID, List<Integer>> e : rks.entrySet()){
			if(e.getValue().size()>max && !freeRiders.contains(e.getKey())){
				res = e.getValue();
			}					
		}

		return res;
	}

	public NodeID getID() {
		// TODO Auto-generated method stub
		return this.id;
	}
	
	public synchronized void addToMsgSent(int num){
		this.numSent += num;
	}

	public int numMsgSent(){
		return this.numSent;
	}

	public synchronized void addToNumChals(int num){
		this.numChals += num;
	}

	public int numChals(){
		return this.numChals;
	}
}

package gsd.appl;

import gsd.api.ConstValues;
import gsd.api.ExternalControlInterface;
import gsd.api.PropertiesFactory;
import gsd.api.TestResults;
import gsd.appl.control.components.CommandLine;
import gsd.common.NodeID;
import gsd.impl.PackDissemination;
import gsd.impl.TestCollect;
import gsd.impl.VideoStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.Vector;

/*
 * This class works is used as a connection point for the new nodes in HyParView
 * It is also the control node that triggers and starts the dissemination
 * 
 * @autor Daniel Quinta
 * 
 * */
public class ServerHyParView {

	public static final int hyparviewPort = 30599;

	//
	private String expN;
	private String var;
	private String testIt;
	private int fanout;
	
	private double numSent = 0; 
	
	private String myId;
	private PropertiesFactory prop;

	public CommandLine cmdl;
	private Controller controller;
	private Dissemination dissemination;
	private Vector<ClientHandler> clientsControl = new Vector<ClientHandler>();
	private List<NodeID> mySlaves = new ArrayList<>();
	private LinkedList<DissemOper> opers = new LinkedList<>();
	private ArrayList<NodeID> listFreeRiders = new ArrayList<NodeID>();
	private HashMap<NodeID, TestResults> listControlSlaves = new HashMap<NodeID, TestResults>();
	// private ArrayList<NodeID> listControlSlaves =
	// ConstValues.SlavesControlInit;

	public ServerHyParView(final String ip, String expN, String var, String testIt) {
		this.myId = ip;
		this.expN = expN;
		this.testIt = testIt;
		this.var = var;
		prop = new PropertiesFactory(Integer.parseInt(expN));
	}

	public String getId() {
		return this.myId;
	}

	public void execute() {

		this.controller = new Controller(this, prop);
		controller.start();

		this.dissemination = new Dissemination(this, prop);
		dissemination.start();

		try{
			controller.join();
			dissemination.join();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public void openConnection(NodeID node) throws IOException {
		System.out.println("!!!Attempting to create socket to " + node.getSocketAddress() + ":"
				+ node.getSocketPort());
		Socket tempPeerConnection = new Socket(
				node.getSocketAddress().getAddress(),
				node.getSocketPort() + 1000);
		tempPeerConnection.setSoTimeout(60000);
		node.setServingSocket(tempPeerConnection);

		node.getOutputStream().writeObject("06:9c:13:69:a1:fa:36:fa:45:1c:2e:9f:e6:99:a1:fa");
		// accept protocol
		try {
			Object v = node.getInputStream().readObject();
			System.out.println(v.toString());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	/*public void openConnections() {
		synchronized(this.listControlSlaves){
			Iterator<NodeID> iter = this.mySlaves.keySet().iterator();
			while (iter.hasNext()) {
				NodeID slaveNode = iter.next();
				try {
					if(slaveNode.getServingSocket() == null){
						openConnection(slaveNode);
					}
				} catch (IOException e) {
					//e.printStackTrace();
					iter.remove();
					this.listControlSlaves.remove(slaveNode);
					//System.out.println("");
				}
			}
		}
	}*/

	public void closeConnections() {
		synchronized(listControlSlaves){
			Iterator<NodeID> iter = this.listControlSlaves.keySet().iterator();
			while (iter.hasNext()) {
				NodeID slaveNode = iter.next();
				try {
					slaveNode.getOutputStream().writeObject('q');
					slaveNode.getOutputStream().flush();
					slaveNode.getServingSocket().close();
				} catch (IOException e) {
					//e.printStackTrace();
					iter.remove();
					this.mySlaves.remove(slaveNode);
					System.out.println("");
				}
			}
		}
		this.controller.term();

		System.exit(1);
	}

	public static void main(String[] args) {

		String expN = args[0];
		String var = args[1];
		String testIt = args[2];
		int exp;
		try {
			exp = ConstValues.setExpProperties();
		} catch (FileNotFoundException e1) {
			System.out.println("File exp not found, please contact Daniel for more info");// TODO
			e1.printStackTrace();
			return;
		}

		PropertiesFactory prop = new PropertiesFactory(exp);
		String myIP = prop.getProperty("serverIP");
		try {
			new ServerHyParView(myIP, expN, var, testIt).execute();
			System.out.println("I am on-line!");
		} catch (Exception e) {
			System.err.println("Fail to go on-line. Unhandled exception: " + e.getClass().getName());
			e.printStackTrace(System.err);
			System.exit(1);
		}
	}

	public static void shuffle(List<NodeID> list) {
		synchronized(list){
			Random rand = new Random();
			for (int i = 0; i < list.size(); i++) {
				int r = rand.nextInt(list.size());
				NodeID temp = list.get(r);
				list.set(r, list.get(i));
				list.set(i, temp);
			}
		}
	}

	private List<NodeID> getRandomSlaveList() {
		List<NodeID> arr = new ArrayList<NodeID>();
		List<NodeID> nodes = new ArrayList<NodeID>();
		nodes.addAll(mySlaves);
		ServerHyParView.shuffle(nodes);
		int limit = nodes.size(); // random [0,limit[
		for (int f = 0; f < fanout && f < limit; f++) {
			arr.add(nodes.get(f));// random [0,limit[, limit cannot be 0
		}
		// System.out.printf("New Random TreeSet"+ list.toString());
		return arr;
	}

	public void removeNode(NodeID node){
		synchronized(listControlSlaves){
			if(!listControlSlaves.containsKey(node)){
				listControlSlaves.remove(node);
				mySlaves.remove(node);
			}
		}
	}

	public void addSlave2SlaveList(NodeID slaveNode) {
		synchronized(listControlSlaves){
			if(!listControlSlaves.containsKey(slaveNode)){
				listControlSlaves.put(slaveNode, new TestResults(slaveNode));
				if(mySlaves.size() < prop.getIntProperty("peerListSize")){
					mySlaves.add(slaveNode);	
				}
				try{
					this.openConnection(slaveNode);
				}
				catch(IOException e){
					//e.printStackTrace();
					this.removeNode(slaveNode);
				}
			}
		}
	}

	public int numberNodes(){
		int res;
		synchronized(this.listControlSlaves){
			res = mySlaves.size();
		}
		return res; 
	}
	
	public void restart(NodeID node){
		try{
			Socket soc = node.getServingSocket();
			if(soc != null){
				soc.close();
			}
		}
		catch(IOException e){

		}
		
		try{
			openConnection(node);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	public int numberAllNodes(){
		int res;
		synchronized(this.listControlSlaves){
			res = listControlSlaves.size();
		}
		return res; 
	}

	protected class Controller extends Thread implements ExternalControlInterface {

		PropertiesFactory prop;
		private ServerHyParView instance;
		private Socket commander = null;
		private ServerSocket ss;

		public Controller(ServerHyParView instance, PropertiesFactory p) {
			this.instance = instance;
			this.commander = null;
			this.prop = p;
		}

		public ServerSocket setListenSocketUp() {
			try {
				ServerSocket ss = new ServerSocket();
				System.out.println(
						"Instance.getId : " + this.instance.getId() + "  port : " + prop.getProperty("primPort"));
				InetSocketAddress add = new InetSocketAddress(this.instance.getId(),prop.getIntProperty("primPort"));
				//InetSocketAddress add = new InetSocketAddress("192.168.1.3", prop.getIntProperty("primPort"));
						
				System.out.println("Binding to " + add);
				ss.bind(add);
				return ss;
			} catch (IOException e) {
				System.err.println("Cannot bind to command and control listenning socket: " + e.getMessage());
				e.printStackTrace(System.err);
				HyParViewLabTest.addToOutLog("COMM", "Failed to create socket: " + e.getMessage());
				System.exit(1);
				return null;
			}
		}

		@Override
		public void run() {
			ss = setListenSocketUp();
			File resetFlag = new File("reset_signal");
			while (true) {
				if (ss == null || ss.isClosed())
					ss = setListenSocketUp();

				System.out.println("Waiting for slaves...");
				try {
					this.commander = ss.accept();
					// Handle new client to free the channel
					System.out.println("Server SlavesList size: " + mySlaves.size());
					System.out.println("Server Peer Known List size: " + listControlSlaves.size());
					// System.out.println("Server:
					// "+commander.getInetAddress().toString().split("/")[1] );

					ClientHandler client = new ClientHandler(commander, instance, prop);
					clientsControl.add(client);
					client.setDaemon(true);
					client.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		public void term() {
			try {
				this.ss.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void sendMessage(Object m) throws IOException {
			throw new IOException("sendMessage not implemented!!!");
		}

	}

	protected class Dissemination extends Thread {

		private ServerHyParView instance;
		private Socket dissemination = null;
		private PropertiesFactory prop;
		private VideoStream video;

		public Dissemination(ServerHyParView instance, PropertiesFactory p) {
			this.instance = instance;
			this.prop = p;
			video = new VideoStream(prop.getIntProperty("videoSize"));
		}

		private void sendFreeRidersSignal() {
			List<NodeID> nodes = new ArrayList<NodeID>();
			nodes.addAll(mySlaves);
			ServerHyParView.shuffle(nodes);
			for (int i = 0; i < 5; i++) {
				NodeID slave = nodes.get(i);
				System.out.println("----Free rider " + slave.getSocketAddress());

				listFreeRiders.add(slave);
				Socket tempPeerConnection = null;
				try {
					tempPeerConnection = new Socket(slave.getSocketAddress().getAddress(),
							slave.getSocketPort() + 1000);
					// System.out.println("Server socket : "+
					// tempPeerConnection.toString());
					ObjectOutputStream outChannel = slave.getOutputStream();
					outChannel.writeObject('x');
					outChannel.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						if (tempPeerConnection != null)
							tempPeerConnection.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}

		public void run() {

			while (true) {
				File dissemination_signal = new File("dissemination_signal");
				System.out.println("Checking dissemination");
				if (dissemination_signal.exists()) { // else sleep
					//instance.openConnections();
					dissemination_signal.delete();
					if (prop.getBoolProperty("freeRiding")) {
						sendFreeRidersSignal();
					}
					
					try{
						long time = prop.getLongProperty("realChalDelay") * 
								prop.getLongProperty("activeViewMaxSize");		
						System.out.println("00000000 Waiting for setup for " + time);
						sleep(time);
						System.out.println("00000000 Ready to disseminate");
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
					disseminateStream();
					Map<NodeID,TestResults> results = collectResults();
					AcumData data = processResults(results);					
					writeAcumData(data);					
					instance.closeConnections();
				} else{
					try {
						sleep(5000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		}		

		private void writeAcumData(AcumData data) {
			try {
				String testDirPath = "../" + expN + "/" + String.format("%07.2f", Double.parseDouble(var)) + "/" + String.format("%02d", Integer.parseInt(testIt));
				File testDir = new File(testDirPath);
				testDir.mkdirs();
				File processedResults = new File(testDirPath + "/processedResults.txt");
				processedResults.delete();
				processedResults.createNewFile();
				FileWriter writer = new FileWriter(processedResults);

				StringBuilder sb = determineResultsString(data);

				writer.write(sb.toString());
				writer.flush();				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private StringBuilder determineResultsString(AcumData data) {
			StringBuilder sb = new StringBuilder(); // TODO Auto-generated method stub
			int videoSize = prop.getIntProperty("videoSize");
			switch (expN) {
			case "1":
				sb.append(Integer.toString(data.isolated) + ";" + data.replies);
				break;
			case "2":
				sb.append(String.format("%.4f%n",data.reliability / data.replies));
				sb.append(String.format("%.4f%n",data.redundancy / data.replies));
				sb.append(data.replies);
				break;
			case "3":
				System.out.println("Selected node: "+ data.node.getSocketAddress());
				for (int i = 0; i < data.ranks.size(); i++) {
					sb.append(String.format("%d,%d%n", i, data.ranks.get(i)));
				}
				break;
			case "4":
				for(int i = 0; i< videoSize; i++){
					sb.append(String.format("%d!%.4f!%.4f%n", i+1, 
							(double)data.falsePos[i]/(double)mySlaves.size(),
							(double)data.numDet[i]/(double)listFreeRiders.size()));
				}
				break;

			case "5":
				for(int i = 0; i < data.falseProb.length; i++){
					sb.append(String.format("%d!%.4f%n", i+1, data.falseProb[i]));
				}				
				break;
			case "6":
				int lastc = prop.getIntProperty("activeViewMaxSize");
				System.out.println("Selected node: "+ data.node.getSocketAddress());
				int i = 0;
				for(List<NodeID> nodes : data.actV){
					int count = 0;
					for(NodeID n : nodes){
						count += (listFreeRiders.contains(n))? 0 : 1;
					}
					lastc = nodes.size();
					sb.append(String.format("%d!%.4f%n", i+1,
							(double)count/(double)lastc));
					i++;
				}
				break;
			case "7":
			case "8":
			case "9":
			case "10":
				if(data.replies > data.freeRide){
					data.reliability = data.reliability/(double)(data.replies - data.freeRide);
					data.msgSent = data.msgSent/(double)(data.replies - data.freeRide);
					data.numChals = data.numChals/(double)(data.replies - data.freeRide);
				}
				if(data.freeRide>0){
					data.reliabilityFree = data.reliabilityFree/(double)(data.freeRide);
					data.msgSentFree = data.msgSentFree/(double)(data.freeRide);
					data.numChalsFree = data.numChalsFree/(double)(data.freeRide);
				}
				
				sb.append(String.format("%.4f;%.4f;%.4f;%.4f;%.4f;%.4f%n",
						data.reliability, data.msgSent, data.numChals,
						data.reliabilityFree, data.msgSentFree, data.numChalsFree));
				sb.append(String.format("Total: %d  Free-riding: %d%n", data.replies, data.freeRide));
				
				sb.append(String.format("%nAltruistic:%n"));
				for(Map.Entry<NodeID, DVars> entry : data.rels.entrySet()){
					sb.append(String.format("%s: %.4f %.4f %.4f%n", entry.getKey().toString(),
							entry.getValue().rel, entry.getValue().numMsgSent));
				}
				
				sb.append(String.format("%nFree-riders:%n"));
				for(Map.Entry<NodeID, DVars> entry : data.relsFree.entrySet()){
					sb.append(String.format("%s: %.2f %.2f%n", entry.getKey().toString(), 
							entry.getValue().rel, entry.getValue().numMsgSent));
				}
				
				break;
			}

			return sb;
		}

		private AcumData processResults(Map<NodeID, TestResults> results) {
			AcumData data = new AcumData();
			int videoSize = prop.getIntProperty("videoSize");

			int ln = 0;
			for(Map.Entry<NodeID,TestResults> peer : results.entrySet()){
				TestResults res = peer.getValue();
				NodeID slave = peer.getKey();
				data.replies++;

				switch (expN) {
				case "1":
					data.isolated += (res.isIsolated())? 1 : 0;
					if(res.isIsolated()){
						System.out.println("ISOLATED: " + slave.getSocketAddress());
						System.out.println(res.getRedundancyCounter());
					}
					break;
				case "2":
					data.reliability += res.calculateReliability(videoSize);
					data.redundancy += res.calculateRedundancy(videoSize);
					break;
				case "3":
					if (!res.isFreeRider()) {
						List<Integer> rks = res.maxNumRanks(listFreeRiders);
						if (rks != null && rks.size() > data.max){
							data.ranks = rks;
							data.max = rks.size();
							data.node = slave;
						}
					}
					break;
				case "4":
					int lastf = res.getLastFrame();
					System.out.println("||||||" + lastf + "--" + 
							res.wasKicked() + "--" + res.isFreeRider());
					if(res.isFreeRider() && res.wasKicked()){
						for(int f = lastf; f < videoSize; f++)
							data.numDet[f]++;
					}
					else if(!res.isFreeRider() && res.wasKicked()){
						for(int f = lastf; f < videoSize; f++){
							data.falsePos[f]++;
						}
					}
					break;
				case "5":
					if(!res.isFreeRider()){
						int count = 0;
						for(NodeID gajoDeAlfama : listControlSlaves.keySet()){
							if(results.containsKey(gajoDeAlfama) && !results.get(gajoDeAlfama).isFreeRider()){
								Map<NodeID,Double> map = results.get(gajoDeAlfama).avgKicks();
								if(map.containsKey(slave)){
									data.falseProb[ln] += map.get(slave);
									count++;
								}
							}
						}
						if(count > 0){
							data.falseProb[ln] = data.falseProb[ln] /(double)count;
							System.out.println(">>>> " + data.falseProb[ln]);
						}
						ln++;
					}
					break;
				case "6":
					if (!res.isFreeRider()) {
						List<List<NodeID>> rks = res.getActViewEvol();
						if (rks != null && rks.size() > data.max){
							data.actV = rks;
							data.max = rks.size();
							data.node = slave;
						}
					}
					break;
				case "7":
				case "8":
				case "9":
				case "10":
					DVars dvar = new DVars(res.calculateReliability(videoSize), 
							res.numMsgSent(), res.numChals());
					if(res.isFreeRider()){
						data.numChalsFree += res.numChals();
						data.reliabilityFree += res.calculateReliability(videoSize);
						data.msgSentFree += res.numMsgSent();
						data.freeRide ++;
						data.relsFree.put(res.getID(), dvar);
					}
					else{
						data.numChals += res.numChals();
						data.reliability += res.calculateReliability(videoSize);
						data.msgSent += res.numMsgSent();
						data.rels.put(res.getID(), dvar);
					}
					break;
				case "11":
					for(int i = 0; i < res.getListChals().size(); i++){
						data.guysChals[i]++;
						data.listChals[i]+= res.getListChals().get(i);
					}
					break;
				}
			}

			return data;
		}

		private void disseminateStream() {
			if (mySlaves.isEmpty()) {
				System.out.println("List listSlaves.isEmpty()!");
				instance.closeConnections();
				System.exit(1);
			}
			
			int numFrames = prop.getIntProperty("videoSize");
			fanout = prop.getIntProperty("activeViewBaseSize");
			if(prop.getBoolProperty("withProbabilistic")){
				fanout = (int) Math.ceil(prop.getBFP() * fanout);
			}


			while (video.getVideoSourceCurrentFrame() <= numFrames) {
				System.out.printf("Dissemination...%n Frame ready: " + video.getVideoSourceCurrentFrame() + ":"
						+ video.getVideoMaxFrame());
				int frameID = video.getVideoSourceCurrentFrame();
				System.out.println("<<<<Fanout: " + fanout);
				List<NodeID> listSlavesR = getRandomSlaveList();

				System.out.println(video.getVideoSourceCurrentFrame() + " - " + numSent);
				long time = prop.getLongProperty("dissemRate");
				synchronized(opers){
					long dif = video.getVideoSourceCurrentFrame() - (long)numSent;
					if(dif > 20){
						System.out.println("CLEAN OPERATION: TOO SLOW");
						
						opers.clear();
					}
					else{
						time = time*dif;
					}
					for (NodeID slave : listSlavesR) {
						try {
							boolean birth = opers.isEmpty();
							
							opers.add(new DissemOper(slave, frameID));
							if(birth){
								Fenix fenix = new Fenix();
								Thread compute = new Thread(fenix);	
								compute.start();
							}
						} finally {
							System.out.println("Server socket : " + slave.getSocketAddress());
						}
					}
					
				}
				
				try {
					sleep(time);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}

				video.incVideoCurrentFrame(1);
			}

			finishStream();
		}


		private void finishStream() {
			boolean ready;
			do{
				try{
					System.out.println("Waiting for end of dissemination");
					sleep(60000);
				}
				catch(Exception e){
					e.printStackTrace();
				}
				ready = true;
				synchronized(opers){
					ready = opers.isEmpty();
				}
			}while(!ready);
		}
	}

	private Map<NodeID,TestResults> collectResults(){
		Map<NodeID,TestResults> results = new HashMap<>();

		for (NodeID slave : listControlSlaves.keySet()) {
			Socket tempPeerConnection;
			// try {
			System.out.println(slave.getSocketAddress());
			tempPeerConnection = slave.getServingSocket();
			System.out.println("Server socket : " + tempPeerConnection);
			ObjectOutputStream outChannel = slave.getOutputStream();
			ObjectInputStream inputChannel = slave.getInputStream();

			TestCollect col = new TestCollect(inputChannel, outChannel, new TestResults(slave));
			try{
				col.sendTest_collection_signal();
				results.put(slave,col.getTestResults());
			}
			catch(IOException e){
				e.printStackTrace();
				System.out.println("Coud not collect from " + slave.getSocketAddress());
			}
		}

		return results;
	}

	class Fenix implements Runnable{
		public Fenix(){
		}

		public void run(){
			boolean dead = false;
			while(!dead){
				DissemOper oper;

				synchronized(opers){
					oper = opers.peek();
				}

				//System.out.println("New dissem from " + pend.connection.getPeer());

				NodeID node = oper.node;
				System.out.println("Disseminating to " + node);
				ObjectOutputStream outChannel = node.getOutputStream();
				ObjectInputStream inputChannel = node.getInputStream();

				try{
					if(outChannel != null){
						outChannel.writeObject('d');
						System.out.println("Sending frame to " + node.getSocketAddress());
						PackDissemination pack = new PackDissemination();
						pack.sendProtocolServer(node, oper.frameID,outChannel,
							inputChannel,prop.getIntProperty("frameSize"));
					}
				}
				catch(IOException e){
					System.out.println("___" + oper.node);
					e.printStackTrace();
					restart(oper.node);
				}
				finally{
					synchronized(opers){
						opers.removeFirst();
						numSent = numSent + 1/(double)fanout;
						dead = (opers.isEmpty());
						//System.out.println(list);
						if(dead){
							System.out.println("FENIX IS DYING!!!!");
						}
					}
				}
			}
		}
	}

	class DissemOper{
		private int frameID;
		private NodeID node;

		public DissemOper(NodeID node, int frameID){
			this.frameID = frameID;
			this.node = node;
		}
	}

	class DVars{
		double rel;
		double numMsgSent;
		double numChals;
		
		public DVars(double rel, double numMsgSent, double numChals){
			this.rel = rel;
			this.numMsgSent = numMsgSent;
			this.numChals = numChals;
		}
	}
	
	class AcumData{
		public double numFalseProbs = 0;
		public NodeID node;
		int replies = 0;
		int freeRide = 0;
		double redundancy = 0;
		int isolated = 0;
		double reliability = 0;
		double reliabilityFree = 0;
		double msgSent = 0;
		double msgSentFree = 0;
		double numChals = 0;
		double numChalsFree = 0;	
		List<Integer> ranks = null;
		List<List<NodeID>> actV = null;
		int max = 0;
		int videoSize = prop.getIntProperty("videoSize");
		int [] falsePos = new int[videoSize];
		int [] numDet = new int[videoSize];
		double [] falseProb = new double[listControlSlaves.size() - listFreeRiders.size()];
		Map<NodeID,DVars> rels = new HashMap<>();
		Map<NodeID,DVars> relsFree = new HashMap<>();
		int []listChals = new int[videoSize];
		int []guysChals = new int[videoSize];
	}

	public List<NodeID> getListSlaves() {
		// TODO Auto-generated method stub
		return this.mySlaves;
	}

	public Map<NodeID,TestResults> getListControlSlaves() {
		return this.listControlSlaves;
	}
}

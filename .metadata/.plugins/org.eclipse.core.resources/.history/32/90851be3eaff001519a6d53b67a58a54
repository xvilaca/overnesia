package gsd.impl;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import gsd.impl.hyparview.ImmortalHyParView;
import gsd.impl.hyparview.messages.NeighborChallengeRequest;

public class CryptoPuzzle {
	/**
	 * Average time to compute single hash of BigInteger(numBitsAnswer)
	 * in milliseconds
	 */
	private static double avg_time = 0;

	public static void setAvgTime(int numBits){
		BigInteger bi = randomBigInteger(numBits);
		avg_time = 0;
		for(int i = 0; i < 100; i++){
			long startTime = System.nanoTime();
			for(int j = 0; j < 1000; j++){				
				generateHash(bi);
			}
			long endTime = System.nanoTime();
			avg_time += (double)(endTime - startTime)/(double)1000000;//Difference in milliseconds
		}
		avg_time = avg_time/100;//average time to compute 1000 hashes
		avg_time = avg_time/1;//average time to compute single hash
		System.out.println("Average hash time: " + avg_time);
	}
		/**
	 * Return BigInteger hashMD5 
	 *  
	 * @autor Daniel Quinta
	 */
	private static byte[] generateHash(BigInteger answer){
		byte[] hash = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			hash = md.digest(answer.toByteArray());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}

	/**
	 * Calculate a positive BigInteger.
	 * 
	 * 
	 * @param numBits BigInteger size
	 * @autor Daniel Quinta
	 */
	private static BigInteger randomBigInteger(int numBits){
		Random rand = new Random();
		BigInteger bInt = new BigInteger(numBits, rand);
		while( bInt.signum() < 0 )
			bInt = new BigInteger(numBits, rand);
		return bInt;
	}


	/**
	 * Return the Hint - difference between an Answer and a smaller BigInteger (size numBitsAnswer).
	 * This hint, together with hashMD5(answer), will be send to ClientSolver.
	 *  
	 * @autor Daniel Quinta
	 */
	private static BigInteger generateHint(int numBitsWindow, BigInteger answer){ 
		BigInteger window = randomBigInteger(numBitsWindow);
		while( window.compareTo(answer) > 0 ) {
			window = randomBigInteger(numBitsWindow);
		}
		return answer.subtract(window);
	}
	
	public static CryptoPuzzle generatePuzzle(int numBitsAnswer, int numBitsWindow){
		BigInteger answer = randomBigInteger(numBitsAnswer-1);
		BigInteger hint = generateHint(numBitsWindow,answer);
		long numIters = answer.subtract(hint).longValue();
		
		
		return new CryptoPuzzle(numIters, generateHash(answer), hint, numBitsAnswer, numBitsWindow);
	}
	
	byte []answerHash;
	BigInteger hint;
	int numBitsAnswer, numBitsWindow;
	long numIters;
	
	public CryptoPuzzle(long numIters, byte []answerHash, BigInteger hint, int numBitsAnswer, int numBitsWindow){
		this.answerHash = answerHash;
		this.hint = hint;
		this.numBitsAnswer = numBitsAnswer;
		this.numBitsWindow = numBitsWindow;
		this.numIters = numIters;
	}

	public long solve() {
		//Solve puzzle
		System.out.println("I am trying to solve this crypto puzzle");
		long startTime = System.nanoTime();
		BigInteger pseudoAnswer = solveHintBasedHashReversalPuzzle(this.numIters,this.answerHash, this.hint, this.numBitsWindow);
		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		System.out.println("Time: " + duration/1000000000 + "seg");
		if(pseudoAnswer == null)
			return -1;
		return duration;
	}
	
	/*
	 * TODO
	 * 
	 * @autor Daniel Quinta
	 * */
	private static BigInteger solveHintBasedHashReversalPuzzle(long numIters, byte[] hashClient, 
			BigInteger hintBruteForce, int numBitsWindow) {
		boolean found = false;
		BigInteger pseudoAnswer = hintBruteForce;
		long it = 0;
		byte [] myHash;
		long max = (long)1<<(long)numBitsWindow;
		
		/*while(!found && it < max) {
			myHash = generateHash(pseudoAnswer);
			if(Arrays.equals(myHash, hashClient)) {
				found = true;
				break;
			}
			pseudoAnswer = pseudoAnswer.add(BigInteger.ONE);
			it++;
		}*/
				
		try{
			long time = (long)(Math.ceil((double)numIters * avg_time));
			System.out.println(String.format("Going to sleep for %d (%.2f)", time, avg_time));
			Thread.sleep(time);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		
		found = true;
		
		if(found) {
			System.out.println("Found after: " + it + "iterations");
		}
		return pseudoAnswer;
	}

	public byte[] getAnswerHash() {
		// TODO Auto-generated method stub
		return this.answerHash;
	}

	public BigInteger getHint() {
		// TODO Auto-generated method stub
		return this.hint;
	}

	public byte[] toByteArray() {
		byte [] ht = this.hint.toByteArray();
		byte [] arr = new byte[this.answerHash.length + ht.length + 8];
		System.arraycopy(this.answerHash, 0, arr, 0, this.answerHash.length);
		System.arraycopy(ht, 0, arr, this.answerHash.length, ht.length);
		
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putLong(this.numIters);
		System.arraycopy(bytes, 0, arr, this.answerHash.length + ht.length, 8);
		
		return arr;
	}

	public static CryptoPuzzle fromByteArray(byte []arr, int hashSize, int hintSize, int numBitsWindow){
		byte [] hash = new byte[hashSize];
		byte []ht = new byte[hintSize];
		System.arraycopy(arr, 0, hash, 0, hashSize);
		System.arraycopy(arr, hashSize, ht, 0, hintSize);
		byte [] nit = new byte[8];
		System.arraycopy(arr, hashSize + hintSize, nit, 0, 8);
		long numIters = ByteBuffer.wrap(nit).getLong();
		
		return new CryptoPuzzle(numIters, hash, new BigInteger(ht), hintSize*8, numBitsWindow);
	}
}

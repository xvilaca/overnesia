#!/bin/bash

baseDir="/home/fastRank/nFastRank"

cd $baseDir/overnesia/Overnesia/bin
rm -r ./results/ #clean results
mkdir results

lib_CP=".:../lib/log4j-1.2.9.jar:../lib/peersim/djep-1.0.0.jar:../lib/peersim/jep-2.3.0.jar:../lib/peersim/peersim-1.0.5.jar:../lib/peersim/peersim-doclet.jar"

export JAVA_HOME=/opt/jdk1.8.0_66
export JRE_HOME=/opt/jdk1.8.0_66/jre
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/texbin:/opt/jdk1.8.0_66/bin:/opt/jdk1.8.0_66/jre/bin
#echo -e "\e[1;34mJava config - YES\e[0m"

#echo "Run HyParView"
echo Starting...

java -cp $lib_CP gsd/appl/HyParViewLabTest
# > "log.txt"

#!/bin/bash

baseDir="/home/fastRank/nFastRank"
echo "export JAVA_HOME=/opt/jdk1.8.0_66" >> $baseDir/.bash_profile
echo "export JRE_HOME=/opt/jdk1.8.0_66/jre" >> $baseDir/.bash_profile
echo "export PATH=\$PATH:/opt/jdk1.8.0_66/bin:/opt/jdk1.8.0_66/jre/bin" >> $baseDir/.bash_profile
source $baseDir/.bashrc
source $baseDir/.bash_profile

#compile HyParView
echo "compile HyParview"
cd $baseDir/overnesia/Overnesia/
mkdir bin
cd src

javac -d ../bin -cp .:../lib/log4j-1.2.9.jar:../lib/peersim/* gsd/appl/HyParViewLabTest.java


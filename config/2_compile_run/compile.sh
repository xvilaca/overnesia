#!/bin/bash

baseDir="/home/fastRank/nFastRank"

cd $baseDir/overnesia/Overnesia/src/

javac -d ../../Overnesia/bin -cp .:../../Overnesia/lib/log4j-1.2.9.jar:../../Overnesia/lib/peersim/* ../../Overnesia/src/gsd/appl/HyParViewLabTest.java

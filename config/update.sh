#!/bin/bash

if [ "$1" == "" ]
then
echo "ERROR: No commit message!"
else
cd ..
git add -f -A
git commit -m "$1"
git push
cd config/
./updateOvernesia.sh
fi
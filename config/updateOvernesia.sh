#!/bin/bash

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

listIP=
getArray

function config {
(ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "cd overnesia/ && sudo -S git fetch origin master && sudo -S git reset --hard FETCH_HEAD && sudo -S git clean -df")
}

for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (config $ip)
done


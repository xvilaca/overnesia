#!/bin/bash

function config {
    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "sudo -S ./overnesia/config/2_compile_run/startCrypto.sh")
}

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}


listIP=
getArray
for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (config $ip)&
done
wait

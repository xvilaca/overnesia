#!/bin/bash

binPath="/Users/xvilaca/Dropbox/DanielQuinta/workspace/overnesia/Overnesia/bin"

expN=6
#### EXP2
##function exp2
for minRank in {1..1} #,0.4,0.5,0.6}
do
#echo $bfp
echo $expN > $binPath/exp ; echo $del >> $binPath/exp
for it in {1..1} #10
do
echo "##### Iteration $it - BFP = $bfp #####"
echo "Initiate Server!"
(sh executeModules/executeServer.sh $expN $minRank $it)&
sleep 5
sh runHPV_PlanetL.sh $expN $minRank $it #> log_HPV_PlanetL.sh 2>&1
sleep 360 # 60 default 5m

echo -e "\e[1;34mSending dissemination signal\e[0m"
touch $binPath/"dissemination_signal"
wait #4m
#sleep 5
echo "<<<<<FINISHED ITERATION>>>>>>>>"
done
done

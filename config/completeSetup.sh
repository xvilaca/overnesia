#!/bin/bash
#$1 expN , $2 activeView Size, $3 bfp



#new node planetlab1.virtues.fi
#declare -a listIP=( "plab4.ple.silweb.pl"

declare -a listIP=( #"plab4.ple.silweb.pl")
"ple2.ait.ac.th" "ple1.ait.ac.th" "planetlab1.mini.pw.edu.pl" "planetlab1.utt.fr" "node2pl.planet-lab.telecom-lille1.eu" "planetlab-1.di.fc.ul.pt" "planetlab1.extern.kuleuven.be" "planetlab-1.fhi-fokus.de" "planetlab1.ifi.uio.no"	"planet2.servers.ua.pt"	 "planetlab-um00.di.uminho.pt"	 "planetlab-tea.ait.ie"	 "planetlab-coffee.ait.ie"	 "prata.mimuw.edu.pl"	 "planetlab1.diku.dk"	 "planetlab-2.ida.liu.se"	 "planetlab-1.ida.liu.se"	 "planet-lab-node2.netgroup.uniroma2.it"	 "planet-lab-node1.netgroup.uniroma2.it" "planetlab2.um.es"	 "planetlab1.um.es"	 "planetlab-2.ing.unimo.it"	 "dannan.disy.inf.uni-konstanz.de"	 "gschembra3.diit.unict.it"	 "planetlab-2.man.poznan.pl"	 "utet.ii.uam.es"   "planetvs2.informatik.uni-stuttgart.de" "planetlab-2.fhi-fokus.de" "planetlab2.s3.kth.se" "chronos.disy.inf.uni-konstanz.de" "planetlab1.informatik.uni-goettingen.de" "planetlab2.dit.upm.es" "planetlab2.ics.forth.gr" "mars.planetlab.haw-hamburg.de""merkur.planetlab.haw-hamburg.de" "planetlab01.dis.unina.it" "planetlab3.cslab.ece.ntua.gr")

#THE OTHER PLANETLAB NODES ARE IN THE END
#"planetlab1.jcp-consult.net" )
#FAILBOOT
#"planetlab2.jcp-consult.net"


function setupMachines {
    #$1 expects remote ip address
    echo "send setup.sh"
    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa 1_config/setup.sh istple_fastrank@$1:.

echo "run setup.sh "
(ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $ip "echo magic | sudo -S ./setup.sh; rm setup.sh")
}


function sendJava8 {
    #$1 expects remote ip address
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa ../../jdk-8u66-linux-i586.tar.gz istple_fastrank@$1:.
}

function compileHPV {
    #$1 expects remote ip address
    echo "update compileHPV.sh"
    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa 2_compile_run/compileHPV.sh istple_fastrank@$1:.

    echo "run compileHPV.sh"
    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "echo magic | sudo -S ./compileHPV.sh; rm compileHPV.sh")
}

for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
#sendJava8
(sendJava8 $ip)
(setupMachines $ip)
(compileHPV $ip)
done


#!/bin/bash

function config {
    #$1 expects remote ip address
    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa 1_config/config_overnesia.sh  istple_fastrank@$1:.

    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "echo magic | sudo -S ./config_overnesia.sh; rm config_overnesia.sh")
}

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodesInd.txt
}

getArray

for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (config $ip)
done


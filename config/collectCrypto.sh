#!/bin/bash

function collect {
    #$1 expects remote ip address
    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa istple_fastrank@$1:temp/crypto.txt ../Overnesia/config/crypto/$1.txt
}

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

getArray
listIp=
for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (collect $ip)
done


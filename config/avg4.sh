#!/bin/bash

function getArray {
    i=1
    while IFS= read -r line ; do
        #echo "$line"
        for val1 in $( echo "$line" | cut -f 2 -d '!' ) ; do
            acum1[$i]=$(echo "${acum1[$i]} + ${val1}" | bc -l)
        done
        for val2 in $( echo "$line" | cut -f 3 -d '!' ) ; do
            acum2[$i]=$(echo "${acum2[$i]} + ${val2}" | bc -l)
        done
#echo "${acum1[$i]} : ${acum2[$i]}"
#echo "${lines[$itn,$ln,1]} : ${lines[$itn,$ln,2]}"
        i=$((i + 1))
    done < "${iter}processedResults.txt"
}

numLines=1000
declare -a acum1
declare -a acum2
for var in */ ; do
    echo "Var: $var"
    rm -r -- "${var}avg"
    mkdir -- "${var}avg"
    for (( i=0; i<$numLines + 2; i++ )); do
        acum1[$i]=0
        acum2[$i]=0
    done
    itn=0
    for iter in ${var}*/ ; do
        if [ "$iter" != "${var}avg/" ];
        then
            echo "$iter"
            getArray
            itn=$((itn + 1))
        fi
    done
    for (( i=0; i<$numLines; i++ )); do
        ln=$((i + 1))
        var1=$(echo "${acum1[$i]} / $itn" | bc -l)
        var2=$(echo "${acum2[$i]} / $itn" | bc -l)
        echo "${ln}_${var1}_${var2}" >> "${var}avg/processedResults.txt"
    done
done
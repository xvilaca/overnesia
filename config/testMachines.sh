#!/bin/bash

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

function test {
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "[ -d /opt/jdk1.8.0_66 -a -d overnesia/Overnesia -a -d overnesia/config ] && echo 'OK' || echo '!!!!PROBLEM!!!!'"
}

listIP=
getArray listIP
it=0
for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (test $ip)
        it=$((it+1))
done

echo "Number of machines: $it"

#!/bin/bash

function getArray {
    i=1
    while IFS= read -r line ; do
        #echo "$line"
        for val in $( echo "$line" | cut -f 2 -d '!' ) ; do
            acum[$i]=$(echo "${acum[$i]} + ${val}" | bc -l)
        done
#echo "${acum1[$i]} : ${acum2[$i]}"
#echo "${lines[$itn,$ln,1]} : ${lines[$itn,$ln,2]}"
        i=$((i + 1))
    done < "${iter}processedResults.txt"
}

numLines=11
declare -a acum1
declare -a acum2
for var in */ ; do
    echo "Var: $var"
    rm -r -- "${var}avg"
    mkdir -- "${var}avg"
    for (( i=0; i<$numLines + 2; i++ )); do
        acum[$i]=0
    done
    itn=0
    for iter in ${var}*/ ; do
        if [ "$iter" != "${var}avg/" ];
        then
            echo "$iter"
            getArray
            itn=$((itn + 1))
        fi
    done
    for (( i=0; i<$numLines; i++ )); do
        ln=$((i + 1))
        val=$(echo "${acum[$i]} / $itn" | bc -l)
        echo "${ln}_${val}" >> "${var}avg/processedResults.txt"
    done
done
#!/bin/bash

#new node planetlab1.virtues.fi
#declare -a listIP=( "plab4.ple.silweb.pl"



function runHyParView {
#    echo "update runHPV.sh"
#    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa 2_compile_run/runHPV.sh istple_fastrank@$1:.

    echo "run runHPV.sh"
    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "echo $expN > exp ; echo $var >> exp; sudo -S sh -c 'mv exp $remote_binPath'; sudo -S ./overnesia/config/2_compile_run/runHPV.sh")
}

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

listIP=
expN=$1
var=$2

remote_binPath="/home/istple_fastrank/overnesia/Overnesia/bin"
getArray

#it=0
for ip in "${listIP[@]}"
    do
#it=$((it+1))
        echo "Iteration: $ip"
#setExp $ip
   (runHyParView $ip)&
done

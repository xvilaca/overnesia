#!/bin/bash

binPath="/Users/xvilaca/Dropbox/DanielQuinta/workspace/overnesia/Overnesia/bin"

expN=7
listTimes=("0" "8" "16" "24" "32")
#### EXP2
##function exp2
for quarTime in ${listTimes[@]} #,0.4,0.5,0.6}
do
#echo $bfp
echo $expN > $binPath/exp ; echo $quarTime >> $binPath/exp
for it in {1..1} #10
do
echo "##### Iteration $it - BFP = $bfp #####"
echo "Initiate Server!"
(sh executeModules/executeServer.sh $expN $quarTime $it)&
sleep 5
sh runHPV_PlanetL.sh $expN $quarTime $it #> log_HPV_PlanetL.sh 2>&1
sleep 60 # 60 default 5m

echo -e "\e[1;34mSending dissemination signal\e[0m"
touch $binPath/"dissemination_signal"
wait #4m
(sh killRemoteHyParView.sh)
#sleep 5
echo "<<<<<FINISHED ITERATION>>>>>>>>"
done
done

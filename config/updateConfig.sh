#!/bin/bash

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

function config {
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa -r ../Overnesia/config/ istple_fastrank@$1:.

(ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "sudo -S rm -r overnesia/Overnesia/config/; sudo -S mv config overnesia/Overnesia/")
}

listIP=
getArray
for ip in "${listIP[@]}"
do
    echo "Iteration: $ip"
    (config $ip)
done

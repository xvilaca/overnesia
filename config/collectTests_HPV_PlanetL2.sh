#!/bin/bash


#Collect - Nodes that were killed are not here!
declare -a listIP=( "plab4.ple.silweb.pl" "ple2.ait.ac.th" "ple1.ait.ac.th" "planetlab1.mini.pw.edu.pl" "planetlab1.utt.fr" "node2pl.planet-lab.telecom-lille1.eu" )



cycleIteration=$1
expN=$2
activeViewSize=$3
bfp=$4
dirAV=activeV_size$activeViewSize$bfp/$cycleIteration
testBaseDir=/Users/drk/Desktop/work/tests

logsDir=$testBaseDir/logs/exp$expN/$dirAV
resultsDir=$testBaseDir/results/exp$expN/$dirAV
#clean
rm -r $logsDir
rm -r $resultsDir
#create
mkdir -p $logsDir
mkdir -p $resultsDir


for ip in "${listIP[@]}"
    do
        echo "collectig data from $ip"

        #move frames to result$ip file
        (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $ip " pwd ; cd /home/istple_fastrank/overnesia/Overnesia/bin/results/ ; echo magic | sudo -S touch results$ip.txt ; echo magic | sudo -S sh -c  'ls -1 pack* > results$ip.txt'")
        #ls -1 | wc -l  > resultsIPX ; cd ..


        # mv result$ip back to server (HERE)
        scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no istple_fastrank@$ip:~/overnesia/Overnesia/bin/results/results$ip.txt $logsDir

    logFilename="$logsDir/results$ip.txt"
    if [ -f ${logFilename} ]
        then
            if [ -s ${logFilename} ]
            then
                echo "File exists and not empty"
                #cat $logFilename
            else
                echo "File exists but empty"
                echo results$ip.txt >> $resultsDir/report
            fi
        else
            echo "File not exists. Server or Peer with problems!"
    fi
done

#Example
#ssh -l istple_fastrank -i ~/.ssh/planetLab 193.136.19.13
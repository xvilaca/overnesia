#!/bin/bash

for exp in {1,2,4,5} ; do
    cp "avg$exp.sh" "../Overnesia/$exp/"
done
cp "avgCrypto.sh" "../Overnesia/config/"

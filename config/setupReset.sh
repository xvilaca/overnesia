#!/bin/bash

baseDir="/home/istple_fastrank"

# Dir to check some installation flags
SetupFlags="setupFlags"

cd $baseDir
rm -r $SetupFlags
mkdir -p $baseDir/$SetupFlags



rm -r overnesia
rm -r overneseia2.0
git clone https://drk_bitbucket:danykintas@bitbucket.org/drk_bitbucket/overnesia.git
touch $baseDir/$SetupFlags/overnesiaOK



cd $baseDir
rm .profile
echo "export JAVA_HOME=/opt/jdk1.8.0_66" >> $baseDir/.bash_profile
echo "export JRE_HOME=/opt/jdk1.8.0_66/jre" >> $baseDir/.bash_profile
echo "export PATH=\$PATH:/opt/jdk1.8.0_66/bin:/opt/jdk1.8.0_66/jre/bin" >> $baseDir/.bash_profile
touch $baseDir/$SetupFlags/javaConfigOK
echo -e "\e[1;34mJava config - YES\e[0m"


source .bash_profile

java -version

#!/bin/bash


#Collect - Nodes that were killed are not here!
declare -a listIP=("planetlab-1.di.fc.ul.pt" "planetlab1.extern.kuleuven.be" "planetlab-1.fhi-fokus.de" "planetlab1.ifi.uio.no"	"planet2.servers.ua.pt"	 "planetlab-um00.di.uminho.pt"	 "planetlab-tea.ait.ie"	 "planetlab-coffee.ait.ie"	 "prata.mimuw.edu.pl"	 "planetlab1.diku.dk"	 "planetlab-2.ida.liu.se"	 "planetlab-1.ida.liu.se"	 "planet-lab-node2.netgroup.uniroma2.it"	 "planet-lab-node1.netgroup.uniroma2.it" "planetlab2.um.es"	 "planetlab1.um.es" "dannan.disy.inf.uni-konstanz.de"	 "gschembra3.diit.unict.it"	 "planetlab-2.man.poznan.pl"	 "utet.ii.uam.es"   "ateaqui" "planetvs2.informatik.uni-stuttgart.de" "planetlab-2.fhi-fokus.de")


#declare -a listIP=( "plab4.ple.silweb.pl" "ple2.ait.ac.th" "ple1.ait.ac.th" "planetlab1.mini.pw.edu.pl" "planetlab1.utt.fr" "node2pl.planet-lab.telecom-lille1.eu" "planetlab-1.di.fc.ul.pt" "planetlab1.extern.kuleuven.be" "planetlab-1.fhi-fokus.de" "planetlab1.ifi.uio.no"	"planet2.servers.ua.pt"	 "planetlab-um00.di.uminho.pt"	 "planetlab-tea.ait.ie"	 "planetlab-coffee.ait.ie"	 "prata.mimuw.edu.pl"	 "planetlab1.diku.dk"	 "planetlab-2.ida.liu.se"	 "planetlab-1.ida.liu.se"	 "planet-lab-node2.netgroup.uniroma2.it"	 "planet-lab-node1.netgroup.uniroma2.it" "planetlab2.um.es"	 "planetlab1.um.es" "dannan.disy.inf.uni-konstanz.de"	 "gschembra3.diit.unict.it"	 "planetlab-2.man.poznan.pl"	 "utet.ii.uam.es"   "ateaqui" "planetvs2.informatik.uni-stuttgart.de" "planetlab-2.fhi-fokus.de")


cycleIteration=$1
expN=$2
activeViewSize=$3
bfp=$4
dirAV=activeV_size$activeViewSize$bfp/$cycleIteration

###DEFINE HERE YOUR OUTPUT DIRECTORY
#DEFAULT
testBaseDir=/Users/drk/Desktop/work/tests

#Laptop
#testBaseDir=/Users/drk/Documents/workspace/XavierINESC/tests
#

logsDir=$testBaseDir/logs/exp$expN/$dirAV
resultsDir=$testBaseDir/results/exp$expN/$dirAV
#clean
#rm -r $logsDir
#rm -r $resultsDir
#create
#mkdir -p $logsDir
#mkdir -p $resultsDir

sleep 1
for ip in "${listIP[@]}"
    do
        echo "collectig data from $ip"

        #move frames to result$ip file
        (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $ip " pwd ; cd /home/istple_fastrank/overnesia/Overnesia/bin/results/ ; echo magic | sudo -S touch results$ip.txt ; echo magic | sudo -S sh -c  'ls -1 pack* > results$ip.txt'")
        #ls -1 | wc -l  > resultsIPX ; cd ..

        # mv result$ip back to server (HERE)
        scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no istple_fastrank@$ip:~/overnesia/Overnesia/bin/results/results$ip.txt $logsDir

    logFilename="$logsDir/results$ip.txt"
    if [ -f ${logFilename} ]
        then
            if [ -s ${logFilename} ]
            then
                echo "File exists and not empty"
                #cat $logFilename
            else
                echo "File exists but empty"
                echo results$ip.txt >> $resultsDir/report
            fi
        else
            echo "File not exists. Server or Peer with problems!"
    fi

done

plotGraph(){
    cd $resultsDir

    lonelynodes=$(cat report | wc -l)
    echo $lonelynodes
    line="$activeViewSize   $lonelynodes"
    echo $line
    cd ..
    touch plot.dat
    echo $line >> data_isolated_nodes.dat
    open .

}

plotGraph
#!/bin/bash

shuffle() {
    local i tmp max rand
    max=$(( 32768 / size * size ))

    for ((i=size-1; i>0; i--)); do
        while (( (rand=$RANDOM) >= max )); do :; done
        rand=$(( rand % (i+1) ))
        tmp=${listIP[i]}
        listIP[i]=${listIP[rand]}
        listIP[rand]=$tmp
    done
}

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

listIP=
size=${#listIP[@]}
getArray
shuffle
for ((i=1; i<= 6; i++)); do
    echo ${listIP[i]} >> "../../Overnesia/config/kill.txt"
    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa ${listIP[i]} "sudo -S killall java ")&
done

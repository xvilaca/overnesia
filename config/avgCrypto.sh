#!/bin/bash

function getArray {
    i=0
    while IFS= read -r line ; do
        for val in $( echo "$line" | cut -f 2 -d ',' ) ; do
            acum[$i]=$(echo "${acum[$i]} + ${val}" | bc -l)
        done
        i=$((i + 1))
    done < "${var}"
}

cd crypto/
numLines=30
declare -a acum
for (( i=0; i<$numLines; i++ )); do
    acum[$i]=0
done
itn=0
for var in * ; do
    echo "$var"
    getArray
    itn=$((itn + 1))
done
for (( i=0; i<$numLines; i++ )); do
    ln=$((i + 1))
    val=$(echo "${acum[$i]} / $itn" | bc)
    echo "${ln},${val}" >> "crypto.txt"
done

mv crypto.txt ../crypto.txt

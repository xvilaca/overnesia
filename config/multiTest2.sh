#!/bin/bash

binPath="/Users/xvilaca/Dropbox/DanielQuinta/workspace/overnesia/Overnesia/bin"

expN=2
#### EXP2
##function exp2
for bfp in {6..10} #,0.4,0.5,0.6}
do
#echo $bfp
echo $expN > $binPath/exp ; echo $activeViewSize >> $binPath/exp ; echo $bfp >> $binPath/exp
for it in {1..5} #10
do

echo "##### Iteration $it - BFP = $bfp #####"
echo "Initiate Server!"
(sh executeModules/executeServer.sh $expN $bfp $it)&
sleep 5
sh runHPV_PlanetL.sh $expN $bfp $it #> log_HPV_PlanetL.sh 2>&1
sleep 60 # 60 default 5m

echo -e "\e[1;34mSending dissemination signal\e[0m"
touch $binPath/"dissemination_signal"
sleep 5 #4m
wait
echo "<<<<<FINISHED ITERATION>>>>>>>>"
done
done
#!/bin/bash

binPath="/Users/xvilaca/Dropbox/DanielQuinta/workspace/overnesia/Overnesia/bin"

expN=1
bfp=1
for activeViewSize in {7..8}
do
echo $expN > $binPath/exp ; echo $activeViewSize >> $binPath/exp
for it in {1..5}
#8}
    do

    echo "##### Iteration $it - Active View = $activeViewSize #####"
    echo "Initiate Server!"

    rm $binPath/dissemination_signal

#START SERVER
    (sh executeModules/executeServer.sh $expN $activeViewSize $it)&
    sleep 5

#START NODES
    sh runHPV_PlanetL.sh $expN $activeViewSize #> log_HPV_PlanetL.sh 2>&1
    sleep 60 # 60 default 5m

#KILL 30 percent
   sh executeModules/launchBomb.sh # > log_launchBomb.sh 2>&1 # 30perc of the population died
   echo "30perc of the population died"
   sleep 20

#START DISSEMINATION
   echo -e "\e[1;34mSending dissemination signal\e[0m"
   touch $binPath/"dissemination_signal"
   wait
   echo "<<<<Finished Iteration>>>>"
    done
done
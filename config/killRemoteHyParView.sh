#!/bin/bash

function getArray {
index=0
while IFS= read -r line # Read a line
do
listIP[$index]=$line
index=$((index+1))
done < onlineNodes.txt
}

listIP=

getArray listIP

it=0
for ip in "${listIP[@]}"
    do
        it=$((it+1))
        echo "Iteration: $ip"

        (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $ip "sudo -S killall java ")
done

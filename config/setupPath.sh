#!/bin/bash

declare -a listIP=( "plab4.ple.silweb.pl" "ple2.ait.ac.th" "ple1.ait.ac.th" "planetlab1.utt.fr" "planetlab1.ifi.uio.no"	"planetlab-tea.ait.ie" "planetlab-coffee.ait.ie" "prata.mimuw.edu.pl" "planetlab-2.ida.liu.se" "planetlab-1.ida.liu.se" "planet-lab-node2.netgroup.uniroma2.it" "dannan.disy.inf.uni-konstanz.de" "planetvs2.informatik.uni-stuttgart.de" "chronos.disy.inf.uni-konstanz.de" "planetlab1.informatik.uni-goettingen.de" "planetlab2.ics.forth.gr" "planetlab01.dis.unina.it" "merkur.planetlab.haw-hamburg.de")

function config {
    scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i ~/.ssh/planetLab_rsa 1_config/config_path.sh  istple_fastrank@$1:.

    (ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l istple_fastrank -i ~/.ssh/planetLab_rsa $1 "echo magic | sudo -S ./config_path.sh; rm config_path.sh")
}

for ip in "${listIP[@]}"
    do
        echo "Iteration: $ip"
        (config $ip)
done


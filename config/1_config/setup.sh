#!/bin/bash

baseDir="/home/istple_fastrank"

# Dir to check some installation flags
SetupFlags="setupFlags"

sudo -S rm -r $SetupFlags

cd $baseDir
mkdir $SetupFlags

#install git
sudo yum -y install git
pwd
touch $SetupFlags/gitOK
pwd
echo -e "\e[1;34mGit installation - YES\e[0m"


#clone project
mkdir $baseDir/overnesia
cd $baseDir/overnesia
if [ -f $baseDir/$SetupFlags/overnesiaOK ]
   then
      echo "Project updated"
   else
        echo "git clone"
git clone https://xvilaca:smeagoll@bitbucket.org/xvilaca/overnesia.git
        touch $baseDir/$SetupFlags/overnesiaOK
fi
echo "Overnesia proj cloned - YES"

#Install Java
if [ -f $baseDir/$SetupFlags/javaInstallOK ]
   then
      echo "java already installed"
   else
      cd $baseDir #/overnesia/Overnesia/javaSE8/
      tar xzf jdk-8u66-linux-i586.tar.gz
      sudo mv jdk1.8.0_66/ /opt/
      touch $baseDir/$SetupFlags/javaInstallOK
fi
echo -e "\e[1;34mJava installation - YES\e[0m"

#Java path config
if [ -f $baseDir/$SetupFlags/javaConfigOK ]
    then
        echo "Java already configured"
        source $baseDir/.profile
#echo ".profile"
#       cat $baseDir/.profile
    else
        touch $baseDir/.profile
        echo ". $HOME/.profile" >> $baseDir/.bashrc
        echo "export JAVA_HOME=/opt/jdk1.8.0_66" >> $baseDir/.profile
        echo "export JRE_HOME=/opt/jdk1.8.0_66/jre" >> $baseDir/.profile
        echo "export PATH=\$PATH:/opt/jdk1.8.0_66/bin:/opt/jdk1.8.0_66/jre/bin" >> $baseDir/.profile
        echo "export JAVA_HOME=/opt/jdk1.8.0_66" >> $baseDir/.bash_profile
        echo "export JRE_HOME=/opt/jdk1.8.0_66/jre" >> $baseDir/.bash_profile
        echo "export PATH=\$PATH:/opt/jdk1.8.0_66/bin:/opt/jdk1.8.0_66/jre/bin" >> $baseDir/.bash_profile
#source $baseDir/.profile
        source $baseDir/.bashrc
        source $baseDir/.bash_profile
        touch $baseDir/$SetupFlags/javaConfigOK
fi
echo -e "\e[1;34mJava config - YES\e[0m"

#run HyParView
#clear
#echo Starting...
#echo HyParView...please start praying!

#cd $baseDir/overnesia/Overnesia/bin
#rm -r ./results/ #clean results
#mkdir results

#lib_CP=".:../lib/log4j-1.2.9.jar:../lib/peersim/djep-1.0.0.jar:../lib/peersim/jep-2.3.0.jar:../lib/peersim/#peersim-1.0.5.jar:../lib/peersim/peersim-doclet.jar"

#java -cp $lib_CP gsd/appl/HyParViewLabTest

# cd overnesia/Overnesia/bin; java -cp .:../lib/log4j-1.2.9.jar:../lib/peersim/djep-1.0.0.jar:../lib/peersim/jep-2.3.0.jar:../lib/peersim/peersim-1.0.5.jar:../lib/peersim/peersim-doclet.jar  gsd/appl/HyParViewLabTest
